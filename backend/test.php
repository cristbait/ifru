<?PHP

	require ('.paths');
	require ('../functions_debug.php');
	include ('sys.inc');

	$mod_url = new URL ('dbconfig.php');

	$factory = new DBConfigFactory ($config, $request);
	$id_name = 'dbconfig_id';

	$tpl = new Template ($config, MY_ADMIN_TPL_PATH);

	$list_url = new URL ($mod_url->build ());
	$list_url->set_var ('area', 'list');
	$tpl->set_var ('list_url', $list_url->build ());

	$edit_url = new URL ($mod_url->build ());
	$edit_url->set_var ('area', 'edit');
	$tpl->set_var ('edit_url', $edit_url->build ());

	
		
			$tpl->set_file ('list', 'xml.html');

			

			// $pager = new Pager ($factory, $request, $config->page_size, $list_url->build (), 'page', $id_name);
			//$item_list = $factory->create_all ($pager->page_size, $pager->offset);

			
		
			$tpl->set_block ('list', 'row', 'rows');
		
			$tpl->set_var ('rows', '');
			
		
			$tpl->set_var ('list_buttons', common_list_buttons (
				$mod_url->build (), $edit_url->build (), $id_name, $factory->movable));
			$tpl->set_var ('pager', ($pager && $pager->page_count > 1) ? common_pager ($pager) : '');
			$tpl->set_var ('total', $pager ? $pager->total_size : sizeof ($item_list));
		
			$tpl->parse ('list', 'list');
			$tpl->p_default ();
		if ($_POST){
		$db2 = &$config->getDB();
			
			header('Content-Type: text/x-csv; charset=utf-8');
header("Content-Disposition: attachment;filename=".date("d-m-Y")."-export.xls");
header("Content-Transfer-Encoding: binary ");
/*
// Теперь можно выводить ранее полученные данные, будь то из MySQL
// будь то из файла, или же полученные в работе скрипта...
// для начала лучше использовать стандартную
// шапку для HTML страницы под вашу кодировку, к примеру utf-8
// чтобы корректно отображалась кириллица в EXCEL
// так же убедитесь что сам файл скрипта установлен в соответствующей
кодировке это можно посмотреть в любом PHP редакторе
// итак, шапка страницы:
*/

$csv_output ='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="author" content="Andrey" />
<title>deamur zapishi.net</title>
</head>
<body>';

// Теперь данные в виде таблицы:
$csv_output .='<table border="1">
<tr><td>Да('.count(geter($_POST,$db2,1,1)).')</td><td>Нет('.count(geter($_POST,$db2,1,2)).')</td></tr>
<tr>
<td>1.Ответ('.count(geter($_POST,$db2,2,1)).')</td><td>2.Ответ('.count(geter($_POST,$db2,2,2)).')</td>
<td>3.Другое('.count(other($_POST,$db2,2)).')</td><td>'.other($_POST,$db2,2,'all').'</td>
</tr>
<tr>
<td>1.Ответ('.count(geter($_POST,$db2,3,1)).')</td><td>2.Ответ('.count(geter($_POST,$db2,3,2)).')</td>
<td>3.Ответ('.count(geter($_POST,$db2,3,3)).')</td><td>4.Ответ('.count(geter($_POST,$db2,3,4)).')</td>
</tr>
<tr>
<td>1.Ответ('.count(geter($_POST,$db2,4,1)).')</td><td>2.Ответ('.count(geter($_POST,$db2,4,2)).')</td>
<td>3.Ответ('.count(geter($_POST,$db2,4,3)).')</td>
</tr>
<tr>
<td>1.Ответ('.count(geter($_POST,$db2,5,1)).')</td><td>2.Ответ('.count(geter($_POST,$db2,5,2)).')</td>
<td>3.Ответ('.count(geter($_POST,$db2,5,3)).')</td>
</tr>
<tr>
<td>1.Ответ('.count(geter($_POST,$db2,6,1)).')</td><td>2.Ответ('.count(geter($_POST,$db2,6,2)).')</td>
<td>3.Ответ('.count(geter($_POST,$db2,6,3)).')</td><td>4.Ответ('.count(geter($_POST,$db2,6,4)).')</td>
<td>5.Ответ('.count(geter($_POST,$db2,6,5)).')</td><td>6.Ответ('.count(geter($_POST,$db2,6,6)).')</td>
<td>7.Ответ('.count(geter($_POST,$db2,6,7)).')</td><td>8.Ответ('.count(geter($_POST,$db2,6,8)).')</td>
<td>9.Ответ('.count(geter($_POST,$db2,6,9)).')</td><td>10.Ответ('.count(other($_POST,$db2,6)).')</td>
<td>'.other($_POST,$db2,6,'all').'</td>
</tr>
<tr>
<td>1.Ответ('.count(geter($_POST,$db2,7,1)).')</td><td>2.Ответ('.count(geter($_POST,$db2,7,2)).')</td>
<td>3.Ответ('.count(geter($_POST,$db2,7,3)).')</td><td>4.Ответ('.count(geter($_POST,$db2,7,4)).')</td>
<td>5.Ответ('.count(geter($_POST,$db2,7,5)).')</td><td>6.Ответ('.count(geter($_POST,$db2,7,6)).')</td>
<td>7.Ответ('.count(geter($_POST,$db2,7,7)).')</td><td>8.Ответ('.count(geter($_POST,$db2,7,8)).')</td>
<td>9.Ответ('.count(other($_POST,$db2,7)).')</td><td>'.other($_POST,$db2,7,'all').'</td>
</tr>
<tr>
<td>1.Ответ('.count(geter($_POST,$db2,8,1)).')</td><td>2.Ответ('.count(geter($_POST,$db2,8,2)).')</td>
<td>3.Ответ('.count(geter($_POST,$db2,8,3)).')</td><td>4.Ответ('.count(geter($_POST,$db2,8,4)).')</td>
<td>5.Ответ('.count(geter($_POST,$db2,8,5)).')</td><td>6.Ответ('.count(geter($_POST,$db2,8,6)).')</td>

</tr>
<tr>
<td>1.Ответ('.count(geter($_POST,$db2,9,1)).')</td><td>2.Ответ('.count(geter($_POST,$db2,9,2)).')</td>


</tr>
<tr>
<td>1.Ответ('.count(geter($_POST,$db2,10,1)).')</td><td>2.Ответ('.count(geter($_POST,$db2,10,2)).')</td>
<td>3.Ответ('.count(geter($_POST,$db2,10,3)).')</td><td>4.Ответ('.count(geter($_POST,$db2,10,4)).')</td>
<td>5.Ответ('.count(geter($_POST,$db2,10,5)).')</td><td>6.Ответ('.count(geter($_POST,$db2,10,6)).')</td>
<td>7.Ответ('.count(geter($_POST,$db2,10,7)).')</td><td>8.Ответ('.count(geter($_POST,$db2,10,8)).')</td>
<td>9.Ответ('.count(geter($_POST,$db2,10,9)).')</td><td>10.Ответ('.count(geter($_POST,$db2,10,10)).')</td>
<td>11.Ответ('.count(geter($_POST,$db2,10,11)).')</td><td>12.Ответ('.count(geter($_POST,$db2,10,12)).')</td>
<td>13.Ответ('.count(geter($_POST,$db2,10,13)).')</td>

</tr>
</table>';

// закрываем тело страницы
$csv_output .='</body></html>';
// И наконец выгрузка в EXCEL - что в скрипте как обычный вывод
echo $csv_output;
			
			
			
			
		}
	include ('menu_f.php');

    $mysql_factory->print_log ();
	function geter($item,$db2,$q,$ans){
	
			$sql="SELECT *  FROM cv as cv1,users as us1 WHERE cv1.user_login = us1.login";
			
			if(!empty($item['citytype_id']) AND $item['citytype_id']!=0){
			$sql.=' AND citytype_id=3';
			}
			if(!empty($item['region_id'])){
			$sql.=' AND region_id='.$item['region_id'];
			}
			if(!empty($item['class_number'])){
			$sql.=' AND class_number='.$item['class_number'];
			}
			if(isset($q)){
			$sql.=" AND q".$q." LIKE '%".$ans."%'";
			}
			
			return $db2->select ($sql);
			
			
	}
	
	function other($item,$db2,$q,$con=null){
	
			$sql="SELECT *  FROM cv as cv1,users as us1 WHERE cv1.user_login = us1.login";
			
			if(!empty($item['citytype_id']) AND $item['citytype_id']!=0){
			$sql.=' AND citytype_id=3';
			}
			if(!empty($item['region_id'])){
			$sql.=' AND region_id='.$item['region_id'];
			}
			if(!empty($item['class_number'])){
			$sql.=' AND class_number='.$item['class_number'];
			}
			if(isset($q)){
			$sql.=" AND q".$q."_inp!=''";
			}
			if(!empty($con)){
			$ra=$db2->select ($sql);
			$out='';
			$s='q'.$q.'_inp';
			foreach($ra as $row3){
			
			$out.=$row3->$s.',';
			}
			$out=substr($out, 0, strlen($out)-1);
			return $out;
			}else{
			return $db2->select ($sql);
			}
			
			
	}
?>