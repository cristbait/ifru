﻿jQuery.noConflict();
(function($) {
    var Forms = {
        select: $('#select_form'),
        forms: $('#forms_div > div'),
        num2: [0,4,4,4,4,4,4,4,4],
        num1: [0,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2],
        add3: $('#add3'),
        num3: 4,
        variants3: $('#variants3 > table'),
        add5: $('#add5'),
        num5: 3,
        variants5: $('#variants5'),
        current: null,
        ru_letters: ['',
            'А',
            'Б',
            'В',
            'Г',
            'Д',
            'Е',
            'Ё',
            'Ж',
            'З',
            'И',
            'Й',
            'К',
            'Л',
            'М',
            'Н',
            'О',
            'П',
            'Р',
            'С',
            'Т'
        ],
        letters: [
            'A',
            'B',
            'C',
            'D',
            'E',
            'F',
            'G',
            'H',
            'I',
            'J',
            'K',
            'L',
            'M',
            'N',
            'O',
            'P',
            'Q',
            'R',
            'S',
            'T',
            'U',
            'V',
            'W',
            'X',
            'Y',
            'Z'
        ],
        init : function(){
            var that = this;
            this.select
                .change(function() {
                that.current = $( "#select_form option:selected" ).data('form');
                that.show_hide_form();
            });
            that.btn_click();
			$('input[type="submit"]').click(function(event){
                event.preventDefault();
                $('#forms_div > div').not("#"+that.current).each(function(){
                    $(this).remove();                    
                })
				var k=1;
				if($('input[name="form_type"]').val() == 1 || $('input[name="form_type"]').val() == 2) {
                    if($('input[name="form_type"]').val() == 1){
                        $('.form_item > textarea').each(function(){
                            var me = $(this);
                            if(me.val() != '') {
                                if($('input[name="radio_variants1'+me.parent().next().next().data('num')+'[]"]:checked').length == 0){
                                    alert('Не выбран правильный ответ в вопросе №'+me.parent().next().next().data('num'));
                                    k=0;
                                }
                            }
                        })
                    }
                    if($('input[name="form_type"]').val() == 2){
                        $('.form_item > textarea').each(function(){
                            var me = $(this);
                            if(me.val() != '') {
                                if($('input[name="check_variants'+me.parent().next().next().data('num')+'[]"]:checked').length == 0){
                                    alert('Не выбран правильный ответ в вопросе №'+me.parent().next().next().data('num'));
                                    k=0;
                                }
                            }
                        })
                    }
                }
				if (k==1){
                $('form').submit();
				}
            })
        },
        btn_click: function(){
            var that = this;
            $('.form1 button').click(function(event){
                event.preventDefault();
                var me = $(this), i = me.parent().data('num');
                that.num1[i]++;
                $('#variants1'+i).append('<div class="form_item">' +
                    that.letters[that.num1[i]]+') <input type="radio" value="'+(that.num1[i]+1)+'" name="radio_variants1'+i+'[]">' +
                    ' <input name="variants1'+i+'[]"> <input type="hidden" name="variants_letter1'+i+'[]" value="'+that.letters[that.num1[i]]+'">' +
                    '</div>')
            })

            $('.form2 button').click(function(event){
                event.preventDefault();
                var me = $(this), i = me.parent().data('num');
                that.num2[i]++;
                $('#variants2'+i).append('<div class="form_item">' +
                    '<input type="checkbox" value="'+that.num2[i]+'" name="check_variants'+i+'[]"> <input type="text" name="variants'+i+'[]">' +
                    '</div>')
            })
            this.add3.click(function(event){
                event.preventDefault();
                that.num3++;
                that.variants3.append('<tr><td>'+
                    that.num3+') <input type="text" name="q_variants[]">'+
                    '</td><td>'+
                    that.ru_letters[that.num3]+') <input type="text" name="a_variants[]">'+
                    '</td></tr>')
            })
            this.add5.click(function(event){
                event.preventDefault();
                that.num5++;
                that.variants5.append('<div class="form_item">' +
                    that.num5+') <input name="variants[]">' +
                    '</div>')
            })
        },
        show_hide_form: function(){
            this.forms.each(function(){
                $(this).hide();
            })
            if(this.current == 'none'){
                return;
            }
            $('#'+this.current).show();
        }
    }
    Forms.init();
})(jQuery);