<?PHP

	require ('.paths');
	include ('sys.inc');

	$tpl = new Template ($config, MY_ADMIN_TPL_PATH);
	$tpl->set_file ('menu', 'menu_f.html');
	$tpl->set_block ('menu', 'section', 'sections');
	$tpl->set_block ('section', 'block', 'blocks');
	$tpl->set_block ('block', 'link', 'links');
	$tpl->set_var ('sections', '');

	require_once 'Config.php';

	function trans ($text) {
		return iconv ('utf-8', 'windows-1251', $text);
	}

    $xmlconfig = new Config ();
    $xmlconf =& $xmlconfig->parseConfig (MY_BACKEND_MENU_FILE,'xml', array ('encoding' => 'utf-8'));
// 
	if (PEAR::isError ($xmlconf)) {
		die ($xmlconf->getMessage());
	}

    $confsystem =& $xmlconf->searchPath(array("config"));
	$menu = $confsystem->searchPath(array('backend-menu'));

    for($i = 0; $i < $menu->countChildren (); $i++){
		$section = $menu->getChild ($i);

		if ($section->getName() == 'section') {
			$tpl->set_var ('section_name', trans ($section->getAttribute("name")));
			$tpl->set_var ('blocks', '');
			$noblock = FALSE;

			for($j = 0; $j < $section->countChildren (); $j++){
				$block = $section->getChild ($j);
				if (! $noblock)
					$tpl->set_var ('links', '');
				
				if ($block->getName() == 'block') {
					$tpl->set_var ('links', '');
					$noblock = FALSE;

					for($k = 0; $k < $block->countChildren (); $k++){
						$link = $block->getChild ($k);

						if ($link->getName() == 'link') {
							$tpl->set_var ('link_name', trans ($href=$link->getAttribute("name")));
							$tpl->set_var ('link_href', $link->getAttribute("href"));
							$tpl->set_var ('link_alt', trans ($link->getAttribute("alt")));

							$target = ($t=$link->getAttribute("target"))?('target="'.$t.'"'): (preg_match("/^https?:|ftp:/",$href)?' target="_blank"':"");
							$tpl->set_var ('link_target', $target);

							$tpl->parse ('links', 'link', TRUE);
						}
					}
				} elseif ($block->getName() == 'link') {
					$tpl->set_var ('link_name', trans ($block->getAttribute("name")));
					$tpl->set_var ('link_href', $block->getAttribute("href"));

					$tpl->parse ('links', 'link', TRUE);
					$noblock = TRUE;
				}

				if (! $noblock) {
					$tpl->parse ('blocks', 'block', TRUE);
				}
			}

			if ($noblock) {
				$tpl->parse ('blocks', 'block', TRUE);
			}

			$tpl->parse ('sections', 'section', TRUE);
		}
	}
	/*
	include ('mod/article.inc');
	// ���� ������
	$tpl->set_var ('article_rows', '');
	
	$factory = new ArticleCategoryFactory ($config, $request);
	
	$items = $factory->create_all ();
	
	if ($items)
	    for ($i = 0; $i < count ($items); $i++) {
	        $item = &$items[$i];
	        
			$item->set_template_vars ($tpl);
	        
	        $tpl->parse ('article_rows', 'article_row', TRUE);
	    }
	*/

	/*
	// instead of cron
	// ������� ����� 
		$auto_factory = new AutoFactory ($config, $request);
		$user_factory = new UserFactory ($config, $request);

		// ������� ����� 
		$auto_factory->delete_nouser_overclocked ();
		$auto_factory->delete_overclocked ();
		$auto_factory->delete_overclocked_a_c ();
		$auto_factory->delete_old_images ();

		$user_factory->delete_overclocked ();

		// ��������
		$auto_factory->send_new_items ();
	//
	*/

	$tpl->parse ('menu', 'menu');
	$tpl->p_default ();
	
	// $mysql_factory->print_log ();

?>