<?php

	require ('.paths');
	include ('sys.inc');

	$mysql = &$config->getDB();
	$mysql->debug = TRUE;

	$data = preg_replace ('/^(#|--)[^\n\r]*$/m', '', join ('', file ('up.sql')));
	$commands = preg_split ("/;\s*$/m", $data);

	foreach ($commands as $com) {
		if (preg_match ("/^\s*$/", $com))
			continue;

		$mysql->command ($com);
	}

	print "<font color=green>ok</font><br>";
	
	$mysql_factory->print_log ();

?>