<?PHP

	set_time_limit (60*10);

 	require ('.paths');
	include ('sys.inc');

	if (! defined (BACKUP_FILES))
		define (BACKUP_FILES, 10);

	function &getBackups () {
		$backups = array ();

		if ($dh = opendir (MY_BACKUP_PATH)) {
			while (($file = readdir ($dh)) !== FALSE) {
				if (! preg_match ('/^backup/i', $file))
					continue;

				$backups[] = $file;
			}

			closedir ($dh);
		}
		sort ($backups);
		
		return $backups;
	}

	if ($request->action) {
		switch ($request->action) {
		case 'do_backup':
			$tpl = new Template ($config, MY_COMMON_TPL_PATH);

			$backup_file_name = sprintf ("backup_%s.sql.gz", date ('Y-m-d_H-i'));
			$backup_file = MY_BACKUP_PATH . '/' . $backup_file_name;

			if (file_exists ($backup_file))
				unlink ($backup_file);
			
			$mysql = &$config->getDB();
			
			$fp = fopen ($backup_file, 'wb');
			$data = $mysql->backup ();
			$data = gzencode ($data, 9);
			fwrite ($fp, $data);
			fclose ($fp);
			
			if ($config->chmod_available)
				chmod ($backup_file, $config->chmod_upload);

			$backups = &getBackups ();

			if (BACKUP_FILES > 0)
			for ($i = 0; $i < sizeof ($backups) - BACKUP_FILES; $i++) {
				$backup_file = MY_BACKUP_PATH . '/' . $backups[$i];
				if (file_exists ($backup_file))
					unlink ($backup_file);
			}
			
			common_redirect ($tpl, $config, '��������� ����������� ������', NULL, '��������� �������', 'backup.php');           
			break;
		}
		
	} else {
		$tpl = new Template ($config, MY_ADMIN_TPL_PATH);
		$tpl->set_file ('backup', 'backup.html');
		$tpl->set_block ('backup', 'backup_row', 'backup_rows');
		
		$backups = &getBackups ();

		for ($i = 0; $i < sizeof ($backups); $i++) {
			$backup_file_name = $backups[$i];
			$backup_file = MY_BACKUP_PATH . '/' . $backup_file_name;
			$rel_backup_file = MY_REL_BACKUP_PATH . '/' . $backup_file_name;
	
			$tpl->set_var ('backup_n', $i + 1);
			$tpl->set_var ('backup_file_name', $backup_file_name);
			$tpl->set_var ('backup_file', $backup_file);
			$tpl->set_var ('backup_file_link', 
						   file_exists ($backup_file) ? sprintf ('<A HREF="%s">%s</A>', $rel_backup_file, $tpl->get_var ('backup_file_name')) : '���');
			$tpl->set_var ('backup_file_size',
						   file_exists ($backup_file) ? sprintf ('%s ����', filesize ($backup_file)) : '');
			$tpl->set_var ('backup_file_date',
						   file_exists ($backup_file) ? date ('d.m.Y H:i:s', filemtime ($backup_file)) : '');

			$tpl->parse ('backup_rows', 'backup_row', TRUE);
		}

		$tpl->parse ('backup', 'backup');
	}

    $tpl->p_default ();

	include ('menu_f.php');

    $mysql_factory->print_log ();

?>