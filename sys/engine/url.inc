<?PHP
	
class URL {
	var $script;
	var $vars;

	function URL ($url) {
		$this->parse_url ($url);
	}
	
	function parse_url ($url) {
		$parsed = explode ('?', $url);
		
		$this->script = $parsed[0];

//		$this->script = preg_replace ('!^'.MY_HOST.'!', '', $this->script);
		
		$query = explode ('&', $parsed[1]);
		
		for (reset ($query); $param = current ($query); next ($query)) {
			$param = explode ('=', $param);
			$this->set_var ($param[0], $param[1]);
		}
	}
	
	function set_var ($name, $value) {
		$this->vars[$name] = $value;
	}

	function get_var ($name) {
		return $this->vars[$name];
	}

	function unset_var ($name) {
		$this->vars[$name] = NULL;
		unset ($this->vars[$name]);
	}
	
	function get_url () {
		if (ini_get("session.use_trans_sid") && session_id())
			$this->set_var (session_name(), session_id());

		if (count ($this->vars) > 0) {
			$keys = array_keys ($this->vars);
		
			for (reset ($keys); $key = current ($keys); next ($keys)) {
				if (($this->vars[$key]))
					$query[] = sprintf ("%s=%s", $key, $this->vars[$key]);
				//echo ($this->vars[$key] == 0);
			}
			$query = (count ($query) > 0) ? implode ('&', $query) : '';		
		}
		
		return (strlen ($query) > 0) ? sprintf ("%s?%s", $this->script, $query) : $this->script;
	}

	function build () {
		return $this->get_url ();
	}
}

?>