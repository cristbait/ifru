<?PHP

include_once ('engine/error.inc');
include_once ('engine/cfg_file.inc');

include_once ('engine/db_config.inc');

class EngineConfig {
	var $cfg;
	var $error;
	// [General]
	var $redirect_timeout;
	var $default_page_size;
	var $session_var;
	// [Files]
	var $chmod_available;
	var $chmod_upload;
	// [Language]
	var $enable_language;
	var $default_language;
	var $supported_languages;

	// [mySQL]
	var $mysql_profile;
	var $mysql_db;
	var $mysql_login;
	var $mysql_password;
	var $mysql_debug;
	var $mysql_charset;

	var $_dbConnections;
	var $_DB_cfg;
	var $treeConfig;

	function EngineConfig ($cfg_file_name, &$error) {
		$this->cfg = new ConfigFile ($cfg_file_name, $error);
		$this->error = &$error;

		$this->load_cfg ();
	}
	
	function load_cfg () {
		// [General]
		$this->redirect_timeout = $this->cfg->get_var ('general', 'redirect_timeout');
		$this->default_page_size = $this->cfg->get_var ('general', 'default_page_size');
		$this->session_var = $this->cfg->get_var ('general', 'session_var');

		// [Files]
		$this->chmod_available = $this->cfg->get_var ('files', 'chmod_available');
		// $this->chmod_upload = $this->cfg->get_var ('files', 'chmod_upload');
		$this->chmod_upload = CHMOD_UPLOAD;

		// [Language]
		$this->enable_language = (strtolower ($this->cfg->get_var ('language', 'enable_language')) == 'on');
		$this->default_language = strtolower ($this->cfg->get_var ('language', 'default_language'));
		$this->supported_languages = strtolower ($this->cfg->get_var ('language', 'supported_languages'));

		/*
		// [MySQL]
		$this->mysql_profile = $this->cfg->get_var ('mysql', 'connection');

		$this->mysql_host = $this->cfg->get_var ($this->mysql_profile, 'mysql_host');
		$this->mysql_db = $this->cfg->get_var ($this->mysql_profile, 'mysql_db');
		$this->mysql_login = $this->cfg->get_var ($this->mysql_profile, 'mysql_login');
		$this->mysql_password = $this->cfg->get_var ($this->mysql_profile, 'mysql_password');
		$this->mysql_debug = (strtolower ($this->cfg->get_var ($this->mysql_profile, 'mysql_debug')) == 'on');
		*/
		$this->default_mysql_profile = isset ($GLOBALS['MYSQL_PROFILE']) ? $GLOBALS['MYSQL_PROFILE'] : 'connection';
		$this->loadMysqlProfile ($this->default_mysql_profile);
	
		$this->page_size = $this->default_page_size ? $this->default_page_size : 15;
	}
	
	function loadMysqlProfile ($profile) {
		$this->mysql_profile = $this->cfg->get_var ('mysql', $profile);
		if (empty ($this->mysql_profile))
			return FALSE;

		$this->mysql_host = $this->cfg->get_var ($this->mysql_profile, 'mysql_host');
		$this->mysql_db = $this->cfg->get_var ($this->mysql_profile, 'mysql_db');
		$this->mysql_login = $this->cfg->get_var ($this->mysql_profile, 'mysql_login');
		$this->mysql_password = $this->cfg->get_var ($this->mysql_profile, 'mysql_password');
		$this->mysql_debug = (strtolower ($this->cfg->get_var ($this->mysql_profile, 'mysql_debug')) == 'on');

		$this->mysql_charset = $this->cfg->get_var ($this->mysql_profile, 'mysql_charset');
		return TRUE;
	}

	function &getDB ($profile = NULL) {
		if (! $profile)
			$profile = $this->default_mysql_profile;

		if(! is_object ($this->_dbConnections[$profile])){
			global $mysql_factory;

			// load current mysql profile
			if (! $this->loadMysqlProfile ($profile)) {
				$null = NULL;
				return $null;
			}

			$this->_dbConnections[$profile] = $mysql_factory->create ($this->mysql_db);
			// init
			if ($this->mysql_charset)
				$this->_dbConnections[$profile]->command (sprintf ("SET NAMES %s", $this->mysql_charset));

			// restore default mysql profile
			if ($profile != $this->default_mysql_profile)
				$this->loadMysqlProfile ($this->default_mysql_profile);
		}

		return $this->_dbConnections[$profile];
	}

	function getDBConfig () {
		if (! is_object ($this->db_cfg)) {
			$this->db_cfg = new DBConfig ($this);
		}

		return $this->db_cfg;
	}
	
	function alias ($alias) {
		$alias_list = $this->cfg->get_section ('aliases');
		
		if ($alias_list)
		while (list ($key, $value_list) = each ($alias_list)) {
			$value_list = explode (' ', $value_list);
			
			while ($value = each ($value_list))
				if (strtolower (trim ($alias)) == strtolower (trim ($value[1]))) {
					$retval = $key;
					break;
				}
			
			if ($retval)
				break;
		}

		return ! empty ($retval) ? $retval : $alias;
	}

	/*
	function getTreeClass ($class) {
		if (! is_object ($this->treeConfig)) {
			$this->treeConfig ($this, $this->request, MY_TREE_CONFIG_FILE);
		}

		return $this->treeConfig->getTreeClass ($class);
	}
	*/
}

$config = new EngineConfig (MY_CONFIG_FILE, $error_obj);

?>