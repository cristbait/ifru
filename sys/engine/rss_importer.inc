<?php

require_once 'XML/Tree.php';
require_once 'XML/RSS.php';

class RSSImporter extends XML_RSS {
	var $_request;
	var $_config;
	var $recode = TRUE;
	var $src_charset = 'utf-8';
	var $dst_charset = 'windows-1251';

	function RSSImporter ( &$config, &$request, $filename ){
		$this->_config = &$config;
		$this->_request = &$request;

		$this->XML_RSS ($filename);// , 'utf-8', 'ISO-8859-1');
		$this->parse ();
	}

	function commitChanges () {
		// ����������� �����
	}


	// ��������� RSS 
	// �) ��� ������ � activeTags � �����������
	// �) �������������� insideTags
	// �) multiple tags - � ������

	var $insideStack = array ();

	var $multipleTags = array ();

	var $multipleOpened = FALSE	;
	var $multipleCounter = 0;

    function startHandler($parser, $element, $attribs) {
        if (in_array($element, $this->parentTags)) {
			if ($this->insideTag)
				array_push ($this->insideStack, $this->insideTag);

			$this->insideTag = $element;
		} else {
			$this->activeTag = $element;

			if (in_array($element, $this->multipleTags)) {
				if (! $this->multipleOpened) {
					$this->multipleCounter = 0;
					$this->multipleOpened = TRUE;
				}
			} else
				$this->multipleOpened = FALSE;

			if (! empty ($attribs))
				$this->_addAttributes ($attribs);

        }
    }

    function endHandler($parser, $element) {
		// parent::endHandler($parser, $element);

		if (substr($element, 0, 4) == "RSS:") {
            $element = substr($element, 4);
        }

        if ($element == $this->insideTag) {
            array_pop($this->insideTagStack);
            $this->insideTag = end($this->insideTagStack);

            $this->struct[] = array_merge(array('elementName' => strtolower($element)),
                                          $this->last);
        }

        if ($element == 'ENTRY') {
            $this->items[] = $this->entry;
            $this->entry = '';
        }

        $this->activeTag = '';

		$tagName = strtolower($this->insideTag);
		$elementName = strtolower($element);

        if ($this->multipleOpened) {
            $this->multipleCounter++;
		}

		if (empty ($this->insideTag) && ! empty ($this->insideStack)) {
			$this->insideTag = array_pop ($this->insideStack);
/*
			$tagName = strtolower($this->insideTag);
            $elementName = strtolower($element);

			// print $tagName. ':'.$elementName."\n";
			// print_r ($this->{$elementName});

			// $this->_add ($insideBack, $this->insideTag, $this->last);
			$this->{$tagName}[$elementName] = $this->last;
			// $this->last = $this->{$tagName};
*/
		}
	}

	function _addAttributes ( $attribs ) {
		$tagName = strtolower($this->insideTag);
		$activeName = strtolower($this->activeTag);

		if ( empty ($tagName) )
			return;

		if ( empty ($this->{$tagName}) )
			$this->{$tagName} = array ();

		$item = &$this->{$tagName}[$activeName];

		if ( empty ($item) )
			$item = array ();

        if ($this->multipleOpened) {
			$item = &$item[$this->multipleCounter];
		}

		if ( empty ($item) ) { // ����� ������ ��� � ���� ������ ��� ����������
			foreach ($attribs as $attr => $value) {
				$item[strtolower($attr)] = $this->recode ? iconv ($this->src_charset, $this->dst_charset, $value) : $value;
			}
		}
	}

    function _add($type, $field, $value)
    {
		if (empty($this->{$type}) || empty($this->{$type}[$field]))
			$this->{$type}[$field] = '';
		
		$item = &$this->{$type}[$field];

        if ($this->multipleOpened) {
			if (! is_array ($item))
				$item = array ();

			$item = &$item[$this->multipleCounter];
		}

		if (is_array ($item)) {
			if (! array_key_exists ('cdata-value', $item) || empty ($item['cdata-value']))
				$item['cdata-value'] = '';

			$item = &$this->{$type}[$field]['cdata-value'];
		}

		$item .= $this->recode ? iconv ($this->src_charset, $this->dst_charset, $value) : $value;

        $this->last = $this->{$type};
    }
}

?>