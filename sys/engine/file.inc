<?PHP

class CFile {

	function CFile () {
	}
	
	function unpack_string ($str) {
		$this->file = $str;

		$retval['file'] = $this->file;		

		$this->update ();

		return $retval;
	}
	
	function update () {
		if ($this->file) {
			$this->real_file = sprintf ('%s/%s', MY_REL_ROOT_PATH, $this->file);
		
			if (file_exists ($this->real_file))
				$this->size = filesize ($this->real_file);
		}
	}
	
	function pack_data () {
		return $this->file;
	}
	
	function proceed_parameters (&$request, $prefix, $path, $name_prefix, $name) {
		if (is_uploaded_file ($tmp_file = $request->_post_files[sprintf ('%s_file', $prefix)]['tmp_name'])) {
			$file_info = explode ('.', basename ($request->_post_files[sprintf ('%s_file', $prefix)]['name']));
			$ext = $file_info[count ($file_info) - 1];
			
			if ($this->file && file_exists ($this->real_file))
				unlink ($this->real_file);
				
			$this->file = sprintf ('%s/%s/%s.%s', MY_FILES_FOLDER, $path, sprintf ('%s%s', $name_prefix, $name ? $name : md5 (uniqid (rand ()))), $ext);

			$to_file = sprintf ('%s/%s', MY_REL_ROOT_PATH, $this->file);
			move_uploaded_file ($tmp_file, $to_file);
			if ($request->config->chmod_available)
				chmod ($to_file, $request->config->chmod_upload);

			$this->update ();
		}
		
		if ($retval = ($request->_post[sprintf ('%s_file_del', $prefix)] == true)) {
			$this->file = '';
			$del_file = $request->_post[sprintf ('%s_file_file', $prefix)];
			if (file_exists ($del_file))
				unlink ($del_file);
		}
		
	}
	
	function get_file_link ($name = '') {
		return sprintf ('<a href="/%s">%s</a>',
			$this->file,
			$name);
	}

	function get_file () {
		if ($this->file && file_exists ($this->real_file)) {
		    return file ($this->real_file);
		} else {
		    return "";
		}
	}

	function control ($prefix, $align = true) {
		global $config;
		
		$tpl = new Template ($config, MY_COMMON_TPL_PATH);
		$tpl->set_file ('file_ctl', 'file_ctl.html');
		
		$alt = sprintf ('%s (%d ����)', $this->real_file, $this->size);
		
		$tpl->set_var (
			sprintf ('file', $prefix),
			$this->file ? sprintf ('<a href="%s" ALT="%s" TITLE="%s">%s</a>', 
				$this->real_file, $alt, $alt, $alt ? $alt : "������ �� ����") : '��� �����');
				
		$tpl->set_var ('file_info', $this->file ? $alt : '&nbsp;');
		$tpl->set_var ('prefix', $prefix);
			
		$tpl->set_var ('file_del', common_checkbox (sprintf ("%s_file_del", $prefix), 'Y', false));
		
		$tpl->set_var (sprintf ("%s_file_file", $prefix), $this->real_file);
		$tpl->set_var ('file_file', sprintf ('<INPUT TYPE="hidden" NAME="%s_file_file" VALUE="%s">', $prefix, $this->real_file));
		
		$tpl->parse ('file_ctl', 'file_ctl');
		
		return $tpl->get_var ('file_ctl');
	}
}

?>