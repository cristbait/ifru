<?PHP
	
class Template {
	var $config;
	var $path;
	var $unknowns;
	var $file = array ();	
	var $data = array ();
	var $default;

	function Template (&$config, $path) {
		$this->config = &$config;
		
		if (!is_dir ($path))
			trigger_error (sprintf ("[%s::Template()]: not a directory: '%s'", get_class ($this), $path), E_USER_ERROR);
			
		$this->path = $path;		
		$this->unknowns = $GLOBALS['DEBUG_MODE'] ? 'comment' : 'remove';

		$this->load_default_vars ();
	}
	
	function load_default_vars () {
		$this->set_var ('http_date', gmdate("D, d M Y H:i:s")." GMT");
		$this->set_var ('http_last_modified', gmdate("D, d M Y H:i:s")." GMT");
	}
	
	function is_file ($file_name = NULL) {
		$file = sprintf ("%s/%s", $this->path, $this->config->alias ($file_name));
		return is_file ($file);
	}

	function set_file ($handle, $file_name = NULL) {
		if (is_array ($handle)) {
			if ($file_name)
				trigger_error (sprintf ("[%s::set_file()]: \$handle is an array and \$file_name is not NULL", get_class ($this)), E_USER_ERROR);

			while (list ($cur_handle, $cur_file) = each ($handle))
				$this->set_single_file ($cur_handle, $cur_file);
		} else
			$this->set_single_file ($handle, $file_name);
	}
	
	function set_single_file ($handle, $file_name) {
		if ($handle) {
			$cur_file = sprintf ("%s/%s", $this->path, $this->config->alias ($file_name));
		
			$this->file[$handle] = $cur_file;
			$this->load_file ($handle);
		} else
			trigger_error (sprintf ("[%s::set_single_file()]: \$handle is empty!", get_class ($this), $cur_file), E_USER_ERROR);
	}
	
	function load_file ($handle) {
		if (!$this->file[$handle])
			trigger_error (sprintf ("[%s::load_file()]: empty handle '%s'!", get_class ($this), $handle), E_USER_ERROR);
		
		if (!file_exists ($this->file[$handle]))
			trigger_error (sprintf ("[%s::load_file()]: file '%s' doesn't exist!", get_class ($this), $this->file[$handle]), E_USER_ERROR);
		
		$file = file ($this->file[$handle]);
		
		$file = implode ('', $file);
		
		$this->set_var ($handle, $file);
		
		return true;
	}
	
	function unset_var ($name) {
		unset ($this->data[strtolower (trim ($name))]);
	}

	function set_var ($name, $value) {
		$this->data[strtolower (trim ($name))] = $value;
	}
	
	function get_var ($name) {
		return $this->data[strtolower (trim ($name))];
	}
	
	function get_var_names () {
		return array_keys ($this->data);
	}
	
	function var_tag ($name) {
		return sprintf ('{%s}', strtolower ($name));
	}
	
	function unknown_tag ($name) {
		switch ($this->unknowns) {
			case 'keep':
				$str = $this->var_tag ($name);
				break;
			case 'remove':
				$str = '';
				break;
			case 'comment':
				$str = sprintf ("<!-- Variable '%s' is undefined -->", $name);
				break;
			default:
				trigger_error (sprintf ("[%s::unknown_tag()]: invalid unknown directive: '%s'!", get_class ($this), $this->unknowns));
		}
		
		return $str;
	}
	
	function subst ($name) {
		$var_body = $this->get_var ($name);
		$var_names = $this->get_var_names ();
		
		for ($i = 0; $i < count ($var_names); $i++)
			$var_body = str_replace ($this->var_tag ($var_names[$i]), $this->get_var ($var_names[$i]), $var_body);
		
		return $var_body;
	}
	
	function get_unknowns ($str) {
		preg_match_all ("/\{([^}]+)\}/", $str, $matches);
		$matches = $matches[1];
		
		$result = array ();

		for ($i = 0; $i < count ($matches); $i++) {
			$match = strtolower (trim ($matches[$i]));
			
			if (!isset ($this->data[$match]))
				$result[] = $match;
		}
		
		return $result;
	}
	
	function proceed_unknowns ($str) {
		$result = $str;
		$unknowns = $this->get_unknowns ($result);
		
		for ($i = 0; $i < count ($unknowns); $i++) {
			$result = str_replace ($this->var_tag ($unknowns[$i]), $this->unknown_tag ($unknowns[$i]), $result);
		}
		
		return $result;
	}
	
	function parse ($target, $source, $append = false) {
		// DIRTY HACK
		$this->set_var ('my_host', MY_HOST);
		// END: DIRTY HACK

		$str = $this->subst ($source);
		
		$str = $this->proceed_unknowns ($str);
		
		if ($append)
			$str = $this->get_var ($target).$str;
		
		$this->set_var ($target, $str);
		
		$this->default = $target;

		return $str;
	}
	
	function set_block ($source, $block, $name = '') {
		$reg = sprintf ("/<!--\s+BEGIN %s\s+-->(.*)<!--\s+END %s\s+-->/sm", $block, $block);

//		$beg = sprintf ("<!--\s+BEGIN %s\s+-->", $block);
//		$end = sprintf ("<!--\s+END %s\s+-->", $block);

//		$reg = sprintf ("/%s([^<]*(<+[^<!][^<]+)*|.*?)*?)%s/sm", $beg, $end);
//		$reg = sprintf ("/%s(.*?%s)/sm", $beg, $end);

		$str = $this->get_var ($source);
		$name_tag = (strlen ($name) > 0) ? $this->var_tag ($name) : '';
		
		preg_match_all ($reg, $str, $matches);
		$str = preg_replace ($reg, $name_tag, $str);
//		$var = ereg_replace (sprintf ('%s$', $end), '', $matches[1][0]);
		$var = $matches[1][0];
		
		$this->set_var ($block, $var);
		$this->set_var ($source, $str);
	}
	
	function p ($name) {
		print $this->get_var ($name);
	}
	
	function p_default () {
		$this->p ($this->default);
	}
	
	function get_default () {
		return $this->get_var ($this->default);
	}
	
	function var_exists ($name) {
		return isset ($this->data[$name]);
	}

	function prepare_html ($var_names) {
		if (is_array ($var_names))
			for ($i = 0; $i < count ($var_names); $i++)
				$this->set_var ($var_names[$i], htmlspecialchars ($this->get_var ($var_names[$i])));
		else
			$this->set_var ($var_names, htmlspecialchars ($this->get_var ($var_names)));
	}

}

?>