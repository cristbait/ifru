<?PHP

function errorHappend() {
	$error_string = "<strong>Извините, данный сервис по техническим причинам недоступен</strong><br/>Попробуйте обратиться чуть позже<br/>";
	echo $error_string;
	
	exit;
}

function error_handler ($errno, $errstr, $errfile, $errline) {
	global $error_obj;

	switch ($errno) {
		case E_ERROR:
		case E_CORE_ERROR:
		case E_COMPILE_ERROR:
		case E_USER_ERROR:
			$error_obj->print_logs ();
			$error_obj->print_error ($errno, $errstr, $errfile, $errline);
			exit -1;
			break;
		case E_WARNING:
		case E_CORE_WARNING:
		case E_COMPILE_WARNING:
		case E_USER_WARNING:
			$error_obj->print_error ($errno, $errstr, $errfile, $errline);
			break;
		case E_NOTICE:
		case E_USER_NOTICE:
			break;
		default:
			$error_obj->print_error ($errno, $errstr, $errfile, $errline);
			break;
	}
}

class Error {
	var $display;
	var $log_sources = array ();
	
	function Error ($display = true) {
		$this->display = $display;
		set_error_handler ('error_handler');
	}
	
	function register_log_source ($source) {
		$this->log_sources[] = $source;
	}
	
	function print_logs () {
		for (reset ($this->log_sources); $log = current ($this->log_sources); next ($this->log_sources))
			$log->print_log ();
	}
	
	function print_error ($errno, $errstr, $errfile, $errline) {
//		printf (
//			"<P><B>%s</B>: %s in <B>%s</B> on line <B>%d</B></P>",
//			$this->error_name ($errno),
//			$errstr,
//			$errfile,
//			$errline);
	}
	
	function error_name ($errno) {
		switch ($errno) {
			case E_NOTICE:
				$retval = 'Notice';
				break;
			case E_WARNING:
				$retval = 'Warning';
				break;
			case E_ERROR:
				$retval = 'Fatal error';
				break;
			case E_PARSE:
				$retval = 'Parse error';
				break;
			case E_CORE_ERROR:
				$retval = 'Core error';
				break;
			case E_CORE_WARNING:
				$retval = 'Core warning';
				break;
			case E_COMPILE_ERROR:
				$retval = 'Compile error';
				break;
			case E_COMPILE_WARNING:
				$retval = 'Compile warning';
				break;
			case E_USER_ERROR:
				$retval = 'User error';
				break;
			case E_USER_WARNING:
				$retval = 'User warning';
				break;
			case E_USER_NOTICE:
				$retval = 'User notice';
				break;
			default:
				$retval = 'Unknown error';
		}
		
		return $retval;
	}
	
	function dump ($object) {
		echo '<PRE>';
		print_r ($object);
		echo '</PRE>';
	}
}

$error_obj = new Error (true);

?>