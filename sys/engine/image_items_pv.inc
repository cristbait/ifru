<?PHP

include_once ('engine/image_items.inc');

class PreviewImageItem extends ImageItem {
	function PreviewImageItem ($id, &$factory) {
		parent::ImageItem ($id, $factory);

	    $this->image_pv = new Image ();
	}

    function load_from_row ($row) {
		parent::load_from_row ($row);

		$this->image_pv->unpack_string ($row->image_pv);
    }
    
	function get_update_array () {
		return array (
			'link_class' => $this->link_class,
			'link_id' => $this->link_id,
			'title_ru' => $this->title_ru,
			'description_ru' => $this->description_ru,
			'description_br' => $this->description_br,
			'title_en' => $this->title_en,
			'big_id' => $this->big_id,
			'image' => $this->image->pack_data (),
			'image_pv' => $this->image_pv->pack_data ()
		);	
	}
	
	function get_insert_array () {
		return array (
			'link_class' => $this->link_class,
			'link_id' => $this->link_id,
			'title_ru' => $this->title_ru,
			'description_ru' => $this->description_ru,
			'description_br' => $this->description_br,
			'title_en' => $this->title_en,
			'big_id' => $this->big_id,
			'image' => $this->image->pack_data (),
			'image_pv' => $this->image_pv->pack_data ()
		);	
	}
	
	function load_from_parameters (&$request) {
		parent::load_from_parameters ($request);

		if ($this->image->just_uploaded) {
			$this->image_pv->copy_resized ($this->image, $this->factory->pv_w, $this->factory->pv_h, MY_FILES_FOLDER.'/image_items_pv', 'image_pv_', $this->id);
		}
	}
	
	function set_template_vars (&$tpl) {
		parent::set_template_vars ($tpl);

		$tpl->set_var ('image_item_image_tag', $this->image->get_img_tag ($this->factory->r_w, $this->factory->r_h, NULL, 0, $this->factory->root_path, $this->title_ru));
		$tpl->set_var ('image_item_image_pv_tag', $this->image_pv->get_img_tag ($this->factory->r_pv_w, $this->factory->r_pv_h, NULL, 0, $this->factory->root_path, $this->title_ru));
	}

	function control ($description = FALSE, $align = TRUE, $insert = TRUE) {
		global $config;
		
		$tpl = new Template ($config, MY_COMMON_TPL_PATH);
		$tpl->set_file ('image_item_ctl', 'image_item_pv_ctl.html');
		$tpl->set_block ('image_item_ctl', 'align_block', 'align_block');
		$tpl->set_block ('image_item_ctl', 'info_block', 'info_block');
		$tpl->set_block ('image_item_ctl', 'actions_block', 'actions_block');

		$this->set_template_vars ($tpl);

		list ($w, $h) = $this->image_pv->find_dims (500, NULL);

		$alt = sprintf ('%s (%dx%d, %d ����)',
			$this->image_pv->file, $this->image_pv->width, $this->image_pv->height, $this->image_pv->size);
		$tpl->set_var (
			sprintf ('image_pv', $prefix),
			$this->image_pv->image ? sprintf ('<IMG SRC="%s" WIDTH="%s" HEIGHT="%s" ALT="%s" border="0">', 
				$this->image_pv->file, $w, $h, $alt, $alt) : '��� ������');
		$tpl->set_var ('image_pv_info', $this->image_pv->image ? $alt : '&nbsp;');
		
		$alt = sprintf ('%s (%dx%d, %d ����)',
			$this->image->file, $this->image->width, $this->image->height, $this->image->size);
		$tpl->set_var (
			sprintf ('image', $prefix),
			$this->image->image ? sprintf ('<IMG SRC="%s" WIDTH="%s" HEIGHT="%s" ALT="%s" border="0">', 
				$this->image->file, $this->image->width, $this->image->height, $alt, $alt) : '��� ��������');
		$tpl->set_var ('image_info', $this->image->image ? $alt : '&nbsp;');

		$tpl->set_var ('image_pv_href', $this->image->image ? ('<a href="'.$this->image->file.'" target="_blank">'.$tpl->get_var ('image_pv').'</a>') : $tpl->get_var ('image_pv'));

		$tpl->set_var ('image_src', $this->image->image);
		$tpl->set_var ('image_width', $this->image->width);
		$tpl->set_var ('image_height', $this->image->height);

		$tpl->set_block ('actions_block', 'button', 'button');

		$tpl->set_var ('prefix', 'image_item');

		$image_id = $this->id ? $this->id : '';
		$tpl->set_var ('image_id', $image_id);
	
		if ($align) {
			$tpl->set_var ('image_align', $this->image->halign);

			$tpl->set_var ('ha_left', common_radio_button (sprintf ('image_item_halign_%s', $image_id), 'left', $this->image->halign == 'left'));
			$tpl->set_var ('ha_right', common_radio_button (sprintf ('image_item_halign_%s', $image_id), 'right', $this->image->halign == 'right'));

			$tpl->parse ('align_block', 'align_block');
		} else {
			$tpl->set_var ('align_block', '');
		}
	
		$tpl->set_var ('image_item_description_br_checkbox', common_checkbox (sprintf ("image_item_description_br_%s", $image_id), 1, $this->description_br));

		$tpl->set_var ('image_del', common_checkbox (sprintf ("image_del_%s", $image_id), 'Y', FALSE));
		
//		$tpl->set_var (sprintf ("%s_image_file", $prefix), $this->file);
		$tpl->set_var ('image_file', sprintf ('<INPUT TYPE="hidden" NAME="image_file_%s" VALUE="%s">', $image_id, $this->image->file));
		
		if (! ($image_id && $insert))
			$tpl->set_var ('button', '&nbsp;');
		else
			$tpl->parse ('button', 'button', FALSE);
			
		if ($image_id > 0) {
			$tpl->parse ('info_block', 'info_block', FALSE);
			$tpl->parse ('actions_block', 'actions_block', FALSE);
		} else {
			$tpl->set_var ('info_block', '');
			$tpl->set_var ('actions_block', '');
		}

		$tpl->parse ('image_item_ctl', 'image_item_ctl');
		
		return $tpl->get_var ('image_item_ctl');
	}
}

class PreviewImageItemFactory extends ImageItemFactory {
	var $link_class;
	var $link_id;

	var $pv_w = 100;
	var $pv_h = 75;
	var $w = 500;
	var $h = 317;

	var $r_pv_w = NULL;
	var $r_pv_h = NULL;
	var $r_w = NULL;
	var $r_h = NULL;

	var $root_path = NULL;
	var $default_preview = 'images/default_preview.jpg';
	var $max_images = NULL;

	var $pv_resize_only = FALSE;
	var $resize_only = FALSE;

	function PreviewImageItemFactory (&$config, &$request, $link_class, $link_id) {
		parent::ListFactory ($config, $request, 'image_items_pv', 'id', 'id', 'PreviewImageItem', FALSE);
		$this->link_class = $link_class;
		$this->link_id = $link_id;
	} 

	function set_pv_sizes ($width, $height) {
		$this->pv_w = $width;
		$this->pv_h = $height;
	}

	function set_sizes ($width, $height) {
		$this->w = $width;
		$this->h = $height;
	}

	function restrict_pv_sizes ($width, $height) {
		$this->r_pv_w = $width;
		$this->r_pv_h = $height;
	}

	function restrict_sizes ($width, $height) {
		$this->r_w = $width;
		$this->r_h = $height;
	}

	function set_resize_only ($pv_im, $im) {
		$this->pv_resize_only = $pv_im;
		$this->resize_only = $im;
	}

	function proceed_images (&$request) {
		$image_id_list = $request->_post['image_id'];

		if ($image_id_list)
		for ($i = 0; $i < count ($image_id_list); $i++) {
			$real_id = $image_id_list[$i] > 0 ? $image_id_list[$i] : 0;
			$image_item = new PreviewImageItem ($real_id, $this);

			$image_item->link_class = $this->link_class;
			$image_item->link_id = $this->link_id;
			$image_id = $image_item->id ? $image_item->id : '';
			$postfix = $image_id_list[$i];

			$image_item->title_ru = $request->_post[sprintf('image_item_title_ru_%s', $postfix)];
			$image_item->title_en = $request->_post[sprintf('image_item_title_en_%s', $postfix)];
			$image_item->description_ru = $request->_post[sprintf('image_item_description_ru_%s', $postfix)];
			$image_item->description_br = $request->_post[sprintf('image_item_description_br_%s', $postfix)];
			$image_item->image->halign = $request->_post[sprintf ('image_item_halign_%s', $postfix)];

			$file_info_param = sprintf('image_item_image_%s', $postfix);

			$db_image = NULL;
			$this->clear_cache (array ($real_id));
			$db_image = $this->create ($real_id);

			$db_image->image->halign = $image_item->image->halign;
			$db_image->title_ru = $image_item->title_ru;
			$db_image->description_ru = $image_item->description_ru;
			$db_image->description_br = $image_item->description_br;
			$db_image->title_en = $image_item->title_en;

			if (is_uploaded_file ($tmp_image = $request->_post_files[$file_info_param]['tmp_name'])) {
				$file_info = explode ('.', basename ($request->_post_files[$file_info_param]['name']));
				$ext = $file_info[count ($file_info) - 1];
				
				if ($db_image->image->file && file_exists ($db_image->image->file))
					unlink ($db_image->image->file);

				if ($real_id)
					$image_item->image->image = sprintf ('%s/image_items_pv/%s.%s', MY_FILES_FOLDER, sprintf ('image_%s_%s',
						$image_item->link_class, $real_id), $ext);

				$image_item->factory->clear_cache (array ($image_item->id));
				$image_item->factory->to_cache ($image_item->id, $image_item);

				$image_item->update ();

				if (! $real_id) {
					$image_id = $image_item->factory->mysql->insert_id ();
					$image_item->id = $image_id;

					$image_item->image->image = sprintf ('%s/image_items_pv/%s.%s', MY_FILES_FOLDER, sprintf ('image_%s_%s',
						$image_item->link_class, $image_id), $ext);

					$image_item->factory->clear_cache (array ($image_id));
					$image_item->factory->to_cache ($image_id, $image_item);

					$image_item->update ();
				}

				$to_file = sprintf ('%s/%s', MY_REL_ROOT_PATH, $image_item->image->image);
				move_uploaded_file ($tmp_image, $to_file);
				if ($request->config->chmod_available)
					chmod ($to_file, $request->config->chmod_upload);

				$image_item->image->update ();
				$image_item->image->just_uploaded = TRUE;

			} elseif ($db_image->image->image)
					$db_image->update ();
			
			if ($retval = ($request->_post[sprintf ('image_del_%s', $image_id)] == true)) {
				$image_item->image->image = '';
				$image_item->image->just_uploaded = TRUE;
				$del_file = $request->_post[sprintf ('image_file_%s', $image_id)];
				if (file_exists ($del_file)) 
					unlink ($del_file);
				$db_image->delete ();
			}

			if ($image_item->image->just_uploaded) {
				if ($this->r_w || $this->r_h)
					if ($this->resize_only) 
						$res = $image_item->image->_resizeImage ( $this->r_w, $this->r_h );
					else
						$res = $image_item->image->_resizeAndCutImage ( $this->r_w, $this->r_h );

				if ($db_image->image_pv->file && file_exists ($db_image->image_pv->file))
					unlink ($db_image->image_pv->file);

				$res = $image_item->image_pv->copy_resized ($image_item->image, $this->r_pv_w, $this->r_pv_h, MY_FILES_FOLDER.'/image_items_pv', sprintf ('image_pv_%s_', $image_item->link_class), $image_id, $this->pv_resize_only);

				if (! $res) {
					$this->copy_default_preview ($image_item);
				}

				$image_item->factory->clear_cache (array ($image_id));
				$image_item->factory->to_cache ($image_id, $image_item);

				$image_item->update ();
			}
		}

		if ($image_id_list && $this->max_images && count ($image_id_list) > $this->max_images) {
			$image_list = $this->create_all (1000, $this->max_images);

			if ($image_list)
			for ($i = 0; $i < count ($image_list); $i++) {
				$image_item = &$image_list[$i];
				// print $image_item->id . "<br>";
				
				if ($image_item->image->file && file_exists ($image_item->image->file))
					unlink ($image_item->image->file);
				if ($image_item->image_pv->file && file_exists ($image_item->image_pv->file))
					unlink ($image_item->image_pv->file);

				$image_item->delete ();
			}
		}
	} 

	function copy_default_preview (&$image_item) {
	    $default = new Image ();
		$default->load_image ($this->default_preview);

		$image_item->image_pv->copy_resized ($default, $this->pv_w, $this->pv_h, MY_FILES_FOLDER.'/image_items_pv', sprintf ('image_pv_%s_', $image_item->link_class), $image_item->id, $this->pv_resize_only);
	}


	function delete_old_images () {
		$row = $this->mysql->select (sprintf ("SELECT image, image_pv FROM %s", $this->table_name));

	    $image_pv = new Image ();
	    $image = new Image ();
		$images = array ();

		$dir = sprintf ('%s/%s/image_items_pv', MY_REL_ROOT_PATH, MY_FILES_FOLDER);
		if ($dh = opendir ($dir)) {
			while (($file = readdir ($dh)) !== FALSE) {
				if (preg_match ('/^\./', $file))
					continue;

				$images[$file] = $dir . '/' . $file;
			}
			closedir ($dh);
		}

		if (sizeof ($row) > 0)
		foreach ($row as $i => $item) {
			$image_pv->unpack_string ($item->image_pv);
			$image->unpack_string ($item->image);

			if ($image_pv->image)
				if (getArrayValue (basename ($image_pv->image), $images)) 
					unset ($images[basename ($image_pv->image)]);

			if ($image->image)
				if (getArrayValue (basename ($image->image), $images)) 
					unset ($images[basename ($image->image)]);
		}

		$res = 0;
		foreach ($images as $file => $filepath) {
			// print $filepath . "<br>";
			if (file_exists ($filepath)) {
				unlink ($filepath);
				$res++;
			}
		}

		return $res;
	}
}




?>