<?PHP

include_once ('engine/url.inc');

class Page {
	var $index;
	var $pager;
	var $url;
	var $first_row;
	var $last_row;
	var $href;
	
	function Page ($index, &$pager) {
		$this->index = $index;
		$this->pager = &$pager;
		
		$this->url = new URL ($this->pager->script);
		
		$this->update ();
	}
	
	function update () {
		$this->first_row = ($this->index * $this->pager->page_size) + 1;
		$this->last_row = $this->first_row + $this->pager->page_size - 1;
		
		if ($this->last_row > $this->pager->total_size)
			$this->last_row = $this->pager->total_size;

		$this->url->set_var ($this->pager->page_name, $this->index);
		
		$this->href = $this->url->get_url ();
	}
	
	function is_current () {
		return ($this->index == $this->pager->current->index);
	}
}

class Pager {
	var $factory;
	var $request;
	var $page_size;
	var $total_size;
	var $page_count;
	var $page_name;
	var $id_name;
	var $script;
	var $current;
	var $offset;
	var $pages;
	
	function Pager (&$list_factory, &$request, $page_size, $script, $page_name, $id_name = NULL) {
		$this->factory = &$list_factory;
		$this->request = &$request;
		$this->page_size = $page_size;
		$this->total_size = $this->factory->get_total_count ();
		$this->script = $script;
		$this->page_name = $page_name;
		$this->id_name = $id_name;
		
		$this->update ();
	}
	
	function update () {
		$this->page_count = ceil ($this->total_size / $this->page_size);
		if ($this->page_count == 0)
			$this->page_count = 1;

		$this->pages = array ();
		for ($i = 0; $i < $this->page_count; $i++) {
			$this->pages[] = new Page ($i, $this);
		}
			
		$this->current = $this->get_page (($this->id_name && ($this->request->_get[$this->id_name] != NULL)) ?
			$this->page_by_id ($this->request->_get[$this->id_name]) :
			$this->request->_get[$this->page_name]);

		$this->offset = ($this->current->first_row - 1);

//		print "Offset: " . $this->offset . "<br>";
	}
	
	function &get_page ($index) {
//		print "Required page index: " . $index ."<br>";
		if (!is_long ($index))
			$index = sprintf ("%d", $index);
		
		if (($index < 0) || ($index > ($this->page_count - 1)))
			trigger_error (sprintf ("[%s::get_page()]: incorrect pager request: no page found with index '%d'.", get_class ($this), $index));

		return $this->pages[$index];
	}
	
	function page_by_index ($index) {
		return floor ($index / $this->page_size);
	}
	
	function page_by_id ($id) {
		$item = $this->factory->create ($id);

		if (!$item)
			$this->goto_first_page ();

//		print "Index by id: " . $this->factory->index_by_id ($id) . ", ID: " . $id . "<br>";
//		if ($this->factory->movable)
			return $this->page_by_index ($this->factory->index_by_id ($id));
	}
	
	function goto_first_page () {
		$url = new URL ($this->script);
		$url->set_var ($this->id_name, '');
			
		header ("HTTP/1.1 303 See Other");
		header (sprintf ("Location: %s", $url->get_url ()));
	}

	function set_template_vars (&$tpl) {
		$tpl->set_var ('pager_first', $this->current->first_row);
		$tpl->set_var ('pager_last', $this->current->last_row);
		$tpl->set_var ('pager_total', $this->total_size);
	}
}

?>