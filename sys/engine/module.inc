<?PHP

class Module {
	var $fullname;
	var $name;
	var $subname;
	var $factory_class;

	var $module_path;
	var $class_path;

	function Module ($name) {
		$this->fullname = $name;

		if (preg_match ('/^([^_]+)_(.+)/', $name, $regs)) {
			$this->name = $regs[1];
			$this->subname = $regs[2];
		} else {
			$this->name = $name;
			$this->subname = NULL;
		}
	}

	function init ($debug = FALSE) {
		$this->module_path = MY_MODULES_PATH.'/'.$this->name;
		$this->class_path = $this->module_path."/".$this->name.".inc";

		if (! $this->exist ()) {
			if ($debug)
				trigger_error (sprintf ('[%s::init()]: ��� ������ ������ "%s"', get_class ($this), $this->name), E_USER_ERROR);

			return FALSE;
		}

		include_once ($this->class_path);
		$this->factory_class = $factory_class;

		return TRUE;
	}

	function exist () {
		return is_dir ($this->module_path) && is_file ($this->class_path);
	}

	function call_backend (&$config, &$request) {
		$module_fullname = $this->fullname;
		$module_name = $this->name;
		$module_subname = $this->subname;
		$module_path = $this->module_path;
		$factory_class = $this->factory_class;

		$mod_url = new URL ($_SERVER['SCRIPT_NAME']);
		$mod_url->set_var ('mod', $this->fullname);

		include_once ($module_path."/backend.inc");
	}

	function call_frontend (&$config, &$request, &$structure) {
		$module_fullname = $this->fullname;
		$module_name = $this->name;
		$module_subname = $this->subname;
		$module_path = $this->module_path;
		$factory_class = $this->factory_class;

		// print $module_name . ":" . $module_subname . "<br>";

		include ($module_path."/frontend.inc");
	}
}

?>