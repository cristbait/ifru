<?PHP

class Language {
	var $request;
	var $config;
	var $code;
	var $supported = array ();
	var $enabled;

	function Language (&$config, &$request) {
		$this->request = &$request;
		$this->config = &$config;

		if ($this->enabled = $this->config->enable_language) {
			$this->code = strtolower ($this->request->lang);
			$this->detect_supported ();
			
			if (! in_array ($this->code, $this->supported)) {
				if (strlen ($this->code) > 0)
					trigger_error (sprintf ("[%s::Language] unsupported language: '%s'", get_class ($this), $this->code), E_USER_WARNING);
					
				$this->code = $this->config->default_language;
			}
		}
	}
	
	function detect_supported () {
		$supported_list = explode (" ", $this->config->supported_languages);
		
		for (reset ($supported_list); $lang = current ($supported_list); next ($supported_list))
			$this->supported[] = strtolower (trim ($lang));
	}
	
}

?>