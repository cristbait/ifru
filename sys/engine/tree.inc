<?php

include_once ('engine/list.inc');

class TreeNode extends ListItem {
	var $children = array ();
	var $parent = NULL;
	var $level = 0;

	var $_first_child = NULL;
	var $_first_child_loaded = FALSE;

    function load_from_row ($row) {
        $this->ord_index = $row->ord_index;
        $this->parent_id = $row->parent_id;
        $this->root_id = $row->root_id;
        $this->publish = $row->publish;
        $this->name = $row->name;
    }

	function load_defaults () {
  		$this->parent_id = $this->factory->request->_get['parent_id'];
  		$this->root_id = NULL;
  		$this->publish = 1;

		if ($this->parent_id) {
			$parent = $this->factory->create ($this->parent_id);
			$this->root_id = $parent->root_id;
		}
	}

	function get_update_array () {
		return array (
			'publish' => $this->publish,
			'name' => $this->name
		);	
	}
	
	function get_insert_array () {
		return array (
			'ord_index' => $this->factory->get_next_index ($this->parent_id),
			'parent_id' => $this->parent_id,
			'root_id' => $this->root_id,
			'publish' => $this->publish,
			'name' => $this->name
		);	
	}

	function load_from_parameters (&$request, $prefix = "") {
		$this->parent_id = $request->_post[$prefix.'parent_id'];
		$this->root_id = $request->_post[$prefix.'root_id'];
		$this->publish = $request->_post[$prefix.'publish'];
		$this->name = $request->_post[$prefix.'name'];
	}
	
	function set_template_vars (&$tpl, $form = FALSE, $prefix = "") {
		$tpl->set_var ($prefix.'id', $this->id);
		$tpl->set_var ($prefix.'ord_index', $this->ord_index);
		$tpl->set_var ($prefix.'parent_id', $this->parent_id);
		$tpl->set_var ($prefix.'root_id', $this->root_id);
		$tpl->set_var ($prefix.'publish', $this->publish);
		$tpl->set_var ($prefix.'name', $this->name);

		$tpl->set_var ($prefix.'publish_full', $this->publish ? "��" : "���");
		$tpl->set_var ($prefix.'publish_full_color', $this->publish ? "<font>������������</font>" : "<font>��������</font>");

		if ($form) {
			$tpl->set_var ($prefix.'publish_checkbox', common_checkbox ($prefix.'publish', 1, $this->publish));
		}
	}

	function count_children () {
		return $this->factory->count_children ($this->id);
	}

	function &load_children ($publish = FALSE) {
		return $this->factory->load_children ($this->id, $publish);
	}

	function &load_related_children ($publish = FALSE) {
		return $this->factory->load_related_children ($this, $publish);
	}

	function &load_branch ($publish = FALSE) {
		return $this->factory->load_branch ($this, $publish);
	}

	function &load_root_node () {
		return $this->factory->create ($this->root_id);
	}

	function get_first_child_id () {
		$item = $this->load_first_child (TRUE);
		
		return is_object ($item) ? $item->id : NULL;
	}

	function &load_first_child ($publish = FALSE) {
		if (! $this->_first_child_loaded) {
			$first = $this->load_children ($publish);

			$this->_first_child = &$first[0];
			$this->_first_child_loaded = TRUE;
		}

		if (is_object ($this->_first_child))
			return $this->_first_child;

		return ($this->_first_child = NULL);
	}

	function make_tree_sublinks (&$url) {
		$url->set_var ('parent_id', $this->id);
		$this->name = sprintf ('<a href="%s" class="ilink">%s (%d)</a>', $url->build (), $this->name, $this->count_children ());
	}

	function get_path_string (&$url, $top = NULL, $showlast = FALSE) {
		$item = &$this;
		$count = 0; 
		$path = array ();

		if ($showlast) {
			$url->set_var ('parent_id', $item->id);
			$path = array_unshift ($path, sprintf ('<a href="%s">%s</a>', $url->build (), $item->name));
		}

		while (! empty ($item->parent_id) && $count < 100) {
			$item = $this->factory->create ($item->parent_id);
			if (! is_object ($item)) break;

			$url->set_var ('parent_id', $item->id);
			array_unshift ($path, sprintf ('<a href="%s">%s</a>', $url->build (), $item->name));
			$count++;
		} 

		if (! empty ($top)) {
			$url->unset_var ('parent_id');
			array_unshift ($path, sprintf ('<a href="%s">%s</a>', $url->build (), $top));
		}

		return join (' - ', $path);
	}

	function updateLevels ($start = 0) {
		$this->level = $start;

		foreach ($this->children as $id => $dummy)
			$this->children[$id]->updateLevels ($start + 1);
	}

	function createSubTree () {
		return NULL;
	}
}


class Tree extends SubListFactory {
	function Tree (&$config, &$request, $table_name, $class_name) {
		$this->SubListFactory ($config, $request, $table_name, 'id', 'ord_index', 'parent_id', $class_name, $movable = TRUE);
	}
		
	function update ($id) {
		$retval = parent::update ($id);

		if ($retval) {
			$item = $this->create ($id);
			if (! $item->root_id) {
				$item->root_id = $item->id;
				$item->parent_id = NULL;

				$this->mysql->command ($item->std_update_string (
					array ('root_id' => $item->root_id, 'parent_id' => $item->parent_id),
					array ($this->id_name => $item->id)
				));
			}
		}
	
		return $retval;
	}

	function count_children ($id) {
		$row = $this->mysql->select_field (sprintf ("SELECT count(*) AS count FROM %s WHERE parent_id = '%d'", $this->table_name, $id));
	
		return $row[0];
	}
	
	function set_parent ($parent_id) {
		$this->sub_id = $parent_id;
		$this->sub_id_name = "parent_id";
	}

	function set_root ($root_id) {
		$this->sub_id = $root_id;
		$this->sub_id_name = "root_id";
	}

	function get_next_index ($parent_id = NULL) {
		$this->set_parent ($parent_id);
		return parent::get_next_index ();
	}

	function get_shift_index ($parent_id = NULL) {
		$this->set_parent ($parent_id);
		return parent::get_shift_index ();
	}

	function repair_index () {
		// �� ������ ������, ��� ���� ��������� parent_id � ��������
		return true;
	}

	function index_by_id ($id) {
		return 0;
	}

	function moveInTree ($id, $up, $parent_id) {
		$this->set_parent ($parent_id);
		return $this->move ($id, $up);
	}

	function &load_children ($parent_id, $publish = FALSE) {
		return $this->create_from_query (sprintf ("SELECT * FROM %s WHERE parent_id = '%d' %s ORDER BY %s ASC", $this->table_name, $parent_id, $publish ? "AND publish = 1" : "", $this->index_name));
	}

	function &load_related_children (&$node, $publish = FALSE) {
		return $this->load_children ($node->id, $publish);
	}

	function &load_root_nodes ($publish = FALSE) {
		return $this->load_children (0, $publish);
	}

	function &create_tree ($limit = NULL, $offset = NULL) {
		$list = $this->create_all ($limit, $offset);

		if ($list)
		foreach ($list as $id => $dummy)
			$list[$id]->load_branch ();

		return $list;
	}

	function load_branch (&$root, $publish = FALSE) {
		$root_id = empty ($root->parent_id) ? $root->id : $root->root_id;

		$all = $this->create_from_query_id (sprintf ("SELECT * FROM %s WHERE root_id = '%d' %s ORDER BY %s ASC", $this->table_name, $root_id, $publish ? "AND publish = 1" : "", $this->index_name));

		if ($all)
		foreach ($all as $id => $dummy) {
			$item = &$all[$id];

			if ($item->parent_id && is_object ($all[$item->parent_id]))	{
				$parent = &$all[$item->parent_id];
				$parent->children[] = &$item;
				$item->parent = &$parent;
			}
		}

		$root->updateLevels ();
	}


	function move ($id, $up, $add = "") {
		$item = $this->create ($id);
		$this->set_parent ($item->parent_id);

		return parent::move ($id, $up);
	}
}
