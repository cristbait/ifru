<?PHP

include_once ('engine/error.inc');
include_once ('engine/config.inc');
include_once ('engine/factory.inc');

class MySQL {
    var $link;
    var $debug;
    var $log_list = array ();
    var $db;

    function MySQL ($host, $db, $login, $password, $debug = false) {
        $this->debug = $debug;
        $this->db = $db;

        $this->link = mysql_connect ('localhost', '043537002_ifru', 'n4f&R+TDfb>5');
        mysql_query("SET NAMES cp1251");
        mysql_query("SET CHARACTER SET 'cp1251'");
        mysql_query("set character_set_client='cp1251'");
        mysql_query("set character_set_results='cp1251'");
        mysql_query("set collation_connection='cp1251_general_ci'");

        if ($this->link)
            mysql_select_db ('ifru_dump', $this->link);
    }

    function select ($query_str) {
//		echo $query_str.'<BR>';
        if (!$this->link)
            return false;
        $query = mysql_query ($query_str, $this->link);
        if ($query) {
            $retval = array ();

            while ($row = mysql_fetch_object ($query))
                $retval[] = $row;


            mysql_free_result ($query);
        } else
            $retval = false;

        if ($this->debug)
            $this->add_log_item ("select", $query_str, $retval !== false);

        if ($retval === false)
            trigger_error (sprintf ("[%s::select()]: %s", get_class ($this), mysql_error ($this->link)), E_USER_WARNING);
//        var_dump(mb_detect_encoding($retval[0]->page_h1));

        return $retval;
    }

    function command ($query_str) {
        if (!$this->link)
            return false;

        $retval = mysql_query ($query_str, $this->link);
        //echo $query_str;

        if ($this->debug)
            $this->add_log_item ("command", $query_str, $retval);

        if (!$retval)
            trigger_error (sprintf ("[%s::command()]: %s", get_class ($this), mysql_error ($this->link)), E_USER_WARNING);

        return $retval;
    }

    function command_affected ($query_str) {
        return $this->command ($query_str) ? $this->affected_rows() : -1;
    }

    function affected_rows () {
        if (!$this->link)
            return FALSE;

        $retval = mysql_affected_rows ($this->link);

        if ($this->debug)
            $this->add_log_item ("affected_rows", sprintf ("AFFECTED_ROWS = %d", $retval), TRUE);

        return $retval;
    }

    function select_field ($query_str) {
        if (!$this->link)
            return false;

        $query = mysql_query ($query_str, $this->link);
        if ($query) {
            while ($row = mysql_fetch_row ($query))
                $retval[] = $row[0];

            mysql_free_result ($query);
        } else
            $retval = false;

        if ($this->debug)
            $this->add_log_item ("select_field", $query_str, $retval !== false);

        if ($retval === false)
            trigger_error (sprintf ("[%s::select_field()]: %s", get_class ($this), mysql_error ($this->link)), E_USER_WARNING);

        return $retval;
    }

    function insert_id () {
        if (!$this->link)
            return FALSE;

        $retval = mysql_insert_id ($this->link);

        if ($this->debug)
            $this->add_log_item ("insert_id", sprintf ("INSERT_ID = %d", $retval), $retval);

        return $retval;
    }

    function add_log_item ($method_name, $query_str, $result) {
        $this->log_list[] = sprintf ('<B>%s::%s ():</B> %s; <B><FONT COLOR="%s">%s</FONT></B><BR>',
            $this->db, $method_name, $query_str, $result ? "green" : "red", $result ? "OK" : "ERROR");
    }

    function print_log () {
        if ($this->debug) {
            print sprintf ("<H3>Printing mySQL log (db='%s', %d items):</H3>", $this->db, count($this->log_list));
            for (reset ($this->log_list); $item = current ($this->log_list); next ($this->log_list))
                print $item;
        }
    }

    function print_empty_log () {
        if ($this->debug) {
            print sprintf ("<H3>Printing mySQL log (db='%s', %d items):</H3>", $this->db, count($this->log_list));
        }
    }

    function get_tables () {
//		$query = @mysql_list_tables ($this->db, $this->link);
        $query = mysql_query ($q = sprintf ("SHOW TABLES FROM %s", $this->db), $this->link);

        if ($query) {
            for ($i = 0; $i < mysql_num_rows ($query); $i++)
                $retval[] = mysql_tablename ($query, $i);

            mysql_free_result ($query);
        } else
            $retval = FALSE;

        if ($this->debug)
            $this->add_log_item ("get_tables", $q, $retval);

        if (!$retval)
            trigger_error (sprintf ("[%s::get_tables()]: %s", get_class ($this), mysql_error ($this->link)), E_USER_WARNING);

        return $retval;
    }

    function get_fields ($table_name) {
        $query = mysql_list_fields ($this->db, $table_name, $this->link);

        if ($query) {
            for ($i = 0; $i < mysql_num_fields ($query); $i++) {
                $retval[] = array (
                    'name' => mysql_field_name ($query, $i),
                    'flags' => explode (' ', mysql_field_flags ($query, $i)));
            }

            mysql_free_result ($query);
        } else
            $retval = FALSE;

        if ($this->debug)
            $this->add_log_item ("get_fields", 'mysql_list_fields()', $retval);

        if (!$retval)
            trigger_error (sprintf ("[%s::get_fields()]: %s", get_class ($this), mysql_error ($this->link)), E_USER_WARNING);

        return $retval;
    }

    function backup () {
        if (!($table_list = $this->get_tables ()))
            return FALSE;

        //$retval = "FLUSH TABLES;\n\n";
        $retval = "SET NAMES cp1251;\r\n\r\n";

        for ($i = 0; $i < count ($table_list); $i++) {
            if (!($field_list = $this->get_fields ($table_list[$i])))
                return FALSE;

            $retval .= sprintf ("DROP TABLE IF EXISTS `%s`;\r\n", $table_list[$i]);

            if (!($cr_table_list = $this->select (sprintf ("SHOW CREATE TABLE `%s`", $table_list[$i]))))
                return FALSE;
            $cr_table_arr = get_object_vars ($cr_table_list[0]);
            $cr_table = str_replace ("\n", "\r\n", $cr_table_arr['Create Table']);

            $retval .= $cr_table.";\r\n\r\n";

            $fields = array ();
            for ($j = 0; $j < count ($field_list); $j++)
                $fields[] = $field_list[$j]['name'];

            $fields = implode (', ', $fields);

            $query = mysql_query ($query_str = sprintf ("SELECT * FROM `%s`", $table_list[$i]), $this->link);
            if ($query) {
                while ($row = mysql_fetch_array ($query)) {
                    $values = array ();
                    for ($j = 0; $j < count ($field_list); $j++) {
                        $field = $field_list[$j];
                        $value = $row[$field['name']] ?
                            sprintf ("'%s'",  mysql_real_escape_string ($row[$field['name']])) :
                            (in_array ('not_null', $field['flags']) ? "''" : 'NULL');
                        $values[] = $value;
                    }

                    $values = implode (', ', $values);

                    $retval .= sprintf ("INSERT INTO `%s` (%s) VALUES (%s);\r\n",
                        $table_list[$i],
                        $fields,
                        $values);
                }

                $retval .= "\r\n";

                mysql_free_result ($query);
            } else
                return FALSE;
        }

        return $retval;
    }

    function table_seek ($tablename) {
        $rslt = mysql_query ($query_str = "SHOW TABLES LIKE '" . mysql_real_escape_string(addCslashes($tablename, "\\%_")) . "'", $this->link);

        if ($this->debug)
            $this->add_log_item ("table_seek", $query_str, $retval !== false);

        return mysql_num_rows ($rslt) > 0;
    }
}

class MySQLFactory extends Factory {
    var $error;

    function MySQLFactory (&$config, &$request) {
        $this->Factory ($config, $request);

        $this->error = &$config->error;

        $this->error->register_log_source ($this);
    }

    function &create_new ($id, $load) {
        $obj = $this->to_cache (
            $id,
            new MySQL (
                $this->config->mysql_host,
                $id,
                $this->config->mysql_login,
                $this->config->mysql_password,
                $this->config->mysql_debug && $GLOBALS['DEBUG_MODE']));

        return $obj;
    }

    function print_log () {
        if (count ($this->cache) > 0)
            for (reset ($this->cache); $mysql = current ($this->cache); next ($this->cache))
                $mysql->print_log ();
    }

    function print_empty_log () {
        if (count ($this->cache) > 0)
            for (reset ($this->cache); $mysql = current ($this->cache); next ($this->cache))
                $mysql->print_empty_log ();
    }
}

$mysql_factory = new MySQLFactory ($config, $request);

?>