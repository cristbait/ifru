<?PHP

include_once ('engine/config.inc');
include_once ('engine/list.inc');

class DBConfig {
	var $config;
	var $mysql;
	var $cache;
	var $table_name;
	var $id_name;
	var $title_name;
	var $value_name;

	function DBConfig (&$config) {
		$this->config = &$config;
		$this->mysql = $config->getDB();
		
		// TODO: ��������� ����� ���������������� ����
		$this->table_name = 'config';
		$this->id_name = 'name';
		$this->title_name = 'title';
		$this->value_name = 'value';
	}
	
	function get_var ($name) {
		if (!isset ($this->cache[$name]))
			$this->update_cache($name);
	
		return $this->cache[$name];
	}
	
	function set_var ($name, $value) {
		if ($retval = $this->update_db ($name, $value))
			$this->cache[$name] = $value;
			
		return $retval;
	}
	
	function update_cache ($name) {
		$obj = $this->mysql->select (sprintf ("SELECT * FROM %s WHERE %s='%s' LIMIT 1",
			$this->table_name, $this->id_name, $name));
			
		$row = get_object_vars ($obj[0]);
		
		$this->cache[$name] = $row[$this->value_name];
	}
	
	function update_db ($name, $value) {
		if ($this->name_exists ($name))
			$query_str = sprintf ("UPDATE %s SET %s='%s' WHERE %s='%s'",
				$this->table_name, $this->value_name, $value, $this->id_name, $name);
		else
			$query_str = sprintf ("INSERT INTO %s (%s, %s) VALUES ('%s', '%s')",
				$this->table_name, $this->id_name, $this->value_name, $name, $value);
			
		return $this->mysql->command ($query_str);
	}
	
	function name_exists ($name) {
		$row = $this->mysql->select_field (sprintf ("SELECT count(*) as name_count FROM %s WHERE %s='%s'",
			$this->table_name, $this->id_name, $name));
		
		return ($row[0] > 0);
	}
}





class DbConfigItem extends ListItem {
   function load_from_row ($row) {
        $this->name = $row->name;
        $this->title = $row->title;
        $this->value = $row->value;
    }

	function load_defaults () {
	}
    
	function get_update_array () {
		return array (
			'name' => $this->name,
			'title' => $this->title,
			'value' => $this->value
		);	
	}
	
	function get_insert_array () {
		return array (
			'name' => $this->name,
			'title' => $this->title,
			'value' => $this->value
		);	
	}

	function load_from_parameters (&$request) {
		$this->name = $request->_post['dbconfig_name'];
		$this->value = $request->_post['dbconfig_value'];
		$this->title = $request->_post['dbconfig_title'];
	}
	
	function set_template_vars (&$tpl, $form = FALSE) {
		$tpl->set_var ('dbconfig_id', $this->id);
		$tpl->set_var ('dbconfig_name', $this->name);
		$tpl->set_var ('dbconfig_title', $this->title);
		$tpl->set_var ('dbconfig_value', $this->value);

        $edit_url = new URL ();
        $edit_url->set_var ('area', 'edit');
        $edit_url->set_var ('dbconfig_id', $this->id);
        $tpl->set_var ('dbconfig_edit', $edit_url->build());

        if ($form) {
		}
	}
}

class DBConfigFactory extends ListFactory {
	function DBConfigFactory (&$config, &$request) {
		$this->ListFactory ($config, $request, 'config', 'id', 'name', 'DbConfigItem', FALSE);
	}
}


?>