<?PHP

include_once ('engine/image.inc');

include_once ('engine/list.inc');
include_once ('engine/common.inc');

class ImageItem extends ListItem {
	var $deleted = FALSE;

	function ImageItem ($id, &$factory) {
		parent::ListItem ($id, $factory);

	    $this->image = new Image ();
	}

    function load_from_row ($row) {
		$this->link_class = $row->link_class;
		$this->link_id = $row->link_id;
        $this->title_ru = $row->title_ru;
        $this->description_ru = $row->description_ru;
        $this->description_br = $row->description_br;
        $this->title_en = $row->title_en;
        $this->big_id = $row->big_id;

		$this->image->unpack_string ($row->image);
    }
    
	function get_update_array () {
		return array (
			'link_class' => $this->link_class,
			'link_id' => $this->link_id,
			'title_ru' => $this->title_ru,
			'description_ru' => $this->description_ru,
			'description_br' => $this->description_br,
			'title_en' => $this->title_en,
			'big_id' => $this->big_id,
			'image' => $this->image->pack_data ()
		);	
	}
	
	function get_insert_array () {
		return array (
			'link_class' => $this->link_class,
			'link_id' => $this->link_id,
			'title_ru' => $this->title_ru,
			'description_ru' => $this->description_ru,
			'description_br' => $this->description_br,
			'title_en' => $this->title_en,
			'big_id' => $this->big_id,
			'image' => $this->image->pack_data ()
		);	
	}
	
	function load_from_parameters (&$request) {
		$this->link_class = $request->_post['image_item_link_class'];
		$this->link_id = $request->_post['image_item_link_id'];
		$this->title_ru = $request->_post['image_item_title_ru'];
		$this->description_ru = $request->_post['image_item_description_ru'];
		$this->description_br = $request->_post['image_item_description_br'];
		$this->title_en = $request->_post['image_item_title_en'];
		$this->big_id = $request->_post['image_item_big_id'];

		$this->image_del = $this->image->proceed_parameters ($request, 'image_item_image', 'image_items', 'image_', $this->id);
	}
	
	function set_template_vars (&$tpl) {
		$tpl->set_var ('image_item_id', $this->id);
		$tpl->set_var ('image_item_big_id', $this->big_id);
		$tpl->set_var ('image_item_link_class', $this->link_class);
		$tpl->set_var ('image_item_link_id', $this->link_id);
		$tpl->set_var ('image_item_title_ru', $this->title_ru);
		$tpl->set_var ('image_item_description_ru', $this->description_ru);
		$tpl->set_var ('image_item_title_en', $this->title_en);

		if ($this->image->image) {
			$tpl->set_var ('image_item_width', $this->image->width);
			$tpl->set_var ('image_item_height', $this->image->height);
		} else {
			$tpl->set_var ('image_item_width', 500);
			$tpl->set_var ('image_item_height', 317);
		}

		$tpl->set_var ('image_item_image_ctl', $this->image->control ('image_item_image', TRUE));
	}

	function control ($description = FALSE, $align = TRUE, $insert = TRUE) {
		global $config;
		
		$tpl = new Template ($config, MY_COMMON_TPL_PATH);
		if ($description)
			$tpl->set_file ('image_item_ctl', 'image_item_description_ctl.html');
		else
			$tpl->set_file ('image_item_ctl', 'image_item_ctl.html');
		$tpl->set_block ('image_item_ctl', 'align_block', 'align_block');

		$this->set_template_vars ($tpl);

		list ($w, $h) = $this->image->find_dims (500, NULL);
		
		$alt = sprintf ('%s (%dx%d, %d ����)',
			$this->image->file, $this->image->width, $this->image->height, $this->image->size);
		$tpl->set_var (
			sprintf ('image', $prefix),
			$this->image->image ? sprintf ('<IMG SRC="%s" WIDTH="%s" HEIGHT="%s" ALT="%s">', 
				$this->image->file, $w, $h, $alt, $alt) : '��� ��������');

		$tpl->set_var ('image_src', $this->image->image);
		$tpl->set_var ('image_width', $this->image->width);
		$tpl->set_var ('image_height', $this->image->height);

		$tpl->set_block ('image_item_ctl', 'button', 'button');

		$tpl->set_var ('image_info', $this->image->image ? $alt : '&nbsp;');
		$tpl->set_var ('prefix', 'image_item');

		$image_id = $this->id ? $this->id : '';
		$tpl->set_var ('image_id', $image_id);

		if ($align) {
			$tpl->set_var ('image_align', $this->image->halign);

			$tpl->set_var ('ha_left', common_radio_button (sprintf ('image_item_halign_%s', $image_id), 'left', $this->image->halign == 'left'));
			$tpl->set_var ('ha_right', common_radio_button (sprintf ('image_item_halign_%s', $image_id), 'right', $this->image->halign == 'right'));

			$tpl->parse ('align_block', 'align_block');
		} else {
			$tpl->set_var ('align_block', '');
		}

		$tpl->set_var ('image_item_description_br_checkbox', common_checkbox (sprintf ("image_item_description_br_%s", $image_id), 1, $this->description_br));

		$tpl->set_var ('image_del', common_checkbox (sprintf ("image_del_%s", $image_id), 'Y', FALSE));
		
//		$tpl->set_var (sprintf ("%s_image_file", $prefix), $this->file);
		$tpl->set_var ('image_file', sprintf ('<INPUT TYPE="hidden" NAME="image_file_%s" VALUE="%s">', $image_id, $this->image->file));
		
		if (! ($image_id && $insert))
			$tpl->set_var ('button', '&nbsp;');
		else
			$tpl->parse ('button', 'button', FALSE);
			
		$tpl->parse ('image_item_ctl', 'image_item_ctl');
		
		return $tpl->get_var ('image_item_ctl');
	}
	
} // end class

class ImageItemFactory extends ListFactory {
	var $link_class;
	var $link_id;

	function ImageItemFactory (&$config, &$request, $link_class, $link_id) {
		parent::ListFactory ($config, $request, 'image_items', 'id', 'id', 'ImageItem', FALSE);
		$this->link_class = $link_class;
		$this->link_id = $link_id;

	} // end func

/*
	function create_all ($limit = NULL, $offset = NULL) {
		$limit_str = $limit ? sprintf (" LIMIT %s%d", $offset ? sprintf ("%d, ", $offset) : '', $limit) : '';

	    $query_str = sprintf ("SELECT * FROM %s WHERE link_class='%s' AND link_id='%d' ORDER BY %s ASC%s",
			$this->table_name, $this->link_class, $this->link_id, $this->index_name, $limit_str);
		return $this->create_from_query ($query_str);
	} // end func

	function get_total_count () {
		$row = $this->mysql->select_field (sprintf ("SELECT count(*) AS total_count FROM %s WHERE link_class='%s' AND link_id='%d'", $this->table_name, $this->link_class, $this->link_id));
	
		return $row[0];
	}
*/

	function conditions () {
		parent::conditions ();

		if ($this->link_class) $this->cond[] = sprintf ("link_class='%s'", $this->link_class);
		if ($this->link_id) $this->cond[] = sprintf ("link_id='%d'", $this->link_id);
	}
	
	function delete_old_images () {
		$row = $this->mysql->select (sprintf ("SELECT image FROM %s", $this->table_name));

	    $image = new Image ();
		$images = array ();

		$dir = sprintf ('%s/%s/image_items', MY_REL_ROOT_PATH, MY_FILES_FOLDER);
		if ($dh = opendir ($dir)) {
			while (($file = readdir ($dh)) !== FALSE) {
				if (preg_match ('/^\./', $file))
					continue;

				$images[$file] = $dir . '/' . $file;
			}
			closedir ($dh);
		}

		if (sizeof ($row) > 0)
		foreach ($row as $i => $item) {
			$image->unpack_string ($item->image);

			if ($image->image)
				if (getArrayValue (basename ($image->image), $images)) 
					unset ($images[basename ($image->image)]);
		}

		$res = 0;
		foreach ($images as $file => $filepath) {
			// print $filepath . "<br>";
			if (file_exists ($filepath)) {
				unlink ($filepath);
				$res++;
			}
		}

		return $res;
	}

	function proceed_images (&$request) {
		$image_id_list = $request->_post['image_id'];

		if ($image_id_list)
			for ($i = 0; $i < count ($image_id_list); $i++) {
			    $image_item = new ImageItem ($image_id_list[$i], $this);

//echo ($image_id_list[$i]);
				$image_item->link_class = $this->link_class;
				$image_item->link_id = $this->link_id;
//echo $x->link_class;
				$image_id = $image_item->id ? $image_item->id : '';

				$image_item->title_ru = $request->_post[sprintf('image_item_title_ru_%s', $image_id)];
				$image_item->title_en = $request->_post[sprintf('image_item_title_en_%s', $image_id)];
				$image_item->description_ru = $request->_post[sprintf('image_item_description_ru_%s', $image_id)];
				$image_item->description_br = $request->_post[sprintf('image_item_description_br_%s', $image_id)];
				$image_item->image->halign = $request->_post[sprintf ('image_item_halign_%s', $image_id)];

				$file_info_param = sprintf('image_item_image_%s', $image_id);

				$db_image = NULL;
			    $db_image = $this->create ($image_id);

				$db_image->image->halign = $image_item->image->halign;
				$db_image->title_ru = $image_item->title_ru;
				$db_image->description_ru = $image_item->description_ru;
				$db_image->description_br = $image_item->description_br;
				$db_image->title_en = $image_item->title_en;

				if (is_uploaded_file ($tmp_image = $request->_post_files[$file_info_param]['tmp_name'])) {
					$file_info = explode ('.', basename ($request->_post_files[$file_info_param]['name']));
					$ext = $file_info[count ($file_info) - 1];
					
					if ($db_image->image->file && file_exists ($db_image->image->file))
						unlink ($db_image->image->file);

					if ($image_id)
						$image_item->image->image = sprintf ('%s/image_items/%s.%s', MY_FILES_FOLDER, sprintf ('image_%s_%s',
							$image_item->link_class, $image_id), $ext);

					$image_item->factory->clear_cache (array ($image_item->id));
					$image_item->factory->to_cache ($image_item->id, $image_item);

					$image_item->update ();

					if (!$image_id) {
						$image_id = $image_item->factory->mysql->insert_id ();
						$image_item->id = $image_id;

						$image_item->image->image = sprintf ('%s/image_items/%s.%s', MY_FILES_FOLDER, sprintf ('image_%s_%s',
							$image_item->link_class, $image_id), $ext);

						$image_item->factory->clear_cache (array ($image_id));
						$image_item->factory->to_cache ($image_id, $image_item);

						$image_item->update ();
					}

					$to_file = sprintf ('%s/%s', MY_REL_ROOT_PATH, $image_item->image->image);
					move_uploaded_file ($tmp_image, $to_file);
					if ($request->config->chmod_available)
						chmod ($to_file, $request->config->chmod_upload);
					#if (CHMOD_AVAILABLE)
					#	chmod ($to_file, CHMOD_UPLOAD);
            

				} elseif ($db_image->image->image)
						$db_image->update ();
				
				if ($retval = ($request->_post[sprintf ('image_del_%s', $image_id)] == true)) {
					$this->image = '';
					$del_file = $request->_post[sprintf ('image_file_%s', $image_id)];
					if (file_exists ($del_file)) 
						unlink ($del_file);
					$db_image->delete ();
				}

//				$tpl->set_var ('image', $image_item->file);

			}
	}

} // end class


?>