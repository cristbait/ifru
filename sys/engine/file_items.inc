<?PHP

	function proceed_files (&$request, &$file_factory) {
		$file_id_list = $request->_post['file_id'];

		if ($file_id_list)
			for ($i = 0; $i < count ($file_id_list); $i++) {
			    $file_item = new FileItem ($file_id_list[$i], $file_factory);

//echo ($file_id_list[$i]);
				$file_item->link_class = $file_factory->link_class;
				$file_item->link_id = $file_factory->link_id;
//echo $x->link_class;
				$file_id = $file_item->id ? $file_item->id : '';

				$file_item->title_ru = $request->_post[sprintf('file_item_title_ru_%s', $file_id)];
				$file_item->title_en = $request->_post[sprintf('file_item_title_en_%s', $file_id)];

				$file_info_param = sprintf('file_item_file_%s', $file_id);

				$db_image = NULL;
			    $db_image = $file_factory->create ($file_id);

				$db_image->title_ru = $file_item->title_ru;
				$db_image->title_en = $file_item->title_en;

				if (is_uploaded_file ($tmp_image = $request->_post_files[$file_info_param]['tmp_name'])) {
					$file_info = explode ('.', basename ($request->_post_files[$file_info_param]['name']));
					$ext = $file_info[count ($file_info) - 1];
					
					if ($db_image->file->file && file_exists ($db_image->file->file))
						unlink ($db_image->file->file);

					if ($file_id)
						$file_item->file->file = sprintf ('%s/file_items/%s.%s', MY_FILES_FOLDER, sprintf ('file_%s_%s',
							$file_item->link_class, $file_id), $ext);

					$file_item->factory->clear_cache (array ($file_item->id));
					$file_item->factory->to_cache ($file_item->id, $file_item);

					$file_item->update ();

					if (!$file_id) {
						$file_id = $file_item->factory->mysql->insert_id ();
						$file_item->id = $file_id;

						$file_item->file->file = sprintf ('%s/file_items/%s.%s', MY_FILES_FOLDER, sprintf ('file_%s_%s',
							$file_item->link_class, $file_id), $ext);

						$file_item->factory->clear_cache (array ($file_id));
						$file_item->factory->to_cache ($file_id, $file_item);

						$file_item->update ();
					}

					move_uploaded_file ($tmp_image, sprintf ('%s/%s', MY_REL_ROOT_PATH, $file_item->file->file));
				} elseif ($db_image->file->file)
						$db_image->update ();
				
				if ($retval = ($request->_post[sprintf ('file_del_%s', $file_id)] == true)) {
					$this->file = '';
					$del_file = $request->_post[sprintf ('file_file_%s', $file_id)];
					if (file_exists ($del_file)) 
						unlink ($del_file);
					$db_image->delete ();
				}

//				$tpl->set_var ('image', $file_item->file);

			}
	    
	} // end func


	function common_file_item_control (&$file_item) {
		global $config;
		
		$tpl = new Template ($config, MY_COMMON_TPL_PATH);
		$tpl->set_file ('file_item_ctl', 'file_item_ctl.html');

		$file_item->set_template_vars ($tpl);
		
		$alt = sprintf ('%s (%d ����)',	$file_item->file->file, $file_item->file->size);

		$tpl->set_var (
			sprintf ('file', $prefix),
			$file_item->file->file ? sprintf ('<a href="%s" ALT="%s" TITLE="%s">%s</a>', 
				$file_item->file->real_file, $alt, $alt, $alt ? $alt : "������ �� ����") : '��� �����');

		$tpl->set_var ('file_src', $file_item->file->file);

		$tpl->set_block ('file_item_ctl', 'button', 'button');

		$tpl->set_var ('file_info', $file_item->file->file ? $alt : '&nbsp;');
		$tpl->set_var ('prefix', 'file_item');

		$file_id = $file_item->id ? $file_item->id : '';
		$tpl->set_var ('file_id', $file_id);

		$tpl->set_var ('file_del', common_checkbox (sprintf ("file_del_%s", $file_id), 'Y', FALSE));
		
//		$tpl->set_var (sprintf ("%s_file_file", $prefix), $this->file);
		$tpl->set_var ('file_file', sprintf ('<INPUT TYPE="hidden" NAME="file_file_%s" VALUE="%s">', $file_id, $file_item->file->file));
		
		if (!$file_id)
			$tpl->set_var ('button', '&nbsp;');
		else
			$tpl->parse ('button', 'button', FALSE);
			

		$tpl->parse ('file_item_ctl', 'file_item_ctl');
		
		return $tpl->get_var ('file_item_ctl');
	}

?>