<?PHP

class Image {
	var $separator;
	var $halign;
	var $valign;
	var $image;
	var $file;
	var $width;
	var $height;
	var $size;
	var $alt;

	var $jpeg_quality = 75;

	var $just_uploaded = FALSE;
	
	function Image ($separator = ';') {
		$this->separator = $separator;
		$this->halign = 'left';
		$this->valign = 'top';
		$this->alt = '';
	}
	
	function unpack_string ($str) {
		$arr = explode ($this->separator, $str, 4);
		
		if (is_array ($arr)) {
			$retval['halign'] = ($this->halign = ($arr[0] ? $arr[0] : 'left'));
			$retval['valign'] = ($this->valign = ($arr[1] ? $arr[1] : 'top'));
			$retval['image'] = ($this->image = $arr[2]);
			if (sizeof ($arr) > 3)
				$retval['alt'] = ($this->alt = $arr[3]);
		} else {
			$retval['halign'] = 'left';
			$retval['halign'] = 'top';
			$retval['image'] = NULL;
			$retval['alt'] = '';
		}
		
		$this->update ();
		
		return $retval;
	}

	function load_image ($name) {
		$this->image = $name;

		$this->update ();
	}
	
	function update () {
		if ($this->image) {
			$this->file = sprintf ('%s/%s', MY_REL_ROOT_PATH, $this->image);
		
			if (! file_exists ($this->file) || filesize ($this->file) == 0) {
			// 	! preg_match ("/^image/i", mime_content_type ($this->file))) {
				$this->image = null;
				return;
			}

			$size = @getimagesize ($this->file);
		
			$this->width = $size[0];
			$this->height = $size[1];
			$this->size = filesize ($this->file);
		}
	}
	
	function pack_data () {
		if ($this->image)
			return sprintf ("%s%s%s%s%s%s%s", 
				$this->halign,
				$this->separator,
				$this->valign,
				$this->separator,
				$this->image,
				$this->separator,
				$this->alt);

		return '';
	}

	
	function proceed_parameters (&$request, $prefix, $path, $name_prefix, $name) {
		$this->halign = $request->_post[sprintf ('%s_halign', $prefix)];
		$this->valign = $request->_post[sprintf ('%s_valign', $prefix)];
		$this->alt = $request->_post[sprintf ('%s_alt', $prefix)];
		
		if (is_uploaded_file ($tmp_image = $request->_post_files[sprintf ('%s_image', $prefix)]['tmp_name'])) {
			$file_info = explode ('.', basename ($request->_post_files[sprintf ('%s_image', $prefix)]['name']));
			$ext = $file_info[count ($file_info) - 1];
			
			if ($this->image) {
				// ���������� ������� ����� ����� (������ ��� ��������)
				unlink ($this->file);
			} else {
				$this->image = sprintf ('%s/%s/%s.%s', MY_FILES_FOLDER, $path, sprintf ('%s%s', $name_prefix, $name ? $name : md5 (uniqid (rand ()))), $ext);
			}

			$to_file = sprintf ('%s/%s', MY_REL_ROOT_PATH, $this->image);
			move_uploaded_file ($tmp_image, $to_file);

			if ($request->config->chmod_available)
				chmod ($to_file, $request->config->chmod_upload);

			$this->update ();
			$this->just_uploaded = TRUE;
		}
		
		if ($retval = ($request->_post[sprintf ('%s_image_del', $prefix)] == true)) {
			$this->image = '';
			$del_file = $request->_post[sprintf ('%s_image_file', $prefix)];
			if (file_exists ($del_file))
				unlink ($del_file);

			$this->update ();
			$this->just_uploaded = TRUE;
		}
	
		return $retval;
	}
	
	function get_img_tag ($width = 0, $height = 0, $align = NULL, $border = 0, $root = NULL, $alt = '', $additional = '') {
		if (empty ($this->image))
			return "";

		$w_rate = $width && $width < $this->width ? $width / $this->width : 1;
		$h_rate = $height && $height < $this->height ? $height / $this->height : 1;

		$w = round (min ($w_rate, $h_rate) * $this->width);
		$h = round (min ($w_rate, $h_rate) * $this->height);

		if ($align === TRUE)
			$align = $this->halign;
//		print $width . "x" . $height . "<br>";

		return sprintf ('<IMG SRC="%s%s" BORDER="%s" WIDTH="%s" HEIGHT="%s" ALT="%s"%s%s>',
			$root ? (preg_match ('#/$#', $root) ? $root : $root.'/') : '',
			$this->image,
			$border,
			$w,
			$h, 
//			 htmlspecialchars ($alt ? $alt : $this->alt),
			$alt ? $alt : $this->alt,
			$align ? sprintf (' ALIGN="%s"', $align) : '',
			$additional ? " ".$additional : '');
	}

	function get_swf_tag ($width = 0, $height = 0, $root = NULL) {
		$image = ($root ? $root.'/' : '') . $this->image;

		return sprintf ('<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="468" height="60">' .
			'<param name="movie" value="%s">' .
			'<param name="quality" value="high">' .
			'<embed src="%s" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="%d" height="%d"></embed></object>',
			$image, $image, $width ,$height);
	}

	function getExt () {
		return $this->getExtFromUrl ( $this->image );
	}

	function getExtFromUrl ( $url ) {
		return strtolower (substr (strrchr( $url, '.' ), 1));
	}

	function copy_resized (&$src, $max_w, $max_h, $path, $name_prefix, $name, $resize_only = FALSE) {
		$ext = $src->getExt ();

		if (file_exists ($this->file))
			unlink ($this->file);

		if (file_exists ($src->file)) {
			$this->image = sprintf ('%s/%s.%s', $path, sprintf ('%s%s', $name_prefix, $name ? $name : md5 (uniqid (rand ()))), $ext);
			$this->file = sprintf ('%s/%s', MY_REL_ROOT_PATH, $this->image);

			copy ($src->file, $this->file);

			if ($resize_only)
				$res = $this->_resizeImage ( $max_w, $max_h );
			else
				$res = $this->_resizeAndCutImage ( $max_w, $max_h );
		} else {
			$this->image = NULL;
			$res = TRUE;
		}

		$this->update ();

		return $res;
	}

	function _resizeImage ( $max_w = NULL, $max_h = NULL ) {
		$ext = $this->getExt ();

		if ($im = $this->_createImage ()) {
			$w = imagesx ($im);
			$h = imagesy ($im);
			
			$rate_w = $max_w && $w && $max_w < $w ? $max_w / $w : 1;
			$rate_h = $max_h && $h && $max_h < $h ? $max_h / $h : 1;
			$rate = min ($rate_h, $rate_w);

			if ($rate == 1)
				return;

			$x = round ($rate * $w);
			$y = round ($rate * $h);

			if ($ext == 'gif')
				$dst_im = imagecreate ($x, $y);
			else
				$dst_im = imagecreatetruecolor ($x, $y);

			$result = FALSE;

			if (function_exists ('imagecopyresampled')) 
				$result = @imagecopyresampled ( $dst_im, $im, 0, 0, 0, 0, $x, $y, $w, $h);
			if (! $result && function_exists ('imagecopyresized'))
				$result = @imagecopyresized ( $dst_im, $im, 0, 0, 0, 0, $x, $y, $w, $h);

			if ($result)
				$this->_stroreImage ( $dst_im );
		}

		return $result;
	}

	function _resizeAndCutImage ( $max_w = NULL, $max_h = NULL ) {
		$ext = $this->getExt ();
		$result = FALSE;

		if ($im = $this->_createImage ()) {
			$w = imagesx ($im);
			$h = imagesy ($im);
			
			$rate_w = $max_w && $w && $max_w < $w ? $max_w / $w : 1;
			$rate_h = $max_h && $h && $max_h < $h ? $max_h / $h : 1;
			$rate = min ($rate_h, $rate_w);

			if ($rate == 1)
				return TRUE;

			$x = $max_w;
			$y = $max_h;

			if ($rate_w < $rate_h) { // ����� �� h ������� => ����� ������
				$new_w = $max_w / $rate_h;
				$start_x = round (($w - $new_w) / 2);
				$start_y = 0;
				$new_h = $h;
			} elseif ($rate_w > $rate_h) { // ����� �� w ������� => ����� ������
				$new_h = $max_h / $rate_w;
				$start_y = round (($h - $new_h) / 2);
				$start_x = 0;
				$new_w = $w;
			} else {
				$start_x = $start_y = 0;
				$new_w = $w;
				$new_h = $h;
			}

			if ($ext == 'gif')
				$dst_im = imagecreate ($x, $y);
			else
				$dst_im = imagecreatetruecolor ($x, $y);

			$result = FALSE;

			if (function_exists ('imagecopyresampled')) 
				$result = @imagecopyresampled ( $dst_im, $im, 0, 0, $start_x, $start_y, $x, $y, $new_w, $new_h);
			if (! $result && function_exists ('imagecopyresized'))
				$result = @imagecopyresized ( $dst_im, $im, 0, 0, $start_x, $start_y, $x, $y, $new_w, $new_h);

			if ($result)
				$this->_stroreImage ( $dst_im );
			else
				return FALSE;
		}

		return $result;
	}

	function &_createImage () {
		$ext = $this->getExt ();

		switch ($ext) {
			case 'jpg':
			case 'jpeg':
				if (imagetypes() & IMG_JPG)
					$im = @imagecreatefromjpeg ($this->file);
				break;
			case 'gif':
				if (imagetypes() & IMG_GIF)
					$im = @imagecreatefromgif ($this->file);
				break;
			case 'png':
				if (imagetypes() & IMG_PNG)
					$im = @imagecreatefrompng ($this->file);
				break;
			case 'wbmp':
				if (imagetypes() & IMG_WBMP)
					$im = @imagecreatefromwbmp ($this->file);
				break;
			default:
				$im = NULL;
		}

		return $im;
	}

	function _stroreImage ( $im ) {
		$ext = $this->getExt ();

		switch ($ext) {
			case 'jpg':
			case 'jpeg':
				if (imagetypes() & IMG_JPG)
					imagejpeg ($im, $this->file, $this->jpeg_quality);
				break;
			case 'gif':
				if (imagetypes() & IMG_GIF)
					imagegif ($im, $this->file);
					// imagejpeg ($im, $this->file);
				break;
			case 'png':
				if (imagetypes() & IMG_PNG)
					imagepng ($im, $this->file);
				break;
			case 'wbmp':
				if (imagetypes() & IMG_WBMP)
					imagewbmp ($im, $this->file);
				break;
		}
	}

	function find_dims ($width = NULL, $height = NULL) {
		$w_rate = $width && $width <  $this->width ? $width / $this->width : 1;
		$h_rate = $height && $height < $this->height ? $height / $this->height : 1;
		$w = round (min ($w_rate, $h_rate) * $this->width);
		$h = round (min ($w_rate, $h_rate) * $this->height);

		return array ($w, $h);
	}


	function control ($prefix, $align = TRUE, $text = TRUE) {
		global $config;
		
		$tpl = new Template ($config, MY_COMMON_TPL_PATH);
		$tpl->set_file ('image_ctl', 'image_ctl.html');
		
		$alt = sprintf ('%s (%dx%d, %d ����)', $this->file, $this->width, $this->height, $this->size);

		list ($w, $h) = $this->find_dims (500, NULL);
		
		$tpl->set_var (
			sprintf ('image', $prefix),
			$this->image ? sprintf ('<IMG SRC="%s" WIDTH="%s" HEIGHT="%s" ALT="%s">', 
				$this->file, $w, $h, $alt, $alt) : '��� ��������');
				
		$tpl->set_var ('image_info', $this->image ? $alt : '&nbsp;');
		$tpl->set_var ('prefix', $prefix);

		$tpl->set_var ('alt', $this->alt);
			
		$tpl->set_var ('ha_left', common_radio_button (sprintf ('%s_halign', $prefix), 'left', $this->halign == 'left'));
		$tpl->set_var ('ha_center', common_radio_button (sprintf ('%s_halign', $prefix), 'center', $this->halign == 'center'));
		$tpl->set_var ('ha_right', common_radio_button (sprintf ('%s_halign', $prefix), 'right', $this->halign == 'right'));
		
		$tpl->set_var ('va_top', common_radio_button (sprintf ('%s_valign', $prefix), 'top', $this->valign == 'top'));
		$tpl->set_var ('va_middle', common_radio_button (sprintf ('%s_valign', $prefix), 'middle', $this->valign == 'middle'));
		$tpl->set_var ('va_bottom', common_radio_button (sprintf ('%s_valign', $prefix), 'bottom', $this->valign == 'bottom'));
		
		$tpl->set_var ('image_del', common_checkbox (sprintf ("%s_image_del", $prefix), 'Y', false));
		
		$tpl->set_var (sprintf ("%s_image_file", $prefix), $this->file);
		$tpl->set_var ('image_file', sprintf ('<INPUT TYPE="hidden" NAME="%s_image_file" VALUE="%s">', $prefix, $this->file));
		
		$tpl->set_block ('image_ctl', 'align', 'align');
		if ($align)
			$tpl->parse ('align', 'align');
		else
			$tpl->set_var ('align', '');
	
		$tpl->set_block ('image_ctl', 'text', 'text');
		if ($text)
			$tpl->parse ('text', 'text');
		else
			$tpl->set_var ('text', '');
		
		$tpl->parse ('image_ctl', 'image_ctl');
		
		return $tpl->get_var ('image_ctl');
	}

}


?>