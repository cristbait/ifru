<?PHP

	include_once ('engine/template.inc');
	include_once ('engine/url.inc');

// ******* COMMON TIME FUNCTIONS *******

	define ("BASE_TIME", getCurrentTime ());
	define ("TIME_LIMIT", 25);

	function getCurrentTime () {
		list ($msec, $sec)  = explode (' ', microtime ());

		return ((float) $sec + (float) $msec);
	}

	function executionTime () {
		   return sprintf ("%.4f", getCurrentTime () - BASE_TIME);
	}

	function number_to_roman ($value) {
		if($value<0) return "";
		if(!$value) return "0";

		$thousands=(int)($value/1000);
		$value-=$thousands*1000;
		$result=str_repeat("M",$thousands);

		$table=array(
			900=>"CM",500=>"D",400=>"CD",100=>"C",
			90=>"XC",50=>"L",40=>"XL",10=>"X",
			9=>"IX",5=>"V",4=>"IV",1=>"I"
		);

		while ($value) {
			foreach($table as $part=>$fragment) if($part<=$value) break;
			$amount=(int)($value/$part);
			$value-=$part*$amount;
			$result.=str_repeat($fragment,$amount);
		}

		return $result;
	}
// ******* COMMON ARRAY FUNCTIONS *******

	function &getArrayValue ($key, &$array) {
		if (!is_array ($array))
			return $result = NULL;

		if (is_array ($key)) {
			if (sizeof ($key) == 0)
				return $result = NULL;
			else {
				$otherKeys = $key;
				$firstKey = array_shift ($otherKeys);
				$result = getArrayValue ($firstKey, $array);

				if (sizeof ($otherKeys) > 0) 
					return getArrayValue ($otherKeys, $result);
				else
					return $result;
			}
		} elseif ((is_scalar ($key) || $key === NULL) && array_key_exists ($key, $array))
			return $array[$key];

		return NULL;
	}

// ******* COMMON STRING FUNCTIONS *******

	function to_upper ($t) {
		$t = strtoupper ($t);
		$t = strtr ($t, '��������������������������������', '�����Ũ��������������������������');
		return $t;
	}
 
	function to_lower ($t) {
		$t = strtolower ($t);
		$t = strtr ($t, '�����Ũ��������������������������', '��������������������������������');
		return $t;
	}

	function first_to_upper ($text) {
		$text = to_lower ($text);

		if (preg_match ('/^([�-�a-z])/', $text, $regs)) {
			$text = substr_replace ($text, to_upper ($regs[1]), 0, 1);
		}

		return $text;
	}

	function e_fix ($field) {
		return convert_cyr_string (stripslashes (trim ($field)), "w", "k");
	}

	function str2hex ($str) {
		for ($i = 0; $i < strlen ($str); $i++) {
		    $retval .= sprintf ('%02x', ord ($str[$i]));
		}

	    return $retval;
	}

	function translit ($str) {
		for ($i = 0; $i < strlen ($str); $i++) {
			$char = $str[$i];

			if ((ord($char) >= ord('�')) && (ord($char) <= '�') || (ord($char) >= ord('�')) && (ord($char) <= '�'))
				$char = strt2hex (ord ($char) + (ord ('�') - ord ($char)));
			
			$retval .= $char;
		}

		return $retval;
	}

	function shorten_words ($str, $n = 10) {
		$str = preg_replace ('/([�-��-�]{'.$n.'})/', '\\1 ', $str);
		return $str;
	}

// ******* COMMON FUNCTIONS *******

    function datef ($format, $time) {
		return date ($format, strtotime ($time));
	}

	function email_valid ( $email ) {
		return preg_match ("/^[a-z0-9._&-]+@[a-z0-9&_]+([&_.-][a-z0-9&_]+)*$/i" , $email);
	}


	function gen_pass ($count = 8) {
		$chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

		$plpasswd = "";
		srand( (double)microtime()*1000000 );

		for ($i = 0; $i < $count; $i++) { 
			$plpasswd .= $chars[rand (0, 61)];
 		}

		return $plpasswd;
	}


// ******* COMMON FORM FUNCTIONS *******

	function another_color ($color = NULL) {
		switch (strtoupper ($color)) {
			case '#C0C0C0': return '#EEEEEE';
			case '#EEEEEE': return '#C0C0C0';
			default: return '#C0C0C0';
		}
	}
	
	function another_color2 ($color = NULL) {
		switch (strtoupper ($color)) {
			case '#D8D8D8': return '#EEEEEE';
			case '#EEEEEE': return '#D8D8D8';
			default: return '#D8D8D8';
		}
	}
	
	function common_pager ($pager) {
		global $config;

		$tpl = new Template ($config, MY_COMMON_TPL_PATH);
		
		$tpl->set_file ('pager', 'pager.html');
	
		$tpl->set_block ('pager', 'pager_page', 'pages');
		$tpl->set_block ('pager', 'pager_current_page', '');
	
		$tpl->set_var ('pages', '');
		for ($i = 0; $i < $pager->page_count; $i++) {
			$page = $pager->get_page ($i);
		
			$tpl->set_var ('href', $page->href);
			$tpl->set_var ('first_row', $page->first_row);
			$tpl->set_var ('last_row', $page->last_row);
		
			$tpl->parse ('pages', ($page->is_current ()) ? 'pager_current_page' : 'pager_page', true);
		}
	
		$tpl->set_var ('total', $pager->total_size);
	
		$tpl->parse ('pager', 'pager', false);
	
		return ($pager->page_count > 1) ? $tpl->get_var ('pager') : '';
	}

	function common_formatting () {
		global $config;
		
		$tpl = new Template ($config, MY_COMMON_TPL_PATH);
		
		$tpl->set_file ('formatting', 'formatting.html');
		$tpl->parse ('formatting', 'formatting', false);
	
		return $tpl->get_var ('formatting');
	}

	function common_style_control ($name = "cssselect") {
		global $config, $language, $mysql_connection;

	    $tpl = new Template ($config, MY_COMMON_TPL_PATH);
		include_once ('mod/css.inc');

		$factory = new CSSFactory ($config, $request);
	    $list = $factory->create_all ();

	    $lst = array ();
		if ($list) {
			for ($i = 0; $i < sizeof ($list); $i++) {
				$lst[$factory->prefix . $list[$i]->name] = $list[$i]->name;
			}
		}

		$tpl->set_file ('css_control', 'style_ctl.html');
		$tpl->set_var ('style_select_button', common_select_button ($name, $lst, "a"));
		$tpl->set_var ('css_select_name', $name);

		$tpl->parse ('css_control', 'css_control');

		return $tpl->get_var ('css_control');
	}

	function common_checkbox ($name, $value, $checked = false, $addon = "") {
		return sprintf ('<INPUT TYPE="checkbox" NAME="%s" VALUE="%s"%s%s>', $name, $value, $checked ? " CHECKED" : "", $addon);
	}
	
	function common_list_buttons ($script, $edit_script, $id_name, $movable) {
		global $config;
		
		$create_script = &$edit_script;

		$tpl = new Template ($config, MY_COMMON_TPL_PATH);
		$tpl->set_file ('list_buttons', 'list_buttons.html');
	
		$tpl->set_var ('script', $script);
		$tpl->set_var ('create_script', $create_script);

		$tpl->set_block ('list_buttons', 'create_button', 'create_button');
		if ($create_script) {
		    $tpl->parse ('create_button', 'create_button', FALSE);
		} else
			$tpl->set_var ('create_button', '');

		$tpl->set_var ('edit_script', $edit_script);
		$tpl->set_var ('id_name', $id_name);
		$tpl->set_var ('sub_id_name', $id_name);
		$tpl->set_var ('sub_id', $id_name);
	
		$tpl->set_block ('list_buttons', 'move_buttons', 'move_buttons');
		if (! $movable)
			$tpl->set_var ('move_buttons', '');
		else
			$tpl->parse ('move_buttons', 'move_buttons');
	
		$tpl->parse ('list_buttons', 'list_buttons');
	
		return $tpl->get_var ('list_buttons');
	}

	function common_tree_list_buttons ($script, $edit_script, $id_name, $movable) {
		global $config;
		
		$create_script = &$edit_script;

		$tpl = new Template ($config, MY_COMMON_TPL_PATH);
		$tpl->set_file ('list_buttons', 'tree_list_buttons.html');
	
		$tpl->set_var ('script', $script);
		$tpl->set_var ('create_script', $create_script);

		$tpl->set_block ('list_buttons', 'create_button', 'create_button');
		if ($create_script) {
		    $tpl->parse ('create_button', 'create_button', FALSE);
		} else
			$tpl->set_var ('create_button', '');

		$tpl->set_var ('edit_script', $edit_script);
		$tpl->set_var ('id_name', $id_name);
		$tpl->set_var ('sub_id_name', $id_name);
		$tpl->set_var ('sub_id', $id_name);
	
		$tpl->set_block ('list_buttons', 'move_buttons', 'move_buttons');
		if (! $movable)
			$tpl->set_var ('move_buttons', '');
		else
			$tpl->parse ('move_buttons', 'move_buttons');
	
		$tpl->parse ('list_buttons', 'list_buttons');
	
		return $tpl->get_var ('list_buttons');
	}


	function common_list_item_buttons ($list_script, $edit_script, $id_name, $id) {
    global $config;

    $tpl = new Template ($config, MY_COMMON_TPL_PATH);
    $tpl->set_file ('list_item_buttons', 'list_item_buttons.html');

    $edit = new URL ($edit_script);
    $edit->set_var ($id_name, $id);

    $tpl->set_var ('script', $list_script);
    $tpl->set_var ('edit_script', $edit->build ());
    $tpl->set_var ('id_name', $id_name);
    $tpl->set_var ('id', $id);

    $tpl->parse ('list_item_buttons', 'list_item_buttons');

    return $tpl->get_var ('list_item_buttons');
    }

    function common_stat_item_buttons ($stat_script, $edit_script, $id_name, $id) {
        global $config;

        $tpl = new Template ($config, MY_COMMON_TPL_PATH);
        $tpl->set_file ('stat_item_buttons', 'stat_item_buttons.html');

        $edit = new URL ($edit_script);
        $edit->set_var ($id_name, $id);

        $tpl->set_var ('script', $stat_script);
        $tpl->set_var ('edit_script', $edit->build ());
        $tpl->set_var ('id_name', $id_name);
        $tpl->set_var ('id', $id);

        $tpl->parse ('stat_item_buttons', 'stat_item_buttons');

        return $tpl->get_var ('stat_item_buttons');
    }


function common_redirect (&$tpl, &$config, $title, $text, $link_name, $href, $result = TRUE, $forceshow = FALSE) {
		if ($result && ! $forceshow)
			header ('Location: ' . $href);

		$tpl->set_file ('redirect', 'redirect.html');
	
		$tpl->set_var ('timeout', $result ? $config->redirect_timeout : '1000');
		$tpl->set_var ('title', $title);
		$tpl->set_var ('text', $text);
		$tpl->set_var ('link_name', $link_name);

//		$h = new URL ($href);
//		$h->set_var ('PHPSESSID', session_id ());
//		$tpl->set_var ('href_meta', $h->get_url ());

		$tpl->set_var ('href_meta', $href);
		$tpl->set_var ('href', $href);
	
		$tpl->parse ('redirect', 'redirect');
	}
	
	function common_edit_buttons ($cancel, $insert = true, $sub_id_name = NULL, $sub_id = NULL) {
		global $config;
		
		$tpl = new Template ($config, MY_COMMON_TPL_PATH);
		$tpl->set_file ('edit_buttons', 'edit_buttons.html');
	
		$tpl->set_block ('edit_buttons', 'insert_button', 'insert_button');
		$tpl->set_block ('edit_buttons', 'cancel_button', 'cancel_button');
		$tpl->set_block ('edit_buttons', 'reset_button', 'reset_button');
		$tpl->set_block ('edit_buttons', 'delete_button', 'delete_button');
	
		if (!$insert)
			$tpl->set_var ('insert_button', '');
	
		$url = new URL ($cancel, $error);
	
		if ($sub_id_name)
			$url->set_var ($sub_id_name, $sub_id);
	
		if (strlen ($cancel) > 0) {
			$tpl->set_var ('list_url', $url->get_url ());
			$tpl->set_var ('cancel', $url->get_url ());
			$tpl->set_var ('reset_button', '');
			$tpl->parse ('cancel_button', 'cancel_button');
			$tpl->parse ('delete_button', 'delete_button');
		} else {
			$tpl->set_var ('delete_button', '');
			$tpl->set_var ('cancel_button', '');
		}
	
		$tpl->parse ('edit_buttons', 'edit_buttons');
	
		return $tpl->get_var ('edit_buttons');
	}

	function common_edit_buttons_row ($cancel, $insert = true, $sub_id_name = NULL, $sub_id = NULL) {
		global $config;
		
		$tpl = new Template ($config, MY_COMMON_TPL_PATH);
		$tpl->set_file ('edit_buttons', 'edit_buttons_row.html');
	
		$tpl->set_block ('edit_buttons', 'insert_button', 'insert_button');
		$tpl->set_block ('edit_buttons', 'cancel_button', 'cancel_button');
		$tpl->set_block ('edit_buttons', 'reset_button', 'reset_button');
		$tpl->set_block ('edit_buttons', 'delete_button', 'delete_button');
	
		if (!$insert)
			$tpl->set_var ('insert_button', '');
	
		$url = new URL ($cancel, $error);
	
		if ($sub_id_name)
			$url->set_var ($sub_id_name, $sub_id);
	
		if (strlen ($cancel) > 0) {
			$tpl->set_var ('list_url', $url->get_url ());
			$tpl->set_var ('cancel', $url->get_url ());
			$tpl->set_var ('reset_button', '');
			$tpl->parse ('cancel_button', 'cancel_button');
			$tpl->parse ('delete_button', 'delete_button');
		} else {
			$tpl->set_var ('delete_button', '');
			$tpl->set_var ('cancel_button', '');
		}
	
		$tpl->parse ('edit_buttons', 'edit_buttons');
	
		return $tpl->get_var ('edit_buttons');
	}

	function common_radio_button ($name, $value, $checked, $additional = "") {
		return sprintf ('<INPUT TYPE="radio" NAME="%s" VALUE="%s"%s%s>'."\n", $name, $value, $checked ? ' CHECKED' : '', $additional ? " " . $additional : "");
	}

	function common_option_button ($value, $name, $selected) {
		return sprintf ('<OPTION VALUE="%s"%s>%s</OPTION>'."\n", $value, $selected ? ' SELECTED' : '', $name);
	}

	function common_select_button ($name, $values, $value, $class="", $onchange="", $style="", $addon="") {
	    $result = sprintf ('<SELECT NAME="%s"%s%s%s%s>'."\n", $name, $class ? ' class="' . $class . '"' : "",  $onchange ? ' onchange="' . $onchange . '"' : "", $style ? ' style="' . $style . '"' : "", $addon);
		foreach ($values as $k => $v) {
			$result .= common_option_button ($k, $v, $k == $value);
		}
		$result .= "</SELECT>\n";
		return $result;
	} 

    function common_date_select ($prefix, $date, $y_s = NULL, $y_e = NULL, $curdate = TRUE) {
		if (ereg ("^([0-9]{2})([0-9]{2})-?([0-9]{2})-?([0-9]{2})", $date, $regs)) {
			$year = $regs[1] * 100 + $regs[2];
			$month = $regs[3];
			$day = $regs[4];
		} else {
			$year = $curdate ? date ('Y') : '';
			$month = $curdate ? date ('m') : '';
			$day = $curdate ? date ('d') : '';
		}

		if (! $y_s) $y_s = date ('Y') - 1;
		if (! $y_e) $y_e = date ('Y') + 1;
		$y_s = $year < $y_s ? $year : $y_s;
		$y_e = $year > $y_e ? $year : $y_e;

		$years = array ();
		for ($i = $y_s; $i <= $y_e; $i++)
			$years[$i] = sprintf ('%04d', $i);;

		$months = array ();
		for ($i = 1; $i <= 12; $i++)
			$months[$i] = sprintf ('%02d', $i);

		$days = array ();
		for ($i = 1; $i <= 31; $i++)
			$days[$i] = sprintf ('%02d', $i);

		return sprintf ("%s - %s - %s", 
			common_select_button ($prefix."year", $years, $year),
			common_select_button ($prefix."month", $months, $month),
			common_select_button ($prefix."day", $days, $day)
			);
	}
	
	function parse_date_select (&$request, $prefix) {
		$year = $request->_post[$prefix."year"];
		$month = $request->_post[$prefix."month"];
		$day = $request->_post[$prefix."day"];

		if ($year && $month && $day)
			return sprintf ("%04d-%02d-%02d", $year, $month, $day);
		else
			return NULL;
	}

// ******* COMMON ... FUNCTIONS *******

?>