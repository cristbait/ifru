<?PHP

include_once ('engine/factory.inc');
//include_once ('engine/url.inc');

class ListItem {
	var $id;
	var $factory;
	var $language;
	
	function ListItem ($id, &$factory) {
		$this->id = $id;
		$this->factory = &$factory;
		$this->language = &$this->factory->language;

		if (empty ($this->id))
			$this->load_defaults ();
	}
	
	function delete () {
		return $this->factory->delete_list (array ($this->id));
	}

	function update () {
//		echo $this->id;
		return $this->factory->update ($this->id);
	}

	function &reload () {
		return $this->factory->reload ($this->id);
	}
	
	function load_from_row ($row) {
		// ����������� �����; ������ �� ������.
	}
	
	function update_language_dependent () {
		// ����������� �����; ������ �� ������.
	}
	
	function load_defaults () {
		// ����������� �����; ������ �� ������.
	}
	
	function std_where_string ($where_arr, $oper) {
		if ($where_arr) {
			$where_keys = array_keys ($where_arr);
		
			for ($i = 0; $i < count ($where_arr); $i++) {
				$value = $where_arr[$where_keys[$i]];
				$wheres[] = sprintf ("%s%s", $where_keys[$i], ($value !== NULL) ? sprintf ("='%s'", mysql_real_escape_string ($value)) : ' IS NULL');
			}
		}
		
		return (($wheres !== NULL) ? sprintf (' WHERE %s', implode (sprintf (' %s ', $oper), $wheres)) : NULL);
	}
	
	function std_null_params ($param_arr, $allow_null) {
		$param_keys = array_keys ($param_arr);
		
		if ($param_arr)
			for ($i = 0; $i < count ($param_arr); $i++) {
				$value = $param_arr[$param_keys[$i]];
				
				if ((!$value) && $allow_null)
					$value = NULL;
					
				$retval[$param_keys[$i]] = $value;
			}
			
		return $retval;
	}
	
    
	function get_update_string () {
		return $this->std_update_string (
			$this->std_null_params ( 
				$this->get_update_array (),	
				TRUE),
			array (
				$this->factory->id_name => $this->id));	
	}
	
	function get_insert_string () {
		return $this->std_insert_string (
			$this->std_null_params (
				$this->get_insert_array (),	
				TRUE),
			NULL);	
	}

	function std_update_string ($param_arr, $where_arr) {
		$param_keys = array_keys ($param_arr);
		
		for ($i = 0; $i < count ($param_arr); $i++) {
			$value = $param_arr[$param_keys[$i]];
			//if (! get_magic_quotes_gpc ()) {
				$value = mysql_real_escape_string ($value);
			//}
			$params[] = sprintf ("%s=%s", $param_keys[$i], ($value !== NULL) ? sprintf ("'%s'", $value) : 'NULL');
		}
		
		return sprintf ('UPDATE %s SET %s%s', 
			$this->factory->table_name,
			implode (', ', $params),
			$this->std_where_string ($where_arr, 'AND'));
	}
	
	function std_insert_string ($param_arr, $where_arr) {
		if ($param_arr) {
			$param_keys = array_keys ($param_arr);
		
			for ($i = 0; $i < count ($param_keys); $i++) {
				$param_names[] = $param_keys[$i];
				$value = $param_arr[$param_keys[$i]];
				//if (! get_magic_quotes_gpc ()) {
					$value = mysql_real_escape_string ($value);
				//}
				$param_vals[] = ($value !== NULL) ? sprintf ("'%s'", $value) : 'NULL';
			}
		}
		
		return sprintf ('INSERT INTO %s (%s) VALUES (%s)%s',
			$this->factory->table_name,
			implode (', ', $param_names),
			implode (', ', $param_vals),
			$this->std_where_string ($where_arr, 'AND'));
	}

	function nl2br () {
		$array = get_object_vars ($this);

		unset ($array['factory']);
		unset ($array['language']);

		if ($array)
	    foreach ($array as $key => $value) {
			if (is_string ($value)) {
		        $this->$key = nl2br ($value);
			}
	    }
	} 

	function htmlspecialchars () {
//		$array = get_object_vars ($this);
//
//		unset ($array['factory']);
//		unset ($array['language']);
//
//		if ($array)
//	    foreach ($array as $key => $value) {
//			if (is_string ($value)) {
////		        $this->$key = htmlspecialchars ($value, ENT_QUOTES);
////				$this->$key = preg_replace ('/\{/', '&#x7B;', $this->$key);
////				$this->$key = preg_replace ('/\}/', '&#x7D;', $this->$key);
//			}
//	    }
	} 

	function stripslashes () {
		if (! get_magic_quotes_gpc ()) {
		    return;
		}

		$array = get_object_vars ($this);

		unset ($array['factory']);
		unset ($array['language']);

		if ($array)
	    foreach ($array as $key => $value) {
	        $this->$key = stripslashes ($value);
	    }
	} 

	function clear_gpc () {
		$this->stripslashes ();
	    $this->htmlspecialchars ();
	} 

}




class ListFactory extends Factory {
	var $mysql;
	
	var $table_name;
	var $id_name;
	var $index_name;
	var $movable;
	
	function ListFactory (&$config, &$request, $table_name, $id_name, $index_name, $class_name, $movable = TRUE) {
		$this->Factory ($config, $request, $class_name);
		$this->mysql = $config->getDB();
	
		$this->table_name = $table_name;
		$this->id_name = $id_name;
		$this->index_name = $index_name;
		$this->movable = $movable;
	}
	
	function &create_new ($id, $load) {
		$item = new $this->class_name ($id, $this);
//echo $id.':'.($id == NULL).'<br>';
		if ($load && ($id != NULL)) {
			$rows = $this->mysql->select (sprintf ("SELECT * FROM %s WHERE %s='%s' LIMIT 1",
				$this->table_name, $this->id_name, $id));
	
			if (count ($rows) == 0)
				return false;
	
			$item->load_from_row ($rows[0]);
		}
	
		return $this->to_cache ($id, $item);
	}

	function &reload ($id) {
		if ($this->in_cache ($id)) {
			$obj = $this->from_cache ($id);

			$rows = $this->mysql->select (sprintf ("SELECT * FROM %s WHERE %s='%s' LIMIT 1",
				$this->table_name, $this->id_name, $id));
	
			if (count ($rows) == 0)
				return false;
	
			$obj->load_from_row ($rows[0]);
		} else {
			$obj = $this->create_new ($id, TRUE);
		}

		return $obj;
	}

	function &create_from_query ($query_str) {
		if (count ($rows = $this->mysql->select ($query_str)) == 0)
			return false;

		if ($rows != false)
			foreach ($rows as $row) {
				$vars = get_object_vars ($row);
				$item = $this->create($vars[$this->id_name], false);
				$item->load_from_row ($row);

				$retval [] = $item;
			}
	
		return $retval;
	}

	function &create_from_query_id ($query_str) {
		if (count ($rows = $this->mysql->select ($query_str)) == 0)
			return false;

		if ($rows != false)
			for (reset ($rows); $row = current ($rows); next ($rows)) {
				$vars = get_object_vars ($row);
				$item = $this->create($vars[$this->id_name], false);
				$item->load_from_row ($row);
	
				$retval[$vars[$this->id_name]] = $item;
			}
	
		return $retval;
	}

	function &create_list ($id_list) {
		$not_cached = array();
		$cached = array();
	
		for (reset ($id_list); $id = current ($id_list); next ($id_list))
			if (!$this->in_cache ($id))
				$not_cached[] = sprintf ("%s='%s'", $this->id_name, $id);
			else
				$cached[] = $this->from_cache ($id);
	
		if (count ($not_cached) > 0) {
			$where_str = implode (' OR ', $not_cached);
			$not_cached = $this->create_from_query (sprintf ("SELECT * FROM %s WHERE %s", $this->table_name, $where_str));
			if (! $not_cached)
				$not_cached = array ();

			$retval = array_merge ($cached, $not_cached);
		} else
			$retval = $cached;
	
		return $retval;
	}
	
	function delete_list ($id_list) {
		for (reset ($id_list); $id = current ($id_list); next ($id_list))
			$where_list[] = sprintf ("%s='%s'", $this->id_name, $id);
	
		if (count ($where_list) > 0) {
			$where_str = implode (' OR ', $where_list);
	
			if ($this->mysql->command (sprintf ("DELETE FROM %s WHERE %s", $this->table_name, $where_str))) {
				$this->clear_cache ($id_list);
	
				return $this->repair_index ();
			}
		}
	
		return false;
	}
	
	// ��������! ����� ������ ����� ������ ��� ����� ��������� ������� � ������������� ����������� ���������.
	// ��� ����������� �������� ��� ������������� ���� �������� ����� refresh_cache_indices ().
	function repair_index () {
		if ($this->movable) {
			$rows = $this->mysql->select_field (sprintf ("SELECT %s FROM %s ORDER BY %s ASC",
				$this->index_name, $this->table_name, $this->index_name));
	
			for ($i = 0; $i < count ($rows); $i++)
				while ($rows[$i] != ($i + 1)) {
					$diff = $rows[$i] - ($i + 1);
	
					$result = ($this->mysql->command (sprintf ("UPDATE %s SET %s=%s-('%d') WHERE %s >= '%d'",
						$this->table_name, $this->index_name, $this->index_name, $diff, $this->index_name, $rows[$i])));
	
					if (! $result)
						return false;
	
					for ($j = $i; $j < count ($rows); $j++)
						$rows[$j] = $rows[$j] - $diff;
				}
		}
	
		return true;
	}
	
	function refresh_cache_indices () {
	}

	/*
	function index_by_id ($id) {
		$item = $this->create ($id);

		$ord_index = $item->factory->index_name;
		if (empty ($ord_index))	return 1;

		$row = $this->mysql->select_field (sprintf ("SELECT count(*) AS index_count FROM %s WHERE %s < '%s'",
			$this->table_name, $this->index_name, $item->$ord_index));

		return ($row[0]);
	}
	*/
	
	function index_by_id ($id) {
		$id_name = $this->id_name;

		if (empty ($id_name)) return 1;

		$this->conditions ();
		$query_str = sprintf ("SELECT %s FROM %s as t %s%s%s", $id_name, $this->table_name, $this->join_string (), $this->where_string (), $this->order_string ());
		$list = $this->create_from_query ($query_str);

		$i = 0;
		if ($list)
			while ($i < sizeof ($list) && $list[$i]->$id_name != $id) 
				$i++;
		return $i;
	}

	/*
	function create_all ($limit = NULL, $offset = NULL) {
		$limit_str = $limit ? sprintf (" LIMIT %s%d", $offset ? sprintf ("%d, ", $offset) : '', $limit) : '';
		$query_str = sprintf ("SELECT * FROM %s ORDER BY %s ASC%s", $this->table_name, $this->index_name, $limit_str);
		return $this->create_from_query ($query_str);
	}
	*/
	
	function update ($id) {
		$item = $this->create ($id);
//echo ((!$id) ? $item->get_insert_string () : $item->get_update_string ());
		$retval = $this->mysql->command ((!$id) ? $item->get_insert_string () : $item->get_update_string ());
//echo $item->image->image;
		if (!$id) {
			$item->id = $this->mysql->insert_id ();
			$this->to_cache ($item->id, $item);
		}
	
		return $retval;
	}
	
	function get_next_index () {
		$row = $this->mysql->select_field (sprintf ("SELECT max(%s) AS next_ord_index FROM %s",
			$this->index_name, $this->table_name));
	
		return ($row[0] + 1);
	}

	function get_shift_index () {
		if ($this->movable) {
			$this->mysql->command (sprintf ('UPDATE %s SET %s = %s + 1', $this->table_name, $this->index_name, $this->index_name));
		}

		$row = $this->mysql->select_field (sprintf ("SELECT min(%s) AS first_ord_index FROM %s",
			$this->index_name, $this->table_name));
	
		return ($row[0] == NULL ? 1 : $row[0] - 1);
	}
	
	/*
	function get_total_count () {
		$row = $this->mysql->select_field (sprintf ("SELECT count(*) AS total_count FROM %s", $this->table_name));
	
		return $row[0];
	}
	*/
	
	function move ($id, $up, $add = '') {
		if (!$this->movable)
			return false;
	
		$item_1 = $this->create ($id);
		$rows = $this->mysql->select_field (sprintf ("SELECT %s FROM %s WHERE %s %s '%s' %s ORDER BY %s %s LIMIT 1",
			$this->id_name,
			$this->table_name,
			$this->index_name,
			$up ? '<' : '>',
			$item_1->ord_index,
			$add,
			$this->index_name,
			$up ? 'DESC' : 'ASC'));
	
		if (count ($rows) == 0)
			return false;
	
		$item_2 = $this->create ($rows[0]);
	
		$old_item_1_index = $item_1->ord_index;
	
		$query_str = "UPDATE %s SET %s='%s' WHERE %s='%s'";
	
		if (!$this->mysql->command (sprintf ($query_str,
			$this->table_name,
			$this->index_name,
			$item_2->ord_index,
			$this->id_name,
			$item_1->id)))
			return false;
	
		$item_1->ord_index = $item_2->ord_index;
	
		if (!$this->mysql->command (sprintf ($query_str,
			$this->table_name,
			$this->index_name,
			$old_item_1_index,
			$this->id_name,
			$item_2->id)))
			return false;
	
		return true;
	}

	function movetotal ($id, $up, $add = '') {
		// print "movetotal<br>";

		if (!$this->movable)
			return false;
	
		$item_1 = $this->create ($id);
		$rows = $this->mysql->select_field (sprintf ("SELECT %s FROM %s WHERE %s %s '%s' %s ORDER BY %s %s LIMIT 1",
			$this->id_name,
			$this->table_name,
			$this->index_name,
			$up ? '<' : '>',
			$item_1->ord_index,
			$add,
			$this->index_name,
			$up ? 'ASC' : 'DESC'));

		if (count ($rows) == 0)
			return false;

		//print $rows[0]; return false;

		$item_2 = $this->create ($rows[0]);

		if (!$this->mysql->command (sprintf ("UPDATE %s SET %s=%s %s 1 WHERE %s %s '%s'",
			$this->table_name,
			$this->index_name,
			$this->index_name,
			$up ? '+' : '-',
			$this->index_name,
			$up ? '<' : '>',
			$item_1->ord_index,
			$add)))
			return false;

		if (!$this->mysql->command (sprintf ("UPDATE %s SET %s='%s' WHERE %s='%s'",
			$this->table_name,
			$this->index_name,
			$item_2->ord_index,
			$this->id_name,
			$item_1->id)))
			return false;

		return true;
	}
	
	function &default_list_action ($list_script, $id_name) {
		if (empty ($id_name))
			$id_name = $this->id_name;

		$tpl = new Template ($this->config, MY_COMMON_TPL_PATH);
		$url = new URL ($list_script);
		
		$id_list = $this->request->_post[$id_name];
		if (! is_array ($id_list) && $id_list) $id_list = array ($id_list);
	
		switch ($this->request->action) {
			case 'delete':
				$item_list = $this->create_list ($id_list);
				$index_list = array ();
				for (reset ($item_list); $item = current ($item_list); next ($item_list))
					$index_list[] = $item->ord_index;
	
				sort ($index_list, SORT_NUMERIC);
	
				$row = $this->mysql->select_field (sprintf ("SELECT %s FROM %s WHERE %s < '%d' ORDER BY %s DESC LIMIT 1",
					$this->id_name, $this->table_name, $this->index_name, $index_list[0], $this->index_name));
	
				$result = $this->delete_list ($id_list);
	
				$url->set_var ($id_name, $result ? $row[0] : $id_list[0]);
	
				$msg = $result ? '�������� ������ �������.' : '������ ��� ��������!';
	
				common_redirect (
					$tpl,
					$this->config,
					$msg,
					'',
					'��������� � ������',
					$url->get_url ());
	
				$tpl->p_default ();
	
				return true;
			case 'move_up':
			case 'move_down':
			case 'movetotal_up':
			case 'movetotal_down':
				if ($this->request->action == 'movetotal_up' || $this->request->action == 'movetotal_down')
					$result = $this->movetotal ($id_list[0], $this->request->action == 'movetotal_up');
				else 
					$result = $this->move ($id_list[0], $this->request->action == 'move_up');
				$msg = $result ? '����������� ������ �������.' : '������ ��� �����������!';

				$url->set_var ('select_'.$id_name, $id_list[0]);
				$url->set_var ($id_name, $id_list[0]);

				common_redirect (
					$tpl,
					$this->config,
					$msg,
					'',
					'��������� � ������',
					$url->get_url (),
					$result);
	
				$tpl->p_default ();
	
				return true;
			case 'publish':
			case 'unpublish':
				if ($id_list)
					$result = $this->mysql->command (sprintf ("UPDATE %s SET publish = %d WHERE %s IN (%s)", $this->table_name, $this->request->action == 'publish' ? 1 : 0, $this->id_name, join (',', $id_list)));
	
				$msg = $result ? '������������ ������ �������.' : '������ ��� ������������!';
	
				common_redirect (
					$tpl,
					$this->config,
					$msg,
					'',
					'��������� � ������',
					$url->get_url ());
	
				$tpl->p_default ();
	
				return true;
			default:
				return false;
		}
	}
	
	function &default_edit_action ($edit_script, $list_script, $id_name) {
		if (empty ($id_name))
			$id_name = $this->id_name;

		$tpl = new Template ($this->config, MY_COMMON_TPL_PATH);
		
		$id = $this->request->_post[$id_name];

		switch ($this->request->action) {
			case 'insert':
			case 'update':
				$item = $this->create ($id);
				$item->load_from_parameters ($this->request);
				
				$result = $item->update ();
	
				$msg = $result ? '������ ������ ������ �������' : '������ ��� ������ ������!';
	
				if ($result && ($this->request->action == 'update')) {
					$link = '��������� � ��������������';
					$url = new URL ($edit_script);
				} else {
					$link = '��������� � ������';
					$url = new URL ($list_script);
				}

				$url->set_var ($id_name, (!$this->request->_post[$id_name]) ?
					$item->id :
					$this->request->_post[$id_name]);

				$this->last_id = $item->id;
	
				common_redirect (
					$tpl,
					$this->config,
					$msg,
					'',
					$link,
					$url->get_url (),
					$result);
				
				$tpl->p_default ();	
				
				return true;
			default:
				return false;
		}
	}
	
	### innovation

	function &create_all ($limit = NULL, $offset = NULL) {
		$limit_str = $limit ? sprintf (" LIMIT %s%d", $offset ? sprintf ("%d, ", $offset) : '', $limit) : '';
		$this->conditions ();

		$query_str = sprintf ("SELECT %s FROM %s as t %s%s%s%s%s", $this->select_string (), $this->table_name, $this->join_string (), $this->where_string (), $this->order_string (), $this->group_string (), $limit_str);

		$list = $this->create_from_query ($query_str);
		return $list;
	}


	function get_total_count () {
		$this->conditions ();

		$row = $this->mysql->select_field (sprintf ("SELECT count(*) AS total_count FROM %s as t %s%s%s", $this->table_name, $this->join_string (), $this->where_string (), $this->group_string ()));
	
		return $row[0];
	}
	
	function select_string () {
		return join (', ', $this->select);
	}

	function where_string () {
		$string = '';
		if (! empty ($this->cond))
			$string = " WHERE " . join (' AND ', $this->cond);

		return $string;
	}

	function join_string () {
		return ' ' . join (' ', $this->join);
	}

	function order_string () {
		$string = '';
		if (! empty ($this->order))
			$string = " ORDER BY " . join (', ', $this->order);

		return $string;
	}

	function group_string () {
		$string = '';
		if (! empty ($this->group))
			$string = " GROUP BY " . join (', ', $this->group);

		return $string;
	}

	### usually redefine
	var $cond = array ();
	var $join = array ();
	var $order = array ();
	var $group = array ();
	var $select = array ("t.*");

	function conditions () {
		$this->cond = array ();
		$this->join = array ();
		$this->order = array ();
		$this->group = array ();
			
		$this->order[] = $this->index_name . " ASC";
	}

	function load_vars () {
		// $this->request
		// abstract
	}

	function set_url_vars (&$url) {
		// abstract
	}


	var $list = NULL;

	function create_cache () {
		if ($this->list == NULL) {
			$this->list = array ();

			$this->do_cache = TRUE;
			if ($list = $this->create_all ())
			$this->do_cache = FALSE;
			for ($i = 0; $i < sizeof ($list); $i++) {
				$this->list[$list[$i]->id] = $list[$i]->name;
			}
		}
	}

	function find ($key) {
		$this->create_cache ();
		return getArrayValue ($key, $this->list);
	}

	function find_id_by_value ($str, $reg = FALSE) {
		$this->create_cache ();
		if ($reg)
			foreach ($this->list as $key => $value) {
				if (preg_match ('/'.$str.'/', $value)) {
					$id = $key;
					break;
				}
			}
		else
			$id = array_search ($str, $this->list);

		return $id ? $id : NULL;
	}

	function select_button ($name, $key, $add_empty_field = FALSE, $class = "") {
		$this->create_cache ();
		if ($add_empty_field !== FALSE) {
			$list = array ('' => is_string ($add_empty_field) ? $add_empty_field : '');
			foreach ($this->list as $id => $val) 
				$list[$id] = $val;
		} else
			$list = &$this->list;
		return common_select_button ($name, $list, $key, $class);
	}
}

class SubListFactory extends ListFactory {
	var $sub_id;
	var $sub_id_name;
	var $page_name;
	var $edit_script;
	var $list_script;
	var $ext_id_name;
	
	function SubListFactory (&$config, &$request, $table_name, $id_name, $index_name, $sub_id_name, $class_name, $movable = true, $sub_id = NULL) {
	
		$this->ListFactory ($config, $request, $table_name, $id_name, $index_name, $class_name, $movable);
	
		$this->sub_id_name = $sub_id_name;
		$this->sub_id = ($sub_id) ? $sub_id : $request->_get[$this->sub_id_name];
	}
	
	function repair_index () {
		// �� ������ ������, ��� ���� ��������� sub_id � ��������
	}

	/*
	function create_all ($limit = NULL, $offset = NULL) {
		$limit_str = $limit ? sprintf (" LIMIT %s%d", $offset ? sprintf ("%d, ", $offset) : '', $limit) : '';
		$query_str = sprintf ("SELECT * FROM %s WHERE %s='%d' ORDER BY %s ASC%s",
			$this->table_name, $this->sub_id_name, $this->sub_id, $this->index_name, $limit_str);
	
		return $this->create_from_query ($query_str);
	}
	
	function get_total_count () {
		$row = $this->mysql->select_field (sprintf ("SELECT count(*) AS total_count FROM %s WHERE %s='%d'", $this->table_name, $this->sub_id_name, $this->sub_id));
	
		return $row[0];
	}
	*/
	
	function conditions () {
		parent::conditions ();

		$this->cond[] = sprintf ("%s='%d'", $this->sub_id_name, $this->sub_id);
	}


	function get_sub_id ($id) {
		$row = $this->mysql->select_field (sprintf ("SELECT %s FROM %s WHERE %s='%s'", $this->sub_id_name, $this->table_name, $this->id_name, $id));
	
		return $row[0];
	}

	function get_next_index () {
		$where = "WHERE ".$this->sub_id_name." ".($this->sub_id ? "= '".$this->sub_id."'" : "IS NULL OR ".$this->sub_id_name." = '0'");

		$row = $this->mysql->select_field (sprintf ("SELECT max(%s) AS next_ord_index FROM %s %s",
			$this->index_name, $this->table_name, $where));
	
		return ($row[0] + 1);
	}


	function get_shift_index () {
		$where = "WHERE ".$this->sub_id_name." ".($this->sub_id ? "= '".$this->sub_id."'" : "IS NULL");

		if ($this->movable) {
			$this->mysql->command (sprintf ('UPDATE %s SET %s = %s + 1 %s', $this->table_name, $this->index_name, $this->index_name, $where));
		}

		$row = $this->mysql->select_field (sprintf ("SELECT min(%s) AS first_ord_index FROM %s %s",
			$this->index_name, $this->table_name, $where));
	
		return ($row[0] == NULL ? 1 : $row[0] - 1);
	}


	/*
	function index_by_id ($id) {
		$item = $this->create ($id);

		$row = $this->mysql->select_field (sprintf ("SELECT count(*) AS index_count FROM %s WHERE %s = '%s' AND %s < '%s'",
			$this->table_name, $this->sub_id_name, $this->sub_id, $this->index_name, $item->ord_index));
	
		return ($row[0]);
	}
	*/


	function move ($id, $up, $add = '') {
		if (!$this->movable)
			return false;
	
		$item_1 = $this->create ($id);
		$rows = $this->mysql->select_field (sprintf ("SELECT %s FROM %s WHERE %s %s '%s' AND %s = '%s' ORDER BY %s %s LIMIT 1",
			$this->id_name,
			$this->table_name,
			$this->index_name,
			$up ? '<' : '>',
			$item_1->ord_index,
			$this->sub_id_name,
			$this->sub_id,
			$this->index_name,
			$up ? 'DESC' : 'ASC'));
	
		if (count ($rows) == 0)
			return false;
	
		$item_2 = $this->create ($rows[0]);
	
		$old_item_1_index = $item_1->ord_index;
	
		$query_str = "UPDATE %s SET %s='%s' WHERE %s='%s'";
	
		if (!$this->mysql->command (sprintf ($query_str,
			$this->table_name,
			$this->index_name,
			$item_2->ord_index,
			$this->id_name,
			$item_1->id)))
			return false;
	
		$item_1->ord_index = $item_2->ord_index;
	
		if (!$this->mysql->command (sprintf ($query_str,
			$this->table_name,
			$this->index_name,
			$old_item_1_index,
			$this->id_name,
			$item_2->id)))
			return false;
	
		return true;
	}

	function movetotal ($id, $up, $add = '') {
		print "movetotal - �� ���������!!!";
		return false;
	}

}

?>