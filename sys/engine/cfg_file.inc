<?PHP

include_once ('engine/error.inc');

class ConfigFile {
	var $file_name;
	var $error;
	var $data;
	
	function ConfigFile ($file_name, &$error) {
		$this->file_name = $file_name;
		$this->error = &$error;
		
		$this->load_data ();
	}
	
	function load_data () {
		if (!file_exists ($this->file_name))
			$this->error->run_error (sprintf ("Cannot open .cfg file '%s'", $this->file_name), true);
			
		$raw_file = file ($this->file_name);
		
		$current_section = "";
		for ($i = 0; $i < count ($raw_file); $i++) {
			$raw_file[$i] = explode (";", $raw_file[$i]);
			$raw_file[$i] = $raw_file[$i][0];
			if (preg_match ("/\[([^]]+)\]/", $raw_file[$i], $raw_section))
				$current_section = $raw_section[1];
			else {
				$raw_expr = explode ("=", $raw_file[$i]);
				
				$name = trim ($raw_expr[0]);
				
				$value = "";
				for ($j = 1; $j < count ($raw_expr); $j++)
					$value .= $raw_expr[$j];
				
				if (strlen ($name) > 0)
					$this->put_var ($current_section, $name, trim ($value));
			}
		}
	}
	
	function put_var ($section, $name, $value) {
		$this->data[strtolower ($section)][strtolower ($name)] = $value;
	}
	
	function get_var ($section, $name) {
		return $this->data[strtolower ($section)][strtolower ($name)];
	}
	
	function get_section ($section) {
		return getArrayValue (strtolower ($section), $this->data);
	}
}

?>