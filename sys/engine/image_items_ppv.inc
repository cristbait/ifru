<?PHP

include_once ('engine/image_items.inc');

class PrePreviewImageItem extends PreviewImageItem {
	function PrePreviewImageItem ($id, &$factory) {
		parent::PreviewImageItem ($id, $factory);

	    $this->image_ppv = new Image ();
	}

    function load_from_row ($row) {
		parent::load_from_row ($row);

		$this->topic_id = $row->topic_id;
		$this->publish_right = $row->publish_right;
		$this->image_ppv->unpack_string ($row->image_ppv);
    }
    
	function get_update_array () {
		return array (
			'link_class' => $this->link_class,
			'link_id' => $this->link_id,
			'title_ru' => $this->title_ru,
			'description_ru' => $this->description_ru,
			'description_br' => $this->description_br,
			'title_en' => $this->title_en,
			'big_id' => $this->big_id,
			'publish_right' => $this->publish_right,
			'topic_id' => $this->topic_id,
			'image' => $this->image->pack_data (),
			'image_pv' => $this->image_pv->pack_data (),
			'image_ppv' => $this->image_ppv->pack_data ()
		);	
	}
	
	function get_insert_array () {
		return array (
			'link_class' => $this->link_class,
			'link_id' => $this->link_id,
			'title_ru' => $this->title_ru,
			'description_ru' => $this->description_ru,
			'description_br' => $this->description_br,
			'title_en' => $this->title_en,
			'big_id' => $this->big_id,
			'publish_right' => $this->publish_right,
			'topic_id' => $this->topic_id,
			'image' => $this->image->pack_data (),
			'image_pv' => $this->image_pv->pack_data (),
			'image_ppv' => $this->image_ppv->pack_data ()
		);	
	}
	
	function load_from_parameters (&$request) {
		// parent::load_from_parameters ($request);
		$this->link_class = $request->_post['image_item_link_class'];
		$this->link_id = $request->_post['image_item_link_id'];
		$this->title_ru = $request->_post['image_item_title_ru'];
		$this->description_ru = $request->_post['image_item_description_ru'];
		$this->description_br = $request->_post['image_item_description_br'];
		$this->title_en = $request->_post['image_item_title_en'];
		$this->big_id = $request->_post['image_item_big_id'];
		$this->topic_id = $request->_post['image_item_topic_id'];
		$this->publish_right = $request->_post['image_item_publish_right'];

		$this->image_del = $this->image->proceed_parameters ($request, 'image_item_image', 'image_items', 'image_', $this->id);

		/*
		if ((! $this->image_pv->image && $this->image->just_uploaded) || $this->image_gen) {
			$this->image_pv->copy_resized ($this->image, $this->factory->pv_w, $this->factory->pv_h, MY_FILES_FOLDER.'/image_items_pv', 'image_pv_', $this->id);
			$this->image_pv->just_uploaded = TRUE;
		}
		if ($this->image_pv->just_uploaded) {
			$this->image_ppv->copy_resized ($this->image_pv, $this->factory->ppv_w, $this->factory->ppv_h, MY_FILES_FOLDER.'/image_items_ppv', 'image_ppv_', $this->id);
		}
		*/
	}
	
	function set_template_vars (&$tpl) {
		parent::set_template_vars ($tpl);

		$tpl->set_var ('image_item_topic_id', $this->topic_id);
		$tpl->set_var ('image_item_publish_right', $this->publish_right);
		$tpl->set_var ('image_item_image_ppv_tag', $this->image_ppv->get_img_tag (NULL, NULL, NULL, 0, $this->factory->root_path, $this->title_ru));
	}

	function control ($description = FALSE, $align = TRUE, $insert = TRUE) {
		global $config;
		
		$tpl = new Template ($config, MY_COMMON_TPL_PATH);
		$tpl->set_file ('image_item_ctl', 'image_item_ppv_ctl.html');
		$tpl->set_block ('image_item_ctl', 'align_block', 'align_block');
		$tpl->set_block ('image_item_ctl', 'info_block', 'info_block');
		$tpl->set_block ('image_item_ctl', 'actions_block', 'actions_block');

		$this->set_template_vars ($tpl);
		$alt_s = sprintf ('%dx%d, %d ����',
			$this->image->width, $this->image->height, $this->image->size);
		$alt = sprintf ('%s (%s)', $this->image->file, $alt_s);
		$tpl->set_var ('image',
			$this->image->image ? $this->image->get_img_tag (0, 0, NULL, 0, $this->factory->root_path, $alt) : '��');
		$tpl->set_var ('image_info', $this->image->image ? $alt : '&nbsp;');
		$tpl->set_var ('image_info_short', $this->image->image ? $alt_s : '&nbsp;');

		list ($w, $h) = $this->image_pv->find_dims (500, NULL);
		$alt_s = sprintf ('%dx%d, %d ����', 
			$this->image_pv->width, $this->image_pv->height, $this->image_pv->size);
		$alt = sprintf ('%s (%s)', $this->image_pv->file, $alt_s);
		$tpl->set_var ('image_pv',
			$this->image_pv->image ? $this->image_pv->get_img_tag ($w, $h, NULL, 0, $this->factory->root_path, $alt) : '�');
		$tpl->set_var ('image_pv_info', $this->image_pv->image ? $alt : '&nbsp;');
		$tpl->set_var ('image_pv_info_short', $this->image_pv->image ? $alt_s : '&nbsp;');

		list ($ppw, $pph) = $this->image_ppv->find_dims (500, NULL);
		$alt_s = sprintf ('%dx%d, %d ����', 
			$this->image_ppv->width, $this->image_ppv->height, $this->image_ppv->size);
		$alt = sprintf ('%s (%s)', $this->image_ppv->file, $alt_s);
		$tpl->set_var ('image_ppv',
			$this->image_ppv->image ? $this->image_ppv->get_img_tag ($ppw, $pph, NULL, 0, $this->factory->root_path, $alt) : '�');
		$tpl->set_var ('image_ppv_info', $this->image_ppv->image ? $alt : '&nbsp;');
		$tpl->set_var ('image_ppv_info_short', $this->image_ppv->image ? $alt_s : '&nbsp;');

		$tpl->set_var ('image_href', $this->image->image ? ('<a href="'.$this->image->file.'" target="_blank">����������<br>�������<br>��������</a>') : $tpl->get_var ('image_pv'));
		$tpl->set_var ('image_pv_href', $this->image->image ? ('<a href="'.$this->image->file.'" target="_blank">'.$tpl->get_var ('image_pv').'</a>') : $tpl->get_var ('image_pv'));
		$tpl->set_var ('image_ppv_href', $this->image->image ? ('<a href="'.$this->image->file.'" target="_blank">'.$tpl->get_var ('image_ppv').'</a>') : $tpl->get_var ('image_ppv'));

		$tpl->set_var ('image_src', $this->image->image);
		$tpl->set_var ('image_width', $this->image->width);
		$tpl->set_var ('image_height', $this->image->height);

		$tpl->set_block ('actions_block', 'button', 'button');

		$tpl->set_var ('prefix', 'image_item');

		$image_id = $this->id ? $this->id : '';
		$tpl->set_var ('image_id', $image_id);
	
		if ($align) {
			$tpl->set_var ('image_align', $this->image->halign);

			$tpl->set_var ('ha_left', common_radio_button (sprintf ('image_item_halign_%s', $image_id), 'left', $this->image->halign == 'left'));
			$tpl->set_var ('ha_right', common_radio_button (sprintf ('image_item_halign_%s', $image_id), 'right', $this->image->halign == 'right'));

			$tpl->parse ('align_block', 'align_block');
		} else {
			$tpl->set_var ('align_block', '');
		}
	
		$tpl->set_var ('image_item_description_br_checkbox', common_checkbox (sprintf ("image_item_description_br_%s", $image_id), 1, $this->description_br));

		$tpl->set_var ('image_gen', common_checkbox (sprintf ("image_gen_%s", $image_id), 'Y', FALSE));
		$tpl->set_var ('image_del', common_checkbox (sprintf ("image_del_%s", $image_id), 'Y', FALSE));
		$tpl->set_var ('publish_right', common_checkbox (sprintf ("image_item_publish_right_%s", $image_id), 'Y', $this->publish_right));
		
//		$tpl->set_var (sprintf ("%s_image_file", $prefix), $this->file);
		$tpl->set_var ('image_file', sprintf ('<INPUT TYPE="hidden" NAME="image_file_%s" VALUE="%s">', $image_id, $this->image->file));
	
		$tpl->set_var ('topic_select_button', $this->factory->topic_select_button (sprintf ('image_item_topic_id_%s', $image_id), $this->topic_id, ""));
		
		if (! ($image_id && $insert))
			$tpl->set_var ('button', '&nbsp;');
		else
			$tpl->parse ('button', 'button', FALSE);
			
		if ($image_id > 0) {
			$tpl->parse ('info_block', 'info_block', FALSE);
			$tpl->parse ('actions_block', 'actions_block', FALSE);
		} else {
			$tpl->set_var ('info_block', '');
			$tpl->set_var ('actions_block', '');
		}

		$tpl->parse ('image_item_ctl', 'image_item_ctl');
		
		return $tpl->get_var ('image_item_ctl');
	}
}

class PrePreviewImageItemFactory extends PreviewImageItemFactory {
	var $topic_factory;

	var $ppv_w = 20;
	var $ppv_h = 15;

	var $ppv_resize_only = FALSE;

	function PrePreviewImageItemFactory (&$config, &$request, $link_class, $link_id, $topic_factory = NULL) {
		parent::ListFactory ($config, $request, 'image_items_ppv', 'id', 'id', 'PrePreviewImageItem', FALSE);
		$this->link_class = $link_class;
		$this->link_id = $link_id;
		$this->topic_factory = &$topic_factory;
	}

	function set_ppv_sizes ($width, $height) {
		$this->ppv_w = $width;
		$this->ppv_h = $height;
	}

	function set_resize_only ($pv_im, $im) {
		parent::set_resize_only ($pv_im, $im);
		$this->ppv_resize_only = FALSE;
	}

	function proceed_images (&$request) {
		$image_id_list = $request->_post['image_id'];

		if ($image_id_list)
		for ($i = 0; $i < sizeof ($image_id_list); $i++) {
			$postfix = $image_id_list[$i];
			$real_id = $image_id_list[$i] > 0 ? $image_id_list[$i] : 0;

			$this->clear_cache (array ($real_id));
			$image_item = $this->create ($real_id);

			if ($retval = ($request->_post[sprintf ('image_del_%s', $postfix)] == true)) {
				$image_item->image->image = '';
				$image_item->deleted = TRUE;
				$del_file = $request->_post[sprintf ('image_file_%s', $postfix)];
				if ($image_item->image->file && file_exists ($image_item->image->file))
					unlink ($image_item->image->file);
				if ($image_item->image_pv->file && file_exists ($image_item->image_pv->file))
					unlink ($image_item->image_pv->file);
				if ($image_item->image_ppv->file && file_exists ($image_item->image_ppv->file))
					unlink ($image_item->image_ppv->file);
				$image_item->delete ();

				continue;
			}

			$image_item->link_class = $this->link_class;
			$image_item->link_id = $this->link_id;
			$image_item->title_ru = htmlspecialchars ($request->_post[sprintf('image_item_title_ru_%s', $postfix)]);
			$image_item->title_en = htmlspecialchars ($request->_post[sprintf('image_item_title_en_%s', $postfix)]);
			$image_item->description_ru = $request->_post[sprintf('image_item_description_ru_%s', $postfix)];
			$image_item->description_br = htmlspecialchars ($request->_post[sprintf('image_item_description_br_%s', $postfix)]);
			$image_item->image->halign = $request->_post[sprintf ('image_item_halign_%s', $postfix)];
			$image_item->topic_id = $request->_post[sprintf ('image_item_topic_id_%s', $postfix)];
			$image_item->publish_right = !$real_id || $request->_post[sprintf ('image_item_publish_right_%s', $postfix)] ? 1 : 0;
			$image_item->image_gen = $request->_post[sprintf ('image_gen_%s', $postfix)] ? 1 : 0;

			$file_info_param = sprintf('image_item_image_%s', $postfix);
			$file_pv_info_param = sprintf('image_item_image_pv_%s', $postfix);

			if (is_uploaded_file ($tmp_image = $request->_post_files[$file_info_param]['tmp_name'])) {
				$file_info = explode ('.', basename ($request->_post_files[$file_info_param]['name']));
				$ext = $file_info[count ($file_info) - 1];
				
				if ($image_item->image->file && file_exists ($image_item->image->file))
					unlink ($image_item->image->file);

				if ($real_id) {
					$image_item->image->image = sprintf ('%s/image_items_ppv/%s.%s', MY_FILES_FOLDER, sprintf ('image_%s_%s', $image_item->link_class, $real_id), $ext);
				}

				$image_item->update ();

				if (! $real_id) {
					$image_item->id = $real_id = $image_item->factory->mysql->insert_id ();

					$image_item->image->image = sprintf ('%s/image_items_ppv/%s.%s', MY_FILES_FOLDER, sprintf ('image_%s_%s', $image_item->link_class, $image_item->id), $ext);

					$image_item->update ();
				}

				$to_file = sprintf ('%s/%s', MY_REL_ROOT_PATH, $image_item->image->image);
				move_uploaded_file ($tmp_image, $to_file);
				if ($request->config->chmod_available)
					chmod ($to_file, $request->config->chmod_upload);

				$image_item->image->update ();
				$image_item->image->just_uploaded = TRUE;
			}

			if (! $real_id)
				return FALSE;

			if (is_uploaded_file ($tmp_image = $request->_post_files[$file_pv_info_param]['tmp_name'])) {
				$file_info = explode ('.', basename ($request->_post_files[$file_pv_info_param]['name']));
				$ext = $file_info[count ($file_info) - 1];
				
				if ($image_item->image_pv->file && file_exists ($image_item->image_pv->file))
					unlink ($image_item->image_pv->file);

				$image_item->image_pv->image = sprintf ('%s/image_items_ppv/%s.%s', MY_FILES_FOLDER, sprintf ('image_pv_%s_%s', $image_item->link_class, $image_item->id), $ext);

				// $image_item->update ();

				$to_file = sprintf ('%s/%s', MY_REL_ROOT_PATH, $image_item->image_pv->image);
				move_uploaded_file ($tmp_image, $to_file);
				if ($request->config->chmod_available)
					chmod ($to_file, $request->config->chmod_upload);

				$image_item->image_pv->update ();
				$image_item->image_pv->just_uploaded = TRUE;
			}
			$image_item->image_pv->image_gen = $image_item->image_gen;


			if ($image_item->image->image && ($image_item->image_pv->image_gen || !$image_item->image_pv->image)) {
				if ($this->w || $this->h)
					if ($this->resize_only) 
						$res = $image_item->image->_resizeImage ( $this->w, $this->h );
					else
						$res = $image_item->image->_resizeAndCutImage ( $this->w, $this->h );

				if (! $image_item->image_pv->just_uploaded) {
					if ($image_item->image_pv->file && file_exists ($image_item->image_pv->file)) {
						unlink ($image_item->image_pv->file);
					}

					$res = $image_item->image_pv->copy_resized ($image_item->image, $this->pv_w, $this->pv_h, MY_FILES_FOLDER.'/image_items_ppv', sprintf ('image_pv_%s_', $image_item->link_class), $image_item->id, $this->pv_resize_only);

					if (! $res) {
						$this->copy_default_preview ($image_item);
					}
				}

				$image_item->image_pv->just_uploaded = TRUE;
			}

			if ($image_item->image_pv->just_uploaded || ($image_item->image_pv->image && !$image_item->image_ppv->image)) {
				if ($image_item->image_ppv->file && file_exists ($image_item->image_ppv->file)) {
					unlink ($image_item->image_ppv->file);
				}

				$res = $image_item->image_ppv->copy_resized ($image_item->image_pv, $this->ppv_w, $this->ppv_h, MY_FILES_FOLDER.'/image_items_ppv', sprintf ('image_ppv_%s_', $image_item->link_class), $image_item->id, $this->ppv_resize_only);

				if (! $res) {
					$this->copy_default_prepreview ($image_item);
				}
				$save = TRUE;
			}

			$image_item->update ();
		}

		if ($image_id_list && $this->max_images && count ($image_id_list) > $this->max_images) {
			$image_list = $this->create_all (1000, $this->max_images);

			if ($image_list)
			for ($i = 0; $i < count ($image_list); $i++) {
				$image_item = &$image_list[$i];
				// print $image_item->id . "<br>";
				
				if ($image_item->image->file && file_exists ($image_item->image->file))
					unlink ($image_item->image->file);
				if ($image_item->image_pv->file && file_exists ($image_item->image_pv->file))
					unlink ($image_item->image_pv->file);
				if ($image_item->image_ppv->file && file_exists ($image_item->image_ppv->file))
					unlink ($image_item->image_ppv->file);

				$image_item->delete ();
			}
		}
	} 

	function copy_default_preview (&$image_item) {
	    $default = new Image ();
		$default->load_image ($this->default_preview);

		$image_item->image_pv->copy_resized ($default, $this->pv_w, $this->pv_h, MY_FILES_FOLDER.'/image_items_ppv', sprintf ('image_pv_%s_', $image_item->link_class), $image_item->id, $this->pv_resize_only);
	}

	function copy_default_prepreview (&$image_item) {
	    $default = new Image ();
		$default->load_image ($this->default_preview);

		$image_item->image_pv->copy_resized ($default, $this->ppv_w, $this->ppv_h, MY_FILES_FOLDER.'/image_items_ppv', sprintf ('image_ppv_%s_', $image_item->link_class), $image_item->id, $this->ppv_resize_only);
	}


	function delete_old_images () {
		$row = $this->mysql->select (sprintf ("SELECT image, image_pv, image_ppv FROM %s", $this->table_name));

	    $image_ppv = new Image ();
	    $image_pv = new Image ();
	    $image = new Image ();
		$images = array ();

		$dir = sprintf ('%s/%s/image_items_ppv', MY_REL_ROOT_PATH, MY_FILES_FOLDER);
		if ($dh = opendir ($dir)) {
			while (($file = readdir ($dh)) !== FALSE) {
				if (preg_match ('/^\./', $file))
					continue;

				$images[$file] = $dir . '/' . $file;
			}
			closedir ($dh);
		}

		if (sizeof ($row) > 0)
		foreach ($row as $i => $item) {
			$image_ppv->unpack_string ($item->image_ppv);
			$image_pv->unpack_string ($item->image_pv);
			$image->unpack_string ($item->image);
 
			if ($image_ppv->image)
				if (getArrayValue (basename ($image_ppv->image), $images)) 
					unset ($images[basename ($image_ppv->image)]);

			if ($image_pv->image)
				if (getArrayValue (basename ($image_pv->image), $images)) 
					unset ($images[basename ($image_pv->image)]);

			if ($image->image)
				if (getArrayValue (basename ($image->image), $images)) 
					unset ($images[basename ($image->image)]);
		}

		$res = 0;
		foreach ($images as $file => $filepath) {
			// print $filepath . "<br>";
			if (file_exists ($filepath)) {
				unlink ($filepath);
				$res++;
			}
		}

		return $res;
	}

	var $tlist = array ();

	function topic_select_button ($name, $key, $add_empty_field = FALSE) {
		$tlist = array ();
		if (is_object ($this->topic_factory))
			$tlist = $this->topic_factory->get_topic_list ();

		if ($add_empty_field !== FALSE) {
			$list = array ('' => is_string ($add_empty_field) ? $add_empty_field : '');
			foreach ($tlist as $id => $val) 
				$list[$id] = $val;
		} else
			$list = &$tlist;

		return common_select_button ($name, $list, $key, "", "", "width:344px;");
	}

	var $portfolio_topic_id = NULL;
	var $excludeIDs = NULL;

	function &create_relative_portfolio_list ($topic_id, $excludeIDs = NULL, $limit = NULL, $offset = NULL) {
		$this->portfolio_topic_id = $topic_id;
		$this->excludeIDs = $excludeIDs;
		$list = $this->create_all ($limit, $offset);
		$this->portfolio_topic_id = NULL;
		return $list;
	}

	function conditions () {
		parent::conditions ();
		if ($this->portfolio_topic_id) {
			$this->join[] = " LEFT JOIN portfolio_work as pw ON t.link_id = pw.id AND t.link_class = 'portfolio'";
			$this->cond[] = sprintf ("t.topic_id='%d'", $this->portfolio_topic_id);
			$this->cond[] = "t.publish_right > 0";
			if (is_array ($this->excludeIDs))
				$this->cond[] = sprintf ("t.id NOT IN (%s)", join (',',$this->excludeIDs));

			$this->order = array ("pw.date DESC", "pw.ord_index ASC", "t.id DESC");
		}
	}
}




?>