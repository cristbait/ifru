<?PHP

class Factory {
	var $config;
	var $request;
	var $language;
	var $class_name;
	var $cache;	

	function Factory (&$config, &$request, $class_name = NULL) {
		$this->config = &$config;
		$this->request = &$request;
		$this->language = &$request->language;
		$this->class_name = $class_name;
	}
	
	function &create ($id, $load = true) {
		if ($this->in_cache ($id))
			$obj = $this->from_cache ($id);
		else
			$obj = $this->create_new ($id, $load);
			
		return $obj;
	}
	
	function clear_cache ($id_list) {
		foreach ($id_list as $id)
			unset ($this->cache[$id]);
	}
	
	function &create_new ($id, $load) {
		if (!$this->class_name)
			trigger_error (sprintf ("[%s::create_new()]: method must be overriden!", get_class ($this)), E_USER_ERROR);
			
		return $this->to_cache ($id, new $this->class_name ($id, $factory));
	}
	
	function in_cache ($id) {
		return isset ($this->cache[$id]);
	}
	
	function &from_cache ($id) {
		if (!$this->in_cache ($id))
			trigger_error (sprintf ("[%s::from_cache()]: object %s is not in cache!", get_class ($this), $id), E_USER_ERROR);
			
		$obj = &$this->cache[$id];
		return $obj;
	}
	
	function &to_cache ($id, &$object) {
		if ($this->in_cache ($id))
			trigger_error (sprintf ("[%s::to_cache()]: object %s is already in cache!", get_class ($this), $id), E_USER_ERROR);

		$this->cache[$id] = &$object;
		return $this->cache[$id];
	}
}

?>