<?PHP

include_once ('engine/language.inc');

class EngineRequest {
	var $config;

	var $_get;
	var $_post;
	var $_post_files;
	var $_cookie;
	var $_session;
	var $param;

	var $has_session;
	
	var $lang;
	var $language;
		
	var $action;
	var $referer;
	var $url;
	var $selected_id;
	var $id;

	var $rewrite_pool;

	function EngineRequest (&$config) {
		$this->config = &$config;

		$this->_get = &$_GET;
		$this->_post = &$_POST;
		$this->_post_files = &$_FILES;
		$this->_cookie = &$_COOKIE;
		$this->_session = &$_SESSION;
		$this->param = array ();

		if (! empty ($this->_session)) $this->param = array_merge ($this->param, $this->_session);
		if (! empty ($this->_cookie)) $this->param = array_merge ($this->param, $this->_cookie);
		if (! empty ($this->_post_files)) $this->param = array_merge ($this->param, $this->_post_files);
		if (! empty ($this->_get)) $this->param = array_merge ($this->param, $this->_get);
		if (! empty ($this->_post)) $this->param = array_merge ($this->param, $this->_post);
//		$this->param = array_merge ($this->_session, $this->_cookie, $this->_post_files, $this->_get, $this->_post);

		$this->load_default_parameters ();
		$this->load_parameters ();
	}
	
	function load_default_parameters () {
		// start_session ();
		$svar = $this->config->session_var;
		$this->has_session = getArrayValue ($svar, $this->_cookie) || getArrayValue ($svar, $this->_get) || getArrayValue($svar, $this->_post) ? TRUE : FALSE;

		if ($this->config->enable_language) {
			$this->lang = $this->register_session_var ('lang', $this->_get, $this->config->default_language);
		} else {
			$this->lang = $this->config->default_language;
		}
		$this->language = new Language ($this->config, $this);
		
		$this->action = getArrayValue ('_action', $this->_post) ? $this->_post['_action'] : getArrayValue ('_action', $this->_get);
		$this->referer = &$GLOBALS['HTTP_REFERER'];
		$this->url = getArrayValue ('url', $this->_get);
		$this->selected_id = getArrayValue ('selected_id', $this->_get);
		$this->id = getArrayValue ('id', $this->_post);
	}
	
	function register_session_var ($name, &$override_src, $default_value = NULL) {
		if (isset ($override_src[$name]))
			$this->_session[$name] = $override_src[$name];

		$value = isset ($this->_session[$name]) ? $this->_session[$name] : $default_value;

		session_register ($name);
		// $_SESSION[$name] = $value;

		return $value;
	}
	
	function load_parameters () {
		if (get_magic_quotes_gpc ()) {
			if ($this->_post)
				foreach ($this->_post as $id => $value)
					if (is_string ($value))
						$this->_post[$id] = stripslashes ($value);

			if ($this->_get)
				foreach ($this->_get as $id => $value)
					if (is_string ($value))
						$this->_get[$id] = stripslashes ($value);

			if ($this->_cookie)
				foreach ($this->_cookie as $id => $value)
					if (is_string ($value))
						$this->_cookie[$id] = stripslashes ($value);
		}
		// јбстрактный метод; ничего не делаем.
	}
}


function start_session () {
	if (! session_id())
		session_start ();
	
	setcookie ("PHPSESSID", session_id (), time () + 3600 * 24 * 30);
}

function stop_empty_session () {
	if (empty ($_SESSION) && session_id()) {
		session_unset ();
		session_destroy ();
		setcookie ("PHPSESSID", '', time ());
	}
}

$request = new EngineRequest ($config);

?>