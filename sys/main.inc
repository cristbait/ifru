<?PHP

include_once ('engine/email.inc');
class MainPage {
	var $pager = '';
	var $pager_obj = NULL;

	var $index = FALSE;
	var $main2 = FALSE;
	var $test = FALSE;

	var $use_variables = FALSE;
	var $page_name = '';
	var $page_title = '';
	var $page_image = '';
	var $url_path = '';
	var$right_menu = array ();

	var $root_url = '/';

	var $right_menu_block = '';
	var $right_menu_block_add = '';
	var $portfolio_scrool = FALSE;
	var $content = '';
	var $content_right = '';
	var $content_left = '';

	var $right_float = '';
	var $show_right = FALSE;
	var $cols2 = FALSE;


	var $replaceBlockCounter = 1;

	var $meta_keywords = '';
	var $meta_description = '';

	function MainPage (&$config, &$request) {
		$this->config = &$config;
		$this->request = &$request;
		$this->dbc = $this->config->getDBConfig ();
	}

	function parse () {
		global $meta_keywords, $meta_description;

		$this->show_right = FALSE;

		$this->tpl = new Template ($this->config, MY_TPL_PATH);

		if ($this->index) {
			$this->tpl->set_file ('main', 'index.html');
		} else {
			$this->tpl->set_file ('main', 'main.html');
		}


		$url = new URL ($_SERVER["REQUEST_URI"]);
//        var_dump($this->request->_post['message']);
        if ($this->request->_post['message'])
        {
            $emails = $this->dbc->get_var ("contact_mail");
			$emails = explode(", ", $emails);
			$from['name'] = $this->dbc->get_var ("mail_from");
			$from['email'] = $this->dbc->get_var ("mail_for_order");
			$user = $this->request->_post ["name"];
            $phone = $this->request->_post ["phone"];
            $user_email = $this->request->_post ["email"];
            $message = $this->request->_post ["message"];
			global $config;
			$mysql = $config->getDB();
			$select = "SELECT `AUTO_INCREMENT` FROM  INFORMATION_SCHEMA.TABLES 
					  WHERE TABLE_SCHEMA = 'p8271_testcentr01' AND TABLE_NAME   = 'feedback'";
			$rows = $mysql->select ($select);
			$feedback_id = $rows[0]->AUTO_INCREMENT;
			$subj = '������ �' . $feedback_id;

			$body = '��� ������������: ' . $user .
                '<br>���������� �������: ' . $phone .
                '<br>E-mail: ' . $user_email .
                '<br>���������: ' . $message;
            if (EmailSend ($from, $emails, $subj, nl2br ($body), '', ''))
			{
				$q_sql = "INSERT INTO `feedback`(`user`, `phone`, `email`, `mes`) VALUES ('$user', '$phone', '$user_email', '$message')";
				$mysql->command ($q_sql);
				$this->tpl->set_var ('message_success', true);
			};
        }
		if ($this->request->_get['print'] == 'yes') {
			$this->tpl->set_file ('main', 'print.html');

			$url->set_var ('print', 'no');
			$this->tpl->set_var ('noprint_href', $url->build ());
		} else {
			$url->set_var ('print', 'yes');
			$this->tpl->set_var ('print_href', $url->build ());
		}

		$this->tpl->set_var ('content', $this->content);
		$this->tpl->set_var ('root_url', $this->root_url);
		$this->tpl->set_var ('content_right', $this->content_right);
		$this->tpl->set_var ('content_left', $this->content_left);


		if (isset ($meta_keywords))
			$this->meta_keywords = $meta_keywords;
		if (isset ($meta_description))
			$this->meta_description = $meta_description;


		//$this->tpl->set_file ('copyright', 'copyright.html');
		//$this->tpl->parse ('copyright', 'copyright');

		$this->tpl->set_file ('counters', 'counters.html');
		$this->tpl->parse ('counters', 'counters');
		$this->tpl->set_var ('vk_link', $this->dbc->get_var ("url_vk"));
		$this->tpl->set_var ('fb_link', $this->dbc->get_var ("url_fb"));
		$this->tpl->set_var ('yt_link', $this->dbc->get_var ("url_yt"));

		if (MY_LOCAL)
			$this->tpl->set_var ('counters', '');

		$this->update_pager ();
		$this->tpl->set_var ('pager', $this->pager);

		# RIGHT MENU 
		$this->tpl->set_block ('main', 'right_menu', 'right_menu_tpl');
		$this->tpl->set_var ('right_menu_tpl', '');
		if (!empty ($this->right_menu)) {
			$this->tpl->set_block ('right_menu', 'right_menu_row', 'right_menu_rows');

			foreach ($this->right_menu as $item) {
				$this->tpl->set_var ('structure_page_address', $item['url']);
				$this->tpl->set_var ('active', $item['active'] ? 'active' : '');
				$this->tpl->set_var ('structure_name', $item['name']);

				$this->tpl->parse ('right_menu_rows', 'right_menu_row', TRUE);
			}

			$this->tpl->parse ('right_menu_tpl', 'right_menu');
		}


		if (is_object ($this->structure)) {
			$struct = &$this->structure;
			$stack = &$struct->rewrite_stack;

			$last = &$struct->getLast ();
			$last->real_name = $last->name;

			if ($mk = $last->get_meta_keywords ())
				$this->meta_keywords = $mk;
			if ($md = $last->get_meta_description ())
				$this->meta_description = $md;

			if (! $this->use_variables) {
				$urlpath = $last->getFullUrlPath ();
				// $urlpath = $struct->getStackUrlPath ();
				$namepath = $last->namepath;

				// array_pop ($urlpath);
				array_unshift ($urlpath, '<a href="/">�������</a>');

				if ($index = &$struct->load_by_class_1 ('index'))
					array_unshift ($namepath, $index->name);

				if ($t = join (' / ', $urlpath)) {
					$this->tpl->set_var ('main_path', $t);
				//	$this->tpl->parse ('main_path_blocks', 'main_path_block');
				}
			//print_arr($last);die();
				if ($page_title = $last->get_page_title ()) 
					array_unshift ($namepath, $page_title);
				$this->tpl->set_var ('page_title', join (' - ', array_reverse($namepath)));

				$h1 = $last->page_h1 ? $last->page_h1 : $last->name;
				
				$this->tpl->set_var ('main_h1', $h1);
				// $this->tpl->set_var ('page_name', $name);
			}


			if ($this->index) {
				$this->tpl->set_var ('page_title', $last->get_page_title ());

				# CENTER MENU

				// $center = &$struct->load_by_class ('top-index');
				$center = &$struct->load_by_document_type ('subsform%');
				$this->tpl->set_block ('main', 'activetest_row', 'activetest_rows');
				$this->tpl->set_block ('activetest_row', 'icon', 'icon_tpl');
				$this->tpl->set_block ('activetest_row', 'noicon', 'noicon_tpl');
				$this->tpl->set_var ('activetest_rows', '');
				$this->tpl->set_var ('noicon_tpl', '');
				foreach ($center as $i => $dummy) {
					$node = &$center[$i];

					$node->name = strip_tags ($node->name);
					$node->content_left = nl2br ($node->content_left);
					$node->set_template_vars ($this->tpl);

//					$this->tpl->set_var ('registration_status', $node->closed ? '����������� �������' : '���� �����������');
					$this->tpl->set_var ('registration_status', $node->getExtendedStatus ());
					$this->tpl->set_var ('registration_css', $node->statusActive ? '' : '2');

					if ($node->image1->image) {
						$this->tpl->parse ('icon_tpl', 'icon', FALSE);						
					} else {
						$this->tpl->parse ('icon_tpl', 'noicon', FALSE);						
					}
					$this->tpl->parse ('activetest_rows', 'activetest_row', TRUE);
				}

			} elseif (! $this->index) {    			
			# start IF NOT INDEX
				# TOP MENU

				/*
				$top = &$struct->load_by_class (array ('top', 'top-index'));
				$this->tpl->set_block ('main', 'top_block', 'top_blocks');
				$this->tpl->set_var ('top_blocks', '');
				$this->tpl->set_block ('top_block', 'top_submenu_block', 'top_submenu_blocks');
				$this->tpl->set_block ('top_submenu_block', 'top_menu_row', 'top_menu_rows');
				$this->tpl->set_block ('main', 'right_menu_block', 'right_menu_blocks');
				$this->tpl->set_block ('right_menu_block', 'right_menu_row', 'right_menu_rows');

				foreach ($top as $i => $dummy) {
					$node = &$top[$i];

					$nchildren = &$node->load_children (TRUE);
					$this->tpl->set_var ('top_submenu_blocks', '');
					$this->tpl->set_var ('top_menu_rows', '');
					$this->tpl->set_var ('right_menu_rows', '');
					if ($nchildren) {
						foreach ($nchildren as $j => $dummy) {
							$nnode = &$nchildren[$j];
							$this->tpl->set_var ('right_menu_rows_tmp', '');
							$sub = false;

							# submenu
//							if ($node->is_active ()) {
							if (1) {
								$schildren = &$nnode->load_children (TRUE);
								if ($schildren)
								foreach ($schildren as $j => $dummy) {
									$snode = &$schildren[$j];
									$this->tpl->set_var ('right_menu_rows_tmp2', '');
									$sub2 = false;


									# submenu2
									if (1) {
										$s2children = &$snode->load_children (TRUE);
										if ($s2children)
										foreach ($s2children as $j => $dummy) {
											$s2node = &$s2children[$j];

											$this->tpl->set_var ('sub', 'submenu2');
											if ($s2node->is_active ()) {
												$this->tpl->set_var ('active', 'active');
												$this->tpl->set_var ('slash', '/');
												$sub2 = true;
											} else {
												$this->tpl->set_var ('active', '');
												$this->tpl->set_var ('slash', '');
											}

											if ($s2node->is_seminar (TRUE)) {
												$this->tpl->set_var ('active', "opened " . $this->tpl->get_var ('active'));
											}

											$s2node->set_template_vars ($this->tpl);
											$this->tpl->parse ('right_menu_rows_tmp2', 'right_menu_row', TRUE);
										}
									}
									# end submenu2

									$this->tpl->set_var ('sub', 'submenu');
									if ($snode->is_active ()) {
										$this->tpl->set_var ('active', $sub2 ? '' : 'active');
										$this->tpl->set_var ('slash', '/');
										$sub = true;
									} else {
										$this->tpl->set_var ('active', '');
										$this->tpl->set_var ('slash', '');
									}

									if ($snode->is_seminar (TRUE)) {
										$this->tpl->set_var ('active', "opened " . $this->tpl->get_var ('active'));
									}

									$snode->set_template_vars ($this->tpl);
									$this->tpl->parse ('right_menu_rows_tmp', 'right_menu_row', TRUE);

									$this->tpl->set_var ('right_menu_rows_tmp', $this->tpl->get_var ('right_menu_rows_tmp') . $this->tpl->get_var ('right_menu_rows_tmp2'));

								}
							}
							# end submenu

							$this->tpl->set_var ('sub', '');
							if ($nnode->is_active ()) {
								$this->tpl->set_var ('active', $sub ? '' : 'active');
								$this->tpl->set_var ('slash', '/');
							} else {
								$this->tpl->set_var ('active', '');
								$this->tpl->set_var ('slash', '');
							}
							if ($nnode->is_seminar (TRUE)) {
								$this->tpl->set_var ('active', "opened " . $this->tpl->get_var ('active'));
							}

							$nnode->set_template_vars ($this->tpl);
							$this->tpl->parse ('right_menu_rows', 'right_menu_row', TRUE);
							if ($nnode->is_active ())
								$this->tpl->set_var ('active', 'active');
							$this->tpl->parse ('top_menu_rows', 'top_menu_row', TRUE);

							// if ($node->is_active ())
								$this->tpl->parse ('right_menu_rows', 'right_menu_rows_tmp', TRUE);
						}

						$this->tpl->parse ('top_submenu_blocks', 'top_submenu_block');

						if ($node->is_active ())
							$this->tpl->parse ('right_menu_blocks', 'right_menu_block');
					}

					if ($node->is_active ()) {
						$this->tpl->set_var ('slash', '/');
						$this->tpl->set_var ('active', 'active');
					} else {
						$this->tpl->set_var ('slash', '');
						$this->tpl->set_var ('active', '');
					}

					$node->set_template_vars ($this->tpl);
					$this->tpl->parse ('top_blocks', 'top_block', TRUE);
				}
				*/

				# RIGHT MENU BLOCK

//				if ($this->right_menu_block)
//					$this->tpl->set_var ('right_menu_blocks', $this->right_menu_block);

//				if ($this->right_menu_block_add)
//					$this->tpl->set_var ('right_menu_blocks', 
//						$this->tpl->get_var ('right_menu_blocks').$this->right_menu_block_add);

//				if ($this->content_right)
//					$this->tpl->set_var ('right_menu_blocks', "<br><br><br>".$this->tpl->get_var ('right_menu_blocks'));
				

				/*

				
				$left = &$struct->load_by_class ('left');
				$this->tpl->set_var ('content_rowspan', sizeof ($left) + 1);
				$this->tpl->set_block ('main', 'left_block', 'left_blocks');
				$this->tpl->set_var ('left_blocks', '');
				$this->tpl->set_block ('left_block', 'left_menu_row', 'left_menu_rows');

				foreach ($left as $i => $dummy) {
					$this->tpl->set_var ('row_number', 3 + $i);
					$node = &$left[$i];
					$this->tpl->set_var ('last_row_id', $i+1 == sizeof ($left) ? 'id="last_row"' : '');

					$nchildren = &$node->load_children (TRUE);
					if ($nchildren) {
						$this->tpl->set_var ('left_menu_rows', '');
						foreach ($nchildren as $j => $dummy) {
							$nnode = &$nchildren[$j];
							$this->tpl->set_var ('subrows_temp', '');

							if ($nnode->is_active ()) {
								$sub_active = FALSE;

								// ACTIVE SUBMENU
								$schildren = $nnode->load_related_children (TRUE);
								
								if (! empty ($schildren) && $nnode->mmenu != 7)
								foreach ($schildren as $k => $dummy) {
									$snode = &$schildren[$k];
									$snode->set_template_vars ($this->tpl);

									if ($snode->is_active ()) {
										$this->tpl->set_var ('active', 'active');
										$sub_active = TRUE;
									} else {
										$this->tpl->set_var ('active', '');
									}
									$this->tpl->set_var ('subclass', 'sub');
									$this->tpl->parse ('subrows_temp', 'top_menu_row', TRUE);
								}

								// END ACTIVE SUBMENU

								$this->tpl->set_var ('active', $sub_active ? '' : 'active');
							} else {
								$this->tpl->set_var ('active', '');
							}

							$nnode->set_template_vars ($this->tpl);
							$this->tpl->set_var ('subclass', '');
							$this->tpl->parse ('left_menu_rows', 'left_menu_row', TRUE);

							if ($nnode->is_active ())
								$this->tpl->parse ('top_menu_rows', 'subrows_temp', TRUE);
						}
					}
					
					$node->set_template_vars ($this->tpl);
					$this->tpl->parse ('left_blocks', 'left_block', TRUE);
				}
			*/

			# end IF NOT INDEX
			} 

		}

		$this->tpl->set_var ('main_meta_title', $this->dbc->get_var ("metaa_title"));
		$this->tpl->set_var ('main_meta_keywords', $this->dbc->get_var ("meta_keywords"));
		$this->tpl->set_var ('main_meta_description', $this->dbc->get_var ("meta_description"));

		$this->tpl->parse ('menu', 'menu', FALSE);
		$this->tpl->parse ('main', 'main', FALSE);
	}


	function create_right_menu ($rootId, $menuclass = 'right', $init = null) {
		if (is_array ($init))
			$this->right_menu[] = $init;
		else
			$this->right_menu = array ();

//		$item = array();
//		$item['name'] = '������ ���������';
//		$item['url'] = $node->get_page_address();
//		$item['active'] = $node->is_active();

		$menu = &$this->structure->load_by_class (array ($menuclass), $rootId);
		if (is_array($menu) || is_object($menu)) {
			foreach ($menu as $i => $dummy) {
				$node = &$menu[$i];
				$item = array();
				$item['name'] = $node->name;
				$item['url'] = $node->get_page_address();
				$item['active'] = $node->is_active();

				$this->right_menu[] = $item;
			}
		}
//		print_r ($this->right_menu);

		return $this->right_menu;
	}

	function create_cabinet_menu ($rootId, $testRootActive = FALSE) {
		$node = $this->structure->create ($rootId);
		$init = array (
			"name"	=>	"����� ������������",
			"url"	=>	$node->get_page_address () . "personal/",
			"active"	=>	$testRootActive
		);

		$this->create_right_menu ($rootId, "right", $init);
	}

	function get_banner_212 ($item) {
		$tpl = new Template ($this->config, MY_TPL_PATH);
		$tpl->set_file ('wrapper', 'wrapper212.html');
		$tpl->set_block ('wrapper', 'image', 'dummy');
		$tpl->set_block ('wrapper', 'flash', 'dummy');
		$tpl->set_block ('wrapper', 'image_flash', 'dummy');
		$tpl->set_var ('dummy', '');

//print $this->replaceBlockCounter++ . " ";
		$item->set_template_vars ($tpl);
		// $tpl->set_var ('title', sprintf('%s<br><a href="%s">%s</a>', $item->title, $item->url, $item->url));

		if ($item->image->image && $item->flash->flash) {
			$tpl->set_var ('cnt', $this->replaceBlockCounter++);
			$tpl->set_var ('flash_path', $item->flash->flash);
			$tpl->set_var ('image_path', $item->image->image);
			$tpl->parse ('wrapper', 'image_flash');
		} elseif ($item->flash-flash) {
			$tpl->set_var ('flash_path', $item->flash->flash);
			$tpl->parse ('wrapper', 'flash');
		} elseif ($item->image->image) {
			$tpl->set_var ('image_path', $item->image->image);
			$tpl->parse ('wrapper', 'image');
		}
		
		return $tpl->get_var ('wrapper');
	}

	function parse_list (&$list, $announces = FALSE) {
		$tpl = new Template ($this->config, MY_TPL_PATH);
		$tpl->set_var ('lists', '');

		if ($list) {
			$tpl->set_file ('list', 'structure_list.html');
			$tpl->set_block ('list', 'list_row', 'list_rows');
			$tpl->set_block ('list_row', 'list_row_children_block', 'list_row_children_blocks');
			$tpl->set_block ('list_row_children_block', 'list_row_child', 'list_row_children');
			$tpl->set_var ('list_rows', '');

			foreach ($list as $i => $dummy) {
				$node = &$list[$i];

				// CHILDREN
				$tpl->set_var ('list_row_children_blocks', '');
				$nchildren = &$node->load_children (TRUE);
				if ($nchildren) {
					$tpl->set_var ('list_row_children', '');
					foreach ($nchildren as $j => $dummy) {
						$nnode = &$nchildren[$j];
						$nnode->set_template_vars ($tpl);
						$tpl->parse ('list_row_children', 'list_row_child', TRUE);
					}
					$tpl->parse ('list_row_children_blocks', 'list_row_children_block');
				}

				$node->set_template_vars ($tpl);
				$tpl->parse ('list_rows', 'list_row', TRUE);
			}

			$tpl->parse ('lists', 'list', FALSE);
			
		}
		return $tpl->get_var ('lists');
	}



	function output () {
		$this->parse ();
		$this->tpl->p_default ();
	}
	
	function get_pager () {
		$this->update_pager ();
		return $this->pager;
	}
	
	function update_pager () {
		if ($this->pager_obj && $this->pager_obj->page_count > 1) {
			$pager_tpl = new Template ($this->config, MY_TPL_PATH);
			$pager_tpl->set_file ('main_pager', 'main_pager.html');
	
			$pager_tpl->set_block ('main_pager', 'main_pager_page', 'main_pager_pages');
			$pager_tpl->set_var ('main_pager_pages', '');
			$pager_tpl->set_block ('main_pager_page', 'main_pager_separator', 'page_separator');
			$pager_tpl->set_block ('main_pager_page', 'main_pager_page_number', 'page_number');
			$pager_tpl->set_block ('main_pager_page_number', 'page_current_number', 'dummy');
			$pager_tpl->set_block ('main_pager_page_number', 'page_common_number', 'dummy');
			$pager_tpl->set_var ('dummy', '');
	
			for ($i = 0; $i < $this->pager_obj->page_count; $i++) {
				$page = $this->pager_obj->get_page ($i);
				$is_last_page = ($i == ($this->pager_obj->page_count - 1));
				$is_current_page = ($page->pager->current->index == $i);

				$pager_tpl->set_var ('page_number_value', $page->index + 1);
				$pager_tpl->set_var ('page_url', $page->href);
				$pager_tpl->set_var ('page_number', $is_current_page ? $pager_tpl->get_var ('page_current_number') : $pager_tpl->get_var ('page_common_number'));
				$pager_tpl->set_var ('page_separator', ($i == ($this->pager_obj->page_count - 1)) ? '' : $pager_tpl->get_var ('main_pager_separator'));
		
				$pager_tpl->set_var ('current_page', $pager_tpl->get_var ('main_pager_page'));
				$pager_tpl->parse ('current_page', 'current_page', FALSE);
				$pager_tpl->parse ('main_pager_pages', 'current_page', TRUE);
			}
	
			$left_sgn = "&lt;&lt;";
			$right_sgn = "&gt;&gt;";

			if ($page = $this->pager_obj->get_page ($this->pager_obj->current->index - 1)) {
				$pager_tpl->set_var ('left_side_navigation', sprintf ('<a href="%s">%s</a>', $page->href, $left_sgn));
			} else {
				$pager_tpl->set_var ('left_side_navigation', "");
			}
			if ($page = $this->pager_obj->get_page ($this->pager_obj->current->index + 1)) {
				$pager_tpl->set_var ('right_side_navigation', sprintf ('<a href="%s">%s</a>', $page->href, $right_sgn));
			} else {
				$pager_tpl->set_var ('right_side_navigation', "");
			}

			$pager_tpl->parse ('main_pager', 'main_pager', FALSE);
			$this->pager = $pager_tpl->get_var ('main_pager');
		} else
			$this->pager = NULL;
	}
}

global $main_page;
$main_page = new MainPage ($config, $request);

?>
