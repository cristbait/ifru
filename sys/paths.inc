<?PHP
	
	$DEBUG_MODE = TRUE;
	error_reporting (E_ALL ^ E_NOTICE);

	$MYSQL_PROFILE = 'connection';
	// ini_set('zend.ze1_compatibility_mode', true);
	date_default_timezone_set ("Europe/Moscow");

	define ('CHMOD_UPLOAD', 0666);
	define ('MY_ROOT_PATH', realpath (MY_REL_ROOT_PATH));
	define ('MY_SYS_PATH', MY_ROOT_PATH."/sys");
	define ('MY_MODULES_PATH', MY_SYS_PATH."/modules");
	define ('MY_PEAR_PATH', MY_SYS_PATH."/pear");

	define ('MY_PATH_SEPARATOR', strchr (ini_get ('include_path'), ';') ? ";" : ":");
	ini_alter ('include_path', join (MY_PATH_SEPARATOR, array (
//		ini_get ('include_path'),
		'.',
		MY_ROOT_PATH,
		MY_SYS_PATH,
		MY_MODULES_PATH,
		MY_PEAR_PATH
		)));

	define ('MY_FILES_FOLDER', "files");
	define ('MY_FILES_PATH', MY_ROOT_PATH."".MY_FILES_FOLDER);

	define ('MY_TPL_PATH', MY_ROOT_PATH."/template");
	define ('MY_COMMON_TPL_PATH', MY_SYS_PATH."/engine/common");

	define ('MY_ADMIN_PATH', MY_ROOT_PATH."/backend");
	define ('MY_ADMIN_TPL_PATH', MY_ADMIN_PATH."/template");
	define ('MY_BACKUP_PATH', MY_ADMIN_PATH."/backup");
	define ('MY_REL_BACKUP_PATH', MY_REL_ROOT_PATH."/backend/backup");
	
	define ('MY_LOCAL', $_SERVER["HTTP_HOST"] == 'ifru.dev');
	define ('MY_MAGICDESIGN', preg_match ('/testcentr01/i', $_SERVER["HTTP_HOST"]));
	define ('MY_HTTP_ROOT_PATH', "http://" . $_SERVER["HTTP_HOST"]);
	define ('MY_HOST', 'http://' . (MY_LOCAL || MY_MAGICDESIGN ? $_SERVER["HTTP_HOST"] : 'olimp-test.ru'));

	if (! defined ('MY_CONFIG_FILE'))
		define ('MY_CONFIG_FILE', MY_SYS_PATH."/engine.cfg");

	define ('MY_BACKEND_MENU_FILE', MY_SYS_PATH."/menu.xml");

	$script = basename	($_SERVER['SCRIPT_NAME']);

?>