<?php

$dir = ".";

// Open a known directory, and proceed to read its contents
if (is_dir($dir)) {
    if ($dh = opendir($dir)) {
        while (($file = readdir($dh)) !== false) {
			if (preg_match ('/^[.]/', $file)) continue;
			$name = $dir . "/" . $file;
			chmod ($name, 0666);
            echo "$file<br>";
        }
        closedir($dh);
    }
}

print "Ok!";

?> 