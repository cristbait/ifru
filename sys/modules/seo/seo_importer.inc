<?php

require_once ("engine/rss_importer.inc");
require_once ("seo/seo.inc");

class SEOImporter extends RSSImporter {
    var $parentTags = array('CHANNEL', 'ITEM', 'IMAGE', 'TEXTINPUT');

	var $channelTags = array('TITLE', 'LINK', 'DESCRIPTION', 'IMAGE', 'ITEMS', 'TEXTINPUT', 'LASTBUILDDATE', 'LANGUAGE', 'TTL', 'GENERATOR');

	var $itemTags = array('TITLE', 'LINK', 'DESCRIPTION', 'PUBDATE', 'CATEGORY', 'ENCLOSURE', 'YANDEX:FULL-TEXT');

	var $multipleTags = array ('CATEGORY');

	var $seo = NULL;

	function commitChanges () {
		/*
		print '<pre>';
		// print_r ($this->getStructure());
		print_r ($this->getItems());
		return;
		*/

		$this->seo = new SEOProjectFactory ( $this->_config, $this->_request );

		$list = $this->getItems();
		for ( $i=0; $i<sizeof($list); $i++ ) {
			$item = &$list[$i];

			$title = trim ($item['title']);
			if (empty ($title))	continue;

			list ($title, $cy, $pr) = preg_split ('/[();]/', $item['title']);
			$title = iconv ('utf-8', 'windows-1251', $title);
			$cy = trim (preg_replace ('/^[^:]+:/', '', $cy));
			$pr = trim (preg_replace ('/^[^:]+:/', '', $pr));

			$project = $this->_findOrCreate ('project', $title);
			$this->seo->words->sub_id = $project->id;

			$report_id = $project->save_report ($this->_getDateFromPubdate ($item['pubdate']));
			print $item['pubdate'] . "<br>\n";
			$item['description'] = iconv ('utf-8', 'windows-1251', $item['description']);
			$positions = $this->_parsePositions ($item['description']);
			/*
			 print '<pre>';
			 print_r ($positions);
			 print "\n\n";
			*/

			$this->seo->words->preImport ();

			foreach ($positions as $wrd => $engines) {
				if (empty ($wrd))
					continue;

				$word = $this->_findOrCreate ('word', $wrd);
				foreach ($engines as $name => $pos) {
					if (empty ($name))
						continue;

					$engine = $this->_findOrCreate ('engine', $name);

					$word->save_position ($report_id, $engine->id, $pos);
				}			
			}

			$project->save_sitestats ($report_id, $cy, $pr);
		}

		// deleting old
		// $news->deleteOldItems (8);

		$this->postProcceed ();
	}

	function postProcceed () {
	}

	function &_findOrCreate ($type, $name) {
		if ($type == 'project')
			$f = &$this->seo;
		if ($type == 'word')
			$f = &$this->seo->words;
		if ($type == 'engine')
			$f = &$this->seo->engines;

		$item = &$f->find_by_name ($name);

		if (! is_object ($item)) {
			$f->clear_cache (array (NULL, 0));
			$item = $f->create_new (NULL, FALSE);
			$item->load_defaults ($name);
			$item->update ();

		} elseif (! $item->publish) {
			$item->publish = 1;
			$item->update ();
		}

		return $item;
	}

	function _getDateFromPubdate ($pubdate) {
		if (ereg ("^([0-9]{2})\.([0-9]{2})\.([0-9]{4})( [0-9]{2}:[0-9]{2}:[0-9]{2})?", $pubdate, $regs)) {
			$pubdate = $regs[3]."-".$regs[2]."-".$regs[1].$regs[4];
		}
		// $pubdate = preg_replace ('/\./', '-', $pubdate);
		return date ('Y-m-d H:i:s', strtotime ($pubdate));
	}

	function _parsePositions ($positions) {
		$res = array ();

		$keyblocks = split ('<br><br>', $positions);
		foreach ($keyblocks as $keyblock) {
			list ($key, $posblocks) = split ('<br>', $keyblock);
			$key = trim ($key);
			$poses = split (';', $posblocks);

			if (preg_match ('/^������:/', $key)) {
				$zapros = trim (substr ($key, strpos ($key, ':') + 1, strlen ($key)));

				foreach ($poses as $pose) {
					list ($engine, $pos) = split (':', $pose);

					$res[$zapros][trim ($engine)] = trim ($pos);
				}
			}
		}

		return $res;
	}

	/*
	function mapItemToRow ( &$item ) {
		$row = array (
		  'n_ntann_id' => $this->_getIdFromLink ($item['link']),
		  'n_category_id' => $this->_findOrCreateCategory ($item['category'][0]),
		  'n_date' => ,
		  'n_title' => addslashes ($item['title']),
		  'n_text' => addslashes ($item['yandex:full-text']),
		  'n_source' => $item['category'][1],
		  'n_image' => $item['enclosure']['url'],
		);

		return $row;
	}

	function _getIdFromLink ($link) {
		if (preg_match ('/[?&]id=([0-9]+)([?&]|$)/i', $link, $regs))
			return $regs[1];

		return 0;
	}
	*/
}

?>