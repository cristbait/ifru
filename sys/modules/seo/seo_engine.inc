<?PHP

$factory_class = "SEOEngineFactory";

include_once ('seo_word.inc');
include_once ('seo_engine.inc');

class SEOEngineItem extends ListItem {
   function load_from_row ($row) {
        $this->id = $row->id;
        $this->ord_index = $row->ord_index;
        $this->name = $row->name;
 		$this->publish = $row->publish;
    }

	function load_defaults ($name = '') {
		$this->name = $name;
		$this->publish = 1;
		// $this->arch_date = date ('Y-m-d', mktime (0, 0, 0, date ('m') + 3, date ('d'), date ('Y')));
	}
    
	function get_update_array () {
		return array (
			'name' => $this->name,
			'publish' => $this->publish
		);	
	}
	
	function get_insert_array () {
		return array (
			'ord_index' => $this->factory->get_next_index (),
			'name' => $this->name,
			'publish' => $this->publish
		);	
	}

	function load_from_parameters (&$request) {
		$this->name = $request->_post['seo_engine_name'];
		$this->publish = $request->_post['seo_engine_publish'];
	}
	
	function set_template_vars (&$tpl, $form = FALSE) {
		$tpl->set_var ('seo_engine_id', $this->id);
		$tpl->set_var ('seo_engine_ord_index', $this->ord_index);
		$tpl->set_var ('seo_engine_name', $this->name);
		$tpl->set_var ('seo_engine_publish', $this->publish);
		$tpl->set_var ('seo_engine_publish_full', $this->publish ? '��' : '���');

		if ($form) {
			$tpl->set_var ('publish_checkbox', common_checkbox ('seo_engine_publish', 1, $this->publish));
		}
	}
}

class SEOEngineFactory extends ListFactory {

	var $frontend = FALSE;

	function SEOEngineFactory (&$config, &$request) {
		$this->ListFactory ($config, $request, 'seo_engines', 'id', 'ord_index', 'SEOEngineItem', TRUE);
	}

	function &find_by_name ($name) {
		$list = $this->create_from_query (sprintf ("SELECT * FROM %s WHERE name='%s'", $this->table_name, $name));

		if ($list)
			return $list[0];

		$list = NULL;
		return $list;
	}

	function conditions () {
		parent::conditions ();

		if ($this->frontend) $this->cond[] = "publish = 1";
	}
}

?>