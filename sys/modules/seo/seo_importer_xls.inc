<?php

require_once ("seo/seo.inc");

class SEOImporterXLS {
	function SEOImporterXLS ( &$config, &$request, $filename, $onlyStats = FALSE ){
		$this->_config = &$config;
		$this->_request = &$request;
		$this->filename = $filename;
		$this->onlyStats = $onlyStats;

		$this->file = file ($filename);

		$this->seo = new SEOProjectFactory ( $this->_config, $this->_request );
	}

	function commitChanges () {
		if (! $this->file)
			return false;		    

		if ($this->onlyStats)
			$this->commitChangesStats ();
		else
			$this->commitChangesPos ();
	}

	function commitChangesPos () {
		$this->idx = $this->idx1 = $this->idx2 = 0;
		$result = 1;
		$go = 0;
		$i = 0;

		$project = NULL;
		$engine = NULL;
		$dates = array ();
		$reports = array (0,0);
print "AAAAAAAAA!!!!";
exit ();
		foreach ($this->file as $str) {
			$i++;

			if (! $result) 
				break;

			if (! ($str = preg_replace ("/^\s+|\r|\n|\s+$/", "", $str))) 
				continue;

			if ($go == 0 && preg_match ("/^[a-zA-Z]+/", trim ($str))) {
				$go = 1;
			}

			if ($go == 0 || $go >= 10)
				continue;

			$str = trim ($str);

			if (! $str || ! preg_replace ("/[;, ]/", "", $str))
				continue;
	
			$test = preg_replace ("/[;, ]+$/", "", $str);

			if (trim ($test) == 'stop') {
				$go = 10;
				continue;
			}

			if ($go == 1 && preg_match ('/\.ru/', $test)) {
				$project = $this->_findOrCreate ('project', $test);
				$this->seo->words->sub_id = $project->id;
				$go = 2;
			}

			if ($go < 2) continue;

			if ($go == 2 && preg_match ('/freq/', $str)) {
				$dates = split (';', trim ($str));

				for ($k = 2; $k <= sizeof ($dates); $k++) {
					$reports[] = $project->save_report ($this->_getDateFromPubdate ($dates[$k]));
				}

				$go = 3;
			}

			if ($go < 3) continue;

 			if (strpos ($test, ';') === FALSE) {
				$engine = $this->_findOrCreate ('engine', trim ($test));

			} elseif ($engine) {
				$list = split (';', $str);

				if (empty ($list[0]))
					continue;

				$word = $this->_findOrCreate ('word', trim ($list[0]));

				if (! empty ($list[1]) && $list[1] != $word->yandex_freq) {
					$word->yandex_freq = $list[1];
					$word->update ();
				}

				for ($k = 2; $k <= sizeof ($dates); $k++) {
					$word->save_position ($reports[$k], $engine->id, $list[$k]);
				}
			}

			// print ($result ? '+ ' : '- ') . $str . "<BR>";
				
		}

		return $result;
	}

	function commitChangesStats () {
		$this->idx = $this->idx1 = $this->idx2 = 0;
		$result = 1;
		$go = 0;
		$i = 0;

		$project = NULL;
		$engine = NULL;
		$dates = array ();
		$reports = array (0,0);
		$cy = array (0,0);
		$pr = array (0,0);
		$y_sites = array (0,0);
		$y_pages = array (0,0);

		foreach ($this->file as $str) {
			$i++;

			if (! $result) 
				break;

			if (! ($str = preg_replace ("/^\s+|\r|\n|\s+$/", "", $str))) 
				continue;

			if ($go == 0 && preg_match ("/^[a-zA-Z]+/", trim ($str))) {
				$go = 1;
			}

			if ($go == 0 || $go >= 10)
				continue;

			$str = trim ($str);

			if (! $str || ! preg_replace ("/[;, ]/", "", $str))
				continue;
	
			$test = preg_replace ("/[;, ]+$/", "", $str);

			if (trim ($test) == 'stop') {
				$go = 10;
				continue;
			}

			if ($go == 1 && preg_match ('/\.ru/', $test)) {
				$project = $this->_findOrCreate ('project', $test);
				$go = 2;
			}

			if ($go < 2) continue;

			if ($go == 2 && preg_match ('/freq/', $str)) {
				$dates = split (';', trim ($str));

				for ($k = 2; $k <= sizeof ($dates); $k++) {
					$reports[] = $project->save_report ($this->_getDateFromPubdate ($dates[$k]));
				}

				$go = 3;
			}

			if ($go < 3) continue;

			$array = split (';', trim ($str));
			$name = $array[0];

			if (in_array ($name, array ("cy", "pr", "y_sites", "y_pages"))) {
				$arr = &$$name;

				for ($k = 2; $k <= sizeof ($array); $k++) {
					$arr[] = $array[$k];
				}
			}

			// print ($result ? '+ ' : '- ') . $str . "<BR>";
				
		}

		for ($k = 2; $k <= sizeof ($reports); $k++) {
			if ($reports[$k] && $dates[$k])
				$project->save_sitestats ($reports[$k], $cy[$k], $pr[$k], $y_sites[$k], $y_pages[$k]);
		}

		return $result;
	}

	function postProcceed () {
	}

	function &_findOrCreate ($type, $name) {
		if ($type == 'project')
			$f = &$this->seo;
		if ($type == 'word')
			$f = &$this->seo->words;
		if ($type == 'engine')
			$f = &$this->seo->engines;

		$item = $f->find_by_name ($name);

		if (! is_object ($item)) {
			$f->clear_cache (array (NULL, 0));
			$item = $f->create_new (NULL, FALSE);
			$item->load_defaults ($name);
			$item->update ();

		} elseif (! $item->publish) {
			$item->publish = 1;
			$item->update ();
		}

		return $item;
	}

	function _getDateFromPubdate ($pubdate) {
		if (ereg ("^([0-9]{2})\.([0-9]{2})\.([0-9]{4})( [0-9]{2}:[0-9]{2}:[0-9]{2})?", $pubdate, $regs)) {
			$pubdate = $regs[3]."-".$regs[2]."-".$regs[1].$regs[4];
		}
		// $pubdate = preg_replace ('/\./', '-', $pubdate);
		return date ('Y-m-d H:i:s', strtotime ($pubdate));
	}

	function _parsePositions ($positions) {
		$res = array ();

		$keyblocks = split ('<br><br>', $positions);
		foreach ($keyblocks as $keyblock) {
			list ($key, $posblocks) = split ('<br>', $keyblock);

			$key = trim (substr ($key, strpos ($key, ':') + 1, strlen ($key)));

			$poses = split (';', $posblocks);
			foreach ($poses as $pose) {
				list ($engine, $pos) = split (':', $pose);

				$res[$key][trim ($engine)] = trim ($pos);
			}		
		}

		return $res;
	}

	/*
	function mapItemToRow ( &$item ) {
		$row = array (
		  'n_ntann_id' => $this->_getIdFromLink ($item['link']),
		  'n_category_id' => $this->_findOrCreateCategory ($item['category'][0]),
		  'n_date' => ,
		  'n_title' => addslashes ($item['title']),
		  'n_text' => addslashes ($item['yandex:full-text']),
		  'n_source' => $item['category'][1],
		  'n_image' => $item['enclosure']['url'],
		);

		return $row;
	}

	function _getIdFromLink ($link) {
		if (preg_match ('/[?&]id=([0-9]+)([?&]|$)/i', $link, $regs))
			return $regs[1];

		return 0;
	}
	*/
}

?>