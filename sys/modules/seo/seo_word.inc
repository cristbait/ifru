<?PHP

$factory_class = "SEOWordFactory";

class SEOWordItem extends ListItem {
   function load_from_row ($row) {
        $this->id = $row->id;
        $this->project_id = $row->project_id;
        $this->ord_index = $row->ord_index;
        $this->word = $row->word;
        $this->yandex_freq = $row->yandex_freq;
        $this->start_date = $row->start_date;
        $this->end_date = $row->end_date;
		$this->publish = $row->publish;
		$this->alt1 = $row->alt1;
		$this->alt2 = $row->alt2;
		$this->alt3 = $row->alt3;
		$this->cnt = $row->cnt;
    }

	function load_defaults ($name = '') {
		$this->word = $name;
		$this->start_date = date ('Y-m-d');
		$this->end_date = NULL;
		$this->publish = 1;
		$this->cnt = 0;

		$this->project_id = $this->factory->sub_id;
	}
    
	function get_update_array () {
		return array (
			'word' => $this->word,
			'alt1' => $this->alt1,
			'alt2' => $this->alt2,
			'alt3' => $this->alt3,
			'yandex_freq' => $this->yandex_freq,
			'start_date' => $this->start_date,
			'end_date' => $this->end_date,
			'publish' => $this->publish
		);	
	}
	
	function get_insert_array () {
		return array (
			'project_id' => $this->project_id,
			'ord_index' => $this->factory->get_next_index (),
			'word' => $this->word,
			'alt1' => $this->alt1,
			'alt2' => $this->alt2,
			'alt3' => $this->alt3,
			'yandex_freq' => $this->yandex_freq,
			'start_date' => $this->start_date,
			'end_date' => $this->end_date,
			'publish' => $this->publish
		);	
	}

	function load_from_parameters (&$request) {
		$this->word = $request->_post['seo_word_word'];
		$this->start_date = $request->_post['seo_word_start_date'];
		$this->end_date = $request->_post['seo_word_end_date'];
		$this->publish = $request->_post['seo_word_publish'];
		$this->project_id = $request->_post['seo_word_project_id'];
		$this->yandex_freq = $request->_post['seo_word_yandex_freq'];
	}
	
	function set_template_vars (&$tpl, $form = FALSE) {
		$tpl->set_var ('seo_word_id', $this->id);
		$tpl->set_var ('seo_project_id', $this->project_id);
		$tpl->set_var ('seo_word_ord_index', $this->ord_index);
		$tpl->set_var ('seo_word_word', $this->word);
		$tpl->set_var ('seo_word_alt1', $this->alt1);
		$tpl->set_var ('seo_word_alt2', $this->alt2);
		$tpl->set_var ('seo_word_alt3', $this->alt3);
		$tpl->set_var ('seo_word_visits', $this->cnt);
		$tpl->set_var ('seo_word_yandex_freq', $this->yandex_freq);
		$tpl->set_var ('seo_word_start_date', $this->start_date);
		$tpl->set_var ('seo_word_end_date', $this->end_date);
		$tpl->set_var ('seo_word_publish', $this->publish);
		$tpl->set_var ('seo_word_publish_full', $this->publish ? '��' : '���');

		$tpl->set_var ('seo_word_start_date_t', datef ("d.m.Y", $this->start_date));
		$tpl->set_var ('seo_word_end_date_t', datef ("d.m.Y", $this->end_date));

		if ($form) {
			$tpl->set_var ('publish_checkbox', common_checkbox ('seo_word_publish', 1, $this->publish));
		}
	}

	function save_position ($report_id, $engine_id, $pos) {
		return $this->factory->mysql->command (sprintf ("REPLACE INTO seo_positions SET word_id='%d', engine_id='%d', report_id='%s', pos='%d'", $this->id, $engine_id, $report_id, $pos));
	}

	function create_stats ($engine_id) {
		$rows = $this->factory->mysql->select (sprintf ("SELECT sp.* FROM seo_positions AS sp INNER JOIN seo_reports AS sr ON sp.report_id=sr.id WHERE sp.word_id = '%d' AND sp.engine_id = '%d' ORDER BY sr.pubdate ASC", $this->id, $engine_id));

		$retval = array ();

		if ($rows != false)
		for (reset ($rows); $row = current ($rows); next ($rows)) {
			$retval[$row->report_id] = $row->pos;
		}

		return $retval;
	}
}

class SEOWordFactory extends SubListFactory {

	var $frontend = FALSE;

	function SEOWordFactory (&$config, &$request) {
		$this->SubListFactory ($config, $request, 'seo_words', 'id', 'ord_index', 'project_id', 'SEOWordItem', FALSE);
	}

	function &find_by_name ($name) {
		$list = $this->create_from_query (sprintf ("SELECT * FROM %s WHERE word='%s' AND project_id='%d'", $this->table_name, $name, $this->sub_id));

		if ($list)
			return $list[0];

		$list = NULL;
		return $list;
	}

	function conditions () {
		parent::conditions ();

		$this->order = array ('word ASC');
		if ($this->frontend) $this->cond[] = "publish = 1";
	}

	function preImport () {
		$this->mysql->command (sprintf ("UPDATE %s SET publish = 0 WHERE project_id='%d'", $this->table_name, $this->sub_id));
	}
}

?>