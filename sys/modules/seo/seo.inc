<?PHP

$factory_class = "SEOProjectFactory";

include_once ('seo_word.inc');
include_once ('seo_engine.inc');

class SEOProjectItem extends ListItem {
 	function SEOProjectItem ($id, &$factory) {
		parent::ListItem ($id, $factory);

	    $this->image = new Image ();
	    $this->small_image = new Image ();
	}

   function load_from_row ($row) {
        $this->id = $row->id;
        $this->ord_index = $row->ord_index;
        $this->url = $row->url;
        $this->start_date = $row->start_date;
        $this->end_date = $row->end_date;
		$this->publish = $row->publish;
    }

	function load_defaults ($name = '') {
		$this->url = $name;
		$this->start_date = date ('Y-m-d');
		$this->end_date = NULL;
		$this->publish = 1;
		// $this->arch_date = date ('Y-m-d', mktime (0, 0, 0, date ('m') + 3, date ('d'), date ('Y')));
	}
    
	function get_update_array () {
		return array (
			'url' => $this->url,
			'start_date' => $this->start_date,
			'end_date' => $this->end_date,
			'publish' => $this->publish
		);	
	}
	
	function get_insert_array () {
		return array (
			'ord_index' => $this->factory->get_next_index (),
			'url' => $this->url,
			'start_date' => $this->start_date,
			'end_date' => $this->end_date,
			'publish' => $this->publish
		);	
	}

	function load_from_parameters (&$request) {
		$this->url = $request->_post['seo_project_url'];
		$this->start_date = $request->_post['seo_project_start_date'];
		$this->end_date = $request->_post['seo_project_end_date'];
		$this->publish = $request->_post['seo_project_publish'];
	}
	
	function set_template_vars (&$tpl, $form = FALSE) {
		$tpl->set_var ('seo_project_id', $this->id);
		$tpl->set_var ('seo_project_ord_index', $this->ord_index);
		$tpl->set_var ('seo_project_url', $this->url);
		$tpl->set_var ('seo_project_start_date', $this->start_date);
		$tpl->set_var ('seo_project_end_date', $this->end_date);
		$tpl->set_var ('seo_project_publish', $this->publish);
		$tpl->set_var ('seo_project_publish_full', $this->publish ? '��' : '���');

		$tpl->set_var ('seo_project_start_date_t', datef ("d.m.Y", $this->start_date));
		$tpl->set_var ('seo_project_end_date_t', datef ("d.m.Y", $this->end_date));

		if ($form) {
			$tpl->set_var ('publish_checkbox', common_checkbox ('seo_project_publish', 1, $this->publish));
		}
	}

	function save_report ($pubdate) {
		$row = $this->factory->mysql->select_field (sprintf ("SELECT id FROM seo_reports WHERE pubdate='%s' AND project_id = '%d'", $pubdate, $this->id));

		if (empty ($row)) {
			$this->factory->mysql->command (sprintf ("INSERT INTO seo_reports SET pubdate='%s', project_id='%d'", $pubdate, $this->id));

			return $this->factory->mysql->insert_id ();
		}

		return $row[0];
	}

	function save_sitestats ($report_id, $cy, $pr, $y_sites = 0, $y_pages = 0) {
		// print sprintf ("project_id='%d', report_id='%d', cy='%d', pr='%d', y_sites='%d', y_pages='%d'<br>", $this->id, $report_id, $cy, $pr, $y_sites, $y_pages);
		return $this->factory->mysql->command (sprintf ("REPLACE INTO seo_sitestats SET report_id='%d', project_id='%d', cy='%d', pr='%d', y_sites='%d', y_pages='%d'", $report_id, $this->id, $cy, $pr, $y_sites, $y_pages));
	}

	function create_sitestats ($reports) {
		if (empty ($reports) || ! is_array ($reports))
			return array ();

		$ids = join (',', array_keys ($reports));

		$rows = $this->factory->mysql->select (sprintf ("SELECT * FROM seo_sitestats WHERE project_id = '%d' AND report_id IN (%s) ORDER BY report_id ASC", $this->id, $ids));

		$retval = array ();

		if ($rows != false)
		for (reset ($rows); $row = current ($rows); next ($rows)) {
			$retval[$row->report_id] = $row;
		}

		return $retval;
	}

	function create_reports ($date_start = NULL, $date_end = NULL, $by_date = NULL, $by_weeks = NULL) {
		$period = array ();

		if ($by_date == '7days') {
			$period[] = "ADDDATE(pubdate, INTERVAL 7 DAY) > NOW()";
		} elseif ($by_date == '10days') {
			$period[] = "ADDDATE(pubdate, INTERVAL 10 DAY) > NOW()";
		} elseif ($by_date == '1month') {
			$period[] = "ADDDATE(pubdate, INTERVAL 1 MONTH) > NOW()";
		} elseif ($by_date == '2month') {
			$period[] = "ADDDATE(pubdate, INTERVAL 2 MONTH) > NOW()";
		} elseif ($by_date == '3month') {
			$period[] = "ADDDATE(pubdate, INTERVAL 3 MONTH) > NOW()";
		} else {
			if ($date_start) $period[] = sprintf ("pubdate >= '%s 00:00:00'", $date_start);
			if ($date_end) $period[] = sprintf ("pubdate <= '%s 23:59:59'", $date_end);
		} 

		$rows = $this->factory->mysql->select ($q = sprintf ("SELECT * FROM seo_reports WHERE project_id = '%d' AND %s %s ORDER BY pubdate ASC", $this->id, join (" AND ", $period), $by_weeks ? " AND (DAYOFWEEK(pubdate) = 2 OR ADDDATE(pubdate, INTERVAL 1 DAY) > NOW())" : ""));
		// print "<br>" . $q; 

		$retval = array ();

		if ($rows != false)
		for (reset ($rows); $row = current ($rows); next ($rows)) {
			$retval[$row->id] = $row;
		}

		return $retval;
	}

	function create_dates () {
		$rows = $this->factory->mysql->select ("SELECT DATE_FORMAT(pubdate, '%Y-%m-%d') as pubdate, DATE_FORMAT(pubdate, '%e %b \'%y, %a') as descr FROM seo_reports WHERE project_id = '".$this->id."' ORDER BY pubdate DESC");
		$retval = array ();

		if ($rows != false)
		foreach ($rows as $i => $row) {
			$retval[$row->pubdate] = $row->descr;
		}

		return $retval;
	}
}

class SEOProjectFactory extends ListFactory {
	var $engines = NULL;
	var $words = NULL;

	var $frontend = FALSE;

	function SEOProjectFactory (&$config, &$request) {
		$this->ListFactory ($config, $request, 'seo_projects', 'id', 'ord_index', 'SEOProjectItem', TRUE);

		$this->engines = new SEOEngineFactory ( $config, $request );
		$this->words = new SEOWordFactory ( $config, $request );
	}

	function &find_by_name ($name) {
		$list = $this->create_from_query (sprintf ("SELECT * FROM %s WHERE url='%s'", $this->table_name, $name));

		if ($list)
			return $list[0];

		$list = NULL;
		return $list;
	}

	function conditions () {
		parent::conditions ();

		if ($this->frontend) $this->cond[] = "publish = 1";
	}

	function set_frontend () {
		$this->frontend = TRUE;
		$this->engines->frontend = TRUE;
		$this->words->frontend = TRUE;
	}

}

?>