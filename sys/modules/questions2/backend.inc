<?PHP

	$factory = new QuestionTree ($config, $request);
	$id_name = 'question_id';

	$tpl = new Template ($config, $module_path);
	$tpl->set_var ('module_fullname', $module_fullname);

	$list_url = new URL ($mod_url->build ());
	$list_url->set_var ('area', 'tree');
//	$list_url->set_var ('parent_id', $_GET['parent_id']);
	$tpl->set_var ('list_url', $list_url->build ());

	$edit_url = new URL ($mod_url->build ());
	$edit_url->set_var ('area', 'edit');
	$edit_url->set_var ('parent_id', $_GET['parent_id']);
	$tpl->set_var ('edit_url', $edit_url->build ());

	switch ($_GET['area']) {
	case 'edit':
		if ($request->_get['parent_id'])
			$parent = $factory->create ($request->_get['parent_id']);
		else
			$parent = NULL;

		$factory->set_children_classes_for ($request->_get['parent_id']);

		if (!$factory->default_edit_action ($edit_url->build (), $list_url->build (), $id_name)) {

			$item = $factory->create ($request->_get[$id_name]);

			if ($item->id) 
				$tpl->set_var ('title', sprintf ("��������: '%s'", $item->name));
			elseif ($parent)
				$tpl->set_var ('title', sprintf ("����� �������� � ����� '%s'", $parent->name));
			else
				$tpl->set_var ('title', "����� ��������");

			if (! $item->id && ! empty ($parent) || in_array ($item->class, array ("set", "setq", "sett"))) {
				$tpl->set_file ('edit', 'backend_edit_set.html');
				if ($item->id) 
					$tpl->set_var ('title', sprintf ("�����: %s", $item->name));
				else
					$tpl->set_var ('title', "����� �����");

			} elseif ($item->class == "part" || $parent->class == "setq") {
				$tpl->set_file ('edit', 'backend_edit_part.html');
				if ($item->id) 
					$tpl->set_var ('title', sprintf ("�����: %s", $item->name));
				else
					$tpl->set_var ('title', sprintf ("����� ����� � ������: %s", $parent->name));

			} elseif ($item->class == "quest" || $parent->class == "part") {
				$tpl->set_file ('edit', 'backend_edit_quest.html');
				if ($item->id) 
					$tpl->set_var ('title', sprintf ("������: %s", $item->name));
				else
					$tpl->set_var ('title', sprintf ("����� ������ � �����: %s", $parent->name));

			} elseif ($item->class == "topic" || $parent->class == "sett") {
				$tpl->set_file ('edit', 'backend_edit_topic.html');
				if ($item->id) 
					$tpl->set_var ('title', sprintf ("����: %s", $item->name));
				else
					$tpl->set_var ('title', sprintf ("����� ���� � ������: %s", $parent->name));

			} elseif ($item->id) {
				die ("������ ������ ��������"); 
			} else {
				die ("������ ������� �������� �������� ��� ����� ��������"); 

			}
		
			// $tpl->set_var ('common_formatting', common_formatting ());

			$item->htmlspecialchars ();
			$item->set_template_vars ($tpl, TRUE);
			
			$cancel_url = new URL ($list_url->build ());
			$cancel_url->set_var ($id_name, $item->id);

			$tpl->set_var ('edit_buttons_row', common_edit_buttons_row ($cancel_url->build (), true));

			$tpl->parse ('edit', 'edit');
			$tpl->p_default ();
		}
		break;

	case 'list':
		if (! $factory->default_list_action ($list_url->build (), $id_name)) {
			$tpl->set_file ('list', 'backend_list.html');
		
			$sub_url = new URL ($list_url->build ());

			if ($parent) {
				$tpl->set_var ('title', sprintf ("��������� ���������� ����� '%s'", $parent->name));
				$tpl->set_var ('parent_path', $parent->get_path_string ($sub_url, '������� �������'));
			} else {
				$tpl->set_var ('title', "��������� ����������");
				$tpl->set_var ('parent_path', "������� �������");
			}

			// $pager = new Pager ($factory, $request, $config->page_size, $list_url->build (), 'page', $id_name);
			$pager = NULL;
		
			$item_list = $factory->create_all ();

			$tpl->set_block ('list', 'row', 'rows');
			$tpl->set_block ('list', 'parent_row', 'parent_rows');
			$color = another_color ();

			if ($parent) {
				$tpl->set_var ('row_color', $color = another_color ($color));
				$sub_url->set_var ('parent_id', $parent->parent_id);
				$tpl->set_var ('parent_url', $sub_url->build ());
				$tpl->parse ('rows', 'parent_row', TRUE);
			}

			if ($item_list)
			for ($i = 0; $i < count ($item_list); $i++) {
				$item = &$item_list[$i];
			
				$tpl->set_var ('id_checkbox',
					common_checkbox ($id_name.'[]', $item->id, $request->_get['select_'.$id_name] == $item->id));
				$tpl->set_var ('row_color', $color = another_color ($color));
				$tpl->set_var ('row_number', $i + $pager->offset + 1);
			
				$item->make_tree_sublinks ($sub_url);
				$item->set_template_vars ($tpl);

				$tpl->set_var ('list_item_buttons', common_list_item_buttons (
					$list_url->build (), $edit_url->build (), $id_name, $item->id));

				$tpl->parse ('rows', 'row', TRUE);
			}
		
			$tpl->set_var ('tree_list_buttons', common_tree_list_buttons (
				$list_url->build (), $edit_url->build (), $id_name, $factory->movable));
			$tpl->set_var ('pager', ($pager && $pager->page_count > 1) ? common_pager ($pager) : '');
			$tpl->set_var ('total', $pager ? $pager->total_size : sizeof ($item_list));
		
			$tpl->parse ('list', 'list');
			$tpl->p_default ();
		}
	case 'tree':
	default:
		if (! $factory->default_list_action ($list_url->build (), $id_name)) {
			$tpl->set_file ('list', 'backend_tree.html');
		
			$sub_url = new URL ($list_url->build ());

			$tpl->set_var ('title', "������ �������� � ��� ��� ����");

			// $pager = new Pager ($factory, $request, $config->page_size, $list_url->build (), 'page', $id_name);
			$pager = NULL;
		
			$factory->set_parent (NULL);
			$tree = $factory->create_tree ();

			$tpl->set_block ('list', 'row', 'rows');
			$tpl->set_block ('list', 'parent_row', 'parent_rows');
			$color = another_color ();

			if ($tree)
			for ($i = 0; $i < count ($tree); $i++) {
				$item = &$tree[$i];
			
				$tpl->set_var ('id_checkbox',
					common_checkbox ($id_name.'[]', $item->id, $request->_get['select_'.$id_name] == $item->id));
				$tpl->set_var ('row_color', $color = another_color2 ($color));
			
				// $item->make_tree_sublinks ($sub_url);
				$n_children = sizeof ($item->children);
				if ($n_children == 0)
					$item->show_children = -1;
				else 
					$item->name = sprintf ('%s (%d)', $item->name, $n_children);

				$item->name = "<b>".$item->name."</b>";

				if ($item->class == "setq" && empty ($item->quantity))
					$item->quantity = "SUM=".$item->get_head_quantity_sum ();

				$item->set_template_vars ($tpl);

				if ($item->class == "quest") {
					$tpl->set_var ("question_answer_count", sprintf ("%s / %s", $item->answer_count (), $item->right_answer_count ()));
				} else {
					$tpl->set_var ("question_answer_count", "");
				}

				$tpl->set_var ('list_item_buttons', common_list_item_buttons (
					$list_url->build (), $edit_url->build (), $id_name, $item->id));

				// $tpl->set_var ('name_prefix', $item->level ? preg_replace ('/9/', '&nbsp;&nbsp;&nbsp;', $item->level * 10 - 1) : '');
				$tpl->set_var ('levelpadding', $item->level * 30 + 17);
				$tpl->set_var ('name_prefix', ""); 
		
				$tpl->parse ('rows', 'row', TRUE);

				if ($item->show_children)
					show_next_level ($tpl, $item, $color, $sub_url, $list_url, $edit_url, $id_name, $request);
			}
		
			$tpl->set_var ('tree_list_buttons', common_tree_list_buttons (
				$list_url->build (), $edit_url->build (), $id_name, $factory->movable));
			$tpl->set_var ('pager', ($pager && $pager->page_count > 1) ? common_pager ($pager) : '');
			$tpl->set_var ('total', $pager ? $pager->total_size : sizeof ($tree));
		
			$tpl->parse ('list', 'list');
			$tpl->p_default ();
		}
	}


	function show_next_level (&$tpl, &$root, &$color, &$sub_url, &$list_url, &$edit_url, $id_name, &$request) {
		
		$tree = &$root->children;

		if ($tree)
		for ($i = 0; $i < count ($tree); $i++) {
			$item = &$tree[$i];
		
			$tpl->set_var ('id_checkbox',
				common_checkbox ($id_name.'[]', $item->id, $request->_get['select_'.$id_name] == $item->id));
			$tpl->set_var ('row_color', $color = another_color2 ($color));
		
			// $item->make_tree_sublinks ($sub_url);
			$n_children = sizeof ($item->children);
			if ($n_children == 0)
				$item->show_children = -1;
			else 
				$item->name = sprintf ('%s (%d)', $item->name, $n_children);

			if ($item->class == "quest") {
				$tpl->set_var ("question_answer_count", sprintf ("%s / %s", $item->answer_count (), $item->right_answer_count ()));
			} else {
				$tpl->set_var ("question_answer_count", "");
			}

			$item->set_template_vars ($tpl);

			$tpl->set_var ('list_item_buttons', common_list_item_buttons (
				$list_url->build (), $edit_url->build (), $id_name, $item->id));

			// $tpl->set_var ('name_prefix', $item->level ? preg_replace ('/9/', '&nbsp;&nbsp;&nbsp;', pow(10, $item->level*2) - 1) : '');
			$tpl->set_var ('levelpadding', $item->level * 30 + 17);
			$tpl->set_var ('name_prefix', ""); 

			$tpl->parse ('rows', 'row', TRUE);

			if ($item->show_children)
				show_next_level ($tpl, $item, $color, $sub_url, $list_url, $edit_url, $id_name, $request);
		}
	}

?>