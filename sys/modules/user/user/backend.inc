<?PHP

	$factory = new UserFactory ($config, $request);
	$id_name = 'user_id';

	$structure_id = $request->param['s'];
	if ($structure_id <> 'all')
		$factory->structure_id = $structure_id;

	$tpl = new Template ($config, $module_path);
	$factory->module_path = $module_path;

	$list_url = new URL ($mod_url->build ());
	$list_url->set_var ('area', 'list');
	$list_url->set_var ('s', $structure_id);
	$tpl->set_var ('clear_url', $list_url->build ());

	$edit_url = new URL ($mod_url->build ());
	$edit_url->set_var ('area', 'edit');
	$edit_url->set_var ('s', $structure_id);

	$stat_url = new URL ($list_url->build ());
	$stat_url->set_var ('mod', 'user_stat');
	$tpl->set_var ('stat_url', $stat_url->build ());

	$factory->sort = $request->param['sort'];

	$factory->init_etaps ();

	// $params = array ("country_id", "region_id", "subject_id");
	$filter = "";
	foreach ($factory->params() as $name) {
		if ($p = $request->param[$name]) {
			$factory->setParam ($name, $p);
			$list_url->set_var ($name, $p);
			$edit_url->set_var ($name, $p);

			$filter .= sprintf ("<tr><td class=thead>%s</td><td>:</td><td>%s</td>\n", $factory->paramFilterName ($name), $factory->paramFilterValue ($name, $p));
		}
	}
	$factory->init_etap_id ();
	$list_url->set_var ('sort', $factory->sort);
	$edit_url->set_var ('sort', $factory->sort);
	$tpl->set_var ('filter', $filter);
	$tpl->set_var ('search_emailfio', $request->param['emailfio']);
	$tpl->set_var ('list_url', $list_url->build ());
	$tpl->set_var ('edit_url', $edit_url->build ());

	if ($n = $factory->getParam ("showby"))  
		$showBy = $n;
	else
		$showBy = 100; /*$config->page_size*/;
	$pagelist_url = new URL ($list_url->build ());
	$pagelist_url->set_var ('page', $request->param['page']);
	$tpl->set_var ('pagelist_url', $pagelist_url->build ());


	if ($request->action != 'delete' && $request->action != 'deleteresult' && ($e = $request->_post['search_emailfio'])) {
		$list_url->set_var ('emailfio', $e);
		header ('Location: ' .($url=$list_url->build ()));
		printf ("<a href='%s'>%s</a>", $url, $url);
		exit ();
	}

	$sort_url = new URL ($list_url->build ());
	$sort_url->set_var ('sort', 'regdate');
	$tpl->set_var ('sort_date', $sort_url->build ());
	$tpl->set_var ('b_date', $factory->sort == 'regdate' ? 'bold' : 'normal');
	$sort_url->set_var ('sort', 'name');
	$tpl->set_var ('sort_name', $sort_url->build ());
	$tpl->set_var ('b_name', $factory->sort == 'name' ? 'bold' : 'normal');
	$sort_url->set_var ('sort', 'email');
	$tpl->set_var ('sort_email', $sort_url->build ());
	$tpl->set_var ('b_email', $factory->sort == 'email' ? 'bold' : 'normal');
	$sort_url->set_var ('sort', 'dategen');
	$tpl->set_var ('sort_dategen', $sort_url->build ());
	$tpl->set_var ('b_dategen', $factory->sort == 'dategen' ? 'bold' : 'normal');
	$sort_url->set_var ('sort', 'dateans');
	$tpl->set_var ('sort_dateans', $sort_url->build ());
	$tpl->set_var ('b_dateans', $factory->sort == 'dateans' ? 'bold' : 'normal');


	switch ($_GET['area']) {
	case 'edit':
		if (!$factory->default_edit_action ($edit_url->build (), $list_url->build (), $id_name)) {
			$tpl->set_file ('edit', 'backend_edit.html');
		
			$item = $factory->create ($request->_get[$id_name]);
			
			if ($item->id) 
				$tpl->set_var ('title', sprintf ("������������: '%s'", $item->fio ()));
			else
				$tpl->set_var ('title', "����� ������������");

			$tpl->set_var ('common_formatting', common_formatting ());

			$item->htmlspecialchars ();
			$item->set_template_vars ($tpl, TRUE);

			$tpl->set_block ('edit', 'confirmblock', 'confirmblock');
			if ($item->confirmed)
				$tpl->set_var ('confirmblock', '');
			$tpl->parse ('confirmblock', 'confirmblock');
			/*
			$tpl->set_block ('edit', 'image_item', 'image_items');
			$tpl->set_var ('image_items', '');
			
			$color = another_color2 ();
			// $color = another_color2 ($color);
			if ($item->id) {
				$image_list = $item->image_factory->create_all ();
				$image_list[] = clone $item->image_factory->create (0);
				
				if ($image_list)
					for ($i = 0; $i < count ($image_list); $i++) {
						$image_item = &$image_list[$i];

						$image_item->set_template_vars ($tpl);

						$tpl->set_var ('image_item_ctl', $image_item->control ());
						$tpl->set_var ('color', $color = another_color2 ($color));

						$tpl->parse ('image_items', 'image_item', TRUE);
					}
			}
			*/


			$tpl->set_block ('edit', 'region_block', 'region_blocks');
			$tpl->set_block ('edit', 'subject_block', 'subject_blocks');
			$regionTree = &$factory->subjects->createRegionTree ();
			$tpl->set_var ('region_blocks', "");
			foreach ($regionTree as $id => $dummy) {
				$region = &$regionTree[$id];
				$region->set_template_vars ($tpl);
				$tpl->set_var ("rfsubjects_id_set", join(',', array_keys ($region->subjects)));
				$tpl->parse ('region_blocks', 'region_block', TRUE);
			}

			$subjectList = &$factory->subjects->list;
			$tpl->set_var ('subject_blocks', "");
			foreach ($subjectList as $sid => $name) {
				$tpl->set_var ("rfsubjects_id", $sid);
				$tpl->set_var ("rfsubjects_name", $name);
				$tpl->parse ('subject_blocks', 'subject_block', TRUE);
			}


			
			$cancel_url = new URL ($list_url->build ());
			$cancel_url->set_var ($id_name, $item->id);

			$tpl->set_var ('edit_buttons_row', common_edit_buttons_row ($cancel_url->build (), true));

			$tpl->parse ('edit', 'edit');
			$tpl->p_default ();
		}
		break;

	case 'list':
	default:
	    if (isset($_POST['mark']))
        {
            global $config;

            $mysql = $config->getDB();
//            $mysql->debug = TRUE;
            $user_id = $_POST['user_id'];
            $mark = $_POST['mark'];
            $eid = $_GET['submited'];
            $q_sql = "UPDATE results SET `mark` = '$mark' where (`eid` = '$eid' and `uid` = '$user_id' and `tip` = 'esse')";
            $mysql->command ($q_sql);

        }

		if (! $structure_id) {
			// ���������� ������ ���������

			$tpl->set_file ('list', 'backend_list_categories.html');
			$tpl->set_var ('title', "��������� �� ������");

			$pager = new Pager ($factory, $request, $showBy, $list_url->build (), 'page', $id_name);
			// $limit_str = $limit ? sprintf (" LIMIT %s%d", $pager->offset ? sprintf ("%d, ", $pager->offset) : '', $pager->page_size) : '';

			$query_str = sprintf ("SELECT id, structure_id, MAX(date) as date, COUNT(structure_id) as group_count FROM %s as t GROUP BY structure_id ORDER BY date DESC", $factory->table_name);
			$item_list = $factory->create_from_query ($query_str);
		
			$tpl->set_block ('list', 'row', 'rows');

			$struct_url = new URL ($list_url->build ());

			$tpl->set_var ('rows', '');
			$color = another_color ();
			if ($item_list)
			for ($i = 0; $i < count ($item_list); $i++) {

				$item = &$item_list[$i];

                $tpl->set_var ('row_color', $color = another_color ($color));
				$tpl->set_var ('row_number', $i + $pager->offset + 1);
			
				$item->set_template_vars ($tpl);
				$struct_url->set_var ('s', $item->structure_id);
				$tpl->set_var ('struct_url', $struct_url->build ());

				$factory->structure_id = $item->structure_id;
				$tpl->set_var ('user_been_count', $factory->get_been_count ());
				$tpl->set_var ('user_confirmed_count', $factory->get_confirmed_count ());

				$tpl->parse ('rows', 'row', TRUE);
			}
			$factory->structure_id = NULL;
			$tpl->set_var ('total', $t1 = $factory->get_total_count ());
			$tpl->set_var ('been_count', $t2 = $factory->get_been_count ());
			$tpl->set_var ('confirmed_count', $t3 = $factory->get_confirmed_count ());

			// ������� "���"
			if ($item_list[0]) {
				$color = another_color ();
				$tpl->set_var ('row_color', $color = another_color ($color));
				$tpl->set_var ('row_number', $i + $pager->offset + 1);
				$query_str = sprintf ("SELECT * FROM %s as t ORDER BY date DESC LIMIT 1", $factory->table_name);
				$item_list = $factory->create_from_query ($query_str);
				$item = &$item_list[0];
				$item->set_template_vars ($tpl);
				$struct_url->set_var ('s', 'all');
				$tpl->set_var ('user_structure_id_name_q', '���');
				$tpl->set_var ('struct_url', $struct_url->build ());
				$tpl->set_var ('user_group_count', $t1);
				$tpl->set_var ('user_been_count', $t2);
				$tpl->set_var ('user_confirmed_count', $t3);
				$tpl->parse ('rows', 'row', TRUE);
			}		

			// $tpl->set_var ('pager', ($pager && $pager->page_count > 1) ? common_pager ($pager) : '');
			$tpl->parse ('list', 'list');
			$tpl->p_default ();

		} elseif (! $factory->default_list_action ($list_url->build (), $id_name)) {
			if ($showStat = $factory->is_full_results ())
				$tpl->set_file ('list', 'backend_full_results.html');
			elseif ($showStat = $factory->is_results ()) 
				$tpl->set_file ('list', 'backend_results.html');
			else
				$tpl->set_file ('list', 'backend_list.html');

			if (empty ($filter)) {
				$tpl->set_block ('list', 'filter_block', 'filter_block');
				$tpl->set_var ('filter_block', "");
			}

			$tpl->set_var ('title', "���������");

			if ($structure_id == 'all') {
				$tpl->set_var ('title', "��������� ���");
			} elseif ($structure_id) {
				$item = &$factory->structure->create ($structure_id);
				// $tpl->set_var ('title', sprintf ("��������� '<a href=\"%s\" target=_blank>%s</a>'", $item->get_page_address (), htmlspecialchars($item->name)));
				if ($factory->is_full_results ())
					$tpl->set_var ('title', sprintf ("������ ����������<br>%s", $item->name));
				elseif ($factory->is_results ())
					$tpl->set_var ('title', sprintf ("������������� ����������<br>%s", $item->name));
				else
					$tpl->set_var ('title', sprintf ("���������<br>%s", $item->name));
			}

			$pager = new Pager ($factory, $request, $showBy, $list_url->build (), 'page', $id_name);
			// $pager = NULL;
		
			$item_list = $factory->create_all ($pager->page_size, $pager->offset);
		
			$tpl->set_block ('list', 'row', 'rows');
			if ($factory->is_full_results ()) {
				$factory->set_blocks_full_results_header ($tpl, 'list');
				$factory->set_blocks_full_results_row ($tpl, 'row');
			} 

			$struct_url = new URL ($list_url->build ());

			if ($factory->is_full_results ()) 
				$factory->parse_full_results_header ($tpl);

			$tpl->set_var ('rows', '');
			$color = another_color ();
			if ($item_list)
			for ($i = 0; $i < count ($item_list); $i++) {
				$item = &$item_list[$i];
			
				$tpl->set_var ('id_checkbox',
					common_checkbox ($id_name.'[]', $item->id, $request->_get['select_'.$id_name] == $item->id));
				$tpl->set_var ('row_color', $color = another_color ($color));
				$tpl->set_var ('row_number', $i + $pager->offset + 1);
			
				$item->set_template_vars ($tpl);
				$struct_url->set_var ('s', $item->structure_id);
				$tpl->set_var ('struct_url', $struct_url->build ());

				if ($factory->is_full_results ()) 
					$item->parse_full_results_row ($tpl);

				if ($showStat) 
					$tpl->set_var ('list_item_buttons', "");
				else
					$tpl->set_var ('list_item_buttons', common_list_item_buttons (
						$list_url->build (), $edit_url->build (), $id_name, $item->id));

				$tpl->parse ('rows', 'row', TRUE);
			}
		
			if ($showStat)
				$tpl->set_var ('list_buttons', "");
			else
				$tpl->set_var ('list_buttons', common_list_buttons (
					$list_url->build (), $edit_url->build (), $id_name, $factory->movable));
			$tpl->set_var ('pager', ($pager && $pager->page_count > 1) ? common_pager ($pager) : '');
			$tpl->set_var ('total', $pager ? $pager->total_size : sizeof ($item_list));
//			$tpl->set_var ('been_count', $factory->get_been_count ());
//			$tpl->set_var ('confirmed_count', $factory->get_confirmed_count ());
		
			$tpl->parse ('list', 'list');
			$tpl->p_default ();
		}
	}
	
?>