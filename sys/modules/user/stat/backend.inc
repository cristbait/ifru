<?PHP

	$factory = new UserFactory ($config, $request);
	$id_name = 'user_id';

	$structure_id = $request->param['s'];
	$etap_id = $request->param['etap'];
	if ($structure_id <> 'all')
		$factory->structure_id = $structure_id;

	$tpl = new Template ($config, $module_path);
	$factory->module_path = $module_path;

	$list_url = new URL ($mod_url->build ());
	$list_url->set_var ('area', 'list');
	$list_url->set_var ('s', $structure_id);
	$tpl->set_var ('list_url', $list_url->build ());

	$edit_url = new URL ($mod_url->build ());
	$edit_url->set_var ('area', 'edit');
	$edit_url->set_var ('s', $structure_id);
	$tpl->set_var ('edit_url', $edit_url->build ());

	$noarea_url = new URL ($list_url->build ());
	$noarea_url->unset_var ('area');
	$tpl->set_var ('noarea_url', $noarea_url->build ());

	$listlink_url = new URL ($mod_url->build ());
	$listlink_url->set_var ('mod', 'user');
	$listlink_url->set_var ('area', 'list');
	$listlink_url->set_var ('s', $structure_id);
	$tpl->set_var ('listlink_users', $listlink_url->build ());


	if ($structure_id) {
		// COMMON PARAMENTERS
		if ($structure_id == 'all') {
			$tpl->set_var ('title', "���������� ���������� ���");
		} else {
			$item = &$factory->structure->create ($structure_id);
			$tpl->set_var ('title', sprintf ("%s", $item->name));
		}
		$tpl->set_var ('current_date', date ("d.m.Y H:i"));
		$factory->init_etaps ();


		switch ($_GET['area']) {
		case 'rso_temp':
		case 'rso_fin':
		// ������ � ������� ��� ���
			if ($_GET['area'] == 'rso_temp') {
				$RSOTEMP = TRUE;
				$tpl->set_file ('list', 'backend_rso.html');
				$tpl->set_var ('title2', "�������������� �������� �� ����������, ����������� � �������� ���������� ������");
			} else {
				$RSOTEMP = FALSE;
				$tpl->set_file ('list', 'backend_rso_fin.html');
				$tpl->set_var ('title2', "����� ������������� �������������� �������� �� ����������, ����������� � ��������");
			}

			$blocks = array (
				"part" => array ("whom" => "����������",	"bywhom" => "�����������",	"param"	=> "got"),
				"priz" => array ("whom" => "��������",		"bywhom" => "���������",	"param"	=> "passed"),
				"winn" => array ("whom" => "�����������",	"bywhom" => "������������",	"param"	=> "win")
			);

			$tpl->set_block ('list', 'header_block1', 'header_block1s');
			$tpl->set_block ('list', 'header_block2', 'header_block2s');
			$tpl->set_block ('list', 'tour_class_block', 'tour_class_blocks');
			$tpl->set_block ('tour_class_block', 'content_block1', 'content_block1s');
			$tpl->set_var ('header_block1s', '');
			$tpl->set_var ('header_block2s', '');
			$tpl->set_var ('content_blocks', '');

			$keys = array_keys ($factory->etapTypes); $lastK = array_pop ($keys);
			$n = 0;

			foreach ($blocks as $blockType => $blockNames) {
				$tpl->set_var ('whom', $blockNames["whom"]);
				$tpl->set_var ('bywhom', $blockNames["bywhom"]);

				$tpl->parse ('header_block1s', 'header_block1', TRUE);
				$tpl->parse ('header_block2s', 'header_block2', TRUE);
			}

			$classes = $factory->classes;
			if ($RSOTEMP)
				$classes[] = "0";

			$rowlink = new URL ($listlink_url->build ());
			$rowlink->set_var ('fullstat_got', 1);
			$regionlink = new URL ($list_url->build ());
			$regionlink->set_var ('area', 'regions1');

			$tpl->set_var ('tour_class_blocks', '');
			foreach ($factory->etapTypes as $tourId => $etapType) {
				if ($RSOTEMP && $tourId == $lastK) break;
				$tour_n++;

//				$factory->setParam ('got', $tourId);
//				$rowlink->set_var ('got', $tourId);
//				$regionlink->set_var ('got', $tourId);
				$tpl->set_var ('tour_n', $tour_n);
				foreach ($classes as $class_n) {
					if ($class_n) {
						if ($RSOTEMP)
							$tpl->set_var ('rowname', sprintf ("%d ���<br>%d �����", $tour_n, $class_n));
						else
							$tpl->set_var ('rowname', sprintf ("%s ��� (%d)", number_to_roman ($tour_n), $class_n));
						$tpl->set_var ('rowclass', "");
					} else {
						$tpl->set_var ('rowname', sprintf ("����� ��<br>%d ����", $tour_n));
						$tpl->set_var ('rowclass', "bold");
					}
					$tpl->set_var ('class_n', $class_n);
					$factory->setParam ('class_number', $class_n);
					$rowlink->set_var ('class_number', $class_n);
					$regionlink->set_var ('class_number', $class_n);

					$tpl->set_var ('content_block1s', '');
					foreach ($blocks as $blockType => $blockNames) {
						// $rowlink->set_var ('', '');
						if ($blockNames["param"]) {
							$factory->setParam ($blockNames["param"], $tourId);
							$rowlink->set_var ($blockNames["param"], $tourId);
							$regionlink->set_var ($blockNames["param"], $tourId);
						}
						$tpl->set_var ('rowlink', $rowlink->build ());
						$tpl->set_var ('regionlink', $regionlink->build ());
						
						$tpl->set_var ('count', $factory->get_total_count ());
						$tpl->set_var ('count_sng', $factory->get_param_count ("notrf"));
						$tpl->set_var ('count_isrural', $factory->get_param_count ("isrural"));
						$tpl->set_var ('count_isorphan', $factory->get_param_count ("isorphan"));
						$tpl->set_var ('count_iscripple', $factory->get_param_count ("iscripple"));
						$tpl->set_var ('count_iscrippleorphan', $factory->get_param_count ("iscrippleorphan"));
						$tpl->set_var ('count_subjects', $factory->get_subjectstotal_count ());

						$tpl->parse ('content_block1s', 'content_block1', TRUE);

						if ($blockNames["param"]) {
							$factory->setParam ($blockNames["param"], null);
							$rowlink->unset_var ($blockNames["param"]);
							$regionlink->unset_var ($blockNames["param"]);
						}
					}

					$tpl->parse ('tour_class_blocks', 'tour_class_block', TRUE);
				}

				$tpl->parse ('tour_blocks', 'tour_block', TRUE);
			}

			break;

		case 'regions1':
		// ���������� �� ��������
			$tpl->set_var ('title2', "������������� ���������� 1 ���� �� �������� ��");

			$tpl->set_file ('list', 'backend_regions.html');
			$tpl->set_block ('list', 'country_block', 'country_blocks');
			$tpl->set_block ('country_block', 'region_block', 'region_blocks');
			$tpl->set_block ('country_block', 'country_table', 'country_table_tpl');
			$tpl->set_block ('region_block', 'subject_block', 'subject_blocks');
			$tpl->set_var ("country_blocks", "");

			
			$filter = "";
			foreach ($factory->params() as $name) {
				if ($p = $request->param[$name]) {
					$factory->setParam ($name, $p);
					$listlink_url->set_var ($name, $p);

					$filter .= sprintf ("<tr><td class=thead>%s</td><td>:</td><td>%s</td>\n", $factory->paramFilterName ($name), $factory->paramFilterValue ($name, $p));
				}
			}
			$tpl->set_var ('filter', $filter);
			if (empty ($filter)) {
				$tpl->set_block ('list', 'filter_block', 'filter_block');
				$tpl->set_var ('filter_block', "");
			}
			$factory->conditions ();

			$db = &$config->getDB();
			/*$sql = sprintf (
				"SELECT 
						u.country_id, 
						u.region_id, 
						u.subject_id, 
						COUNT(u.id) as cnt, 
						t1.name as country_name, 
						t2.name as rfregion_name, 
						t3.name as rfsubject_name,
						t3.code as rfsubject_code,
						t3.region_id as pid
					FROM users as u 
						LEFT JOIN countries as t1 on u.country_id=t1.id 
						LEFT JOIN rf_regions as t2 on u.region_id=t2.id 
						LEFT JOIN rf_subjects as t3 on u.subject_id=t3.id 
					WHERE u.structure_id = %s 
					GROUP BY country_id, region_id, subject_id 
					ORDER BY country_id, region_id, subject_id",
				$structure_id);
				*/
			$sql = sprintf (
				"SELECT 
						t.country_id, 
						t.region_id, 
						t.subject_id, 
						COUNT(t.id) as cnt, 
						srt1.name as country_name, 
						srt2.name as rfregion_name, 
						srt3.name as rfsubject_name,
						srt3.code as rfsubject_code,
						srt3.region_id as pid
					FROM users as t 
						LEFT JOIN countries as srt1 on t.country_id=srt1.id 
						LEFT JOIN rf_regions as srt2 on t.region_id=srt2.id 
						LEFT JOIN rf_subjects as srt3 on t.subject_id=srt3.id 
						%s
					%s 
					GROUP BY country_id, region_id, subject_id 
					ORDER BY country_id, region_id, subject_id",
				$factory->join_string (), $factory->where_string ());
			
			$rows = $db->select ($sql);

			$countries = array ();
			if ($rows)
			foreach ($rows as $row) {
				$cid = sprintf ("%d", $row->country_id);
				$rid = sprintf ("%d", $row->region_id);
				$sid = sprintf ("%d", $row->subject_id);
				$countries[$cid]["name"] = $row->country_name;
				$countries[$cid]["count"] = 0;
				$countries[$cid]["subjectstotal"] = 0;
				$countries[$cid]["regions"][$rid]["name"] = $row->rfregion_name;
				$countries[$cid]["regions"][$rid]["count"] = 0;
				$countries[$cid]["regions"][$rid]["subjects"][$sid]["name"] = $row->rfsubject_name;
				$countries[$cid]["regions"][$rid]["subjects"][$sid]["code"] = $row->rfsubject_code;
				$countries[$cid]["regions"][$rid]["subjects"][$sid]["count"] = $row->cnt;
				$countries[$cid]["regions"][$rid]["subjects"][$sid]["region_id"] = $row->pid;
			}

			foreach ($countries as $cid => $dummy) {
				$c = &$countries[$cid];
				foreach ($c["regions"] as $rid => $dummy) {
					$r = &$c["regions"][$rid];
					foreach ($r["subjects"] as $sid => $dummy) {
						$s = &$r["subjects"][$sid];
						$c["count"]+=$s["count"];
						$r["count"]+=$s["count"];
						$c["subjectstotal"]++;
					}
				}
			}
			// print "<pre>"; print_r ($countries); print "</pre>";
			$tpl->set_var ('subjectstotal', $countries[1]['subjectstotal']);

			$tpl->set_var ("listlink", "");
	//			unset ($countries[0]);

			foreach ($countries as $cid => $dummy) {
				$c = &$countries[$cid];
				$tpl->set_var ("country_id", $cid);
				$tpl->set_var ("country_name", $c["name"]);
				$tpl->set_var ("country_count", $c["count"]);
				$tpl->set_var ("country_error", empty($cid) ? "<font color=red><b>!!!</b></font> " : "");
				$listlink_url->set_var ('country_id', $cid ? $cid : 'null');
				
				$tpl->set_var ("country_table_tpl", "");
				if ($cid == 1) {
					$tpl->set_var ("region_blocks", "");
					foreach ($c["regions"] as $rid => $dummy) {
						$r = &$c["regions"][$rid];
						$tpl->set_var ("rfregion_id", $rid);
						$tpl->set_var ("rfregion_name", $r["name"]);
						$tpl->set_var ("rfregion_count", $r["count"]);
						$tpl->set_var ("rfregion_error", empty($rid) ? "<font color=red>!!!</font>" : "");
	//					$tpl->set_var ('row1_color', $color = another_color ($color));
						$listlink_url->set_var ('region_id', $rid ? $rid : 'null');

						$tpl->set_var ("subject_blocks", "");
						foreach ($r["subjects"] as $sid => $dummy) {
							$s = &$r["subjects"][$sid];
							$tpl->set_var ("rfsubject_id", $sid);
							$tpl->set_var ("rfsubject_name", $s["name"]);
							$tpl->set_var ('rfsubject_code', sprintf ("%02d", $s["code"]));
							$tpl->set_var ("rfsubject_count", $s["count"]);
							$tpl->set_var ("rfsubject_error", $s["region_id"]!=$rid || empty($sid) ? "<font color=red>!!!</font>" : "");
							$tpl->set_var ('row_color', $color = another_color2 ($color));
							$listlink_url->set_var ('subject_id', $sid ? $sid : 'null');

							$tpl->set_var ('listlink', $listlink_url->build ());
							$tpl->parse ("subject_blocks", "subject_block", true);
						}
						$tpl->set_var ('rows', sizeof ($r["subjects"]));
	//					$tpl->set_var ('row2_color', $color = another_color ($color));

						$listlink_url->unset_var ('subject_id');
						$tpl->set_var ('listlink', $listlink_url->build ());
						$tpl->parse ("region_blocks", "region_block", true);
					}

					$listlink_url->unset_var ('region_id');
					$tpl->parse ("country_table_tpl", "country_table", true);
				}

				$tpl->set_var ('listlink', $listlink_url->build ());
				$tpl->parse ("country_blocks", "country_block", true);
			}

			break;

		case 'diff':
				$tpl->set_file ('list', 'backend_diffscore.html');
				$tpl->set_block ('list', 'result_block', 'result_blocks');
				$tpl->set_var ('result_blocks', '');
				$tpl->set_var ('etap_id', $etap_id);

				$item2 = &$factory->structure->create ($etap_id);
				$tpl->set_var ('title2', sprintf ("���������� �� ������<BR>%s", $item2->name));
				$tpl->set_var ('submited_count', $total = $factory->get_param_count ("submited", $etap_id));
		
				$sql = sprintf ("SELECT mark, class_number, count(*) as cnt FROM results INNER JOIN users ON users.id=results.uid WHERE tid='%d' AND eid='%d' AND results.submited=1 GROUP BY mark,class_number ORDER BY mark DESC,class_number ASC",
					$structure_id, $etap_id);
				$rows = $factory->mysql->select ($sql);
				$marks = array ();

				if ($rows != false)
				foreach ($rows as $row) {
					$vars = get_object_vars ($row);
					if (empty ($row->mark))
						$row->mark = 0;
					$marks[$row->mark][$row->class_number] = $row->cnt;
					$marks[$row->mark]["exact"] += $row->cnt;
				}

				$tillHere = 0;
				foreach ($marks as $m => $mark) {
					$tillHere += $marks[$m]["exact"];
					$marks[$m]["exactormore"] = $tillHere;
					$marks[$m]["less"] = $total - $tillHere;
				}
				foreach ($marks as $m => $mark) {
					$tpl->set_var ("mark", $m);
					$tpl->set_var ("mark_q", $m ? $m : "noll");
					$tpl->set_var ("cnt", $mark["exact"]);
					$tpl->set_var ("cntmore", $mark["exactormore"]);
					$tpl->set_var ("cntless", $mark["less"]);
					$tpl->set_var ("cnt9", $mark[9]);
					$tpl->set_var ("cnt10", $mark[10]);
					$tpl->set_var ("cnt11", $mark[11]);

					$tpl->set_var ("cnt_percent", (round ($mark["exact"] / $total * 100, 1)) . "%");
					$tpl->set_var ("cntmore_percent", (round ($mark["exactormore"] / $total * 100, 1)) . "%");
					$tpl->set_var ("cntless_percent", (round ($mark["less"] / $total * 100, 1)) . "%");

					$tpl->set_var ('row_color', $color = another_color2 ($color));
					$tpl->parse ('result_blocks', 'result_block', TRUE);
				}
			break;

		case 'list':
		default:
			$tpl->set_file ('list', 'backend_list.html');
			$tpl->set_block ('list', 'tour_block_test', 'dummy');
			$tpl->set_block ('list', 'tour_block_esse', 'dummy');
			$tpl->set_block ('list', 'tour_block_manual', 'dummy');
			$tpl->set_block ('tour_block_test', 'passed_block', 'passed_block_tpl');
			$tpl->set_block ('tour_block_test', 'no_passed_block', 'dummy');
			$tpl->set_var ('dummy', '');
			$tpl->set_var ('tour_blocks', '');

			$tpl->set_var ('total', $t = $factory->get_total_count ());
			$tpl->set_var ('been_count', $factory->get_param_count ("been"));
			$tpl->set_var ('confirmed_count', $c = $factory->get_param_count ("confirmed"));
			$tpl->set_var ('notconfirmed_count', $t - $c);
			$tpl->set_var ('isrural_count', $factory->get_param_count ("isrural"));
			$tpl->set_var ('isorphan_count', $factory->get_param_count ("isorphan"));
			$tpl->set_var ('iscripple_count', $factory->get_param_count ("iscripple"));
			$tpl->set_var ('subjectstotal', $factory->get_subjectstotal_count ());
			$tpl->set_var ('notrf_count', $factory->get_param_count ("notrf"));
			$tpl->set_var ('class9_count', $factory->get_param_count ("class_number", 9));
			$tpl->set_var ('class10_count', $factory->get_param_count ("class_number", 10));
			$tpl->set_var ('class11_count', $factory->get_param_count ("class_number", 11));

			$etaps = &$factory->structure->load_by_document_type ('etap%', $structure_id);
			$firstetap = $prevID = null;
			foreach ($etaps as $i => $dummy) {
				$etap = &$etaps[$i];

				$tpl->set_var ('previous_id', $prevID);
				if ($etap->class2 == 'test') {
					$tpl->set_var ('got_count', $g = $factory->get_param_count ("got", $etap->id));
//					$tpl->set_var ('notgot_count', $factory->get_param_count ("notgot", $etap->id));
					$tpl->set_var ('notgot_count', $t - $g);
					$tpl->set_var ('submited_count', $factory->get_param_count ("submited", $etap->id));
					$tpl->set_var ('notsubmited_count', $factory->get_param_count ("notsubmited", $etap->id));
					if (empty ($etap->passmark)) {
						$tpl->parse ('passed_block_tpl', 'no_passed_block');
						$t = 0;
					} else {
						$tpl->set_var ('structure_id', $etap->id);
						$tpl->set_var ('passed_count', $t = $factory->get_param_count ("passed", $etap->id));
						$tpl->set_var ('notpassed_count', $factory->get_param_count ("notpassed", $etap->id));
						$tpl->parse ('passed_block_tpl', 'passed_block');
					}

				} elseif ($etap->class2 == 'esse') {
					$tpl->set_var ('submited_count', $g = $factory->get_param_count ("submited", $etap->id));
					$tpl->set_var ('notgot_count', $t - $g);

				} else {

				}
				if (empty ($firstetap))
					$firstetap = $g;
				$prevID = $etap->id;

				if (empty ($etap->passmark))
					$etap->passmark = "<font color=red>--</font>";

				$etap->set_template_vars ($tpl);
				$tpl->parse ('tour_blocks', 'tour_block_'.$etap->class2, TRUE);
			}
			$factory->previousPass = 0;

			$tpl->set_var ('got_firstetap_count', $firstetap);		
			$tpl->set_var ('current_date', date ("d.m.Y H:i"));		
		}

		$tpl->parse ('list', 'list');
		$tpl->p_default ();

	} else {
	// ���������� ������ ��������
		$tpl->set_file ('list', 'backend_list_categories.html');
		$tpl->set_var ('title', "���������� � ��������� ������������� �� ������");

		$pager = new Pager ($factory, $request, 100/*$config->page_size*/, $list_url->build (), 'page', $id_name);
		// $limit_str = $limit ? sprintf (" LIMIT %s%d", $pager->offset ? sprintf ("%d, ", $pager->offset) : '', $pager->page_size) : '';

		$query_str = sprintf ("SELECT id, structure_id, MAX(date) as date, COUNT(structure_id) as group_count FROM %s as t GROUP BY structure_id ORDER BY date DESC", $factory->table_name);
		$item_list = $factory->create_from_query ($query_str);
	
	
		$tpl->set_block ('list', 'row', 'rows');

		$struct_url = new URL ($list_url->build ());

		$tpl->set_var ('rows', '');
		$color = another_color ();
		if ($item_list)
		for ($i = 0; $i < count ($item_list); $i++) {
			$item = &$item_list[$i];
			$tpl->set_var ('row_color', $color = another_color ($color));
			$tpl->set_var ('row_number', $i + $pager->offset + 1);
		
			$item->set_template_vars ($tpl);
			$struct_url->set_var ('s', $item->structure_id);
			$tpl->set_var ('struct_url', $struct_url->build ());

			$factory->structure_id = $item->structure_id;
			$tpl->set_var ('user_been_count', $factory->get_been_count ());
			$tpl->set_var ('user_confirmed_count', $factory->get_confirmed_count ());

			$tpl->parse ('rows', 'row', TRUE);
		}
		$factory->structure_id = NULL;
		$tpl->set_var ('total', $t1 = $factory->get_total_count ());
		$tpl->set_var ('been_count', $t2 = $factory->get_been_count ());
		$tpl->set_var ('confirmed_count', $t3 = $factory->get_confirmed_count ());

		// ������� "���"
/*			if ($item_list[0]) {
			$color = another_color ();
			$tpl->set_var ('row_color', $color = another_color ($color));
			$tpl->set_var ('row_number', $i + $pager->offset + 1);
			$query_str = sprintf ("SELECT * FROM %s as t ORDER BY date DESC LIMIT 1", $factory->table_name);
			$item_list = $factory->create_from_query ($query_str);
			$item = &$item_list[0];
			$item->set_template_vars ($tpl);
			$struct_url->set_var ('s', 'all');
			$tpl->set_var ('user_structure_id_name_q', '���');
			$tpl->set_var ('struct_url', $struct_url->build ());
			$tpl->set_var ('user_group_count', $t1);
			$tpl->set_var ('user_been_count', $t2);
			$tpl->set_var ('user_confirmed_count', $t3);
			$tpl->parse ('rows', 'row', TRUE);
		}		
*/

		// $tpl->set_var ('pager', ($pager && $pager->page_count > 1) ? common_pager ($pager) : '');
		$tpl->parse ('list', 'list');
		$tpl->p_default ();

	}
	
?>