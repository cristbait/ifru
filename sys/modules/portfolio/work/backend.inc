<?PHP

	$factory = new PortfolioWorkFactory ($config, $request);
//	$portfolio_factory = new PortfolioFactory ($config, $request);
	$id_name = 'portfoliowork_id';

	$tpl = new Template ($config, $module_path);

	$group_1 = $request->_get['group_1'];
	$group_2 = $request->_get['group_2'];
	$factory->group_1 = $group_1;
	$factory->group_2 = $group_2;

	$list_url = new URL ($mod_url->build ());
	$list_url->set_var ('area', 'list');
	$list_url->set_var ('group_1', $group_1);
	$list_url->set_var ('group_2', $group_2);
//	$list_url->set_var ('portfolio_id', $portfolio_id);
	$tpl->set_var ('list_url', $list_url->build ());

	$edit_url = new URL ($mod_url->build ());
	$edit_url->set_var ('area', 'edit');
	$edit_url->set_var ('group_1', $group_1);
	$edit_url->set_var ('group_2', $group_2);
//	$edit_url->set_var ('portfolio_id', $portfolio_id);
	$tpl->set_var ('edit_url', $edit_url->build ());

//	$category = $portfolio_factory->create ($portfolio_id);
//	$category->set_template_vars ($tpl);

	switch ($_GET['area']) {
	case 'edit':
		if (!$factory->default_edit_action ($edit_url->build (), $list_url->build (), $id_name)) {
			$tpl->set_file ('edit', 'backend_edit.html');
		
			$item = $factory->create ($request->_get[$id_name]);
			
			if ($item->id) 
				$tpl->set_var ('title', sprintf ("������ ��������: '%s'", $item->title));
			else
				$tpl->set_var ('title', "����� ������ ��������");
//				$tpl->set_var ('title', sprintf ("����� ������ �������� � ���������: '%s'", $category->name));

			$item->htmlspecialchars ();
			$item->set_template_vars ($tpl, TRUE);

			$tpl->set_block ('edit', 'image_item', 'image_items');
			$tpl->set_var ('image_items', '');
			
			$color = another_color2 ();
			$color = another_color2 ($color);
			if ($item->id) {
				$image_list = $item->image_factory->create_all ();
				// $images = 4 - ($image_list ? sizeof ($image_list) : 0); 
				// $images = $images < 0 ? 0 : $images;
				$images = 2;

				for ($i = 0; $i <= $images; $i++) {
					$image_list[] = clone $item->image_factory->create (0);
					$image_list[sizeof($image_list)-1]->id = $i ? -$i : NULL;
				}

				if ($image_list)
					for ($i = 0; $i < count ($image_list); $i++) {
						$image_item = &$image_list[$i];
						$image_item->set_template_vars ($tpl);

						$tpl->set_var ('auto_image_item_ctl', $image_item->control (FALSE, FALSE, FALSE));
						$tpl->set_var ('color', $color = another_color2 ($color));

						$tpl->parse ('image_items', 'image_item', TRUE);
					}
			} else {
				$tpl->set_var ('image_items', '<TR BGCOLOR="#EEEEEE"><TD align="center" COLSPAN="2" CLASS="thead">�������� ���������� ����� �������� ����� ���������� �������� ("��������")</TD></TR>');
			}
			
			$cancel_url = new URL ($list_url->build ());
			$cancel_url->set_var ($id_name, $item->id);

			$tpl->set_var ('edit_buttons_row', common_edit_buttons_row ($cancel_url->build (), true));

			$tpl->parse ('edit', 'edit');
			$tpl->p_default ();
		}
		break;

	case 'list':
	default:
		if (! $factory->default_list_action ($list_url->build (), $id_name)) {
			$tpl->set_file ('list', 'backend_list.html');

			if ($group_1) $g1 = &$factory->group_1_factory->create ($group_1);
			if ($group_2) $g2 = &$factory->group_2_factory->create ($group_2);
	
			// $tpl->set_var ('title', sprintf ("������ ��������: '%s'", $category->name));
			$tpl->set_var ('title', "��������" . 
				($group_1 ? " �� ���� '" . $g1->name . "'" : "") . 
				($group_2 ? " �� ����������� '" . $g2->name . "'" : ""));

			$purl = new URL ($mod_url->build ());
			if ($group_1) {
				$purl->set_var ('mod', $module_name . "_group_1");
				$tpl->set_var ('parent_path', sprintf ('<a href="%s">������ �����</a>', $purl->build ()));
			} elseif ($group_2) {
				$purl->set_var ('mod', $module_name . "_group_2");
				$tpl->set_var ('parent_path', sprintf ('<a href="%s">������ �����������</a>', $purl->build ()));
			} else {
				$tpl->set_var ('parent_path', "");
			}

			$pager = new Pager ($factory, $request, $config->page_size, $list_url->build (), 'page', $id_name);
			// $pager = NULL;

			$item_list = $factory->create_all ($pager->page_size, $pager->offset);
		
			$tpl->set_block ('list', 'row', 'rows');
		
			$tpl->set_var ('rows', '');
			$color = another_color ();
			if ($item_list)
			for ($i = 0; $i < count ($item_list); $i++) {
				$item = &$item_list[$i];
			
				$tpl->set_var ('id_checkbox',
					common_checkbox ($id_name.'[]', $item->id, $request->_get['select_'.$id_name] == $item->id));
				$tpl->set_var ('row_color', $color = another_color ($color));
				$tpl->set_var ('row_number', $i + $pager->offset + 1);
			
				$item->title .= sprintf (' (%d)', $item->image_factory->get_total_count ());
				$item->set_template_vars ($tpl);

				$tpl->set_var ('list_item_buttons', common_list_item_buttons (
					$list_url->build (), $edit_url->build (), $id_name, $item->id));

				$tpl->parse ('rows', 'row', TRUE);
			}
		
			$tpl->set_var ('list_buttons', common_list_buttons (
				$list_url->build (), $edit_url->build (), $id_name, $factory->movable));
			$tpl->set_var ('pager', ($pager && $pager->page_count > 1) ? common_pager ($pager) : '');
			$tpl->set_var ('total', $pager ? $pager->total_size : sizeof ($item_list));
		
			$tpl->parse ('list', 'list');
			$tpl->p_default ();
		}
	}
	
?>