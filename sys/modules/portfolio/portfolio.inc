<?PHP

include_once ("clients.inc");

$factory_class = "PortfolioFactory";

/****      PORTFOLIO FACTORY (FOR TYPES)        ****/


class PortfolioItem extends ListItem {
   function load_from_row ($row) {
        $this->name = $row->name;
        $this->ord_index = $row->ord_index;
        $this->announce = $row->announce;
    }

	function load_defaults () {
	}
    
	function get_update_array () {
		return array (
			'name' => $this->name,
			'announce' => $this->announce
		);	
	}
	
	function get_insert_array () {
		return array (
			'ord_index' => $this->factory->get_next_index (),
			'name' => $this->name,
			'announce' => $this->announce
		);	
	}

	function load_from_parameters (&$request) {
		$this->name = $request->_post['portfolio_name'];
		$this->announce = $request->_post['portfolio_announce'];
	}
	
	function set_template_vars (&$tpl, $form = FALSE) {
		$tpl->set_var ('portfolio_id', $this->id);
		$tpl->set_var ('portfolio_ord_index', $this->ord_index);
		$tpl->set_var ('portfolio_name', $this->name);
		$tpl->set_var ('portfolio_announce', $this->announce);
	}
}

class PortfolioFactory extends ListFactory {
	function PortfolioFactory (&$config, &$request) {
		$this->ListFactory ($config, $request, 'portfolio', 'id', 'ord_index', 'PortfolioItem', TRUE);
	}
}


/****      PORTFOLIO GROUP 1        ****/

class PortfolioGroup1Item extends ListItem {
   function load_from_row ($row) {
        $this->name = $row->name;
        $this->ord_index = $row->ord_index;
        $this->announce = $row->announce;
    }

	function load_defaults () {
	}
    
	function get_update_array () {
		return array (
			'name' => $this->name,
			'announce' => $this->announce
		);	
	}
	
	function get_insert_array () {
		return array (
			'ord_index' => $this->factory->get_next_index (),
			'name' => $this->name,
			'announce' => $this->announce
		);	
	}

	function load_from_parameters (&$request) {
		$this->name = $request->_post['group_name'];
		$this->announce = $request->_post['group_announce'];
	}
	
	function set_template_vars (&$tpl, $form = FALSE) {
		$tpl->set_var ('group_id', $this->id);
		$tpl->set_var ('group_ord_index', $this->ord_index);
		$tpl->set_var ('group_name', $this->name);
		$tpl->set_var ('group_announce', $this->announce);
	}
}

class PortfolioGroup1Factory extends ListFactory {
	function PortfolioGroup1Factory (&$config, &$request) {
		$this->ListFactory ($config, $request, 'portfolio_group_1', 'id', 'name', 'PortfolioGroup1Item', FALSE);
	}
}

/****      PORTFOLIO GROUP 2        ****/

class PortfolioGroup2Item extends ListItem {
   function load_from_row ($row) {
        $this->name = $row->name;
        $this->ord_index = $row->ord_index;
        $this->announce = $row->announce;
    }

	function load_defaults () {
	}
    
	function get_update_array () {
		return array (
			'name' => $this->name,
			'announce' => $this->announce
		);	
	}
	
	function get_insert_array () {
		return array (
			'ord_index' => $this->factory->get_next_index (),
			'name' => $this->name,
			'announce' => $this->announce
		);	
	}

	function load_from_parameters (&$request) {
		$this->name = $request->_post['group_name'];
		$this->announce = $request->_post['group_announce'];
	}
	
	function set_template_vars (&$tpl, $form = FALSE) {
		$tpl->set_var ('group_id', $this->id);
		$tpl->set_var ('group_ord_index', $this->ord_index);
		$tpl->set_var ('group_name', $this->name);
		$tpl->set_var ('group_announce', $this->announce);
	}
}

class PortfolioGroup2Factory extends ListFactory {
	function PortfolioGroup2Factory (&$config, &$request) {
		$this->ListFactory ($config, $request, 'portfolio_group_2', 'id', 'name', 'PortfolioGroup2Item', FALSE);
	}
}


/****      PORTFOLIO WORKS        ****/

class PortfolioWorkItem extends ListItem {
	var $ppv_w = 96;
	var $ppv_h = 96;
	var $pv_w = 212;
	var $pv_h = 212;
	var $w = 640;
	var $h = 640;

 	function PortfolioWorkItem ($id, &$factory) {
		parent::ListItem ($id, $factory);

	    $this->image = new Image ();

		$this->image_factory = new PrePreviewImageItemFactory ($this->factory->config, $this->factory->request, 'portfolio', $this->id, $this->factory);

		$this->set_image_sizes ();
		$this->image_factory->root_path = '/'; // MY_REL_ROOT_PATH;
		$this->image_factory->default_preview = 'img/default_preview.jpg';
		// $this->image_factory->max_images = 5;
	}

	function set_image_sizes () {
		$this->image_factory->set_ppv_sizes ($this->ppv_w, $this->ppv_h);
		$this->image_factory->set_pv_sizes ($this->pv_w, $this->pv_h);
		$this->image_factory->set_sizes ($this->w, $this->h);

		$this->image_factory->set_resize_only (FALSE, TRUE);
	}

	function load_from_row ($row) {
        // $this->portfolio_id = $row->portfolio_id;
        $this->date = $row->date;
        $this->ord_index = $row->ord_index;
        $this->title = $row->title;
        $this->publish = $row->publish;
        $this->announce = $row->announce;
        $this->content = $row->content;
        $this->first = $row->first;
        $this->client_id = $row->client_id;
        $this->url = $row->url;
        $this->url_exist = $row->url_exist;
        $this->urllocal_use = $row->urllocal_use;
        $this->urllocal = $row->urllocal;
        $this->title_1 = $row->title_1;
        $this->group_1 = $row->group_1;
        $this->group_2 = $row->group_2;

		$this->image->unpack_string ($row->image);
    }

	function load_defaults () {
		$this->date = date ('Y-m-d');
		$this->publish = 'Y';
		$this->url_exist = 'Y';
		$this->urllocal_use = '';
		$this->group_1 = $this->factory->group_1;
		$this->group_2 = $this->factory->group_2;
		// $this->portfolio_id = $this->factory->request->_get['portfolio_id'];
	}
    
	function get_update_array () {
		return array (
			'publish' => $this->publish,
			'image' => $this->image->pack_data (),
			'client_id' => $this->client_id,
			'first' => $this->first,
			'title' => $this->title,
			'date' => $this->date,
			'url' => $this->url,
			'url_exist' => $this->url_exist,
			'urllocal_use' => $this->urllocal_use,
			'urllocal' => $this->urllocal,
			'title_1' => $this->title_1,
			'group_1' => $this->group_1,
			'group_2' => $this->group_2,
			'announce' => $this->announce,
			'content' => $this->content
		);	
	}
	
	function get_insert_array () {
		return array (
			'publish' => $this->publish,
			'image' => $this->image->pack_data (),
			'first' => $this->first,
			'client_id' => $this->client_id,
//			'portfolio_id' => $this->portfolio_id,
			'date' => $this->date,
			'url' => $this->url,
			'url_exist' => $this->url_exist,
			'urllocal_use' => $this->urllocal_use,
			'urllocal' => $this->urllocal,
			'ord_index' => $this->factory->get_shift_index (),
			'title' => $this->title,
			'title_1' => $this->title_1,
			'group_1' => $this->group_1,
			'group_2' => $this->group_2,
			'announce' => $this->announce,
			'content' => $this->content
		);	
	}

	function load_from_parameters (&$request) {
//		$this->portfolio_id = $request->_post['portfolio_id'];
		$this->publish = $request->_post['portfoliowork_publish'];
		$this->date = $request->_post['portfoliowork_date'];
		$this->first = $request->_post['portfoliowork_first'];
		$this->title = $request->_post['portfoliowork_title'];
		$this->announce = $request->_post['portfoliowork_announce'];
		$this->content = $request->_post['portfoliowork_content'];
		$this->client_id = $request->_post['portfoliowork_client_id'];
		$this->url = $request->_post['portfoliowork_url'];
		$this->url_exist = $request->_post['portfoliowork_url_exist'];
		$this->urllocal = $request->_post['portfoliowork_urllocal'];
		$this->urllocal_use = $request->_post['portfoliowork_urllocal_use'];
		$this->title_1 = $request->_post['portfoliowork_title_1'];
		$this->group_1 = $request->_post['portfoliowork_group_1'];
		$this->group_2 = $request->_post['portfoliowork_group_2'];

		if (empty ($this->urllocal)) $this->urllocal_use = '';
		if (empty ($this->url)) $this->url_exist = '';
		$this->url = preg_replace ('/^http:\/\//', '', $this->url);
		$this->urllocal = preg_replace ('/^http:\/\//', '', $this->urllocal);
		// $this->image->proceed_parameters ($request, 'portfoliowork_image', 'news', 'image_', $this->id);

		$this->image_factory->proceed_images ($request);

		$image_list = $this->image_factory->create_all (1);
		if ($image_list) {
			$this->image->image = $image_list[0]->image_pv->image;
		} else {
			$this->image->image = '';
		}
	}
	
	function set_template_vars (&$tpl, $form = FALSE) {
		$tpl->set_var ('portfoliowork_id', $this->id);
//		$tpl->set_var ('portfolio_id', $this->portfolio_id);
		$tpl->set_var ('portfoliowork_date', $this->date);
		$tpl->set_var ('portfoliowork_ord_index', $this->ord_index);
		$tpl->set_var ('portfoliowork_first', $this->first ? $this->first : '');
		$tpl->set_var ('portfoliowork_title', $this->title);
		$tpl->set_var ('portfoliowork_title_1', $this->title_1);
		$tpl->set_var ('portfoliowork_title_1_or_title', $this->title_1 ? $this->title_1 : $this->title);
		$tpl->set_var ('portfoliowork_announce', $this->announce);
		$tpl->set_var ('portfoliowork_announce_or_title', $this->announce ? $this->announce : $this->title);
		$tpl->set_var ('portfoliowork_content', $this->content);
		$tpl->set_var ('portfoliowork_url', $this->url);
		$tpl->set_var ('portfoliowork_url_exist', $this->url_exist);
		$tpl->set_var ('portfoliowork_urllocal', $this->urllocal);
		$tpl->set_var ('portfoliowork_urllocal_use', $this->urllocal_use);
		$tpl->set_var ('portfoliowork_client_id', $this->client_id);
		$tpl->set_var ('portfoliowork_client_full', $this->factory->client_factory->find ($this->client_id));
		$tpl->set_var ('portfoliowork_group_1', $this->group_1);
		$tpl->set_var ('portfoliowork_group_1_full', $this->factory->group_1_factory->find ($this->group_1));
		$tpl->set_var ('portfoliowork_group_2', $this->group_2);
		$tpl->set_var ('portfoliowork_group_2_full', $this->factory->group_2_factory->find ($this->group_2));

		$tpl->set_var ('portfoliowork_date_t', datef ("d.m.Y", $this->date));
		$tpl->set_var ('portfoliowork_date_r', datef ("d/m/Y", $this->date));

		$tpl->set_var ('portfoliowork_publish', $this->publish);
		$tpl->set_var ('portfoliowork_publish_full', $this->publish ? "��" : "���");
		$tpl->set_var ('portfoliowork_publish_full_color', $this->publish ? "<font color=green>��</font>" : "<font color=#990000>���</font>");

		$tpl->set_var ('portfoliowork_url_exist_full', $this->url_exist ? "��" : "���");
		$tpl->set_var ('portfoliowork_url_exist_full_color', $this->url_exist ? "<font color=green>��</font>" : "<font color=#990000>���</font>");
		$tpl->set_var ('portfoliowork_urllocal_use_full', $this->urllocal_use ? "��" : "���");
		$tpl->set_var ('portfoliowork_urllocal_use_full_color', $this->urllocal_use ? "<font color=green>��</font>" : "<font color=#990000>���</font>");

		if (! $this->image->image) {
			$this->image->image = $this->image_factory->default_preview;
			$this->image->update ();
		}
		$tpl->set_var ('portfoliowork_image_tag', $this->image->get_img_tag ($this->pv_w, $this->pv_h, FALSE, 0, MY_HTTP_ROOT_PATH, $this->title, sprintf(' id="main%d"', $this->id)));
		/*$tpl->set_var ('portfoliowork_image_80_tag', $this->image->get_img_tag (round($this->pv_w*0.8), round($this->pv_h*0.8), FALSE, 0, MY_HTTP_ROOT_PATH, $this->title, sprintf(' id="main%d"', $this->id)));*/

		$name = empty ($this->url) ? $this->urllocal : $this->url;
		$name_s = preg_replace ("/^(.{20}).+/", "\\1...", $name);
		$url = ! $this->url_exist && ! $this->urllocal_use ? "" : 
			($this->urllocal_use || empty ($this->url) ? $this->urllocal : $this->url);

		if (empty ($url)) {
			$tpl->set_var ('portfoliowork_url_real_link', $name);
			$tpl->set_var ('portfoliowork_url_real_link_short', $name_s);
		} else {
			$tpl->set_var ('portfoliowork_url_real_link', sprintf('<A href = "http://%s" target="_blank">%s</A>', $url, $name));
			$tpl->set_var ('portfoliowork_url_real_link_short', sprintf('<A href = "http://%s" target="_blank">%s</A>', $url, $name_s));
		}

		if ($form) {
			// $tpl->set_var ('portfoliowork_image_ctl', $this->image->control ('portfoliowork_image', TRUE));
			$tpl->set_var ('portfoliowork_publish_checkbox', common_checkbox ('portfoliowork_publish', 1, $this->publish));
			$tpl->set_var ('portfoliowork_url_exist_checkbox', common_checkbox ('portfoliowork_url_exist', 1, $this->url_exist));
			$tpl->set_var ('portfoliowork_urllocal_use_checkbox', common_checkbox ('portfoliowork_urllocal_use', 1, $this->urllocal_use));

			$tpl->set_var ('client_select_button', $this->factory->client_factory->select_button ('portfoliowork_client_id', $this->client_id, ""));
			$tpl->set_var ('group_1_select_button', $this->factory->group_1_factory->select_button ('portfoliowork_group_1', $this->group_1, ""));
			$tpl->set_var ('group_2_select_button', $this->factory->group_2_factory->select_button ('portfoliowork_group_2', $this->group_2, ""));
		}
	}

	function no_preview () {
		if (! $this->image->image)
			return true;

		if (preg_match ('/'.basename($this->image_factory->default_preview).'/',$item->image->image))
			return true;

		return false;
	}

	function get_list_block (&$read_url, $mini = TRUE, $client = TRUE) {
		$tpl = $this->factory->get_list_block_tpl ();

		$this->set_template_vars ($tpl);

		$tpl->set_var ('image_count', $i_cnt = $this->image_factory->get_total_count ());
		$read_url->set_var ('pw', $this->id);
		$tpl->set_var ('read_url', $read_url->build ());

		/*
		if ($i_cnt > 1) {
			$if = &$this->image_factory;
			$image_list = $if->create_all (4);
				
			for ($i = 0; $i < 5; $i++) {
				if ($i < sizeof ($image_list)) {
					$image_item = &$image_list[$i];
					// $image_item->set_template_vars ($tpl);
					$root = $image_item->factory->root_path;
					$img = $image_item->image_ppv;

					$tpl->set_var ('portfoliowork_image_ppv_tag', $img->get_img_tag (NULL, NULL, NULL, 0, $root, $image_item->title_ru, 
						sprintf (" onclick=\"directChange('main%d', '%s%s')\" style=\"cursor: pointer;\"", 
							$this->id,
							preg_match ('#/$#', $root) ? $root : $root.'/',
							preg_replace ('/_ppv_/', '_pv_', $img->image))));

					$tpl->parse ('mini_works', 'mini_work', TRUE);
				} elseif ($i_cnt > 4) {
					$tpl->parse ('mini_works', 'more_work', TRUE);
				} else {
					$tpl->parse ('mini_works', 'empty_work', TRUE);
				}
			}

			$tpl->parse ('mini_blocks', 'mini_block');
		}
		*/

		$tpl->parse ('result', 'list_block');
		return $tpl->get_var ('result');
	}
}

class PortfolioWorkFactory extends ListFactory {
	var $first = FALSE;
	var $frontend = FALSE;
	var $structure_id = NULL;
	var $client_id = NULL;
	var $group_1 = NULL;
	var $group_2 = NULL;
	var $exclude_works = NULL;

	var $listBlockTpl = NULL;

	function PortfolioWorkFactory (&$config, &$request) {
		// $this->SubListFactory ($config, $request, 'portfolio_work', 'id', 'ord_index', 'portfolio_id', 'PortfolioWorkItem', TRUE);
		$this->ListFactory ($config, $request, 'portfolio_work', 'id', 'date', 'PortfolioWorkItem', FALSE);

		$this->client_factory = new ClientFactory ($config, $request);
		$this->group_1_factory = new PortfolioGroup1Factory ($config, $request);
		$this->group_2_factory = new PortfolioGroup2Factory ($config, $request);
	}

	function &create_output_list ($limit = NULL, $offset = NULL) {
		$limit_str = $limit ? sprintf (" LIMIT %s%d", $offset ? sprintf ("%d, ", $offset) : '', $limit) : '';
		$query_str = sprintf ("SELECT * FROM %s WHERE COALESCE(announce, '') <> '' AND NOW() < arch_date ORDER BY date DESC, id DESC%s", $this->table_name, $limit_str);
		return $this->create_from_query ($query_str);
	}

	function &create_first_list ($limit = NULL, $offset = NULL) {
		$this->first = TRUE;
		$list = $this->create_all ($limit, $offset);
		$this->first = FALSE;
		return $list;
	}

	// ��������� ������ � ��� ��������
	function draw_relative_good_blocks_2x ($structure_id, $excludeIDs = NULL, $template = 'portfoliowork_relative_col.html') {
		$image_factory = new PrePreviewImageItemFactory ($this->config, $this->request, 'portfolio', NULL, $this);
		$image_factory->root_path = '/';
		$list = $image_factory->create_relative_portfolio_list ($structure_id, $excludeIDs);

		$tpl = new Template ($this->config, MY_TPL_PATH);
		$tpl->set_file ('tpl', $template);
		$tpl->set_block ('tpl', 'row', 'rows');
		$tpl->set_block ('row', 'col', 'cols');
		$tpl->set_block ('row', 'dummy_col', 'dummy_cols');
		$tpl->set_var ('dummy_col', '');
		$tpl->set_var ('w_percent', '50%');
		
		$read_url = new URL ('/portfolio/');

		$cols = 2;
		$rows = ceil (sizeof ($list) / $cols);

		if ($list)
		for ($i = 0; $i < $rows; $i++) {
			$tpl->set_var ('cols', '');

			for ($j = 0; $j < $cols; $j++) {
				if ($i * $cols + $j < sizeof ($list)) {
					$image = &$list[$i * $cols + $j];

					$image->title_ru = shorten_words ($image->title_ru, 12);
					$image->set_template_vars ($tpl);
					$tpl->set_var ('image_item_image_pv_tag', $image->image_pv->get_img_tag (round($image->image_pv->width*0.75), round($image->image_pv->height*0.75), NULL, 0, $image->factory->root_path, $image->title_ru));

					$read_url->set_var ('pw', $image->link_id);
					$read_url->set_var ('img', $image->id);
					$tpl->set_var ('read_url', $read_url->build ());

					$tpl->parse ('cols', 'col', TRUE);
				} else {
					$tpl->parse ('cols', 'dummy_col', TRUE);
				}
			}
			$tpl->parse ('rows', 'row', TRUE);
		}
		$tpl->parse ('tpl', 'tpl');
		return $tpl->get_var ('tpl');
	}

	// ��������� ������ � ���� �������
	function draw_relative_good_blocks ($structure_id, $excludeIDs = NULL, $template = 'portfoliowork_relative.html') {
		$image_factory = new PrePreviewImageItemFactory ($this->config, $this->request, 'portfolio', NULL, $this);
		$image_factory->root_path = '/';
		$list = $image_factory->create_relative_portfolio_list ($structure_id, $excludeIDs);

		$tpl = new Template ($this->config, MY_TPL_PATH);
		$tpl->set_file ('tpl', $template);
		$tpl->set_block ('tpl', 'row', 'rows');
		
		$read_url = new URL ('/portfolio/');

		if ($list)
		foreach ($list as $i => $dummy) {
			$image = &$list[$i];
			$image->set_template_vars ($tpl);

			$read_url->set_var ('pw', $image->link_id);
			$read_url->set_var ('img', $image->id);
			$tpl->set_var ('read_url', $read_url->build ());

			$tpl->parse ('rows', 'row', TRUE);
		}

		$tpl->parse ('tpl', 'tpl');
		return $tpl->get_var ('tpl');
	}

	function conditions () {
		parent::conditions ();
		$this->order = array ("t.date DESC", "t.id DESC");
			
		if ($this->first) {
			$this->cond = array ("t.first > 0", "publish = 'Y'");
			$this->order = array ("t.first ASC", "t.date DESC", "t.id DESC");
		} 

		if ($this->structure_id) {
			$this->cond = array ("t.topic_id='".$this->structure_id."'");
//			$this->order = array ("t.date DESC");
		}

		if ($this->frontend) 
			$this->cond[] = "publish = 'Y'"; 

		if ($this->client_id) 
			$this->cond[] = sprintf ("t.client_id = '%d'", $this->client_id); 

		if ($this->group_1) 
			$this->cond[] = sprintf ("t.group_1 = '%d'", $this->group_1); 

		if ($this->group_2) 
			$this->cond[] = sprintf ("t.group_2 = '%d'", $this->group_2); 

		if (is_array ($this->exclude_works))
			$this->cond[] = sprintf ("t.id NOT IN (%s)", join (',',$this->exclude_works));
		
	}

	var $pos_list = NULL;

	function &get_topic_list () {
		if ($this->pos_list == NULL) {
			include_once ('structure/structure.inc');
			$this->pos_list = array ();

			$struct = new StructureTree ($this->config, $this->request);

			if ($list = $struct->get_pos_materials (FALSE, FALSE))
			for ($i = 0; $i < sizeof ($list); $i++) {
				$this->pos_list[$list[$i]->id] = ($list[$i]->parent_id != 0 ? '-- ' : '') .$list[$i]->name;
			}
		}
		return $this->pos_list;
	}

	function &get_list_block_tpl () {
		if (empty ($this->listBlockTpl)) {
			$this->listBlockTpl = new Template ($this->config, MY_TPL_PATH);
			$this->listBlockTpl->set_file ('portfolio', 'portfolio.html');
			$this->listBlockTpl->set_block ('portfolio', 'portfolio_work_block', 'portfolio_work_block');
			$this->listBlockTpl->set_var ('list_block', $this->listBlockTpl->get_var ('portfolio_work_block'));
//			$this->listBlockTpl->set_block ('list_block', 'mini_block', 'mini_blocks');
//			$this->listBlockTpl->set_block ('mini_block', 'mini_work', 'mini_works');
//			$this->listBlockTpl->set_block ('mini_block', 'more_work', 'dummy');
//			$this->listBlockTpl->set_block ('mini_block', 'empty_work', 'dummy');
			$this->listBlockTpl->set_var ('dummy', '');
		}
	
//		$this->listBlockTpl->set_var ('mini_blocks', '');
//		$this->listBlockTpl->set_var ('mini_works', '');

		return $this->listBlockTpl;
	}
}



?>