<?php

	$stack = &$structure->rewrite_stack;
	$last = &$stack[sizeof ($stack) - 1];
	$type = array_shift ($structure->rewrite_pool);

	include_once ('main.inc');

//	$main_page->page_name = $short_page_name = $last->name;
	// $meta_keywords = '';
	// $meta_description = '';

//	$factory = new PortfolioFactory ($config, $request);
	$work_factory = new PortfolioWorkFactory ($config, $request);
	$work_factory->frontend = TRUE;
	$client_factory = new ClientFactory ($config, $request);
	$client_factory->front = TRUE;

	$tpl = new Template ($config, MY_TPL_PATH);

	$pw = $request->_get['pw'];
	$cli = $request->_get['cli'];
	$img = $request->_get['img'];
	$show = $request->_get['show'];
	$page = $request->_get['page'];
	$group_1 = $request->_get['type'];
	$group_2 = $request->_get['dir'];
	$work_factory->group_1 = $group_1;
	$work_factory->group_2 = $group_2;

	$url = new URL ("");
	// $factory->load_vars ($request, $url);

	$path_url = new URL ("");
	$path_url->set_var ('page', $page);

	$text = "";

	$tpl->set_file ('portfolio', 'portfolio.html');

	if ($pw && ($item = $work_factory->create ($pw)) && $item->id) {
		$main_page->url_path[$path_url->build ()] = $short_page_name;

		$path_url->set_var ('portfolio', $item->id);
		// $main_page->url_path[$path_url->build ()] = '������';

		$item->set_template_vars ($tpl);

		// $main_page->page_name .= " / " . $item->title;
		$main_page->page_name = $item->title;

		$tpl->set_block ('portfolio', 'portfolio_work_right', 'portfolio_work_right');
		$tpl->set_block ('portfolio_work_right', 'topic_row', 'topic_rows');
		$tpl->set_var ('topic_rows', '');
		$topics = array ();

		$tpl->set_block ('portfolio', 'portfolio_work');
		$tpl->set_block ('portfolio_work', 'image_item', 'image_items');
		$tpl->set_block ('portfolio_work', 'image_item_pv_row', 'image_item_pv_rows');
		$tpl->set_block ('image_item_pv_row', 'image_item_pv', 'image_items_pv');
		$tpl->set_block ('portfolio', 'descript_row', 'descript_rows');
		$tpl->set_var ('image_items', '');
		$tpl->set_var ('image_items_pv', '');
		$tpl->set_var ('descript_rows', '');
		$tpl->set_block ('portfolio_work', 'image_big_clickable', 'image_big_clickable');
		$tpl->set_block ('portfolio_work', 'image_big', 'image_big');

		$if = &$item->image_factory;
		$image_list = $if->create_all ($if->max_images > 0 ? $if->max_images : NULL);
			
		if ($image_list) {
			$activeImageI = 0;
			$excludeIDs = array ();
			for ($i = 0; $i < count ($image_list); $i++) {
				$excludeIDs[] = $image_list[$i]->id;
			}

			$main_page->p2x = TRUE;

			for ($i = 0; $i < count ($image_list); $i++) {
				$image_item = &$image_list[$i];
				if (! $image_item->topic_id)
					$image_item->topic_id = 0;
				$image_item->set_template_vars ($tpl);

				if ($image_item->id == $img)
					$activeImageI = $i;

				if (! $image_item->image_pv->image) {
					$image_item->image_pv->image = $if->default_preview;
					$image_item->image_pv->update ();
				}

				if (! $image_item->image->image)
					$image_item->image = &$image_item->image_pv;

				$tpl->set_var ('i', $i);
				$tpl->set_var ('ii_src', '/'.$image_item->image->image);
				list ($w, $h) = $image_item->image->find_dims ($if->w, $if->h);
				$tpl->set_var ('ii_width', $w);
				$tpl->set_var ('ii_height', $h);
				$tpl->set_var ('ii_alt', $image_item->title_ru);

				$tpl->set_var ('ii_pv_src', '/'.$image_item->image_pv->image);
				list ($pv_w, $pv_h) = $image_item->image_pv->find_dims ($if->pv_w, $if->pv_h);
				$tpl->set_var ('ii_pv_width', $pv_w);
				$tpl->set_var ('ii_pv_height', $pv_h);
				$tpl->set_var ('ii_pv_alt', $image_item->title_ru);

				$tpl->parse ('image_items', 'image_item', TRUE);
				$tpl->parse ('image_items_pv', 'image_item_pv', TRUE);

				$tid = $image_item->topic_id;
				if ($tid && empty ($topics[$tid])) {
					$tpl->set_var ('same_topic_list', $work_factory->draw_relative_good_blocks_2x ($tid, $excludeIDs));

					$topic = $structure->create ($tid);
					$tpl->set_var ('topic_title', $topic->name);
					$tpl->parse ('topic_rows', 'topic_row', TRUE);

					$topics[$tid] = TRUE;
				}

				$tpl->parse ('descript_rows', 'descript_row', TRUE);
			}

			if (count ($image_list) > 1)
				$tpl->parse ('image_item_pv_rows', 'image_item_pv_row');
			else
				$tpl->set_var ('image_item_pv_rows', '');

			$tpl->set_var ('active_image_i', $activeImageI);

			$image_item = &$image_list[$activeImageI];
			$image_item->set_template_vars ($tpl);
			$tpl->set_var ('ii_src', '/'.$image_item->image->image);
			list ($w, $h) = $image_item->image->find_dims ($if->w, $if->h);
			$tpl->set_var ('ii_width', $w);
			$tpl->set_var ('ii_height', $h);
			$tpl->set_var ('ii_alt', $image_item->title_ru);

			if (count ($image_list) > 1) {
				$tpl->parse ('image_big_clickable', 'image_big_clickable');
				$tpl->set_var ('image_big', '');
			} else {
				$tpl->parse ('image_big', 'image_big');
				$tpl->set_var ('image_big_clickable', '');
			}
		}

		$tpl->parse ('portfolio_work_right', 'portfolio_work_right');
		$main_page->announces = $tpl->get_var ('portfolio_work_right');
		$main_page->announce_name = "������� ������";


		$tpl->set_var ('other_projects', '');
		/*
		if (($client = $client_factory->create ($item->client_id)) && $client->id) {
			$work_factory->client_id = $client->id;
			$work_factory->exclude_works = array ($item->id);
			$list = $work_factory->create_all ();
			
			if ($list) { 
				$tpl->set_file ('portfolio_list', 'portfolio_list.html');
				$tpl->set_block ('portfolio_list', 'block', 'other_projects');
				$tpl->set_block ('block', 'row', 'rows');
				$tpl->set_block ('row', 'col', 'cols');
				$tpl->set_block ('row', 'dummy_col', 'dummy');
				$tpl->set_var ('other_projects', '');
				$tpl->set_var ('dummy', '');

				$main_page->headers[] = $page_name;

				$read_url = new URL ("");
				$read_url->set_var ('page', $page);

				$work_factory->client_id = $client->id;
				$work_factory->exclude_clients = array ($client->id);
				$list = $work_factory->create_all ();

				$cols = 2;
				$rows = ceil (sizeof ($list) / $cols);

				for ($i = 0; $i < $rows; $i++) {
					$tpl->set_var ('cols', '');

					for ($j = 0; $j < $cols; $j++) {
						if ($i * $cols + $j < sizeof ($list)) {
							$item = &$list[$i * $cols + $j];

							$tpl->set_var ('portfolio_work_block', $item->get_list_block ($read_url));

							$tpl->parse ('cols', 'col', TRUE);
						} else {
							$tpl->parse ('cols', 'dummy_col', TRUE);
						}
					}
					$tpl->parse ('rows', 'row', TRUE);
				}

				$tpl->parse ('other_projects', 'block');
				$tpl->set_var ('other_projects', "<h2>������ �������, ����������� ��� �������</h2>".$tpl->get_var ('other_projects'));
			}

			$read_url = new URL ('/portfolio/');
			$read_url->set_var ('cli', $client->id);
			$tpl->set_var ('client_url', $read_url->build ());

			if ($client->publish)
				$client->set_template_vars ($tpl);
		}
		*/

		$tpl->parse ('portfolio_final', 'portfolio_work');

	} elseif (0 && $cli && ($client = $client_factory->create ($cli)) && $client->id) {
		$page_title = sprintf ("������� ��� �������: %s", $client->title);
		$main_page->page_name = $page_title;

		$tpl->set_block ('portfolio', 'portfolio_list_client', 'portfolio_list_client');
		$tpl->set_block ('portfolio_list_client', 'row', 'rows');

		$read_url = new URL ("");
		$read_url->set_var ('page', $page);

		$work_factory->client_id = $client->id;
		$list = $work_factory->create_all ();

		if ($list)
		for ($i = 0; $i < sizeof ($list); $i++) {
			$item = &$list[$i];

			$tpl->set_var ('portfolio_work_block', $item->get_list_block ($read_url));
			/*
			$item->set_template_vars ($tpl);

			$tpl->set_var ('image_count', $item->image_factory->get_total_count ());

			$read_url->set_var ('pw', $item->id);
			$tpl->set_var ('read_url', $read_url->build ());
			*/

			$tpl->parse ('rows', 'row', TRUE);
		}

		$client->set_template_vars ($tpl);

		$show_clients = TRUE;

		$tpl->parse ('portfolio_final', 'portfolio_list_client');
	

	} elseif ($module_subname == 'clients' || $show == 'clients') {
		$list = &$client_factory->create_all ();
		if ($list) { 
			$tpl->set_block ('portfolio', 'portfolio_clients_2c', 'dummy');
			$tpl->set_block ('portfolio_clients_2c', 'row', 'rows');
			$tpl->set_block ('row', 'col', 'cols');
			$tpl->set_block ('row', 'dummy_col', 'dummy');
			$tpl->set_var ('rows', '');
			$tpl->set_var ('dummy', '');

			$read_url = new URL ('/portfolio/');

			$cols = 3;
			$rows = ceil (sizeof ($list) / $cols);

			for ($i = 0; $i < $rows; $i++) {
				$tpl->set_var ('widthper', round (100 / sizeof ($list)) . "%");
				$tpl->set_var ('cols', '');

				for ($j = 0; $j < $cols; $j++) {
					if ($i * $cols + $j < sizeof ($list)) {
						$client = &$list[$i * $cols + $j];
						$client->set_template_vars ($tpl);

						// $read_url->set_var ('cli', $client->id);
						// $tpl->set_var ('read_url', $read_url->build ());

						$tpl->parse ('cols', 'col', TRUE);
					} else {
						$tpl->parse ('cols', 'dummy_col', TRUE);
					}
				}
				$tpl->parse ('rows', 'row', TRUE);
			}
		}

		$tpl->parse ('portfolio_final', 'portfolio_clients_2c');

	} else {
		$text = $last->content;

		if ($page_title = $last->get_page_title ()) 
			$main_page->page_title = $page_title;

		$tpl->set_block ('portfolio', 'portfolio_list', 'portfolio_list');
		// $tpl->set_block ('portfolio', 'block', 'blocks');
		$tpl->set_block ('portfolio_list', 'row', 'rows');
		$tpl->set_block ('row', 'col', 'cols');
		$tpl->set_block ('row', 'dummy_col', 'dummy');
		$tpl->set_var ('blocks', '');
		$tpl->set_var ('dummy', '');

		$main_page->headers[] = $page_name;

		$read_url = new URL ("");
		$read_url->set_var ('page', $page);

		$list = $work_factory->create_all ();

		$cols = 3;
		$rows = ceil (sizeof ($list) / $cols);

		$main_page->p2x = TRUE;

		if ($list)
		for ($i = 0; $i < $rows; $i++) {
			$tpl->set_var ('widthper', round (100 / sizeof ($list)) . "%");
			$tpl->set_var ('cols', '');

			for ($j = 0; $j < $cols; $j++) {
				if ($i * $cols + $j < sizeof ($list)) {
					$item = &$list[$i * $cols + $j];

					$tpl->set_var ('portfolio_work_block', $item->get_list_block ($read_url));
					/*
					$item->set_template_vars ($tpl);

					$tpl->set_var ('image_count', $item->image_factory->get_total_count ());

					$read_url->set_var ('pw', $item->id);
					$tpl->set_var ('read_url', $read_url->build ());
					*/

					$tpl->parse ('cols', 'col', TRUE);
				} else {
					$tpl->parse ('cols', 'dummy_col', TRUE);
				}
			}
			$tpl->parse ('rows', 'row', TRUE);
		}
		$tpl->parse ('portfolio_final', 'portfolio_list');
	}


	// BEGIN right menu

	if ($module_subname == 'clients') {
		$main_page->right_menu_block_add = '<br><h3>�, �������,</h3><A href = "/portfolio/"">���������</A><BR>';
	} else {
		$tpl->set_block ('portfolio', 'portfolio_right_menu', 'portfolio_right_menu');
		$tpl->set_block ('portfolio_right_menu', 'row_1', 'rows_1');
		$tpl->set_block ('portfolio_right_menu', 'row_1_inactive', 'dummy');
		$tpl->set_block ('portfolio_right_menu', 'row_2', 'rows_2');
		$tpl->set_block ('portfolio_right_menu', 'row_2_inactive', 'dummy');
		$tpl->set_var ('dummy', '');
		$read_url = new URL ("");
		$read_url->set_var ('dir', $group_2);

		$list = $work_factory->group_1_factory->create_all ();
		$item0 = &$work_factory->group_1_factory->create (0);
		$item0->name = '���';
		array_unshift ($list, $item0); 

		foreach ($list as $i => $dummy) {
			$item = &$list[$i];

			$work_factory->group_1 = $item->id;
			$count = $work_factory->get_total_count ();
			$item->name .= $count ? sprintf (' (%d)', $count) : '';
			$item->set_template_vars ($tpl);

			if ($count > 0) {
				$read_url->set_var ('type', $item->id);
				$tpl->set_var ('read_url', $read_url->build ());
				if ($group_1 - $item->id == 0 && empty ($show)) {
					$tpl->set_var ('active', 'active');
					$tpl->set_var ('slash', '/');
				} else {
					$tpl->set_var ('active', '');
					$tpl->set_var ('slash', '');
				}
				$tpl->parse ('rows_1', 'row_1', TRUE);
			} else {
				$tpl->parse ('rows_1', 'row_1_inactive', TRUE);
			}
		}
		$work_factory->group_1 = $group_1;
		$read_url->set_var ('type', $group_1);

		$list = $work_factory->group_2_factory->create_all ();
		$item0 = &$work_factory->group_2_factory->create (0);
		$item0->name = '���';
		array_unshift ($list, $item0); 

		foreach ($list as $i => $dummy) {
			$item = &$list[$i];

			$work_factory->group_2 = $item->id;
			$count = $work_factory->get_total_count ();
			$item->name .= $count ? sprintf (' (%d)', $count) : '';
			$item->set_template_vars ($tpl);

			if ($count > 0) {
				$read_url->set_var ('dir', $item->id);
				$tpl->set_var ('read_url', $read_url->build ());
				if ($group_2 - $item->id == 0 && empty ($show)) {
					$tpl->set_var ('active', 'active');
					$tpl->set_var ('slash', '/');
				} else {
					$tpl->set_var ('active', '');
					$tpl->set_var ('slash', '');
				}
				$tpl->parse ('rows_2', 'row_2', TRUE);
			} else {
				$tpl->parse ('rows_2', 'row_2_inactive', TRUE);
			}
		}
		$work_factory->group_2 = $group_2;


		$read_url = new URL ("/we/clients/");
	//	$read_url = new URL ("");
	//	$read_url->set_var ('show', 'clients');
		$tpl->set_var ('clients_url', $read_url->build ());
		if ($show == 'clients') {
			$tpl->set_var ('active', 'active');
			$tpl->set_var ('slash', '/');
		} else {
			$tpl->set_var ('active', '');
			$tpl->set_var ('slash', '');
		}

		$tpl->parse ('portfolio_right_menu', 'portfolio_right_menu');
		$main_page->right_menu_block = $tpl->get_var ('portfolio_right_menu');

	}
	// END right menu



	$main_page->structure = &$structure;
	$main_page->content = $text . $tpl->get_var ('portfolio_final');
//	$main_page->use_variables = TRUE;
	$main_page->output ();

	/*
	$portfolio = new PortfolioFactory ($config, $request);

	if ($module_subname == 'document') {
		$structure->runPageAsDefault ($type, $last->id);

	} elseif (! $order->procceed ('/request/ok/')) {
		$tpl = new Template ($config, MY_TPL_PATH);
		$tpl->set_file ('order', 'order.html');
		$tpl->set_block ('order', 'row', 'rows');

		$types = $order->get_types ();

		if ($types)
		foreach ($types as $id => $name) {
			$tpl->set_var ('id', $id);
			$tpl->set_var ('name', $name);
			$tpl->set_var ('checked', $id == $type ? ' CHECKED' : '');

			$tpl->parse ('rows', 'row', TRUE);
		}

		$tpl->parse ('order', 'order');


		include_once ('main.inc');
		$main_page->structure = &$structure;
		$main_page->content = $last->content . $tpl->get_var ('order');
		$main_page->output ();

	}
	*/
?>