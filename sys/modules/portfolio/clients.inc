<?php

class ClientItem extends ListItem {
 	function ClientItem ($id, &$factory) {
		parent::ListItem ($id, $factory);

	    $this->image = new Image ();
	}

	function load_from_row ($row) {
        $this->ord_index = $row->ord_index;
        $this->publish = $row->publish;
        $this->href = $row->href;
        $this->title = $row->title;
        $this->announce = $row->announce;
        $this->content = $row->content;

		$this->image->unpack_string ($row->image);
    }

	function load_defaults () {
  		$this->publish = 1;
		// $this->portfolio_id = $this->factory->request->_get['portfolio_id'];
	}
    
	function get_update_array () {
		return array (
			'image' => $this->image->pack_data (),
			'publish' => $this->publish,
			'href' => $this->href,
			'title' => $this->title,
			'announce' => $this->announce,
			'content' => $this->content
		);	
	}
	
	function get_insert_array () {
		return array (
			'ord_index' => $this->factory->get_next_index (),
			'image' => $this->image->pack_data (),
			'publish' => $this->publish,
			'href' => $this->href,
			'title' => $this->title,
			'announce' => $this->announce,
			'content' => $this->content
		);	
	}

	function load_from_parameters (&$request) {
		$this->publish = $request->_post['client_publish'];
		$this->href = $request->_post['client_href'];
		$this->title = $request->_post['client_title'];
		$this->announce = $request->_post['client_announce'];
		$this->content = $request->_post['client_content'];

		$this->image->proceed_parameters ($request, 'client_image', 'clients', 'client_', $this->id);
	}
	
	function set_template_vars (&$tpl, $form = FALSE) {
		$tpl->set_var ('client_id', $this->id);
		$tpl->set_var ('client_ord_index', $this->ord_index);
		$tpl->set_var ('client_href', $this->href);
		$tpl->set_var ('client_title', $this->title);
		$tpl->set_var ('client_announce', $this->announce);
		$tpl->set_var ('client_content', $this->content);

		$tpl->set_var ('client_image_tag', $this->image->get_img_tag (0, 0, FALSE, 0, MY_HTTP_ROOT_PATH, $this->title));

		$tpl->set_var ('client_image_or_title', $this->image->image ? $tpl->get_var ('client_image_tag') : "<b>".$this->title."</b>");

		$tpl->set_var ('client_image_or_title_thin', $this->image->image ? $this->image->get_img_tag (80, 0, FALSE, 0, MY_HTTP_ROOT_PATH, htmlspecialchars ($this->title)) : "<b>".$this->title."</b>");

		$tpl->set_var ('client_publish_full', $this->publish ? '<font color=009900>��</font>' : '<font color=990000>���</font>');

		if ($form) {
			$tpl->set_var ('client_image_ctl', $this->image->control ('client_image', TRUE));

			$tpl->set_var ('client_publish_checkbox', common_checkbox ('client_publish', 1, $this->publish));
		}
	}
}

class ClientFactory extends ListFactory {
	var $front = FALSE;
	var $do_cache = NULL;

	function ClientFactory (&$config, &$request) {
		$this->ListFactory ($config, $request, 'clients', 'id', 'ord_index', 'ClientItem', TRUE);
	}

	/*
	function &create_output_list ($limit = NULL, $offset = NULL) {
		$limit_str = $limit ? sprintf (" LIMIT %s%d", $offset ? sprintf ("%d, ", $offset) : '', $limit) : '';
		$query_str = sprintf ("SELECT * FROM %s WHERE COALESCE(announce, '') <> '' AND NOW() < arch_date ORDER BY date DESC, id DESC%s", $this->table_name, $limit_str);
		return $this->create_from_query ($query_str);
	}

	function create_first_list ($limit = NULL, $offset = NULL) {
		$this->first = TRUE;
		$list = $this->create_all ($limit, $offset);
		$this->first = FALSE;
		return $list;
	}
	*/

	function conditions () {
		parent::conditions ();
			
		if ($this->front) $this->cond = array ("t.publish > 0");  
		if ($this->do_cache) $this->order = array ("t.title ASC", "t.ord_index ASC"); 
	}

	var $list = NULL;

	function create_cache () {
		if ($this->list == NULL) {
			$this->list = array ();

			$this->do_cache = TRUE;
			if ($list = $this->create_all ())
			$this->do_cache = FALSE;
			for ($i = 0; $i < sizeof ($list); $i++) {
				$this->list[$list[$i]->id] = $list[$i]->title;
			}
		}
	}

	function find ($key) {
		$this->create_cache ();
		return getArrayValue ($key, $this->list);
	}

	function select_button ($name, $key, $add_empty_field = FALSE) {
		$this->create_cache ();
		if ($add_empty_field !== FALSE) {
			$list = array ('' => is_string ($add_empty_field) ? $add_empty_field : '');
			foreach ($this->list as $id => $val) 
				$list[$id] = $val;
		} else
			$list = &$this->list;
		return common_select_button ($name, $list, $key);
	}

}







?>