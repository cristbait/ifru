<?PHP

	switch ($module_subname) {
		case 'group_1':
		case 'group_2':
			$module_path .= '/groups';
			include ('groups/backend.inc');
			break;
		case 'clients':
			$module_path .= '/clients';
			include ('clients/backend.inc');
			break;
		case 'work':
		default:
			$module_path .= '/work';
			include ('work/backend.inc');
			break;
//			$module_path .= '/category';
//			include ('category/backend.inc');
	}

?>