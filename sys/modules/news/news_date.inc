<?PHP
	
class NewsDate {
	var $date;
	var $year;
	var $month;

	function NewsDate ($date, $month = FALSE) {
		$this->setDate ($date, $month);
	}
	
	function parse_date () {
		$this->year = 0;
		$this->month = 0;
		
		if (preg_match ('/^([0-9]{4})-([0-9]{2})/', $this->date, $regs)) {
			$this->year = $regs[1];
			$this->month = $regs[2];
		}
	}

	function setDate ($date, $month = FALSE) {
		if ($month) {
			$this->year = $date;
			$this->month = $month;
			$this->date = sprintf ('%04d-%02d', $this->year, $this->month);
		} else {
			$this->date = $date;
			$this->parse_date ();
		}
	}

	function getMonthName () {
		$months = array (
			'������', '�������', '����', '������',
			'���', '����', '����', '������',
			'��������', '�������', '������', '�������'
			);
		return getArrayValue ($this->month - 1, $months);
	}

	function getName () {
		return sprintf ("%s %s", $this->getMonthName (), $this->year);
	}

}

?>