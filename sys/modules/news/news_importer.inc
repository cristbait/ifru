<?php

require_once ("engine/rss_importer.inc");
require_once ("news/news.inc");

class NewsImporter extends RSSImporter {
    var $parentTags = array('FEED', 'ENTRY', 'AUTHOR');

	var $feedTags = array('ID', 'UPDATED', 'TITLE', 'SUBTITLE', 'LINK', 'AUTHOR', 'GENERATOR', 'OPENSEARCH:TOTALRESULTS', 'OPENSEARCH:STARTINDEX', 'OPENSEARCH:ITEMSPERPAGE');

	var $entryTags = array('ID', 'CATEGORY', 'PUBLISHED', 'UPDATED', 'TITLE', 'AUTHOR', 'CONTENT', 'LINK', 'MEDIA:THUMBNAIL', 'THR:TOTAL');

	var $authorTags = array('NAME', 'EMAIL');

	var $multipleTags = array ('CATEGORY', 'LINK');

	var $seo = NULL;

	var $recordsCount = 0;

	function commitChanges () {
		/*
		print '<pre>';
		print_r ($this->getStructure());
		print_r ($this->getItems());
		return;
		*/

		$this->news = new NewsFactory ( $this->_config, $this->_request );

		$list = $this->getItems();
		for ( $i=0; $i<sizeof($list); $i++ ) {
			$item = &$list[$i];

			$rssid = trim ($item['id']);
			if (preg_match ('/post-([0-9]+)$/', $rssid, $regs))
				$rssid = $regs[1];
			$date = $this->_getDateFromPubdate ($item['published']);
			$archDate = $this->_addMonths ($item['published']);

			$title = ! empty ($item['title']['cdata-value']) ? trim ($item['title']['cdata-value']) : trim ($item['title']);
			$content = ! empty ($item['content']['cdata-value']) ? trim ($item['content']['cdata-value']) : trim ($item['content']);
			if (empty ($title))	continue;

			$announce = $content;
			if (preg_match ("/^(.*)<a name='more'>/m", $announce, $regs))
				$announce = $regs[1];

			$href = '';
			$links = $item['link'];
			foreach ($links as $id => $temp)
				if ($temp['rel'] == 'alternate')
					$href = $temp['href'];

			/*
			 print $rssid . "<br>";
			 print $date . "<br>";
			 print $archDate . "<br>";
			 print $title . "<br>";
			 print $announce . "<br>";
			 print $content . "<br>";
			 print $link . "<br>";
			 print "<br>";
			 continue;
			*/

			$news = &$this->_findOrCreate ($rssid);

			$news->rss_id = $rssid;
			$news->rss_id = $rssid;
			$news->date = $date;
			$news->arch_date = $archDate;
			$news->title = $title;
			$news->announce = $announce;
			$news->content = $content;
			$news->href = $href;
			$news->update ();

			$this->recordsCount++;

		}

		$this->postProcceed ();
	}

	function postProcceed () {
	}

	function &_findOrCreate ($id) {
		$f = $this->news;
		$item = &$f->find_by_rss_id ($id);

		if (! is_object ($item)) {
			$f->clear_cache (array (NULL, 0));
			$item = $f->create_new (NULL, FALSE);
			$item->load_defaults ($id);
			$item->update ();

		} elseif (! $item->publish) {
			$item->publish = 1;
			$item->update ();
		}

		return $item;
	}

	function _getDateFromPubdate ($pubdate) {
		return date ('Y-m-d H:i:s', strtotime ($pubdate));
	}

	function _addMonths ($pubdate, $months = 3) {
		$d = strtotime ($pubdate);
		return date("Y-m-d H:i:s", mktime(date('H',$d), date('i',$d), date('s',$d), date('m',$d)+$months, date('d',$d), date('Y',$d)));
	}

	/*
	function mapItemToRow ( &$item ) {
		$row = array (
		  'n_ntann_id' => $this->_getIdFromLink ($item['link']),
		  'n_category_id' => $this->_findOrCreateCategory ($item['category'][0]),
		  'n_date' => ,
		  'n_title' => addslashes ($item['title']),
		  'n_text' => addslashes ($item['yandex:full-text']),
		  'n_source' => $item['category'][1],
		  'n_image' => $item['enclosure']['url'],
		);

		return $row;
	}

	function _getIdFromLink ($link) {
		if (preg_match ('/[?&]id=([0-9]+)([?&]|$)/i', $link, $regs))
			return $regs[1];

		return 0;
	}
	*/
}

?>