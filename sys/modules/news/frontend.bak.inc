<?PHP

	include_once ('main.inc');

	// $meta_keywords = '';
	// $meta_description = '';
	
	// $factory = new PageTextFactory ($config, $request);
	// $text = $factory->getText ('advert');

	$factory = new NewsFactory ($config, $request);
	
	$page_size = 20;

	$arch = $request->_get['arch'];
	$news_id = $request->_get['item'];
	$page = $request->_get['page'];

	$list_url = new URL ("");
	$list_url->set_var ('page', $page);

	if (preg_match ('/^([0-9]{4})-([0-9]{2})$/', $arch, $regs)) {
		$factory->a_date = TRUE;
		$factory->a_year = $regs[1];
		$factory->a_month = $regs[2];
	}

	if ($arch) {
		$main_page->url_path = array ($list_url->get_url () => '�������');
		$supTitle = '�����';
		$main_page->page_name = '����� ��������';

		if ($factory->a_date) {
			$list_url->set_var ('arch', 'yes');
			$main_page->url_path[$list_url->get_url ()] = '�����';

			$d = new NewsDate ($arch);
			$supTitle = $d->getName ();
			$main_page->page_name .= ' - ' . $d->getName ();
		}
		
		$list_url->set_var ('arch', $arch);

	} else {
		$supTitle = '�������';
		$main_page->page_name = '�������';
	}

	$read_url = new URL ($list_url->get_url ());

	$tpl = new Template ($config, MY_TPL_PATH);
	$tpl->set_file ('news', 'news.html');

	if (ereg ('^[0-9]+$', $news_id)) {
		$main_page->url_path[$list_url->get_url ()] = $supTitle;

		$tpl->set_block ('news', 'news_show', 'news_show');
		$item = $factory->create ($news_id);

		$main_page->page_name .= ' - '.$item->title;
		$list_url->set_var ('item', $item->id);
		// $main_page->url_path[$list_url->get_url ()] = $item->title;

		if (empty ($item->content))
			$item->content = $item->announce;

		$item->nl2br ();
		$item->set_template_vars ($tpl);

		$tpl->parse ('news', 'news_show');

	} elseif ($arch && ! $factory->a_date) {
		$item_list = $factory->create_archive_date_list ();

		$tpl->set_block ('news', 'news_date_list', 'news_date_list');
		$tpl->set_block ('news_date_list', 'news_row', 'news_rows');
		$tpl->set_block ('news_row', 'delimeter', 'delimeter_tpl');
		$tpl->set_var ('news_rows', '');

		$date_url = new URL ('');

		if ($item_list)
		for ($i = 0; $i < sizeof ($item_list); $i++) {
			$item = &$item_list[$i];
		
 			$date_url->set_var ('arch', $item->date);
			$tpl->set_var ('date_url', $date_url->get_url ());

			$tpl->set_var ('date_title', $item->getName ());

			if ($i + 1 < sizeof ($item_list))
				$tpl->parse ('delimeter_tpl', 'delimeter');
			else
				$tpl->set_var ('delimeter_tpl', '');

			$tpl->parse ('news_rows', 'news_row', true);
		}

		$tpl->parse ('news', 'news_date_list');

	} else {
		$pager = new Pager ($factory, $request, $page_size, $list_url->get_url (), 'page', 'item');
		$main_page->pager_obj = &$pager;

		if ($arch) {
			$pager->total_size = $factory->get_archive_count ();
			$pager->update ();
			$t_name = 'news_archive_list';
			$item_list = $factory->create_archive_list ($pager->page_size, $pager->offset);
		} else {
			$pager->total_size = $factory->get_output_count ();
			$pager->update ();
			$t_name = 'news_list';
			$item_list = $factory->create_output_list ($pager->page_size, $pager->offset);
		}

		$tpl->set_block ('news', $t_name, $t_name);
		$tpl->set_block ($t_name, 'news_row', 'news_rows');
		$tpl->set_block ('news_row', 'more_block', 'more_block_tpl');
		$tpl->set_block ('news_row', 'delimeter', 'delimeter_tpl');
		$tpl->set_var ('news_rows', '');

		if ($item_list)
		for ($i = 0; $i < sizeof ($item_list); $i++) {
			$item = &$item_list[$i];
		
			$item->nl2br ();
			$item->set_template_vars ($tpl);
			$read_url->set_var ('item', $item->id);
			$tpl->set_var ('news_read_url', $read_url->get_url ());

			if ($i + 1 < sizeof ($item_list))
				$tpl->parse ('delimeter_tpl', 'delimeter');
			else
				$tpl->set_var ('delimeter_tpl', '');

			if (! empty ($item->content))
				$tpl->parse ('more_block_tpl', 'more_block');
			else
				$tpl->set_var ('more_block_tpl', '');

			$tpl->parse ('news_rows', 'news_row', true);
		}

		$tpl->set_var ('pager', $main_page->get_pager ());

		$tpl->parse ('news', $t_name);
	}

	
	$main_page->content = $tpl->get_var ('news');
	$main_page->structure = &$structure;
	$main_page->use_variables = TRUE;
	$main_page->output ();

?>

