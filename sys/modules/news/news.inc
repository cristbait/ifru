<?PHP

$factory_class = "NewsFactory";

include_once ('news_date.inc');

class NewsItem extends ListItem {
 	function NewsItem ($id, &$factory) {
		parent::ListItem ($id, $factory);

	    $this->image = new Image ();
	    $this->small_image = new Image ();
	}

   function load_from_row ($row) {
        $this->date = $row->date;
        $this->rss_id = $row->rss_id;
        $this->publish = $row->publish;
        $this->arch_date = $row->arch_date;
        $this->ord_index = $row->ord_index;
        $this->title = $row->title;
        $this->announce = $row->announce;
        $this->content = $row->content;
		$this->normal_send = $row->normal_send;
		$this->href = $row->href;

		$this->image->unpack_string ($row->image);
		$this->small_image->unpack_string ($row->small_image);
    }

	function load_defaults ($rss_id = NULL) {
		$this->date = date ('Y-m-d H:i:s');
		$this->arch_date = date ('Y-m-d', mktime (date('H'), date('i'), date('s'), date ('m') + 3, date ('d'), date ('Y')));
		$this->rss_id = $rss_id;
		$this->publish = 1;
	}
    
	function get_update_array () {
		return array (
			'image' => $this->image->pack_data (),
			'small_image' => $this->small_image->pack_data (),
			'normal_send' => $this->normal_send,
			'publish' => $this->publish,
			'title' => $this->title,
			'rss_id' => $this->rss_id,
			'date' => $this->date,
			'href' => $this->href,
			'arch_date' => $this->arch_date,
			'announce' => $this->announce,
			'content' => $this->content
		);	
	}
	
	function get_insert_array () {
		return array (
			'image' => $this->image->pack_data (),
			'small_image' => $this->small_image->pack_data (),
			'normal_send' => $this->normal_send,
			'publish' => $this->publish,
			'date' => $this->date,
			'href' => $this->href,
			'rss_id' => $this->rss_id,
			'arch_date' => $this->arch_date,
			'ord_index' => $this->factory->get_next_index (),
			'title' => $this->title,
			'announce' => $this->announce,
			'content' => $this->content
		);	
	}

	function load_from_parameters (&$request) {
		$this->date = $request->_post['news_date'];
		$this->rss_id = $request->_post['news_rss_id'];
		$this->arch_date = $request->_post['news_arch_date'];
		$this->publish = $request->_post['news_publish'];
		$this->title = $request->_post['news_title'];
		$this->announce = $request->_post['news_announce'];
		$this->content = $request->_post['news_content'];
		$this->normal_send = $request->_post['news_normal_send'];
		$this->href = $request->_post['news_href'];

		$this->image->proceed_parameters ($request, 'news_image', 'news', 'image_', $this->id);
		$this->small_image->proceed_parameters ($request, 'news_small_image', 'news', 'small_image_', $this->id);
	}
	
	function set_template_vars (&$tpl, $form = FALSE) {
		$tpl->set_var ('news_id', $this->id);
		$tpl->set_var ('news_rss_id', $this->rss_id);
		$tpl->set_var ('news_date', $this->date);
		$tpl->set_var ('news_arch_date', $this->arch_date);
		$tpl->set_var ('news_ord_index', $this->ord_index);
		$tpl->set_var ('news_title', $this->title);
		$tpl->set_var ('news_href', $this->href);
		$tpl->set_var ('news_announce', $this->announce);
		$tpl->set_var ('news_content', $this->content);
		$tpl->set_var ('news_normal_send', $this->normal_send);
		$tpl->set_var ('news_normal_send_full', $this->normal_send ? '��' : '���');
		$tpl->set_var ('news_publish', $this->publish);
		$tpl->set_var ('news_publish_full', $this->publish ? '<font color=009900>��</font>' : '<font color=990000>���</font>');

		$tpl->set_var ('news_date_t', datef ("d.m.Y", $this->date));
		$tpl->set_var ('news_date_r', datef ("d/m/Y", $this->date));
		$tpl->set_var ('news_arch_date_t', datef ("d.m.Y", $this->arch_date));
		$tpl->set_var ('news_arch_date_r', datef ("d/m/Y", $this->arch_date));

		$tpl->set_var ('news_index_image_tag', $this->small_image->get_img_tag (100, 100, TRUE, 0, MY_HTTP_ROOT_PATH, '', ' style="margin-bottom:10px;margin-'.($this->small_image->halign=='right'?'left':'right').':10px;"')); 
		$tpl->set_var ('news_small_image_tag', $this->small_image->get_img_tag (100, 100, NULL, 0, MY_HTTP_ROOT_PATH, '', ' vspace="5"'));
		$tpl->set_var ('news_image_tag', $this->image->get_img_tag (300, 300, TRUE, 0, MY_HTTP_ROOT_PATH, ''));

		$tpl->set_var ('news_announce_for_index', $this->title);

		if ($form) {
			$tpl->set_var ('publish_checkbox', common_checkbox ('news_publish', 1, $this->publish));
			$tpl->set_var ('normal_send_checkbox', common_checkbox ('news_normal_send', 1, $this->normal_send));

			$tpl->set_var ('news_image_ctl', $this->image->control ('news_image', TRUE));
			$tpl->set_var ('news_small_image_ctl', $this->small_image->control ('news_small_image', TRUE));
		}
	}
}

class NewsFactory extends ListFactory {
	var $a_date = FALSE;
	var $a_year;
	var $a_month;

	function NewsFactory (&$config, &$request) {
		$this->ListFactory ($config, $request, 'news', 'id', 'date', 'NewsItem', FALSE);
	}

	function &create_all ($limit = NULL, $offset = NULL) {
		$limit_str = $limit ? sprintf (" LIMIT %s%d", $offset ? sprintf ("%d, ", $offset) : '', $limit) : '';
		$query_str = sprintf ("SELECT * FROM %s ORDER BY date DESC, id DESC%s", $this->table_name, $limit_str);
		return $this->create_from_query ($query_str);
	}

	function &create_first_list ($limit = NULL, $offset = NULL) {
		$limit_str = $limit ? sprintf (" LIMIT %s%d", $offset ? sprintf ("%d, ", $offset) : '', $limit) : '';
		$query_str = sprintf ("SELECT * FROM %s WHERE COALESCE(title, '') <> '' AND publish = 1 ORDER BY date DESC, id DESC%s", $this->table_name, $limit_str);
		return $this->create_from_query ($query_str);
	}

	function &create_output_list ($limit = NULL, $offset = NULL) {
		$limit_str = $limit ? sprintf (" LIMIT %s%d", $offset ? sprintf ("%d, ", $offset) : '', $limit) : '';
		$query_str = sprintf ("SELECT * FROM %s WHERE (COALESCE(announce, '') <> '' OR COALESCE(title, '') <> '') AND publish = 1 ORDER BY date DESC, id DESC%s", $this->table_name, $limit_str);
		return $this->create_from_query ($query_str);
	}

	function get_output_count () {
		$row = $this->mysql->select_field (sprintf ("SELECT count(*) AS total_count FROM %s WHERE (COALESCE(announce, '') <> '' OR COALESCE(title, '') <> '') AND publish = 1", $this->table_name));
	
		return $row[0];
	}

	/*
	function &create_output_list ($limit = NULL, $offset = NULL) {
		$limit_str = $limit ? sprintf (" LIMIT %s%d", $offset ? sprintf ("%d, ", $offset) : '', $limit) : '';
		$query_str = sprintf ("SELECT * FROM %s WHERE (COALESCE(announce, '') <> '' OR COALESCE(title, '') <> '')  AND NOW() < arch_date ORDER BY date DESC, id DESC%s", $this->table_name, $limit_str);
		return $this->create_from_query ($query_str);
	}

	function get_output_count () {
		$row = $this->mysql->select_field (sprintf ("SELECT count(*) AS total_count FROM %s WHERE (COALESCE(announce, '') <> '' OR COALESCE(title, '') <> '') AND NOW() < arch_date", $this->table_name));
	
		return $row[0];
	}
	*/

	function &create_archive_list ($limit = NULL, $offset = NULL) {
		if ($this->a_date) {
			$date_str = sprintf ("MONTH(date) = '%d' AND YEAR(date) = '%d'", $this->a_month, $this->a_year);
		} else {
			$date_str = 'NOW() >= arch_date';
		}

		$limit_str = $limit ? sprintf (" LIMIT %s%d", $offset ? sprintf ("%d, ", $offset) : '', $limit) : '';
		$query_str = sprintf ("SELECT * FROM %s WHERE COALESCE(announce, '') <> '' AND %s ORDER BY date DESC, id DESC%s", $this->table_name, $date_str, $limit_str);
		return $this->create_from_query ($query_str);
	}

	function get_archive_count () {
		if ($this->a_date) {
			$date_str = sprintf ("MONTH(date) = '%d' AND YEAR(date) = '%d'", $this->a_month, $this->a_year);
		} else {
			$date_str = 'NOW() >= arch_date';
		}

		$row = $this->mysql->select_field (sprintf ("SELECT count(*) AS total_count FROM %s WHERE COALESCE(announce, '') <> '' AND %s", $this->table_name, $date_str));
	
		return $row[0];
	}

	function &create_archive_date_list () {
		$list = $this->mysql->select_field (sprintf ("SELECT DISTINCT DATE_FORMAT(date, '%%Y-%%m') as date FROM %s WHERE date > 0 ORDER BY date DESC", $this->table_name));

		$result = array ();

		if ($list)
		for ($i = 0; $i < sizeof ($list); $i++) {
			$result[] = new NewsDate ($list[$i]);
		}

		return $result;
	}


	function &find_by_rss_id ($rss_id) {
		$list = $this->create_from_query (sprintf ("SELECT * FROM %s WHERE rss_id='%s'", $this->table_name, $rss_id));

		if ($list)
			return $list[0];

		$list = NULL;
		return $list;
	}

}


?>