<?php

	$last = &$structure->getLast ();

	global $main_page;
	include_once ('main.inc');
	$main_page->structure = &$structure;

// 	$parent = &$last->get_parent ();

	if (preg_match ('/{topic_list}/', $last->content)) {
		$content = "";

		global $user_factory;
		include_once (MY_MODULES_PATH."/questions/questions.inc");

		$questions = new QuestionTree ($config, $request);
		$test = &$last->get_nearest_section ();
		$etaps = &$structure->load_by_document_type ('etap_esse', $test->id);
		$etap = $etaps[0];
		$questions->load_context ($test, $etap, $user_factory->loggedUser);
		$topic = &$questions->create_topic_list ();

		if ($topic->id) {
			$content = "<h2>".$topic->name."</h2>\n";
			if (! empty ($topic->children)) {
				$content .= "<ol>\n";
				foreach ($topic->children as $i => $child) {
					$content .= "<li>".$child->name."</li>\n";
				}
				$content .= "</ol>\n";
			} else {
				$content .= "<b>������ ��� �������� �� ��������</b>";
			}
		} else {
			$content = "<h2>������ ��� �������� �� ��������</h2>";
		}

		$last->content = preg_replace ('/{topic_list}/', $content, $last->content);

	}

/*
	if (preg_match ('/{topic_list_([0-9]+)\/([0-9]+)}/', $last->content, $regs)) {
		$content = "";

		global $user_factory;
		include_once (MY_MODULES_PATH."/questions/questions.inc");

		$questions = new QuestionTree ($config, $request);
		$test = $etap = NULL;
		$questions->load_context ($test, $etap, $user_factory->loggedUser);
		$topic = &$questions->create_topic_list ($regs[1], $regs[2]);

		if ($topic->id) {
			$content = "<h2>".$topic->name."</h2>\n";
			if (! empty ($topic->children)) {
				$content .= "<ol>\n";
				foreach ($topic->children as $i => $child) {
					$content .= "<li>".$child->name."</li>\n";
				}
				$content .= "</ol>\n";
			} else {
				$content .= "<b>������ ��� �������� �� ��������</b>";
			}
		} else {
			$content = "<h2>������ ��� �������� �� ��������</h2>";
		}

		$last->content = preg_replace ('/{topic_list_([0-9]+\/[0-9]+)}/', $content, $last->content);

	}
*/


	$main_page->content = $last->content;
	$main_page->output ();


?>