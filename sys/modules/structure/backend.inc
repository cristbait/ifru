<?PHP

	$factory = new StructureTree ($config, $request);
	$id_name = 'structure_id';

	$tpl = new Template ($config, $module_path);

	$list_url = new URL ($mod_url->build ());
	$list_url->set_var ('area', 'tree');
//	$list_url->set_var ('parent_id', $_GET['parent_id']);
	$tpl->set_var ('list_url', $list_url->build ());

	$edit_url = new URL ($mod_url->build ());
	$edit_url->set_var ('area', 'edit');
	$edit_url->set_var ('parent_id', $_GET['parent_id']);
	$tpl->set_var ('edit_url', $edit_url->build ());

    $stat_url = new URL ();
    $stat_url->set_var ('mod', 'user_stat');
    $stat_url->set_var ('area', 'list');
	$tpl->set_var ('stat_url', $stat_url->build ());

	switch ($_GET['area']) {
	case 'edit':
		if ($request->_get['parent_id']) {
			$parent = $factory->create ($request->_get['parent_id']);
			$factory->set_children_classes_for ($request->_get['parent_id']);
		} elseif (($id = $request->_get[$id_name]) || ($id = $request->_post[$id_name])) {
			$item = $factory->create ($id);
			if ($item->parent_id) {
				$parent = $factory->create ($item->parent_id);
				$factory->set_children_classes_for ($item->parent_id);
			} else {
				$parent = NULL;
				$factory->set_children_classes_for (NULL);
			}
		} else {
			$parent = NULL;
			$factory->set_children_classes_for (NULL);
		}

		if (!$factory->default_edit_action ($edit_url->build (), $list_url->build (), $id_name)) {
			$item = $factory->create ($request->_get[$id_name]);
			
			if ($item->id && preg_match ('/^etap/', $item->class) || !$item->id && preg_match ('/^subsform/', $parent->class))
				$tpl->set_file ('edit', 'backend_edit_tour.html');
			else
				$tpl->set_file ('edit', 'backend_edit.html');
		
			if ($item->parent_id)
				$factory->set_children_classes_for ($item->parent_id);
			
			if ($item->id) 
				$tpl->set_var ('title', sprintf ("��������: '%s'", $item->name));
			elseif ($parent)
				$tpl->set_var ('title', sprintf ("����� �������� � ����� '%s'", $parent->name));
			else
				$tpl->set_var ('title', "����� ��������");

			// $tpl->set_var ('common_formatting', common_formatting ());

			$item->htmlspecialchars ();
			$item->set_template_vars ($tpl, TRUE);
			
			$cancel_url = new URL ($list_url->build ());
			$cancel_url->set_var ($id_name, $item->id);

			$tpl->set_var ('edit_buttons_row', common_edit_buttons_row ($cancel_url->build (), true));

			$tpl->parse ('edit', 'edit');
			$tpl->p_default ();
		}
		break;

	case 'list':
		if (! $factory->default_list_action ($list_url->build (), $id_name)) {
			$tpl->set_file ('list', 'backend_list.html');
		
			$sub_url = new URL ($list_url->build ());

			if ($parent) {
				$tpl->set_var ('title', sprintf ("��������� ���������� ����� '%s'", $parent->name));
				$tpl->set_var ('parent_path', $parent->get_path_string ($sub_url, '������� �������'));
			} else {
				$tpl->set_var ('title', "��������� ����������");
				$tpl->set_var ('parent_path', "������� �������");
			}

			// $pager = new Pager ($factory, $request, $config->page_size, $list_url->build (), 'page', $id_name);
			$pager = NULL;
		
			$item_list = $factory->create_all ();

			$tpl->set_block ('list', 'row', 'rows');
			$tpl->set_block ('list', 'parent_row', 'parent_rows');
			$color = another_color ();

			if ($parent) {
				$tpl->set_var ('row_color', $color = another_color ($color));
				$sub_url->set_var ('parent_id', $parent->parent_id);
				$tpl->set_var ('parent_url', $sub_url->build ());
				$tpl->parse ('rows', 'parent_row', TRUE);
			}

			if ($item_list)
			for ($i = 0; $i < count ($item_list); $i++) {
				$item = &$item_list[$i];
			
				$tpl->set_var ('id_checkbox',
					common_checkbox ($id_name.'[]', $item->id, $request->_get['select_'.$id_name] == $item->id));
				$tpl->set_var ('row_color', $color = another_color ($color));
				$tpl->set_var ('row_number', $i + $pager->offset + 1);
			
				$item->make_tree_sublinks ($sub_url);
				$item->set_template_vars ($tpl);

				$tpl->set_var ('list_item_buttons', common_list_item_buttons (
					$list_url->build (), $edit_url->build (), $id_name, $item->id));

				$tpl->parse ('rows', 'row', TRUE);
			}
		
			$tpl->set_var ('tree_list_buttons', common_tree_list_buttons (
				$list_url->build (), $edit_url->build (), $id_name, $factory->movable));
			$tpl->set_var ('pager', ($pager && $pager->page_count > 1) ? common_pager ($pager) : '');
			$tpl->set_var ('total', $pager ? $pager->total_size : sizeof ($item_list));
		
			$tpl->parse ('list', 'list');
			$tpl->p_default ();
		}
	case 'tree':
	default:
		if (! $factory->default_list_action ($list_url->build (), $id_name)) {
			$tpl->set_file ('list', 'backend_tree.html');
		
			$sub_url = new URL ($list_url->build ());

			$tpl->set_var ('title', "��������� ����������");

			// $pager = new Pager ($factory, $request, $config->page_size, $list_url->build (), 'page', $id_name);
			$pager = NULL;
		
			$factory->set_parent (NULL);
			$tree = $factory->create_tree ();

			$tpl->set_block ('list', 'row', 'rows');
			$tpl->set_block ('list', 'parent_row', 'parent_rows');
			$color = another_color ();

			if ($tree)
			for ($i = 0; $i < count ($tree); $i++) {
				$item = &$tree[$i];
				$tpl->set_var ('id_checkbox',
					common_checkbox ($id_name.'[]', $item->id, $request->_get['select_'.$id_name] == $item->id));
				$tpl->set_var ('row_color', $color = another_color2 ($color));
			
				// $item->make_tree_sublinks ($sub_url);
				$item->name = "<b>".$item->name."</b>";
				$item->set_template_vars ($tpl);

                $edit_url->set_var ($id_name, $item->id);
                $tpl->set_var ('edit_link', $edit_url->build ());
                $tpl->set_var ('list_item_buttons', common_list_item_buttons (
					$list_url->build (), $edit_url->build (), $id_name, $item->id));

                $stat_url->set_var ('s', $item->id);
                if ($item->class != 'subsform_olymp') {
                    $stat_item_buttons = '';
                }
                else {
                    $stat_item_buttons = common_stat_item_buttons($stat_url->build(), $edit_url->build(), $id_name, $item->id);
                }
                $tpl->set_var ('stat_item_buttons', $stat_item_buttons);

				$tpl->set_var ('name_prefix', $item->level ? preg_replace ('/9/', '&nbsp;&nbsp;&nbsp;', $item->level * 10 - 1) : '');

				$tpl->parse ('rows', 'row', TRUE);

				show_next_level ($tpl, $item, $color, $sub_url, $list_url, $edit_url, $id_name, $request);
			}
		
			$tpl->set_var ('tree_list_buttons', common_tree_list_buttons (
				$list_url->build (), $edit_url->build (), $id_name, $factory->movable));
			$tpl->set_var ('pager', ($pager && $pager->page_count > 1) ? common_pager ($pager) : '');
			$tpl->set_var ('total', $pager ? $pager->total_size : sizeof ($tree));
		
			$tpl->parse ('list', 'list');
			$tpl->p_default ();
		}
	}


	function show_next_level (&$tpl, &$root, &$color, &$sub_url, &$list_url, &$edit_url, $id_name, &$request) {
		
		$tree = &$root->children;

		if ($tree)
		for ($i = 0; $i < count ($tree); $i++) {
			$item = &$tree[$i];
		
			$tpl->set_var ('id_checkbox',
				common_checkbox ($id_name.'[]', $item->id, $request->_get['select_'.$id_name] == $item->id));
			$tpl->set_var ('row_color', $color = another_color2 ($color));
		
			// $item->make_tree_sublinks ($sub_url);
			$item->set_template_vars ($tpl);

            $edit_url->set_var ($id_name, $item->id);
            $tpl->set_var ('edit_link', $edit_url->build ());

            if ($item->class != 'subsform_olymp') {
                $tpl->set_var ('stat_item_buttons', '');
            }

			$tpl->set_var ('list_item_buttons', common_list_item_buttons (
				$list_url->build (), $edit_url->build (), $id_name, $item->id));

			$tpl->set_var ('name_prefix', $item->level ? preg_replace ('/9/', '&nbsp;&nbsp;&nbsp;', pow(10, $item->level*2) - 1) : '');

			$tpl->parse ('rows', 'row', TRUE);

			show_next_level ($tpl, $item, $color, $sub_url, $list_url, $edit_url, $id_name, $request);
		}
	}

?>