<?PHP

$factory_class = "CountryFactory";

class CitytypeItem extends ListItem {
   function load_from_row ($row) {
        $this->publish = $row->publish;
        $this->name = $row->name;
        $this->ord_index = $row->ord_index;
        $this->announce = $row->announce;
    }

	function load_defaults () {
		$this->publish = 1;
	}
    
	function get_update_array () {
		return array (
			'name' => $this->name,
			'publish' => $this->publish,
			'announce' => $this->announce
		);	
	}
	
	function get_insert_array () {
		return array (
			'ord_index' => $this->factory->get_next_index (),
			'name' => $this->name,
			'publish' => $this->publish,
			'announce' => $this->announce
		);	
	}

	function load_from_parameters (&$request) {
		$this->name = $request->_post['citytypes_name'];
		$this->announce = $request->_post['citytypes_announce'];
		$this->publish = $request->_post['citytypes_publish'];
	}
	
	function set_template_vars (&$tpl, $form = FALSE) {
		$tpl->set_var ('citytypes_id', $this->id);
		$tpl->set_var ('citytypes_ord_index', $this->ord_index);
		$tpl->set_var ('citytypes_name', $this->name);
		$tpl->set_var ('citytypes_announce', $this->announce);
		$tpl->set_var ('citytypes_publish', $this->publish);
		$tpl->set_var ('citytypes_publish_full', $this->publish ? '<font color=green>��</font>' : '���');

		if ($form) {
			$tpl->set_var ('citytypes_publish_checkbox', common_checkbox ('citytypes_publish', 1, $this->publish));
		}
	}
}

class CitytypeFactory extends ListFactory {
	function CitytypeFactory (&$config, &$request) {
		$this->ListFactory ($config, $request, 'citytypes', 'id', 'ord_index', 'CitytypeItem', TRUE);
	}
}



?>