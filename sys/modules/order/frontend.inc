<?php

	$stack = &$structure->rewrite_stack;
	$last = &$stack[sizeof ($stack) - 1];
	$type = array_shift ($structure->rewrite_pool);

	$last->no_request = TRUE;
	$address = $last->get_page_address ();

	if ($module_subname == 'audit') {
		include_once ('order_audit.inc');
		$order = new OrderAudit ($config, $request);
	} else
		$order = new Order ($config, $request);

	if ($type == 'ok') {
		$structure->runPageAsDefault ($type, $last->id);

	} elseif (! $order->procceed ($address.'ok/')) {
		$tpl = new Template ($config, MY_TPL_PATH);
		$tpl->set_file ('order', $order->form_file);
		$tpl->set_block ('order', 'row', 'rows');
		$tpl->set_var ('form_action', $address);

		$types = $order->get_types ();

		if ($types)
		foreach ($types as $id => $name) {
			$tpl->set_var ('id', $id);
			$tpl->set_var ('name', $name);
			$tpl->set_var ('checked', $id == $type || ! empty ($order->types_checked[$id]) ? ' CHECKED' : '');

			$tpl->parse ('rows', 'row', TRUE);
		}

		$tpl->parse ('order', 'order');


		include_once ('main.inc');
		$main_page->structure = &$structure;
		$main_page->content = $last->content . $tpl->get_var ('order');

		$main_page->output ();

	}

?>