<?PHP

include_once ("order.inc");

$factory_class = "OrderAudit";

class OrderAudit extends Order {
	var $form_file = 'order_audit.html';
	var $types = array (
		'audit'	=> '����� �����',
		'site'	=> '��������� �� ����� �� ����������� ������',
	);

	var $types_checked = array (
		'audit'	=> 1
	);

	function procceed ($ok = 'ok/') {
		$post = &$this->request->_post;

		if ($post['name'] && $post['phone'] && $post['request']) {
			header ('Location: ' . $ok);
			// print ('Location: ' . $ok);

			$n = $this->get_next_order_number ("order_number_audit");

			$website = $post['website'];
			$company = $post['company'];
			$name = $post['name'];
			$phone = $post['phone'];
			$email = $post['email'];
			$request = $post['request'];
			$from = $this->dbc->get_var ("mail_from");
			$subject = "����� ������ ����� #$n on-line �� MagicDesign.ru";
			$to = $this->dbc->get_var ("mail_for_order");
			$date = date ('d.m.Y H:i');

			$sections = array ();
			$production = FALSE;
			$types = $this->get_types ();

			if ($types)
			foreach ($types as $id => $tname) {
				if (getArrayValue ('order_'.$id, $this->request->_post)) {
					// if (in_array ($id, $this->production))
					//	$production = TRUE;

					$sections[] = $tname;
				}
			}

			$body = "����� \"������ �����\" on-line <b>#$n</b> ������ �� �������: <b>$to</b>\n��������� ��������� ������� - ����� <b>Sales</b>\n\n<b>����� �������:</b> $date\n\n<b>������ � �������</b>\n<b>����:</b> $website\n<b>��������:</b> $company\n<b>���:</b> $name\n<b>�������:</b> $phone\n<b>E-mail:</b> $email\n\n<b>���������� �������:</b> " . join (', ', $sections) . "\n<b>�����������:</b>\n$request";

			include_once ('engine/email.inc');
			EmailSend ($from, $to, $subject, nl2br ($body), 'info@magicdesign.ru');

			return TRUE;
		}

		return FALSE;
	}
}

?>