<?PHP

$factory_class = "Order";

class Order {
	var $form_file = 'order.html';
	var $types = array (
		'site'	=> '����',
		'siteaudit'	=> '����� �����',
		'redesign'	=> '��������',
		'ike'	=> '��������-������-������',
		'fstyle'	=> '��������� �����',
		'art'	=> '���-�������'
	);

	var $types_checked = array (
		'none'	=> 1
	);
/*
	var $production = array (
		'posm', 
//		'expo',
		'poly', 
		'souvenir', 
//		'outdoor'
		);
*/

	var $config;
	var $request;

	function Order (&$config, &$request) {
		$this->config = &$config;
		$this->request = &$request;
		$this->dbc = $this->config->getDBConfig ();
	}

	function get_types () {
		return $this->types;
	}

	function get_next_order_number ($name = 'order_number') {
		$n = $this->dbc->get_var ($name);
		$this->dbc->set_var ($name, $n + 1);

		return $n + 1;
	}

	function procceed ($ok = '/order/ok/') {
		$post = &$this->request->_post;

		if ($post['name'] && $post['phone'] && $post['request']) {
			header ('Location: ' . $ok);
			// print ('Location: ' . $ok);

			$n = $this->get_next_order_number ();

			$company = $post['company'];
			$name = $post['name'];
			$phone = $post['phone'];
			$email = $post['email'];
			$request = $post['request'];
			$from = $this->dbc->get_var ("mail_from");
			$subject = "������ #$n on-line �� MagicDesign.ru";
			$to = $this->dbc->get_var ("mail_for_order");
			$date = date ('d.m.Y H:i');

			$sections = array ();
			$production = FALSE;
			$types = $this->get_types ();

			if ($types)
			foreach ($types as $id => $tname) {
				if (getArrayValue ('order_'.$id, $this->request->_post)) {
					// if (in_array ($id, $this->production))
					//	$production = TRUE;

					$sections[] = $tname;
				}
			}

			$body = "������ � ����� <b>#$n</b> ������ �� �������: <b>$to</b>\n��������� ��������� ������� - ����� <b>Sales</b>\n\n<b>����� �������:</b> $date\n\n<b>������ � �������</b>\n<b>��������:</b> $company\n<b>���:</b> $name\n<b>�������:</b> $phone\n<b>E-mail:</b> $email\n\n<b>���������� �������:</b> " . join (', ', $sections) . "\n<b>�����������:</b>\n$request";

			include_once ('engine/email.inc');
			EmailSend ($from, $to, $subject, nl2br ($body), 'info@magicdesign.ru');

			return TRUE;
		}

		return FALSE;
	}
}

?>