<?PHP

$factory_class = "RFRegionsItem";

class RFRegionsItem extends ListItem {
	var $subjects = array ();

   function load_from_row ($row) {
//        $this->ord_index = $row->ord_index;
        $this->publish = $row->publish;
        $this->name = $row->name;
        $this->announce = $row->announce;
    }

	function load_defaults () {
		$this->publish = 1;
	}
    
	function get_update_array () {
		return array (
			'name' => $this->name,
			'publish' => $this->publish,
			'announce' => $this->announce
		);	
	}
	
	function get_insert_array () {
		return array (
//			'ord_index' => $this->factory->get_next_index (),
			'name' => $this->name,
			'publish' => $this->publish,
			'announce' => $this->announce
		);	
	}

	function load_from_parameters (&$request) {
		$this->name = $request->_post['rfregions_name'];
		$this->announce = $request->_post['rfregions_announce'];
		$this->publish = $request->_post['rfregions_publish'];
	}
	
	function set_template_vars (&$tpl, $form = FALSE) {
		$tpl->set_var ('rfregions_id', $this->id);
//		$tpl->set_var ('rfregions_ord_index', $this->ord_index);
		$tpl->set_var ('rfregions_name', $this->name);
		$tpl->set_var ('rfregions_announce', $this->announce);
		$tpl->set_var ('rfregions_publish', $this->publish);
		$tpl->set_var ('rfregions_publish_full', $this->publish ? '<font color=green>��</font>' : '���');

		if ($form) {
			$tpl->set_var ('rfregions_publish_checkbox', common_checkbox ('rfregions_publish', 1, $this->publish));
		}
	}
}

class RFRegionsFactory extends ListFactory {
	function RFRegionsFactory (&$config, &$request) {
		$this->ListFactory ($config, $request, 'rf_regions', 'id', 'name', 'RFRegionsItem', FALSE);
	}
}



// ******************************************************************************************
// ******************************************************************************************
// ******************************************************************************************

$factory_class = "RFSubjectsItem";

class RFSubjectsItem extends ListItem {
   function load_from_row ($row) {
//        $this->ord_index = $row->ord_index;
        $this->publish = $row->publish;
        $this->name = $row->name;
        $this->region_id = $row->region_id;
        $this->announce = $row->announce;
        $this->code = $row->code;
    }

	function load_defaults () {
		$this->publish = 1;
		$this->region_id = $this->factory->region_id;
	}
    
	function get_update_array () {
		return array (
			'name' => $this->name,
			'region_id' => $this->region_id,
			'publish' => $this->publish,
			'code' => $this->code,
			'announce' => $this->announce
		);	
	}
	
	function get_insert_array () {
		return array (
//			'ord_index' => $this->factory->get_next_index (),
			'name' => $this->name,
			'publish' => $this->publish,
			'region_id' => $this->region_id,
			'code' => $this->code,
			'announce' => $this->announce
		);	
	}

	function load_from_parameters (&$request) {
		$this->region_id = $request->_post['rfsubjects_region_id'];
		$this->name = $request->_post['rfsubjects_name'];
		$this->announce = $request->_post['rfsubjects_announce'];
		$this->publish = $request->_post['rfsubjects_publish'];
	}
	
	function set_template_vars (&$tpl, $form = FALSE) {
		$tpl->set_var ('rfsubjects_id', $this->id);
//		$tpl->set_var ('rfsubjects_ord_index', $this->ord_index);
		$tpl->set_var ('rfsubjects_name', $this->name);
		$tpl->set_var ('rfsubjects_region_id', $this->region_id);
		$tpl->set_var ('rfsubjects_region_id_full', $this->factory->region_factory->find ($this->region_id));
		$tpl->set_var ('rfsubjects_announce', $this->announce);
		$tpl->set_var ('rfsubjects_publish', $this->publish);
		$tpl->set_var ('rfsubjects_code', sprintf ("%02d", $this->code));
		$tpl->set_var ('rfsubjects_publish_full', $this->publish ? '<font color=green>��</font>' : '���');

		if ($form) {
			$tpl->set_var ('rfsubjects_publish_checkbox', common_checkbox ('rfsubjects_publish', 1, $this->publish));

			$tpl->set_var ('region_select_button', $this->factory->region_factory->select_button ('rfsubjects_region_id', $this->region_id, ""));
		}
	}
}

class RFSubjectsFactory extends ListFactory {
	var $region_factory = NULL;
	var $region_id = NULL;

	function RFSubjectsFactory (&$config, &$request) {
		$this->ListFactory ($config, $request, 'rf_subjects', 'id', 'name', 'RFSubjectsItem', FALSE);

		$this->region_factory = new RFRegionsFactory ($config, $request);
	}

	function conditions () {
		parent::conditions ();

//		if ($this->do_cache)
//			return;
//		$this->order = array ("t.date DESC", "t.id DESC");
			
		if ($this->region_id) 
			$this->cond[] = sprintf ("t.region_id = '%d'", $this->region_id); 
	}

	var $regionTree = NULL;
	function &createRegionTree () {
		if ($this->regionTree == NULL) {
			$this->regionTree = array ();

			$this->do_cache = TRUE;
			$this->region_factory->do_cache = TRUE;

			$list = &$this->region_factory->create_all ();
			$sublist = &$this->create_all ();

			if ($list && $sublist) {
				for ($i = 0; $i < sizeof ($list); $i++) {
					$this->regionTree[$list[$i]->id] = &$list[$i];
					$this->regionTree[$list[$i]->id]->subjects = array ();
				}
				for ($i = 0; $i < sizeof ($sublist); $i++) {
					$region = &$this->regionTree[$sublist[$i]->region_id];
					if (is_object ($region)) {
						$region->subjects[$sublist[$i]->id] = &$sublist[$i];
					}
				}
			}

			$this->do_cache = FALSE;
			$this->region_factory->do_cache = FALSE;
		}

		return $this->regionTree;
	}
}




?>