<?PHP

include_once (MY_MODULES_PATH."/user/user.inc");

$factory_class = "Subsform";

class Subsform {
	var $form_file = 'subsform.html';

	var $types = array (
/*		'pr'	=> 'PR � ��������',
		'site'	=> '���-�����',
		'creative'	=> '������� � ����',
		'sales'	=> '������� � �������',
		'manage'	=> '����������',
		'hr'	=> '���������� ����������',
		'style'	=> '������ � ������� �����',
		'art'	=> '��������� � ���-�������',
		'fenshuy'	=> '���-���'
*/
	);

	var $types_checked = array (
		'none'	=> 1
	);

//	var $from = 'MagicDesignLab <nobody@atom.mtw.ru>';
	var $from = 'Ifru-Testcenter.ru <bulat.rinchinov2013@yandex.ru>';

	var $config;
	var $request;
	var $error_message;
	var $last_user = null;

	function Subsform (&$config, &$request) {
		$this->config = &$config;
		$this->request = &$request;
		$this->dbc = $this->config->getDBConfig ();
	}

	function get_types () {
		return $this->types;
	}

	/*
	function get_next_subsform_number ($name = 'subsform_number') {
		$n = $this->dbc->get_var ($name);
		$this->dbc->set_var ($name, $n + 1);

		return $n + 1;
	}
	*/

	function &lastUser () {
		if (! is_object ($this->last_user)) {
			$user_factory = new UserFactory ($this->config, $this->request);
			$this->last_user = $user_factory->create (NULL);
			$this->last_user->load_from_parameters ($this->request);
			$this->last_user->load_defaults ($structure_id);
		}
		return $this->last_user;
	}

	function procceed ($http = "", $ok = 'ok/', $get = 'confirm/', $login = 'login/', $structure_id = NULL, $subject = "�����������") {
		$post = &$this->request->_post;

		if (empty ($post['user_email']))
			return FALSE;

		$user_factory = new UserFactory ($this->config, $this->request);
		$user = $user_factory->create (NULL);
		$user->load_from_parameters ($this->request);
		$user->load_defaults ($structure_id);

		$this->last_user = &$user;
		
		$user->validate ();
		$this->error_message = $user->error_message;

		if (! $this->error_message) {
			start_session ();
			$_SESSION['regemail'] = $user->email;

			header ('Location: ' . $ok);

//			$n = $this->get_next_subsform_number ("subsform_number_audit");

			$result = $user->update ();

			$email = $user->email;
			$date = $user->date;
            $from['name'] = $this->dbc->get_var ("mail_from");
            $from['email'] = $this->dbc->get_var ("mail_for_order");
            $replyTo = $from ? $from : 'bulat1922@gmail.com';
			$to = $this->dbc->get_var ("mail_for_order");

			$tpl = new Template ($this->config, MY_TPL_PATH);
			$tpl->set_file ('subsform_file', $this->form_file);
			$tpl->set_block ('subsform_file', 'mail_user', 'mail_user');
			$tpl->set_block ('subsform_file', 'mail_admin', 'mail_admin');
			
			$tpl->set_var ('topic', $subject);
			$tpl->set_var ('http', $http);
			$tpl->set_var ('link', $this->get_download_link ($get, $user));
			$tpl->set_var ('link_login', $login);
			$user->set_template_vars ($tpl);

			include_once ('engine/email.inc');

//			$subj = $this->get_admin_subject ($subject);
//			$body = $tpl->parse ('mail_admin', 'mail_admin');
//			EmailSend ($from, $to, $subj, nl2br ($body), 'mail@ifru-testcenter.ru', $replyTo);

			$subj = '����������� �� olimp-test.ru';
			$body = $tpl->parse ('mail_user', 'mail_user');
            EmailSend ($from, $email, $subj, nl2br ($body), '', $replyTo);

			$user->add_to_mailer ();

			return TRUE;
		}

		return FALSE;
	}

	function get_admin_subject ($subject) {
		return 'Ifru-Testcenter.ru :: ' . $subject;
	}

	function repeat_mail_user ($user) {
		$struct = &$user->getStructureNode ();
		$http = $struct->get_page_address ();
		$get = $http . "confirm/";
		$login = $http . "login/";

        $from['name'] = $this->dbc->get_var ("mail_from");
        $from['email'] = $this->dbc->get_var ("mail_for_order");
        $replyTo = $from ? $from : 'mail@ifru-testcenter.ru';
		$email = $user->email;
		$subject = $struct->page_h1 ? $struct->page_h1 : $struct->name;
		$subj = '�������������� ������ �� olimp-test.ru';

		$tpl = new Template ($this->config, MY_TPL_PATH);
		$tpl->set_file ('subsform_file', $this->form_file);
		$tpl->set_block ('subsform_file', 'mail_user', 'mail_user');

		$tpl->set_var ('topic', $subject);
		$tpl->set_var ('http', $http);
		if (empty ($user->code)) {
			$user->code = gen_pass (8);
			$user->update();
		}
		$tpl->set_var ('link', $this->get_download_link ($get, $user));
		$tpl->set_var ('link_login', $login);
		$user->set_template_vars ($tpl);
		$body = $tpl->parse ('mail_user', 'mail_user');

		include_once ('engine/email.inc');
        EmailSend ($from, $email, $subj, nl2br ($body), '', $replyTo);

		return TRUE;
	}

	function repeat_password_user ($user) {
		$struct = &$user->getStructureNode ();
		$http = $struct->get_page_address ();
		$get = $http . "confirm/";
		$login = $http . "login/";
		$reset = $http . "passwordreset/";

        $from['name'] = $this->dbc->get_var ("mail_from");
        $from['email'] = $this->dbc->get_var ("mail_for_order");
        $replyTo = $from ? $from : 'mail@ifru-testcenter.ru';
		$email = $user->email;
		$subject = $struct->page_h1 ? $struct->page_h1 : $struct->name;

		$tpl = new Template ($this->config, MY_TPL_PATH);
		$tpl->set_file ('subsform_file', $this->form_file);

		$tpl->set_var ('topic', $subject);
		$tpl->set_var ('http', $http);
		$tpl->set_var ('link_login', $login);

		if ($user->passkey) {
			$subj = '�������������� ������ �� olimp-test.ru';
			$tpl->set_block ('subsform_file', 'mail_user_password', 'mail_user_password');
			$tpl->set_var ('link', $this->get_download_link ($get, $user));
			$user->set_template_vars ($tpl);
			$body = $tpl->parse ('mail_user_password', 'mail_user_password');
			$sent = TRUE;
		} else {
			$subj = 'Ifru-Testcenter :: �������������� ������';
			$tpl->set_block ('subsform_file', 'mail_user_password_reset', 'mail_user_password_reset');
			$user->code = gen_pass (8);
			$user->update ();
			$tpl->set_var ('link', $this->get_download_link ($reset, $user));
			$user->set_template_vars ($tpl);
			$body = $tpl->parse ('mail_user_password_reset', 'mail_user_password_reset');
			$sent = FALSE;
		}

		include_once ('engine/email.inc');
        EmailSend    ($from, $email, $subj, nl2br ($body), '', $replyTo);

		return $sent;
	}


	function get_download_link ($get, $user) {
		$url = new URL ($get);
		$url->set_var ('id', $user->id);
		$url->set_var ('code', $user->code);
		return $url->build ();
	}
}

?>