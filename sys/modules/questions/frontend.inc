<?php

	// $tpl = new Template ($config, MY_TPL_PATH);
	// $tpl->set_file ('index', 'index.html');
	// $tpl->parse ('index', 'index');
	// $tpl->p_default ();
	$last = $structure->getLast ();

	$catalog = $last->createSubTree ('QuestionTree');
	$second = $catalog->goOnRewrite ();

	include_once ('main.inc');
	$main_page->structure = &$catalog;

	$main_page->show_right = FALSE;

	// $main_page->structure = &$structure;

	
/*
	$parent = $last->get_parent ();
	if (! is_object ($parent) || ! $parent->publish_announces) {
		if ($last->publish_announces)
			$parent = &$last;
	}
	if (is_object ($parent) && $parent->publish_announces) {
		$main_page->children[$pos->id] = $pos->load_children (TRUE);
		$main_page->right_float = is_object ($pos) ? $main_page->get_right_menu ($parent->load_children (TRUE), $parent->announce_ttl ? $parent->announce_ttl : '�������') : '';
	}
*/
	$section = &$pos;
	if (is_object ($section) && ($float = $section->get_floating_and_levels ())) {
		$main_page->children[$float->id] = $float->load_children (TRUE);
		$main_page->right_float = $main_page->get_right_menu ($main_page->children[$float->id], $float->name);


	} elseif (is_object ($parent) && $parent->publish_announces) {
		$main_page->children[$pos->id] = $pos->load_children (TRUE);
		$main_page->right_float = is_object ($pos) ? $main_page->get_right_menu ($parent->load_children (TRUE), $parent->announce_ttl ? $parent->announce_ttl : '�������') : '';
	}


	if (empty ($catalog->rewrite_stack)) {
		$main_page->content = $last->content;

		$nchildren = $last->load_related_children (TRUE);
		if (! empty ($nchildren))
			foreach ($nchildren as $id => $dummy)
				if ($dummy->publish_at_page != 'Y')
					unset ($nchildren[$id]);

		if ($module_subname == 'stand') {
			// $main_page->content .= $main_page->parse_icons ($nchildren, 4);
//			$main_page->content .= $main_page->parse_dropdown ($nchildren, 75, 135);
			$main_page->content .= $main_page->parse_dropdown ($nchildren);
		} else {
//			$main_page->content .= $main_page->parse_icons2level ($nchildren, 4);
//			$main_page->content .= $main_page->parse_dropdown ($nchildren, 75, 135);
			$main_page->content .= $main_page->parse_dropdown ($nchildren);
			$main_page->show_right = TRUE;

		}

		if (empty ($last->content) && empty ($nchildren) && empty ($last->image->image))
			$main_page->content = "<p>������ ��������� � ����������</p>";


	} elseif ($request->_get['show'] == 'window') {
		$tpl = new Template ($config, MY_TPL_PATH);
		$tpl->set_file ('window', 'window.html');

		$tpl->set_file ('content', 'question_image.html');
		$second->set_template_vars ($tpl);
		$tpl->set_var ('page_title', $second->name);
		$tpl->parse ('content', 'content');

		if (empty ($second->content) && empty ($nchildren) && empty ($second->image->image))
			$tpl->set_var ('content', "<p>������ ��������� � ����������</p>");

		$tpl->parse ('window', 'window');

		print $tpl->get_var ('window');

	} else {
		$main_page->show_right = TRUE;

		// insert image
		$tpl = new Template ($config, MY_TPL_PATH);

		if (empty($second->content_right))
			if ($second->image->image) 
				$tpl->set_file ('content', 'question_image.html');
			else
				$tpl->set_file ('content', 'question_noimage.html');
		else 
			if ($second->image->image) 
				$tpl->set_file ('content', 'question_image_right.html');
			else
				$tpl->set_file ('content', 'question_noimage_right.html');

		$nchildren = $second->load_children (TRUE);
		if (! empty ($nchildren)) {
			foreach ($nchildren as $id => $dummy)
				if ($dummy->publish_at_page != 'Y')
					unset ($nchildren[$id]);

			switch ($second->viewtype) {
				case "3":			// announces
					// TODO
					$tpl->set_var ('catalog', $main_page->parse_list ($nchildren, TRUE));
					break;

				case "4":			// icons2level
					$tpl->set_var ('catalog', $main_page->parse_icons2level ($nchildren, 4));
					break;

				case "2":			// list
					$tpl->set_var ('catalog', parse_goods ($nchildren, $second->publish_prices));
					break;

				case "5":			// list2level
					$tpl->set_var ('catalog', parse_goods2level ($nchildren, $second->publish_prices));
					break;

				case "7":			// list3level
					$tpl->set_var ('catalog', parse_goods2level ($nchildren, $second->publish_prices, 4));
					break;

				case "8":			// icons&preview
					$tpl->set_var ('catalog', $main_page->parse_iconsAndPreview ($nchildren, empty($second->content_right) ? 4 : 4));
					break;

				case "9":			// 2leveldrop
					$tpl->set_var ('catalog', $main_page->parse_2leveldrop_text ($nchildren));
					break;

				case "10":			// 2leveldrop
					$tpl->set_var ('catalog', $main_page->parse_1leveldrop_text ($nchildren));
					break;

				case "11":			// 2leveldrop
					$tpl->set_var ('catalog', $main_page->parse_icon_and_text ($nchildren));
					break;

				case "1":			// icons
					$tpl->set_var ('catalog', $main_page->parse_icons ($nchildren, 4));
					break;

				case "0":
				default:
					// TODO ������� �����
					break;
			}
		}

		if (empty ($second->page_h1))
			$second->page_h1 = $second->getAlt ();
		$second->set_template_vars ($tpl);
		$tpl->parse ('content', 'content');
		$main_page->content = $tpl->get_var ('content');

		if (empty ($second->content) && empty ($nchildren) && empty ($second->image->image))
			$main_page->content = "<p>������ ��������� � ����������</p>";
	}

	if ($request->_get['show'] != 'window') {
		if ($main_page->show_right) {
			include_once ('portfolio/portfolio.inc');
			$projects = new PortfolioWorkFactory ($config, $request);
			$pos = $last->get_nearest_product ();

			$main_page->announces = $projects->draw_relative_good_blocks ($pos->id);
		}

		if ($last->announce && ! $last->publish_in_announces)
			$main_page->announces .= $last->announce;

		// $main_page->announces .= $last->get_announce_list_float();

		if ($main_page->announces)
			$main_page->announce_name = $last->announce_ttl ? $last->announce_ttl : '���� ������';


		$main_page->output ();
	}

function parse_goods (&$list, $prices = FALSE) {
	global $config;

	$tpl = new Template ($config, MY_TPL_PATH);
	$tpl->set_var ('lists', '');

	if ($list) {
		$tpl->set_file ('list', 'structure_goods.html');
		$tpl->set_block ('list', 'head_price', 'head_price');
		$tpl->set_block ('list', 'list_row', 'list_rows');
		$tpl->set_block ('list_row', 'row_price', 'row_price_tpl');
		$tpl->set_var ('list_rows', '');

		if (! $prices) {
			$tpl->set_var ('head_price', '');
			$tpl->set_var ('row_price', '');
		}

		foreach ($list as $i => $dummy) {
			$node = &$list[$i];
			$node->set_template_vars ($tpl);

			$tpl->set_var ('i', $i+1);
			$tpl->parse ('row_price_tpl', 'row_price');
			$tpl->parse ('list_rows', 'list_row', TRUE);
		}

		$tpl->parse ('lists', 'list');
		
	}
	return $tpl->get_var ('lists');
}

function parse_goods2level (&$list, $prices = FALSE, $levels = 2) {
	global $config;

	$tpl = new Template ($config, MY_TPL_PATH);
	$tpl->set_var ('lists', '');

	if ($list) {
		$tpl->set_file ('list', 'structure_goods2level.html');
		$tpl->set_block ('list', 'head_price', 'head_price');
		$tpl->set_block ('list', 'list_row', 'list_rows');
		$tpl->set_block ('list_row', 'row_price', 'row_price_tpl');
		$tpl->set_block ('list', 'list_head', 'dummy');
		$tpl->set_block ('list_head', 'row_price', 'row_price_tpl');
		$tpl->set_var ('list_rows', '');
		$tpl->set_var ('dummy', '');

		if (! $prices) {
			$tpl->set_var ('head_price', '');
			$tpl->set_var ('row_price', '');
		}

		foreach ($list as $i => $dummy) {
			$node = &$list[$i];

			$node->set_template_vars ($tpl);
		
			$tpl->set_var ('i', $i+1);
			$tpl->parse ('row_price_tpl', 'row_price');
			$tpl->parse ('list_rows', 'list_head', TRUE);

			$list2 = $node->load_children (TRUE);
			if (! empty ($list2)) {
				foreach ($list2 as $j => $dummy) {
					$node2 = &$list2[$j];
					if ($levels == 3) {
						$list3 = $node2->load_children (TRUE);

						if (! empty ($list3))
							$node2->name = "<u>".$node2->name."</u>";
					}
					$node2->set_template_vars ($tpl);

					$tpl->set_var ('i', ($i+1).".".($j+1));
					$tpl->parse ('row_price_tpl', 'row_price');
					$tpl->parse ('list_rows', 'list_row', TRUE);

					if ($levels == 3) {
						if (! empty ($list3)) {
							foreach ($list3 as $k => $dummy) {
								$node3 = &$list3[$k];
								/*
								$node3->name = "<i>".$node3->name."</i>";
								$node3->material = "<i>".$node3->material."</i>";
								$node3->dimensions = "<i>".$node3->dimensions."</i>";
								$node3->price = "<i>".$node3->price."</i>";
								*/
								$node3->set_template_vars ($tpl);

								$tpl->set_var ('i', ($i+1).".".($j+1).".".($k+1));
								$tpl->parse ('row_price_tpl', 'row_price');
								$tpl->parse ('list_rows', 'list_row', TRUE);

								if ($levels == 4) {
								}
							}
						}

					}
				}
			}
		}

		$tpl->parse ('lists', 'list');
		
	}
	return $tpl->get_var ('lists');
}

?>