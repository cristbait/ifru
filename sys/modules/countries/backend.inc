<?PHP

//	$work_factory = new PortfolioWorkFactory ($config, $request);
	$factory = new CountryFactory ($config, $request);
	$id_name = 'countries_id';

	$tpl = new Template ($config, $module_path);

	$list_url = new URL ($mod_url->build ());
	$list_url->set_var ('area', 'list');
	$tpl->set_var ('list_url', $list_url->build ());

	$edit_url = new URL ($mod_url->build ());
	$edit_url->set_var ('area', 'edit');
	$tpl->set_var ('edit_url', $edit_url->build ());

//	$list_works_url = new URL ($mod_url->build ());
//	$list_works_url->set_var ('mod', $module_name.'_work');

	switch ($_GET['area']) {
	case 'edit':
		if (!$factory->default_edit_action ($edit_url->build (), $list_url->build (), $id_name)) {
			$tpl->set_file ('edit', 'backend_edit.html');
		
			$item = $factory->create ($request->_get[$id_name]);
			
			if ($item->id) 
				$tpl->set_var ('title', sprintf ("������: '%s'", $item->name));
			else
				$tpl->set_var ('title', "����� ������");
//
//			$item->htmlspecialchars ();
			$item->set_template_vars ($tpl, TRUE);
			
			$cancel_url = new URL ($list_url->build ());
			$cancel_url->set_var ($id_name, $item->id);

			$tpl->set_var ('edit_buttons_row', common_edit_buttons_row ($cancel_url->build (), true));

			$tpl->parse ('edit', 'edit');
			$tpl->p_default ();
		}
		break;

	case 'list':
	default:
		if (! $factory->default_list_action ($list_url->build (), $id_name)) {
			$tpl->set_file ('list', 'backend_list.html');

			$tpl->set_var ('title', "������");

			$pager = new Pager ($factory, $request, $config->page_size, $list_url->build (), 'page', $id_name);
		
			$item_list = $factory->create_all ($pager->page_size, $pager->offset);
		
			$tpl->set_block ('list', 'row', 'rows');
		
			$tpl->set_var ('rows', '');
			$color = another_color ();
			if ($item_list)
			for ($i = 0; $i < count ($item_list); $i++) {
				$item = &$item_list[$i];
			
				$tpl->set_var ('id_checkbox',
					common_checkbox ($id_name.'[]', $item->id, $request->_get['select_'.$id_name] == $item->id));
				$tpl->set_var ('row_color', $color = another_color ($color));
				$tpl->set_var ('row_number', $i + $pager->offset + 1);
			
//				$work_factory->sub_id = $item->id;
//				$item->name .= sprintf (' (%d)', $work_factory->get_total_count ());
				$item->set_template_vars ($tpl);

//				$list_works_url->set_var ('countries_id', $item->id);
//				$tpl->set_var ('list_works_url', $list_works_url->build ());

				$tpl->set_var ('list_item_buttons', common_list_item_buttons (
					$list_url->build (), $edit_url->build (), $id_name, $item->id));

				$tpl->parse ('rows', 'row', TRUE);
			}
		
			$tpl->set_var ('list_buttons', common_list_buttons (
				$list_url->build (), $edit_url->build (), $id_name, $factory->movable));
			$tpl->set_var ('pager', ($pager && $pager->page_count > 1) ? common_pager ($pager) : '');
			$tpl->set_var ('total', $pager ? $pager->total_size : sizeof ($item_list));
		
			$tpl->parse ('list', 'list');
			$tpl->p_default ();
		}
	}
	
?>