<?php

	$last = &$structure->getLast ();
	$etap = &$last;
	$test = &$last->get_parent ();

	$tpl = new Template ($config, MY_TPL_PATH);
	$root_url = $test->get_page_address ();
	$tpl->set_var ('root_url', $root_url);
	global $user_factory;
	$user_factory->loggedUser->set_template_vars ($tpl);

	$show = $request->_get['show'];
	$action = $request->_post["action"] ? $request->_post["action"] : $request->_get["action"];


	if ($module_subname == 'test') {			//	'etap_test'	=> '���� ���������: ����',

	} elseif ($module_subname == 'esse') {		//	'etap_esse'	=> '���� ���������: ����',

	} elseif ($module_subname == 'manual') {	//	'etap_manual'	=> '���� ���������: ������ ����'
		die ("������ �������!");
	} else
		die ("������ �������!");

	
	include_once (MY_MODULES_PATH."/questions/questions.inc");
	$questions = new QuestionTree ($config, $request);
	
	$questions->load_context ($test, $etap, $user_factory->loggedUser);
	$questSet = &$questions->find_appropriate_set ();
	
//	$questSet->get_max_mark ();
//	$questSet->get_user_mark ();
//	$questSet->set_template_vars ($tpl);
	$tpl->set_var ("url", $GLOBALS['REQUEST_URI']);
	$tpl->set_var ("test_name", $test->name);
	$tpl->set_var ("question_set_name", $questSet->name ? $questSet->name : "��� ����");

	if ($module_subname == 'test') {
		$tpl->set_file ('test', 'etap_test.html');

		if ($questions->load_results ()) {
			$tpl->set_block ("test", "step3", "step3");

			$tpl->set_var ('mark', $questions->mark);
			$tpl->set_var ('total_mark', $questions->total_mark);
			$tpl->set_var ('quantity', $questions->quantity);
			$tpl->set_var ('total_quantity', $questions->total_quantity);

			// $last->name = "��� ���������: " . $mark . " �� " . $questions->total_mark;
			$tpl->parse ('test', 'step3');
            header ('Location: ' . ($root_url . '/personal'));

        } elseif ($action == 'catchit') {
			header ('Location: ' . ($url = $etap->get_page_address ()));
			printf ("<a href='%s'>%s</a>", $url, $url);
			$mark = $questions->check_results ();
			exit ();

		} else {
			if ($show == "enter") {
				$block = "step2";
			} else {
				$block = "step1";
			}

			$tpl->set_block ("test", $block, $block);
			$tpl->set_block ($block, "part_block", "part_blocks");
			$tpl->set_block ("part_block", "question_block", "question_blocks");
			$tpl->set_var ('part_blocks', "");

			$qlist = &$questions->create_test ($questSet->id);
			
			$parts = &$questions->parts;
			$k = 0;
			
			if ($parts)
			for ($i = 0; $i < sizeof ($parts); $i++) {
				$part = &$parts[$i];
				
				$tpl->set_var ('question_blocks', "");
				for ($j = 0; $j < sizeof ($part->questions); $j++) {
					$quest = &$part->questions[$j];
					$quest->question = nl2br ($quest->question);
					
					$quest->set_template_vars ($tpl);
					if ($show == "enter")
						$tpl->set_var ('answers', $quest->set_answer_block ());
					else
						$tpl->set_var ('answers', $quest->set_answer_show_block (FALSE));

					$tpl->set_var ('qnum', ++$k);
					$tpl->parse ('question_blocks', 'question_block', true);
				}

				$part->set_template_vars ($tpl);
				$tpl->parse ('part_blocks', 'part_block', true);
			} elseif ($qlist) {
				$tpl->set_var ('question_blocks', "");
				foreach ($qlist as $j => $dummy) {
					$quest = &$qlist[$j];
					
					$quest->question = nl2br ($quest->question);

					$quest->set_template_vars ($tpl);
					if ($show == "enter")
						$tpl->set_var ('answers', $quest->set_answer_block ());
					else
						$tpl->set_var ('answers', $quest->set_answer_show_block (FALSE));

					$tpl->set_var ('qnum', ++$k);
					$tpl->parse ('question_blocks', 'question_block', true);
				}
				$tpl->parse ('part_blocks', 'part_block', true);
			}

	//		$last->name = "�������� �� �������";
			$tpl->parse ('test', $block);
        }

	} elseif ($module_subname == 'esse') {
		$tpl->set_file ('test', 'etap_esse.html');

		if ($questions->load_results ()) {
			$tpl->set_block ("test", "step3", "step3");

			$tpl->set_var ('mark', $questions->mark);
			$tpl->set_var ('total_mark', $questions->total_mark);
			$tpl->set_var ('quantity', $questions->quantity);
			$tpl->set_var ('total_quantity', $questions->total_quantity);

			// $last->name = "��� ���������: " . $mark . " �� " . $questions->total_mark;
			$tpl->parse ('test', 'step3');
            header ('Location: ' . ($root_url . '/personal'));

        } elseif ($action == 'catchit') {
			header ('Location: ' . ($url = $etap->get_page_address ()));
			printf ("<a href='%s'>%s</a><br />\n", $url, $url);
			$mark = $questions->save_file_results ();
			exit ();

		} elseif ($show == "enter") {
			$block = "step2";
			$tpl->set_block ("test", $block, $block);
			$tpl->set_block ($block, "part_block", "part_blocks");

			if ($questSet) 
				$tpl->set_var ('topic_select', $questSet->get_topic_select_button ());
			else 
				$tpl->set_var ('topic_select', "<font color=red>������ ��� �������� �� ��������</font>");



	//		$last->name = "�������� �� �������";
			$tpl->parse ('test', $block);
		} else {
			$tpl->set_block ("test", 'step1', 'step1');
			$questSet->children = &$questSet->load_children (TRUE);

			if (! empty ($questSet) && ! empty ($questSet->children)) {
				$content .= "<ol>\n";
				foreach ($questSet->children as $i => $child) {
					$content .= "<li>".$child->name."</li>\n";
				}
				$content .= "</ol>\n";
			} else {
				$content .= "<b>������ ��� �������� �� ��������</b>";
			}

			$tpl->set_var ('topic_list', $content);
			$tpl->parse ('test', 'step1');
        }

	}



	global $main_page;
	include_once ('main.inc');
	$main_page->structure = &$structure;
	$main_page->root_url = $root_url;
	$main_page->content = $last->content . $tpl->get_default ();
	$main_page->output ();



?>