function showFlash (id) {
	var scroolLeft = self.pageXOffset || (document.documentElement && document.documentElement.scrollLeft) || (document.body && document.body.scrollLeft);
	var scroolTop = self.pageYOffset || (document.documentElement && document.documentElement.scrollTop) || (document.body && document.body.scrollTop);
	var clientWidth = document.compatMode=='CSS1Compat' && !window.opera?document.documentElement.clientWidth:document.body.clientWidth;
	var clientHeight = document.compatMode=='CSS1Compat' && !window.opera?document.documentElement.clientHeight:document.body.clientHeight;
	var flash = $("#"+id);
	var H = flash.outerHeight();
	var W = flash.outerWidth();
	// alert (scroolLeft + "x" + scroolTop + "\n" + clientWidth + "x" + clientHeight + "\n" + W + "x" + H + "\n" +  (scroolLeft+parseInt(clientWidth/2)-parseInt(W/2)) + "x" + (scroolTop+parseInt(clientHeight/2)-parseInt(H/2)));

	flash.css("top", parseInt(clientHeight/2)+scroolTop-parseInt(H/2) + "px");
	flash.css("left", parseInt(clientWidth/2)+scroolLeft-parseInt(W/2) + "px");
	flash.css("display", "block");
	$("#black").css("display","block");
	$("#black").css("top", scroolTop);
	$("#black").css("left", scroolLeft);
}

function hideFlash (id) {
	$("#"+id).css("display", "none");
	$("#black").css("display","none");
}

function sethtml (nr, htmlcode) {
	if (document.layers){
		document.layers[nr].innerHTML = htmlcode;
	} else if (document.all) {
		document.all[nr].innerHTML = htmlcode;
	} else if (document.getElementById) {
		document.getElementById(nr).innerHTML = htmlcode; 
	}
}

function gethtml (nr) {
	if (document.layers){
		return document.layers[nr].innerHTML;
	} else if (document.all) {
		return document.all[nr].innerHTML;
	} else if (document.getElementById) {
		return document.getElementById(nr).innerHTML; 
	}
}

function toggle(item) {
   obj=document.getElementById(item);
   if (obj.style.display == "none") {
	   obj.style.display="block";
   } else {
	   obj.style.display="none";
   }
}