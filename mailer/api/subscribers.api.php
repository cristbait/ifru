﻿<?php

// Setup PHP and start page setup.
	@ini_set("include_path", "../includes");
	@ini_set("allow_url_fopen", 1);
	@ini_set("session.name", md5(str_replace("/api", "", dirname(__FILE__))));
	@ini_set("session.use_trans_sid", 0);
	@ini_set("session.cookie_lifetime", 0);
	@ini_set("session.cookie_secure", 0);
	@ini_set("session.referer_check", "");
	@ini_set("error_reporting",  E_ALL ^ E_NOTICE);
	@ini_set("magic_quotes_runtime", 0);

	require_once("pref_ids.inc.php");
	require_once("config.inc.php");
	require_once("classes/adodb/adodb.inc.php");
	require_once("dbconnection.inc.php");
	require_once("functions.inc.php");
	
	session_start();
	
	if((isset($_SESSION["isAuthenticated"])) || ((bool) $_SESSION["isAuthenticated"])) {
		$search_limit	= 50;
		$search_term	= "";
		
		if(isset($_POST["q"])) {
			$search_term = clean_input($_POST["q"], array("trim"));
		} elseif(isset($_GET["q"])) {
			$search_term = clean_input($_GET["q"], array("trim"));
		}
		
		if(isset($_POST["limit"])) {
			$search_limit = clean_input($_POST["limit"], array("trim", "int"));
		} elseif(isset($_GET["q"])) {
			$search_limit = clean_input($_GET["limit"], array("trim", "int"));
		}
		
		if(($search_limit < 0) || ($search_limit > 100)) {
			$search_limit = 50;
		}
		
		if($search_term) {
			$query		= "
						SELECT  a.`users_id`, b.`group_name`, CONCAT_WS(' ', a.`firstname`, a.`lastname`) AS `name`, a.`email_address`
						FROM `".TABLES_PREFIX."users` AS a
						LEFT JOIN `".TABLES_PREFIX."groups` AS b
						on a.`group_id` = b.`groups_id`
						WHERE CONCAT_WS(' ', a.`firstname`, a.`lastname`) LIKE ".$db->qstr("%".$search_term."%", get_magic_quotes_gpc())."
						OR a.`email_address` LIKE ".$db->qstr("%".$search_term."%", get_magic_quotes_gpc())."
						OR CONCAT('\"', CONCAT_WS(' ', a.`firstname`, a.`lastname`), '\" <',a.`email_address`, '>') LIKE ".$db->qstr("%".$search_term."%", get_magic_quotes_gpc())."
						GROUP BY a.`users_id` 
						ORDER BY a.`lastname` ASC, a.`firstname` ASC
						LIMIT 0, ".$search_limit;
			$results	= $db->GetAll($query);
			if($results) {
				/**
				 * Don't worry, if you don't have JSON compiled with PHP
				 * I have provided a work around, thanks to php.net :D
				 */
				echo json_encode($results);
			}
		}
	}
?>
