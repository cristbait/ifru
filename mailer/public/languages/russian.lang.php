<?php
/*
<language name="Russian" version="2.2.0">
	<translator_name>Andrey Ryskov.</translator_name>
	<translator_email>ntoket@gmail.com</translator_email>
	<translator_url>http://www.ifchome.com/index.php</translator_url>
	<updated>19:48 16-04-2010</updated>
	<notes>Default Language File</notes>
</language>
*/
$LANGUAGE_PACK = array();

$LANGUAGE_PACK["default_page_title"]					= "�������� ��������";
$LANGUAGE_PACK["default_page_message"]					= "���������� �������� ��� ������� �������, ����� ����������� �� ��� ���� ��������� �� ������ ��� ����� ������� ��������.";
$LANGUAGE_PACK["error_default_title"]					= "������ � �������";
$LANGUAGE_PACK["error_invalid_action"]					= "������������� �������� �����������. ���������� ��������� ��� ���� � ����� ���������. ���� ��� ����������� ���������� ������, ���������� ��������� � ��������������� ��������.";

$LANGUAGE_PACK["error_subscribe_no_groups"]				= "�� ������ ������� �� ������� ���� ���� ������ ��������, ����� �����������. ���� ��� ����������� ���������� ������, ���������� ��������� � ��������������� ��������.";
$LANGUAGE_PACK["error_subscribe_group_not_found"]		= "������ ��������, �� ������� �� ��������� ����������� ������ �� ���������� � ���� �������. ���� ��� ����������� ���������� ������, ���������� ��������� � ��������������� ��������.";
$LANGUAGE_PACK["error_subscribe_email_exists"]			= "��������� ����� ����������� �����, ���������� � ������ (���) ��������, ������� �� �������. ���� ��� ����������� ���������� ������, ���������� ��������� � ��������������� ��������.";
$LANGUAGE_PACK["error_subscribe_no_email"]				= "���������� ������� ����� ����������� �����, ������� �� ������ �� ������ �� ��� ������ ��������.";
$LANGUAGE_PACK["error_subscribe_invalid_email"]			= "����� ����������� �����, ������� �� �����, �� ��������� ��� ��������������. ������� ������ ����� ����������� �����.";
$LANGUAGE_PACK["error_subscribe_banned_email"]			= "������ ����������� �����, ������� �� �����, ������� � ������ ������.";
$LANGUAGE_PACK["error_subscribe_banned_domain"]			= "��� ��������� ����� ������ ����������� �����, ������� �� �����, ��������� �������� �� ��������.";
$LANGUAGE_PACK["error_subscribe_invalid_domain"]		= "�������� ��� ������ ����������� �����, ������� �� �����, �������, �� ������������� ��� ���� ����������� �����.";
$LANGUAGE_PACK["error_subscribe_required_cfield"]		= "&quot;[cfield_name]&quot; �������� ����������� ����� ��� ����������. ���������� ��������� � ������� ��� ����������.";	// Requires [cfield_name] variable in sentence.
$LANGUAGE_PACK["error_subscribe_failed_optin"]			= "��, � ���������, �� ������ ������� ������ � �������������� ���������� ������ ������ � ������ ��������. ���������� ��������� � ��������������� �������� � �������� ��� �� ���� ��������.";
$LANGUAGE_PACK["error_subscribe_failed"]				= "��, � ���������, �� ������ ��������� ��� ����� ����������� ����� �� ��� ������ ��������. ���������� ��������� � ��������������� �������� � �������� �� ������������ ���� ��������.";
$LANGUAGE_PACK["success_subscribe_optin_title"]			= "�������������� ������ ���� ������� �� ���� �����";
$LANGUAGE_PACK["success_subscribe_optin_message"]		= "���������� ��� �� ����������� ������� � ������ ������ ��������. ������ �� �������� ����������� � ������������� ����������.";
$LANGUAGE_PACK["success_subscribe_title"]				= "�������� ������� ���������";
$LANGUAGE_PACK["success_subscribe_message"]				= "���������� ��� �� ����������� ������� � ������ ������ ��������. ��� ����� ����������� ����� ��� ������� �������� � ������ ��������, ��� ������� ��������� ����� ���������� �� ����.";

$LANGUAGE_PACK["error_unsubscribe_no_groups"]			= "�� ������ ������� �� ������� ���� ���� ������ ��������, ����� ���������� �� ����. ���� ��� ����������� ���������� ������, ���������� ��������� � ��������������� ��������.";
$LANGUAGE_PACK["error_unsubscribe_group_not_found"]		= "������ ��������, �� �������� �� ��������� ����������, ������ �� ���������� � ����� �������. ���� ��� ����������� ���������� ������, ���������� ��������� � ��������������� ��������.";
$LANGUAGE_PACK["error_unsubscribe_email_not_found"]		= "����� ����������� �����, ������� �� �����, �� ���������� � ����� ���� ������. ���� ��� ����������� ���������� ������, ���������� ��������� � ��������������� ��������.";
$LANGUAGE_PACK["error_unsubscribe_email_not_exists"]	= "����� ����������� �����, ������� �� �����, �� ���������� � ������ ��������, �� �������� �� ������ ������� ����. ���� ��� ����������� ���������� ������, ���������� ��������� � ��������������� ��������.";
$LANGUAGE_PACK["error_unsubscribe_no_email"]			= "���������� ������� ����� ����������� �����, ����� ������� �� � ������ �������� ��� �� ����� ��������.";
$LANGUAGE_PACK["error_unsubscribe_invalid_email"]		= "����� ����������� �����, ������� �� �����, ��������������.";
$LANGUAGE_PACK["error_unsubscribe_failed_optout"]		= " � ���������, � ������ ������, �� �� ������ ������� ��� �������������. ���������� ��������� � ��������������� �������� � �������� ��� � ��������.";
$LANGUAGE_PACK["error_update_profile"]					= "� ���������, � ������ ������, �� �� ������ ������� ��� ����������� � ������������� ���������� �������. ���������� ��������� � ��������������� ��������.";
$LANGUAGE_PACK["success_unsubscribe_optout_title"]		= "������������ ��������� �� ������ ��� �������� �������� �� ��� �����";
$LANGUAGE_PACK["success_unsubscribe_optout_message"]	= "�� �������� �� ����� �����. ����� ��������� �������, ��������� ��� ��� ���� �����.";
$LANGUAGE_PACK["success_unsubscribe_title"]				= "�������� ���� �������";
$LANGUAGE_PACK["success_unsubscribe_message"]			= "�� �������� � ����� �����. ������, ���� �� ������ ���������, �� ������ ����� ����";

$LANGUAGE_PACK["error_invalid_captcha"]					= "��� ������������, � ������� �� �����, ������������. ���������� ���������, � ������� �����, ������� �������� � ����������� ������.";
$LANGUAGE_PACK["error_expired_code"]					= "���� ������������� �������� ����� ����� 7 ����. ����� �������� ��� �������, ���������� ��������� � ���������������.";
$LANGUAGE_PACK["error_confirm_invalid_request"]			= "�� �� ���������� ���������� ������������� � ����� �������. ���������� ���������� ������ ������������� �������� �� ������ � �������� � ������ ��������.";
$LANGUAGE_PACK["error_confirm_completed"]				= "�������, �� ��� ����������� ��������. ������� ���������� �������� �� ���������.";
$LANGUAGE_PACK["error_confirm_unable_request"]			= "�������� ��������� �� ����������, ������, �� �� ������ � ������ ����� ���������� ��� ������. ���������� ��������� � ��������������� �������� � �������� ��� �� ���� ��������.";
$LANGUAGE_PACK["error_confirm_unable_find_info"]		= "�������� ��������� �� ����������, ������, �� �� ����� ����� ������� ���������� �� ������ ������ � �����  ������� ��������. ���������� ��������� � ��������������� �������� � �������� ��� �� ���� ��������.";
$LANGUAGE_PACK["page_confirm_title"]					= "���������� �������������";
$LANGUAGE_PACK["page_confirm_message_sentence"]			= "���������� ����������� ��������� ���������� ����� �� ������.";
$LANGUAGE_PACK["page_confirm_firstname"]				= "���:";
$LANGUAGE_PACK["page_confirm_lastname"]					= "�������:";
$LANGUAGE_PACK["page_confirm_email_address"]			= "����������� �����:";
$LANGUAGE_PACK["page_confirm_group_info"]				= "�������� ����:";
$LANGUAGE_PACK["page_confirm_cancel_button"]			= "��������";
$LANGUAGE_PACK["page_confirm_submit_button"]			= "�����������";

$LANGUAGE_PACK["page_unsubscribe_title"]				= "������������� �������";
$LANGUAGE_PACK["page_unsubscribe_message_sentence"]		= "�������� ���� �� ������� �� ������ �� ����������:";
$LANGUAGE_PACK["page_unsubscribe_list_groups"]			= "[email] �� [groupname].";	// Requires [email] and [groupname] variable in sentence.
$LANGUAGE_PACK["page_unsubscribe_cancel_button"]		= "��������";
$LANGUAGE_PACK["page_unsubscribe_submit_button"]		= "����������";

$LANGUAGE_PACK["page_help_title"]						= "������";
$LANGUAGE_PACK["page_help_message_sentence"]			= "����� ���������� � ���� ������ ������ ��������. ���� ���� ������ ������� ��� ����� ����� �� ��������� �������� �������. ���� �� �� ����� ������, ����������, ��������� � ��������������� � [abuse_address].";	// Requires [abuse_address] variable in sentence.
$LANGUAGE_PACK["page_help_subtitle"]					= "����� ���������� �������:";
$LANGUAGE_PACK["page_help_question_1"]					= "��� � ���������� �� ���� ��������?";
$LANGUAGE_PACK["page_help_answer_1_optin"]				= "� ��������� ����� �� �������, ����� ����������, ������ ����������� ���� ��������, ������, ��� ������������ ����������� �� ����� �� ����� ��������. ��� ��������, ��� ��, ��� ��� - �� ��������� ��� ����� ����������� �����, �������� ���� ������������ � ������ ������, ����� ���� ������� ������� ������ � �������������� ��������, ������� ���� ������������ ���� ��� ��� ���� ��������� ��� ����� ����������� ����� . ���� �� �� ������������ �������� ��������������, ������ ��������� � ��������������� ���������� �� ������  [abuse_address].";	// Requires [abuse_address] variable in sentence.
$LANGUAGE_PACK["page_help_answer_1_no_optin"]			= "� ��������� ����� �� �� �������, �������� ������������� �������� �� ��������. ��� ��������, ��� �� ��� ��� - ��, ��������� ��� ����� ����������� ����� ����� � �������, � ����� � ���� �� �� ���� ������� ������������ ��������. ������ ���� �������� �� ������ ����������� ������ ������ �������������� �� [abuse_address].";	// Requires [abuse_address] variable in sentence.
$LANGUAGE_PACK["page_help_question_2"]					= "��� ��� ����������?";
$LANGUAGE_PACK["page_help_answer_2_optout"]				= "��������� ��� ����� � �� ������ ��� ��������� ������ � �������������� ������ �������..";
$LANGUAGE_PACK["page_help_answer_2_no_optout"]			= "���� �� ������ ������� ���� �� ������ ��� ����� ������� ��������, �� ������ ��������� ��������� �����. ��� ������ �� ������� ����� ����������� ����� � �������� �� ������ ������ �������� ������� ���� ���������, ��� ����� ����� ���������� ����� �� ����� �������.";
$LANGUAGE_PACK["page_help_question_3"]					= "��� ��� �� ������, ������� � �������?";
$LANGUAGE_PACK["page_help_answer_3"]					= "���� ������ ������ �������� �� � ��������� ���������� ���������� ���������, ������� �� ��������; ������, ���� �� ��������� �� ���� �������� �����, ��������, ��� ���������, ������� �� ��������, �������, ��������� ���� ����������� ����������� ���������� �������� ��������. ���� �� ���������, ��� �������� ��������� ��������, ���������� ��������� � ��������������� � [abuse_address] � �������� ��� � ������ ��������.";	// Requires [abuse_address] variable in sentence.
$LANGUAGE_PACK["page_help_question_4"]					= "��� �������� ��� ������ ������ � ������ ��������?";
$LANGUAGE_PACK["page_help_answer_4"]					= "�� ������ �������� ���� ����������, ����� �� ������: <a href=\"[URL]\">�������� ������� ����������</a> "; // Requires [URL] variable.

$LANGUAGE_PACK["page_archive_closed_title"]				= "����� ������";
$LANGUAGE_PACK["page_archive_closed_message_sentence"]	= "�� ������ ������ ��� ����� ������ ��� �����������. ���� �� ���������� � �������������� ������, ��������� � ���������������  [abuse_address].";	// Requires [abuse_address] variable in sentence.
$LANGUAGE_PACK["page_archive_opened_title"]				= "�����";
$LANGUAGE_PACK["page_archive_opened_message_sentence"]	= "����� ���������� � ��� �����. ����� �� ������� �������� ���� ���������, ������� �� ��������� �����  �����������. �� ��� �� ������ ����������� �� ��� [rssfeed_url]."; // Requires [rssfeed_url] in sentence.
$LANGUAGE_PACK["page_archive_view_title"]				= "�����";
$LANGUAGE_PACK["page_archive_error_html_title"]			= "������, ������������ ���������� ��������� � HTML";
$LANGUAGE_PACK["page_archive_error_no_message"]			= "��������� ��������� �� ���� ������� � ����� ������ ���������. ���������� ��������� � ������.";
$LANGUAGE_PACK["page_archive_error_no_messages"]		= "� ��������� ����� ��� ������� ���������;  ���������� ��� ��� �����.";
$LANGUAGE_PACK["page_archive_view_from"]				= "��:";
$LANGUAGE_PACK["page_archive_view_subject"]				= "����:";
$LANGUAGE_PACK["page_archive_view_date"]				= "����:";
$LANGUAGE_PACK["page_archive_view_to"]					= "�:";
$LANGUAGE_PACK["page_archive_view_attachments"]			= "����������:";
$LANGUAGE_PACK["page_archive_view_missing_attachment"]	= "���������� ������ �� ����������";
$LANGUAGE_PACK["page_archive_view_message_from"]		= "��������� ��";
$LANGUAGE_PACK["page_archive_view_message_subject"]		= "���� ���������";
$LANGUAGE_PACK["page_archive_view_message_sent"]		= "���� ��������";
$LANGUAGE_PACK["page_archive_rss_title"]				= "�������������� ��������� RSS";
$LANGUAGE_PACK["page_archive_rss_description"]			= "����� ���������� �� RSS ������ ��������� �����. ����� �� ������� �������� ���� ���������, ������� �� ��������� ����� ����� �����������..";
$LANGUAGE_PACK["page_archive_rss_link"]					= "http://www.com"; // You can optionally set this to the web-address of your website.

$LANGUAGE_PACK["page_profile_closed_title"]				= "��������� �������, �������";
$LANGUAGE_PACK["page_profile_closed_message_sentence"]	= "���� ������ ���������� ������� ���������� � ��������� ����� �������. ��������� � ���� [abuse_address].";	// Requires [abuse_address] variable in sentence.
$LANGUAGE_PACK["page_profile_opened_title"] 			= "�������� ������� ����������";
$LANGUAGE_PACK["page_profile_instructions"] 			= "���������� �� ��������� ����� ����������. ����� ���������� ���������� ����� ���������� ����������, ������� � ��� ����� � ����� ����. ������� ����� ������ ��� ����������� �����, ���������� ����������� ������, � ������� ������� �� ������� ������� ����������� ��������� .";
$LANGUAGE_PACK["page_profile_submit_button"] 			= "����������";
$LANGUAGE_PACK["page_profile_update_button"] 			= "��������";
$LANGUAGE_PACK["page_profile_close_button"] 			= "�������";
$LANGUAGE_PACK["page_profile_cancel_button"] 			= "��������";
$LANGUAGE_PACK["page_profile_email_address"]			= "����������� �����:";
$LANGUAGE_PACK["page_profile_step1_complete"] 			= "����� �������� ���� ����������, �� �������, ����� �� �����������, ��� �� ������������� �������� ����� ������ ����������� �����. ������ �� �������� ����������� ������������� ��������� �������.";
$LANGUAGE_PACK["page_profile_step2_instructions"] 		= "����� ���������� ���������� ����� ���������� ���������� ����������� ����� ���� � ������� ����� ����������� ���������.";
$LANGUAGE_PACK["page_profile_step2_complete"] 			= "���� ���������� ���� ���������.���������� �� ��������� ����������.";
$LANGUAGE_PACK["update_profile_confirmation_subject"]	= "���������� �� ���������� ���������� ����������";
$LANGUAGE_PACK["update_profile_confirmation_message"]	= <<<UPDATEPROFILE
������������ [name].


����� �������� ���������� ����������, ����������� �� ������ ����:
[url]

���� �� �� ������ ������ ��� ���������� ����� ����������, ����������, �������������� ��� ����������� �����, � �� �������� �� �������������� ������.

� ��.,
[from]
UPDATEPROFILE;

$LANGUAGE_PACK["unsubscribe_message"]				= <<<UNSUBSCRIBEMSG
-------------------------------------------------------------------
�� �������� ��� ������ �� [email] , ������ ��� ��������� ��, �� ������� ����, ���� �� ����� ������� ��������. ���� �� ������ ������� ���� �� ������ ������, ���������� ����������� �� ������ 
[unsubscribe_url]
UNSUBSCRIBEMSG;

$LANGUAGE_PACK["subscribe_confirmation_subject"]		= "������������� ��������";
$LANGUAGE_PACK["subscribe_confirmation_message"]		= <<<SUBSCRIBEEMAIL
������������ [name]
�� �������, ����� ��� ����� ����������� ����� ��� ������� � ���� �� ����� ������� ��������. ��� ������ �������, ����� �� ������ ����������� ������� �������� ��������. ���������� ����������� �� ������ ����:
[url]


� ��.,
[from]
SUBSCRIBEEMAIL;

$LANGUAGE_PACK["unsubscribe_confirmation_subject"]	= "������������� ��������";
$LANGUAGE_PACK["unsubscribe_confirmation_message"]	= <<<UNSUBSCRIBEEMAIL
�� ��� ��� ���� ������, �������� ������� ��� ����� �� ������ ������:

���� �� ������������� ������� ������� ���, ������� �� ������ ����
[url]


� ��.,
[from]
UNSUBSCRIBEEMAIL;

$LANGUAGE_PACK["subscribe_notification_subject"]		= "����� ����������";
$LANGUAGE_PACK["subscribe_notification_message"]		= <<<SUBSCRIBENOTICEEMAIL
����� ���������� ���������� �� �������� 

��� ������:

���:\t[firstname] [lastname]
E-Mail:\t[email_address]
�����:
[group_ids]

-------------------------------------------------------------------
�� ��������� ��� �����������, ������ ��� � ����������������� �������� ���������� ��� � ����� �����������.
SUBSCRIBENOTICEEMAIL;

$LANGUAGE_PACK["unsubscribe_notification_subject"]	= "���������� ��������� �� ��������";
$LANGUAGE_PACK["unsubscribe_notification_message"]	= <<<UNSUBSCRIBENOTICEEMAIL
��� ������ ���������� ���, � ���, ��� ���� �� ����������� ��������� �� ��������

��� ������:
���:\t[firstname] [lastname]
E-Mail:\t[email_address]
��������� ��:
[group_ids]

-------------------------------------------------------------------
UNSUBSCRIBENOTICEEMAIL;


$LANGUAGE_PACK["error_subscribe_banned_ip"]				= "IP �����, � �������� �� ��������� �����������, ������������ � ���� �������. ���������� ��������� � ��������������� �������� ��� ���������� ������.";

$LANGUAGE_PACK["page_captcha_invalid"]					= "��� ������������, ������� �� �����, ��� ������������, ���������� �������� ������� � �����, ������� ���������� �� ��������.";
$LANGUAGE_PACK["page_captcha_title"]					= "CAPTCHA ��� ������������";
$LANGUAGE_PACK["page_captcha_message_sentence"]			= "����� ������������� �������������� �������� �������, �� ������ ��� ������ �����, ������� �� ������ �� ����������������� ��������.";
$LANGUAGE_PACK["page_captcha_label"]					= "��� ������������";


$LANGUAGE_PACK["page_forward_title"]					= "��������� �����";
$LANGUAGE_PACK["page_forward_closed_title"]				= "��������� ��������� ����� ����������";
$LANGUAGE_PACK["page_forward_closed_message_sentence"]	= "������� ��������� ��������� ����� �������� ���������. �� ���� �������� ����������� � �������������� �� ������ [abuse_address].";	// Requires [abuse_address] variable in sentence.
$LANGUAGE_PACK["page_forward_error_no_message"]			= "�� �� ����� ����� ���������, ������� �� ��������� ��������� �����.";
$LANGUAGE_PACK["page_forward_error_private"]			= "���������, ������� �� ��������� ���������, ���� ������� ������ �� �������� �������, � ��� ���������� ��������� �������.";
$LANGUAGE_PACK["page_forward_message_sentence"]			= "���� �� ������ ��������� ��� ��������� ������ �����, ������� �� ���������� ������ � ����� ����. ����� �� ������ ����������� ������ ������ ��������� ��� ���.";
$LANGUAGE_PACK["page_forward_from_header"]				= "���� ����������";
$LANGUAGE_PACK["page_forward_from_name"]				= "���� ���";
$LANGUAGE_PACK["page_forward_from_email"]				= "��� E-Mail";
$LANGUAGE_PACK["page_forward_friend_header"]			= "���������� ������ �����";
$LANGUAGE_PACK["page_forward_friend_name"]				= "��� �����";
$LANGUAGE_PACK["page_forward_friend_surname"]			= "������� �����";
$LANGUAGE_PACK["page_forward_friend_email"]				= "E-Mail �����";
$LANGUAGE_PACK["page_forward_optional_message"]			= "���������"; // (����������)";
$LANGUAGE_PACK["page_forward_cancel_button"] 			= "��������";
$LANGUAGE_PACK["page_forward_submit_button"] 			= "���������";
$LANGUAGE_PACK["page_forward_error_from_name"] 			= "����������, ������� ���� ���.";
$LANGUAGE_PACK["page_forward_error_from_email"] 		= "����������, ������� ���� E-mail.";
$LANGUAGE_PACK["page_forward_error_friend_name"] 		= "����������, ������� ��� ������ �����.";
$LANGUAGE_PACK["page_forward_error_friend_surname"] 	= "����������, ������� ������� ������ �����.";
$LANGUAGE_PACK["page_forward_error_friend_email"] 		= "����������, ������� E-mail �����.";
$LANGUAGE_PACK["page_forward_error_failed_send"] 		= "�� �� ������ ��������� ��� ��������� ������ ����� � ���� ���. �� ���������� �� ��������� ����������.";
$LANGUAGE_PACK["page_forward_successful_send"]			= "��������� ���� ������� �������� �� ������ [email_address].";
$LANGUAGE_PACK["page_forward_subject_prefix"]			= "FWD: ";
$LANGUAGE_PACK["page_forward_subject_suffix"]			= "";

/*
$LANGUAGE_PACK["page_forward_text_message_prefix"]		= <<<TEXTPREFIX
������������, [name],

[from_name] �������(�), ��� ��� ����� ���� ��������� ��� ���������.
[optional_message]
[subscribe_paragraph]
TEXTPREFIX;
*/

$LANGUAGE_PACK["page_forward_text_message_prefix"]		= <<<TEXTPREFIX
������������, [name],

[from_name] �������(�), ��� ��� ����� ���� ��������� ��� ���������.
[optional_message]
TEXTPREFIX;

$LANGUAGE_PACK["page_forward_text_subscribe_paragraph"]	= <<<SUBSCRIBEPARAGRAPH
�� �� ���� ��������� � ������ ��������, �� ���� �� ������ �� ���� �����������, �� ������ �������� �� ��������� ������:
[subscribe_url]
SUBSCRIBEPARAGRAPH;

$LANGUAGE_PACK["page_forward_text_message_suffix"]		= "";

/*
$LANGUAGE_PACK["page_forward_html_message_prefix"]		= <<<HTMLPREFIX
������������ <strong>[name]</strong>,
<br /><br />
[from_name] �������(�), ��� ��� ����� ���� ��������� ��� ���������.<br />
[optional_message]
[subscribe_paragraph]
HTMLPREFIX;
*/

$LANGUAGE_PACK["page_forward_html_message_prefix"]		= <<<HTMLPREFIX
������������ <strong>[name]</strong>,
<br /><br />
[from_name] �������(�), ��� ��� ����� ���� ��������� ��� ���������.<br />
[optional_message]
HTMLPREFIX;

$LANGUAGE_PACK["page_forward_html_subscribe_paragraph"]	= <<<SUBSCRIBEPARAGRAPH
�� �� ���� ��������� � ������ ��������, �� ���� �� ������ �� ���� �����������, �� ������ �������� �� ��������� ������:<br />
<a href="[subscribe_url]">[subscribe_url]</a>
SUBSCRIBEPARAGRAPH;

$LANGUAGE_PACK["page_forward_html_message_suffix"]		= "";


?>
