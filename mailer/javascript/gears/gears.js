/**
 * @copyright Copyright (C) 2008 Blue Flame IT (Jersey) Ltd. All rights
 *            reserved.
 * @link http://www.phil-taylor.com
 * @author Phil Taylor <me@phil-taylor.com>
 */

JGears = {

	createStore : function() {
		if ('undefined' == typeof google || !google.gears)
			return;

		if ('undefined' == typeof localServer)
			localServer = google.gears.factory.create("beta.localserver");

		store = localServer.createManagedStore(this.storeName());
		store.manifestUrl = "javascript/gears/gears-manifest.php";
		store.enabled = true;
		store.checkForUpdate();
	
	},

	getPermission : function() {
		if ('undefined' != typeof google && google.gears) {
			if (!google.gears.factory.hasPermission)
				google.gears.factory
						.getPermission(
								'ListMessenger',
								'images/listmessenger.gif',
								'This site would like to use Google Gears to enable faster ListMessenger Administration.');

			try {
				this.createStore();
			} catch (e) {
			} // silence if canceled
		}
	},

	storeName : function() {
		var name = window.location.protocol + window.location.host;

		name = name.replace(/[\/\\:*"?<>|;,]+/g, '_'); // gears beta doesn't allow certain chars in the store name
		name = 'lm_' + name.substring(0, 60); // max length of name is 64
		// chars

		return name;
	},

		I : function(id) {
		return document.getElementById(id);
	}
};

( function() {
	if ('undefined' != typeof google && google.gears)
		return;

	var gf = false;
	if ('undefined' != typeof GearsFactory) {
		gf = new GearsFactory();
	} else {
		try {
			gf = new ActiveXObject('Gears.Factory');
			if (factory.getBuildInfo().indexOf('ie_mobile') != -1)
				gf.privateSetGlobalObject(this);
		} catch (e) {
			if (('undefined' != typeof navigator.mimeTypes)
					&& navigator.mimeTypes['application/x-googlegears']) {
				gf = document.createElement("object");
				gf.style.display = "none";
				gf.width = 0;
				gf.height = 0;
				gf.type = "application/x-googlegears";
				document.documentElement.appendChild(gf);
			}
		}
	}

	if (!gf)
		return;
	if ('undefined' == typeof google)
		google = {};
	if (!google.gears)
		google.gears = {
			factory :gf
		};
})();