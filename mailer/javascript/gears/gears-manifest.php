<?php
/**
 * @copyright	Copyright (C) 2008 Blue Flame IT (Jersey) Ltd. All rights reserved.
 * @link 		http://www.phil-taylor.com
 * @author 		Phil Taylor <me@phil-taylor.com> 
 */
@set_time_limit ( 0 );
@ini_set ( 'memory_limit', '32M' );
define ( 'JPATH_ROOT', realpath ( dirname ( __FILE__ ) . '/../../../' ) );
define ( 'DS', DIRECTORY_SEPARATOR );
@ini_set ( "include_path", str_replace ( "\\", "/", dirname ( __FILE__ ) ) . "/includes" );
@ini_set ( "allow_url_fopen", 1 );
@ini_set ( "session.name", "LMPSID" );
@ini_set ( "session.use_trans_sid", 0 );
@ini_set ( "session.cookie_lifetime", 0 );
@ini_set ( "session.cookie_secure", 0 );
@ini_set ( "session.referer_check", "" );
@ini_set ( "error_reporting", E_ALL ^ E_NOTICE );
@ini_set ( "magic_quotes_runtime", 0 );

session_start();


class JGearsManifest {
	
	private $folders = array ();
	
	private $JVersion = '';
	
	private $output = '';
	
	private $manifest = '{
				"betaManifestVersion" : 1,
				"version" : "%s",
				"entries" : [
	%s
				{ "url" : "../../../administrator/images/tick.png" }
				]}';
	
	private $fileTypes = array ('png', 'css', 'gif', 'jpg', 'js' );
	
	private $customFiles = array ();
	
	public function __construct() {
		$this->_setJVersion ();
		$this->_setFolders ();
		$this->_parseFolders ();
	}
	
	private function _parseFolders() {
		
		if (count ( $this->customFiles )) {
			foreach ( $this->customFiles as $file ) {
				$this->output .= sprintf ( '{ "url" : "../../../%s" },' . "\n\t", $file );
			}
		
		}
		
		foreach ( $this->folders as $folder ) {
			
			foreach ( $this->fileTypes as $type ) {
				$this->_parseFiles ( $folder, $type );
			}
		
		}
	
	}
	
	private function _getManifestVersion() {
		return md5 ( $this->output . $this->JVersion );
	}
	
	private function _parseFiles($folder, $suffix) {
		
		$dir = $this->mosReadDirectory ( $folder, '.' . $suffix, true, true );
		foreach ( $dir as $file ) {
			
			if (! is_dir ( $file )) {
				$this->output .= sprintf ( '{ "url" : "../..%s" },' . "\n\t", str_replace ( realpath ( dirname ( __FILE__ ) . DS . '..' . DS . '..' ), '', $file ) );
			}
		}
	
	}
	
	private function _setJVersion() {
		$this->JVersion = $_SESSION ["config"] [5];
	}
	
	private function _setFolders() {
		$this->folders [] = realpath ( dirname ( __FILE__ ) . DS . '..' . DS . '..' );
	}
	
	/**
	 * Utility function to read the files in a directory
	 * @param string The file system path
	 * @param string A filter for the names
	 * @param boolean Recurse search into sub-directories
	 * @param boolean True if to prepend the full path to the file name
	 */
	function mosReadDirectory($path, $filter = '.', $recurse = false, $fullpath = false) {
		
		$arr = array ();
		if (! @is_dir ( $path )) {
			return $arr;
		}
		$handle = opendir ( $path );
		
		while ( $file = readdir ( $handle ) ) {
			$dir = $this->mosPathName ( $path . '/' . $file, false );
			$isDir = is_dir ( $dir );
			if (($file != ".") && ($file != "..") && ($file != ".svn")) {
				if (preg_match ( "/$filter/", $file )) {
					if ($fullpath) {
						$arr [] = trim ( $this->mosPathName ( $path . '/' . $file, false ) );
					} else {
						$arr [] = trim ( $file );
					}
				}
				if ($recurse && $isDir) {
					$arr2 = $this->mosReadDirectory ( $dir, $filter, $recurse, $fullpath );
					$arr = array_merge ( $arr, $arr2 );
				}
			}
		}
		closedir ( $handle );
		asort ( $arr );
		return $arr;
	}
	
	/**
	 * Function to strip additional / or \ in a path name
	 * @param string The path
	 * @param boolean Add trailing slash
	 */
	function mosPathName($p_path, $p_addtrailingslash = true) {
		$retval = "";
		
		$retval = str_replace ( '\\', '/', $p_path );
		if ($p_addtrailingslash) {
			if (substr ( $retval, - 1 ) != '/') {
				$retval .= '/';
			}
		}
		// Remove double //
		$retval = str_replace ( '//', '/', $retval );
		
		return $retval;
	}
	
	public function __toString() {
		return sprintf ( $this->manifest, $this->_getManifestVersion (), $this->output );
	
	}

}

echo new JGearsManifest ( );
?>