﻿<?php
// Setup PHP and start page setup.
	@ini_set("include_path", str_replace("\\", "/", dirname(__FILE__)."/../../../../../../includes"));
	@ini_set("allow_url_fopen", 1);
	@ini_set("session.name", md5(str_replace("/javascript/wysiwyg/tiny_mce/plugins/imagemanager/includes", "", dirname(__FILE__))));
	@ini_set("session.use_trans_sid", 0);
	@ini_set("session.cookie_lifetime", 0);
	@ini_set("session.cookie_secure", 0);
	@ini_set("session.referer_check", "");
	@ini_set("error_reporting",  E_ALL ^ E_NOTICE);
	@ini_set("magic_quotes_runtime", 0);

	require_once("pref_ids.inc.php");
	require_once("config.inc.php");
	require_once("classes/adodb/adodb.inc.php");
	require_once("dbconnection.inc.php");

	session_start();
?>