﻿<?php
/**
 * This class handles ListMessenger authentication to MCImageManager.
 * It is basically a copy of the SessionAuthenticator.php file accept requires
 * that the session is already started by lm_session.inc.php.
 *
 * @package LMAuthenticator
 */
class Silentweb_LMAuthenticator extends Moxiecode_ManagerPlugin {
	/**#@+
	 * @access public
	 */

	/**
	 * SessionAuthenciator contructor.
	 */
	function SessionAuthenticator() {
	}

	/**
	 * Gets called on a authenication request. This method should check sessions or simmilar to
	 * verify that the user has access to the backend.
	 *
	 * This method should return true if the current request is authenicated or false if it's not.
	 *
	 * @param ManagerEngine $man ManagerEngine reference that the plugin is assigned to.
	 * @return bool true/false if the user is authenticated.
	 */
	function onAuthenticate(&$man) {
		$mcConfig =& $man->getConfig();

		// Support both old and new format
		$loggedInKey = isset($mcConfig['LMAuthenticator.logged_in_key']) ? $mcConfig['LMAuthenticator.logged_in_key'] : $mcConfig["authenticator.session.logged_in_key"];
		$userKey = isset($mcConfig['LMAuthenticator.user_key']) ? $mcConfig['LMAuthenticator.user_key'] : $mcConfig["authenticator.session.user_key"];
		$pathKey = isset($mcConfig['LMAuthenticator.path_key']) ? $mcConfig['LMAuthenticator.path_key'] : $mcConfig["authenticator.session.path_key"];
		$rootPathKey = isset($mcConfig['LMAuthenticator.rootpath_key']) ? $mcConfig['LMAuthenticator.rootpath_key'] : $mcConfig["authenticator.session.rootpath_key"];
		$mcConfigPrefix = (isset($mcConfig['LMAuthenticator.config_prefix']) ? $mcConfig['LMAuthenticator.config_prefix'] : "mcmanager") . ".";

		// Switch path
		if (isset($_SESSION[$pathKey]))
			$mcConfig['filesystem.path'] = $_SESSION[$pathKey];

		// Switch root
		if (isset($_SESSION[$rootPathKey]))
			$mcConfig['filesystem.rootpath'] = $_SESSION[$rootPathKey];

		$user = isset($_SESSION[$userKey]) ? $_SESSION[$userKey] : "";
		$user = preg_replace('/[\\\\\\/:]/i', '', $user);

		// Override by prefix
		foreach ($_SESSION as $key => $value) {
			if (strpos($key, $mcConfigPrefix) === 0)
				$mcConfig[substr($key, strlen($mcConfigPrefix))] = $value;
		}

		foreach ($mcConfig as $key => $value) {
			// Skip replaceing {$user} in true/false stuff
			if ($value === true || $value === false)
				continue;

			$value = str_replace('${user}', $user, $value);
			$mcConfig[$key] = $value;
		}

		return isset($_SESSION[$loggedInKey]) && checkBool($_SESSION[$loggedInKey]);
	}
}

// Add plugin to MCManager
$man->registerPlugin("LMAuthenticator", new Silentweb_LMAuthenticator());
?>