<?php
	// General settings
	//$mcConfig['general.engine'] = 'GoogleSpell';
	$mcConfig['general.engine'] = 'PSpell';
	//$mcConfig['general.engine'] = 'PSpellShell';
	//$mcConfig['general.remote_rpc_url'] = 'http://some.other.site/some/url/rpc.php';

	// PSpell settings
	$mcConfig['PSpell.mode'] = PSPELL_FAST|PSPELL_RUN_TOGETHER;
	$mcConfig['PSpell.spelling'] = "";
	$mcConfig['PSpell.jargon'] = "";
	$mcConfig['PSpell.encoding'] = "";

	// PSpellShell settings
	$mcConfig['PSpellShell.mode'] = PSPELL_FAST;
	$mcConfig['PSpellShell.aspell'] = '/usr/bin/aspell';
	$mcConfig['PSpellShell.tmp'] = '/tmp';

	// Windows PSpellShell settings
	//$mcConfig['PSpellShell.aspell'] = '"c:\Program Files\Aspell\bin\aspell.exe"';
	//$mcConfig['PSpellShell.tmp'] = 'c:/temp';
?>
