﻿<?php

	print "OK";
	die ();

// Setup PHP and start page setup.
	@ini_set("include_path", str_replace("\\", "/", dirname(__FILE__))."/includes");
	@ini_set("allow_url_fopen", 1);
	@ini_set("session.name", md5(dirname(__FILE__)));
	@ini_set("session.use_trans_sid", 0);
	@ini_set("session.cookie_lifetime", 0);
	@ini_set("session.cookie_secure", 0);
	@ini_set("session.referer_check", "");
	@ini_set("error_reporting",  E_ALL ^ E_NOTICE);
	@ini_set("magic_quotes_runtime", 0);

	require_once("pref_ids.inc.php");
	require_once("config.inc.php");
	require_once("classes/adodb/adodb.inc.php");
	require_once("dbconnection.inc.php");

	session_start();

	if((!isset($_SESSION["isAuthenticated"])) || (!(bool) $_SESSION["isAuthenticated"])) {
		echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"DTD/xhtml1-transitional.dtd\">\n";
		echo "<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\" lang=\"en\">\n";
		echo "<body>\n";
		echo "<script language=\"JavaScript\" type=\"text/javascript\">\n";
		echo "alert('It appears as though you are either not currently logged into ListMessenger or your session has expired. You will now be taken to the ListMessenger login page; please re-login.');\n";
		echo "if(window.opener) {\n";
		echo "	window.opener.location = './index.php?action=logout';\n";
		echo "	top.window.close();\n";
		echo "} else {\n";
		echo "	window.location = './index.php?action=logout';\n";
		echo "}\n";
		echo "</script>\n";
		echo "</body>\n";
		echo "</html>\n";
		exit;
	} else {
		require_once("functions.inc.php");
		?>
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=<?php echo html_encode($_SESSION["config"][PREF_DEFAULT_CHARSET]); ?>" />

			<title>About ListMessenger</title>

			<meta name="MSSmartTagsPreventParsing" content="true" />
			<meta http-equiv="imagetoolbar" content="no" />
	
			<link rel="shortcut icon" href="./images/listmessenger.ico" />
			<link rel="stylesheet" type="text/css" href="./css/common.css" media="all" />
			<link rel="stylesheet" type="text/css" href="./css/ui.tabs.css" media="all" />
			<link rel="stylesheet" type="text/css" href="./css/cluetip.css" media="all" />
			<link rel="stylesheet" type="text/css" href="./css/autocomplete.css" media="all" />
	
			<script type="text/javascript" src="./javascript/common.js"></script>
			<script type="text/javascript" src="./javascript/jquery/jquery.js"></script>
			<script type="text/javascript" src="./javascript/jquery/jquery-ui.min.js"></script>
			<script type="text/javascript" src="./javascript/jquery/jquery.bgiframe.js"></script>
			<script type="text/javascript" src="./javascript/jquery/jquery.ajaxqueue.js"></script>
			<script type="text/javascript" src="./javascript/jquery/jquery.autocomplete.js"></script>
			<script type="text/javascript" src="./javascript/jquery/jquery.textarearesizer.js"></script>
			<script type="text/javascript" src="./javascript/jquery/jquery.hoverintent.js"></script>
			<script type="text/javascript" src="./javascript/jquery/jquery.cluetip.js"></script>
			
			<script type="text/javascript" src="./javascript/gears/gears_init.js"></script>
			<script type="text/javascript" src="./javascript/gears/gears.js"></script>
			<script type="text/javascript">
				$(document).ready(function() {
					$('#tab-pane-1 > ul').tabs();
				});
			</script>
			
			<style type="text/css">
				.ui-tabs-panel {
				 	height: 195px;
					overflow: auto;
				}
			</style>
		</head>
		<body scroll="no" onblur="self.focus()">
		<table style="width: 100%" cellspacing="0" cellpadding="0" border="0">
		<colgroup>
			<col style="width: 30%" />
			<col style="width: 70%" />
		</colgroup>
		<tbody>
			<tr>
				<td>
					<img src="./images/listmessenger.gif" width="139" height="167" alt="ListMessenger" title="ListMessenger" />
				</td>
				<td>
					<table style="width: 100%" cellspacing="0" cellpadding="0" border="0">
					<colgroup>
						<col style="width: 25%" />
						<col style="width: 75%" />
					</colgroup>
					<tbody>
						<tr>
							<td style="padding-bottom: 10px" colspan="2"><span class="titlea-positive">List</span><span class="titleb-positive">Messenger</span> <span class="titlea-positive"><?php echo html_encode(VERSION_TYPE)." ".html_encode(VERSION_INFO); ?></span></td>
						</tr>
						<tr>
							<td>Website:</td>
							<td><a href="http://www.ru" target="_blank">http://www.ru</a></td>
						</tr>
						<tr>
							<td>Author:</td>
							<td><a href="mailto:msimpson@listmessenger.com">Matt Simpson</a></td>
						</tr>
						<tr>
							<td>Copyright:</td>
							<td>Copyright &copy; <?php echo gmdate("Y", time() + ($_SESSION["config"][PREF_TIMEZONE] * 3600)); ?> <a href="http://www.silentweb.ca" target="_blank">Silentweb</a></td>
						</tr>
						<tr>
							<td style="padding-top: 10px" colspan="2">
								ListMessenger exceeds expectations as a well designed, easy to use and extremely robust electronic mailing list management solution for your website.
							</td>
						</tr>
					</tbody>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2" style="padding-top: 10px">
					<div id="tab-pane-1">
						<ul>
							<li><a href="#fragment-1"><span>Credits</span></a></li>
							<li><a href="#fragment-2"><span>Licence</span></a></li>
							<li><a href="#fragment-3"><span>Registration</span></a></li>
						</ul>
						<div id="fragment-1">
							<table style="width: 100%" cellspacing="1" cellpadding="1" border="0">
							<colgroup>
								<col style="width: 30%" />
								<col style="width: 70%" />
							</colgroup>
							<tbody>
								<tr>
									<td>&nbsp;<img src="./images/record-next-on.gif" width="9" height="9" alt="" title="" />&nbsp;<a href="http://www.karlawithak.ca" target="_blank">Karla Simpson</a>&nbsp;&nbsp;</td>
									<td>For all her love and support.</td>
								</tr>
								<tr>
									<td>&nbsp;<img src="./images/record-next-on.gif" width="9" height="9" alt="" title="" />&nbsp;<a href="http://listmessenger.net" target="_blank">Erik Geurts</a>&nbsp;&nbsp;</td>
									<td>For all his help on the forums and the User Guide.</td>
								</tr>
								<tr>
									<td>&nbsp;<img src="./images/record-next-on.gif" width="9" height="9" alt="" title="" />&nbsp;<a href="http://www.33sticks.com" target="_blank">Nathaniel Murray</a>&nbsp;&nbsp;</td>
									<td>For designing the ListMessenger logo.</td>
								</tr>
								<tr>
									<td>&nbsp;<img src="./images/record-next-on.gif" width="9" height="9" alt="" title="" />&nbsp;<a href="http://www.pixelpoint.com" target="_blank">PixelPoint</a>&nbsp;&nbsp;</td>
									<td>(Nina Vecchi) for initial project sponsorship.</td>
								</tr>
								<tr>
									<td>&nbsp;<img src="./images/record-next-on.gif" width="9" height="9" alt="" title="" />&nbsp;<a href="http://www.hotscripts.com/?RID=9600" target="_blank">HotScripts</a>&nbsp;&nbsp;</td>
									<td>For providing a dependable listing service.</td>
								</tr>
								<tr>
									<td>&nbsp;<img src="./images/record-next-on.gif" width="9" height="9" alt="" title="" />&nbsp;<a href="http://phpmailer.sourceforge.net" target="_blank">Brent Matzelle</a>&nbsp;&nbsp;</td>
									<td>For creating the PHPMailer class.</td>
								</tr>
								<tr>
									<td>&nbsp;<img src="./images/record-next-on.gif" width="9" height="9" alt="" title="" />&nbsp;<a href="http://adodb.sourceforge.net" target="_blank">John Lim</a>&nbsp;&nbsp;</td>
									<td>For creating the ADOdb database library.</td>
								</tr>
								<tr>
									<td colspan="2" style="padding-top: 5px">
										<a href="http://www.hotscripts.com/?RID=9600" target="_blank"><img src="./images/logo-hotscripts.png" style="float: left" width="173" height="56" alt="HotScripts - Application Resources" title="HotScripts - Application Resources" border="0" /></a>
										<a href="http://www.jdrf.ca" target="_blank"><img src="./images/logo-jdrf.jpg" style="float: right" width="223" height="56" alt="Juvenile Diabetes Research Foundation of Canada" title="Juvenile Diabetes Research Foundation of Canada" border="0" /></a>
									</td>
								</tr>								
							</tbody>
							</table>
						</div>
						<div id="fragment-2">
							<iframe style="width: 100%; height:185px; border: 0px; margin: 0px; padding: 0px" src="licence.html"></iframe>
						</div>
						<div id="fragment-3">
							<table style="width: 100%" cellspacing="1" cellpadding="1" border="0">
							<colgroup>
								<col style="width: 30%" />
								<col style="width: 70%" />
							</colgroup>
							<tbody>
								<tr>
									<td><strong>Program Name:</strong>&nbsp;</td>
									<td>ListMessenger</td>
								</tr>
								<tr>
									<td><strong>Program Version:</strong>&nbsp;</td>
									<td><?php echo html_encode(VERSION_INFO)." ".((VERSION_BUILD != "") ? "<span class=\"small-grey\">Build ".html_encode(VERSION_BUILD)."</span>" : ""); ?></td>
								</tr>
								<tr>
									<td><strong>Release Type:</strong>&nbsp;</td>
									<td><?php echo html_encode(VERSION_TYPE); ?></td>
								</tr>
								<tr>
									<td colspan="2">&nbsp;</td>
								</tr>
								<tr>
									<td><strong>Registered To:</strong>&nbsp;</td>
									<td><?php echo html_encode($_SESSION["config"][REG_NAME]); ?></td>
								</tr>
								<tr>
									<td><strong>E-Mail Address:</strong>&nbsp;</td>
									<td><?php echo html_encode($_SESSION["config"][REG_EMAIL]); ?></td>
								</tr>
								<tr>
									<td><strong>Registered Domain:</strong>&nbsp;</td>
									<td><?php echo html_encode($_SESSION["config"][REG_DOMAIN]); ?></td>
								</tr>
								<tr>
									<td><strong>Authorization Code:</strong>&nbsp;</td>
									<td><?php echo html_encode($_SESSION["config"][REG_SERIAL]); ?></td>
								</tr>
							</tbody>
							</table>
						</div>
					</div>
				</td>
			</tr>
		</tbody>
		</table>

		</body>
		</html>
		<?php
	}
?>
