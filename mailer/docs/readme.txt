
ListMessenger Pro Readme File
Version:	2.2.0
Website:	http://www.ru
Support:	http://forum.listmessenger.com
Author:		
Tab Stop:	4 spaces
================================================================================

INSTALLATION)
	For information on installing ListMessenger, please read the install.txt
	file which outlines the installation procedure.

UPGRADING)
	For information on upgrading ListMessenger Light to Pro:
	- If you are upgrading ListMessenger Light to Pro, please read the
	  upgrade_light.txt file.

	For information on upgrading from a previous version of ListMessenger:
	- If you are upgrading from ListMessenger 2.x, please read the
	  upgrade_2.x.txt file.

	- If you are upgrading from any other version, please read the
	  upgrade_1.x.txt file.

TECHNICAL SUPPORT)
	For technical support, please visit our public forums 24/7 which can be
	accessed from: http://forum.listmessenger.com

	Please do not e-mail me with technical support issues unless requested as
	it has proved very difficult to track issues this way.