<?php

// print_r ($argv);

	require ('.paths');
	$DEBUG_MODE = FALSE;
// 	$MYSQL_PROFILE = 'stat';
	set_time_limit (60*15);
	error_reporting(E_ALL ^ E_NOTICE);
	ini_set("allow_url_fopen", 1);

	include ('sys.inc');

	$BLOG_URL = 'http://magicdesignlab.blogspot.com/feeds/posts/default';

	if ($DEBUG_MODE) {
		$mode = 'blogspot';
		$file = "c:/Apache/users/magic/_tmp/blog-feed.xml";
		// $mode = 'xls';
		// $file = "C:/Apache/users/aplar/_src/aplar1.csv";
		// $file = "C:/Apache/users/aplar/_src/bez1.csv";
		// $file = "C:/Apache/users/aplar/_src/kapriz1.csv";
		// $mode = 'xls-stat';
		// $file = "C:/Apache/users/aplar/_src/aplar2.csv";
		// $file = "C:/Apache/users/aplar/_src/bez2.csv";
		// $file = "C:/Apache/users/aplar/_src/kapriz2.csv";
		// header("Content-type: text/xml; charset=windows-1251");
	} elseif (! empty ($BLOG_URL)) {
		$file = $BLOG_URL;
		$mode = 'blogspot';
	} else {
		if (! isset ($argv) || sizeof ($argv) < 2)
			die ("Not enough parameters for execution!/n");

		$file = $argv[1];
		$mode = $argv[2] ? $argv[2] : 'blogspot';
	}

	if (! eregi('^[a-z]+://', substr($file, 0, 10)) && ! file_exists ($file))
		die ("Datafile not exists!/n");

	switch ($mode) {
		case 'blogspot':
			require_once "news/news_importer.inc";
			$importer = new NewsImporter ( $config, $request, $file );
			break;
/*
		case 'xls':
		case 'xls-stat':
			require_once "seo/seo_importer_xls.inc";
			$importer = new SEOImporterXLS ( $config, $request, $file, $mode == 'xls-stat' );
			break;
*/
		default:
			$importer = NULL;
	}

	if (is_object ($importer)) {
		$importer->commitChanges ();
	} else
		die ("Importer is not an object!/n");

	print sprintf ("%d records imported<br>\n", $importer->recordsCount);

	$mysql_factory->print_log ();

?>