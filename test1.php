<?PHP

	require ('.paths');
	include ('sys.inc');
	include_once (MY_MODULES_PATH.'/questions/questions.inc');

	$structure = new StructureTree ($config, $request);
	$last = &$structure->create (5);
	$structure->rewrite_stack[] = &$last;

	$questions = new QuestionTree ($config, $request);
	$tpl = new Template ($config, MY_TPL_PATH);
	$tpl->set_file ('test', 'test_test.html');
	$tpl->set_var ("url", $GLOBALS['REQUEST_URI']);
	
	$step = $request->_post["step"] ? $request->_post["step"] : $request->_get["step"];
	if (empty ($step))
		$step = 1;

	if ($step == 3) {
		$tpl->set_block ("test", "step3", "step3");
		$tpl->set_block ("step3", "question_block", "question_blocks");

		// print_r ($request->_post["answer"]);
		$mark = $questions->check_results ();
		$tpl->set_var ('mark', $questions->mark);
		$tpl->set_var ('total_mark', $questions->total_mark);
		$tpl->set_var ('quantity', $questions->quantity);
		$tpl->set_var ('total_quantity', $questions->total_quantity);

		if ($questions->questions)
		foreach ($questions->questions as $id => $dummy) {
			$quest = &$questions->questions[$id];
			$quest->question = nl2br ($quest->question);

			$quest->set_template_vars ($tpl);
			$tpl->set_var ('answers', $quest->set_answer_show_block (TRUE));

			$tpl->set_var ('qnum', ++$k);
			$tpl->parse ('question_blocks', 'question_block', true);
		}

		$last->name = "��� ���������: " . $mark . " �� " . $questions->total_mark;
		$tpl->parse ('test', 'step3');

	} elseif ($step == 2 && ($id = $request->_post["test_id"]) && ($test = &$questions->create ($id))) {
		$tpl->set_block ("test", "step2", "step2");
		$tpl->set_block ("step2", "part_block", "part_blocks");
		$tpl->set_block ("part_block", "question_block", "question_blocks");
		$tpl->set_var ('part_blocks', "");

		$qlist = &$questions->create_test ($id);
		$parts = &$questions->parts;
		$k = 0;

		if ($parts)
		for ($i = 0; $i < sizeof ($parts); $i++) {
			$part = &$parts[$i];
			
			$tpl->set_var ('question_blocks', "");
			for ($j = 0; $j < sizeof ($part->questions); $j++) {
				$quest = &$part->questions[$j];
				$quest->question = nl2br ($quest->question);

				$quest->set_template_vars ($tpl);
				$tpl->set_var ('answers', $quest->set_answer_block ());

				$tpl->set_var ('qnum', ++$k);
				$tpl->parse ('question_blocks', 'question_block', true);
			}

			$part->set_template_vars ($tpl);
			$tpl->parse ('part_blocks', 'part_block', true);
		}

		$last->name = "�������� �� �������";
		$tpl->parse ('test', 'step2');

	} else {
		$tpl->set_block ("test", "step1", "step1");

		$list = &$questions->get_test_list ();
		$tpl->set_var ('test_select', common_select_button ('test_id', $list, ""));

		$last->name = "�������� ����";
		$tpl->parse ('test', 'step1');
	}



	
	include_once ('main.inc');
	$main_page->structure = &$structure;
	$main_page->content = $tpl->get_var ('test');
	$main_page->output ();



	$mysql_factory->print_log ();
	ob_flush ();

?> 

