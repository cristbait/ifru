/**
 * polyfill CustomEvent для IE11.
 */
(function () {
	function CustomEvent ( event, params ) {
		params = params || { bubbles: false, cancelable: false, detail: undefined };
		let evt = document.createEvent( 'CustomEvent' );
		evt.initCustomEvent( event, params.bubbles, params.cancelable, params.detail );
		return evt;
	}
 
	CustomEvent.prototype = window.Event.prototype;
 
	window.CustomEvent = CustomEvent;
})();
 
/**
 * Принимает любое количество аргументов в виде объектов.
 * @returns {{}} Возвращает объект
 */
function mergeObjects(){
	let obj = {},
		i = 0,
		il = arguments.length,
		key;
	for (; i < il; i++) {
		for (key in arguments[i]) {
			if (arguments[i].hasOwnProperty(key)) {
				obj[key] = arguments[i][key];
			}
		}
	}
	return obj;
}
 
/**
 *
 * @param element - DOM-элемент, предков которого мы хотим проверить
 * @param closestElement - DOM-элемент для проверки является ли он предком первого
 * @returns {boolean} возвращает true или false
 */
function isThereClosestElement(element, closestElement){
	let parent = element.parentElement;
	if (parent == null) {
		return false;
	} else {
		if (parent == closestElement) {
			return true;
		} else {
			return isThereClosestElement(parent, closestElement);
		}
	}
}
 
/**
 *
 * @param options - объект, содержащий свойства анимации {draw, timingFunction, duration}
 */
function animate(options) {
 
	let start = performance.now();
 
	requestAnimationFrame(function animate(time) {
		// timeFraction от 0 до 1
		let timeFraction = (time - start) / options.duration;
		if (timeFraction > 1) timeFraction = 1;
 
		// текущее состояние анимации
		let progress = options.timing(timeFraction);
 
		options.draw(progress);
 
		if (timeFraction < 1) {
			requestAnimationFrame(animate);
		}
 
	});
}
 
/**
 *
 * @param popup Строка CSS-селектор по которому находится нужный попап
 * @param opener Строка CSS-селектор по которому можно найти один или несколько элементов
 * @param config объект с конфигами для попапа
 */
const popup = function(popup, opener, config){
	let self = this;
	self.default = {
		cssAnimation: false,
		animationDuration: 300,
		onShow: null,
		onClose: null,
		closeButtonSelector: '.popup-exit',
		contentAreaSelector: '.popup-content',
		popupSelector: '.popup',
		closeOnClickOutside: true,
		closeOnEsc: true
	};
	self.config = mergeObjects(self.default, config);
	self.popup = document.querySelector(popup);
	self.opener = document.querySelectorAll(opener);
	self.closeButton = self.popup.querySelector(self.config.closeButtonSelector);
	self.content = self.popup.querySelector(self.config.contentAreaSelector);
	self.opened = false;
	self.show = function(e){
		if (e) {
			e.preventDefault();
		}
		document.body.classList.add('-popupOpened');
		self.popup.style.display = 'block';
		if (!self.config.cssAnimation) {
			animate({
				duration: self.config.animationDuration,
				timing: function(timeFraction) {
					return timeFraction;
				},
				draw: function(progress) {
					self.popup.style.opacity = progress * 1;
				}
			});
		}
		self.popup.classList.add('-showed');
		self.opened = true;
 
		if (self.config.closeOnEsc) {
			if (!document.closePopupOnEsc) {
				document.closePopupOnEsc = self.closePopupOnEsc;
				document.addEventListener('keydown', self.closePopupOnEsc);
			}
		}
		self.popup.dispatchEvent(CustomEvent('show', { bubbles: true, cancelable: false, detail: undefined }));
	};
	self.hide = function(e){
		if (e) {
			e.preventDefault();
		}
		document.body.classList.remove('-popupOpened');
		if (!self.config.cssAnimation) {
			animate({
				duration: self.config.animationDuration,
				timing: function linear(timeFraction) {
					return 1 - timeFraction;
				},
				draw: function(progress) {
					self.popup.style.opacity = progress * 1;
				}
			});
			setTimeout(function(){
				self.popup.style.display = 'none';
			}, self.config.animationDuration);
		} else {
			let time = parseFloat(window.getComputedStyle(self.popup).animationDuration) * 1000;
			setTimeout(function(){
				self.popup.style.display = 'none';
			}, time);
		}
 
		self.popup.classList.remove('-showed');
		self.opened = false;
 
		if (self.config.closeOnEsc) {
			document.closePopupOnEsc = null;
			document.removeEventListener('keydown', self.closePopupOnEsc);
		}
		self.popup.dispatchEvent(CustomEvent('close', { bubbles: true, cancelable: false, detail: undefined }));
	};
 
	self.init = function(){
		if (!self.popup.popup) {
			for (let i = 0; i < self.opener.length; i++){
				self.opener[i].addEventListener('click', self.show);
				self.opener[i].addEventListener('touch', self.show);
			}
 
			self.closeButton.addEventListener('click', self.hide);
			self.closeButton.addEventListener('touch', self.hide);
 
			if (self.config.closeOnClickOutside) {
				self.popup.addEventListener('click', function(e){
					let target = e.target;
					let content = isThereClosestElement(target, self.content);
					if (!content) {
						self.hide();
					}
				});
			}
 
			self.popup.addEventListener('show', self.config.onShow);
			self.popup.addEventListener('close', self.config.onClose);
 
			self.popup.popup = self;
		}
	};
 
	self.closePopupOnEsc = function(e) {
		e = e || window.event;
		if (e.keyCode == 27) {
			let popups = document.querySelectorAll(self.config.popupSelector);
			for (let i = 0; i < popups.length; i++) {
				if (popups[i].popup && popups[i].popup.opened) {
					popups[i].popup.hide();
				}
			}
		}
	};
 
	self.init();
};

jQuery(document).ready(function(){
	jQuery("input[name=phone]").mask("+7 (999) 999-99-99");
	if (document.querySelector('.popup')) {
		new popup('.popup', '[data-popup="popup"]', {});
	}
	new formValidation($('#popupForm'));

	var UserTable = document.querySelectorAll(".userInfo");
	for (var k = 0; k < UserTable.length; k++) {
		UserTable[k].addEventListener('click', reName);
	}
});

function reName (event) {
	var el = event.currentTarget;
	var UserId = el.getAttribute("user_id");
	var UserFio = el.getAttribute("user_fio");
	var UserName = document.querySelector(".userName");
	var UserNewId = document.querySelector(".userId");
	var UserIdInput = document.querySelector('#user_id');
	console.log(UserIdInput);
	UserName.innerHTML = UserFio;
	UserNewId.innerHTML = UserId;
	UserIdInput.value = UserId;

}


var formValidation = function(el) {
	var self = $(this);
	self.button = el.find('[type="submit"]');
	self.inputs = el.find('input[type="text"]');
	self.form = el;
	self.textareas = el.find('textarea');
	self.loadingBlock = $('<div class="loading"></div>');
	self.shadow = $('<div class="loading-shadow"></div>');
	self.toggleLoading = function(){
		if (self.button.hasClass('__loading')) {
			self.button.removeClass('__loading');
			self.shadow.remove();
			self.loadingBlock.remove();
		} else {
			self.button.addClass('__loading');
			self.shadow.appendTo(self.button);
			self.loadingBlock.appendTo(self.button);
		}
	};

	self.successTextEng = 'Thank you!';
	self.successTextIt = 'Grazie!';

	self.success = function(){
		var text = $('<p></p>');
		text.css({
			'position': 'absolute',
			'top':'40%',
			'left':'0',
			'width':'100%',
			'padding:':'20px',
			//'color':'#000',
			'font-size':'18px',
			'text-align': 'center',
			'font-family': '"HelveticaNeue-Medium"',
			//'z-index':'3',
			'opacity':'0',
			'animation':'fadeIn 0.3s 0.2s forwards'
		});
		if (language) {
			if (language == 'eng') {
				text.text(self.successTextEng);
			} else {
				text.text(self.successTextIt);
			}
		} else {
			text.text(self.successTextEng);
		}
		self.form.css({
			'position': 'relative',
			'pointer-events': 'none'
		});
		self.form.children().css({
			'transition': '0.3s',
			'opacity': '0',
		});
		self.form.append(text);
		setTimeout(function(){
			text.remove();
			self.form.children().css({
				'opacity': '1',
			});
			self.form.css({
				'position': 'static',
				'pointer-events': 'auto'
			});
		}, 5000);
	};
	self.validation = function () {
		for (var i = 0; i < self.inputs.length; i++) {
			var input = self.inputs.eq(i);
			if (input.val() == '') {
				return i;
			}
		}
	};
	self.form.on('submit', function(e){
		self.inputs.removeClass('-invalid');
		var index = self.validation();
		if (index >= 0) {
			self.inputs.eq(index).addClass('-invalid');
			self.inputs.eq(index).focus();
			e.preventDefault();
			return false;
		} else {
			self.button.prop('disabled', true);
			clearTimeout(someTo);
			var someTo = setTimeout(function(){
				self.button.prop('disabled', false);
			}, 1000);
		}
	});
	// Отправка формы
	/*self.form.on('submit', function (e) {
		e.preventDefault();
		self.inputs.removeClass('-invalid');
		var index = self.validation();
		if (index >= 0) {
			self.inputs.eq(index).addClass('-invalid');
			self.inputs.eq(index).focus();
			return false;
		}
		self.button.prop('disabled', true);
		self.toggleLoading();
		//var formData = new FormData($(this)[0]);
		$.ajax({
			method: "POST",
			url: "/send.php",
			dataType: "json",
			data: self.form.serialize(),
			success: function(response){
				console.log(response);
				if (response.success) {
					console.log(response);
					self.inputs.val('');
					self.textareas.val('');
					self.button.prop('disabled', false);

					self.success();

					self.toggleLoading();
				} else {
					self.button.prop('disabled', false);
					self.toggleLoading();
					//console.log(response);
					//alert(response.message);
				}
			},
		});
		return false;
	});*/
};
	
	