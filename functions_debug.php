<?php

function print_arr( $var, $die = false, $title = '', $ignore_cookie = false, $return = false )
{

	//if ($_COOKIE['debug']) {

		debug_declare_styles();

		$type = gettype( $var );
		
		$out = print_r( $var, true );
		$out = htmlspecialchars( $out );
		$out = str_replace('  ', '&nbsp; ', $out );
		$global_count = ++$GLOBALS['debug_global_count'];
		if( $type == 'boolean' ) {
			$content = $var ? 'true' : 'false';

			$out = '
			<div class="debug_main_frame">
				<span class="debug_var_type">('.$type.')</span> '.$content.' <span class="debug_var_title">'.$title.'</span>
			</div>';
		}
		else {

			$object_definition = '';

			if ($type=='object') {
				$class_name = get_class($var);
				if ( get_parent_class($var) ) { $object_parent_class='&nbsp;exted&nbsp;'.get_parent_class($var); } else { $object_parent_class=''; }
				$object_definition = '
				<div>
					<a href="javascript://" id="debug_object_definition_'.$global_count.'_lnk" onclick="debug_functions_expand(\'debug_object_definition_'.$global_count.'\');" class="lnkExpand">+</a>&nbsp;&nbsp;&nbsp;&nbsp;<span class="debug_var_type">Functions</span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="debug_var_title"><a href="javascript://" onClick="debug_expand(\'debug_variable_definition_'.$global_count.'\');" id="debug_variable_definition_'.$global_count.'_lnk">[-]</a>&nbsp;&nbsp;'.$title.'</span>
					<div class="debug_object_definition" id="debug_object_definition_'.$global_count.'">
						<ul id="debug_object_definition_vars">';
				foreach ( get_class_vars($class_name) as $class_var) {
					$object_definition .= '<li>' . $class_var . '</li>';
				}
				$object_definition .= '</ul>';

				$object_definition .= '<ul id="debug_object_definition_functions">';
				foreach ( get_class_methods($var) as $method) {
					$object_definition .= '<li>' . $method . '()</li>';
				}
				$object_definition .= '
						</ul>
					</div>
					<hr>
				</div>';

				$content = nl2br( $out );
				$out = '
				<div class="debug_main_frame" id="debug_variable_definition_'.$global_count.'">'.$object_definition.'
					<span class="debug_var_type">('.$type.$object_parent_class.')</span> '.$content.'
				</div>';

			} else {
				$content = nl2br( $out );
				if (!empty($title)) {
					$out = '
					<div class="debug_main_frame" id="debug_variable_definition_'.$global_count.'">
						<span class="debug_var_title"><a href="javascript://" onClick="debug_expand(\'debug_variable_definition_'.$global_count.'\');" id="debug_variable_definition_'.$global_count.'_lnk">[-]</a>&nbsp;&nbsp;'.$title.'</span>
						<span class="debug_var_type">('.$type.')</span> '.$content.'
					</div>';
				} else {
					$out = '
					<div class="debug_main_frame" id="debug_variable_definition_'.$global_count.'">
						<span class="debug_var_title"><a href="javascript://" onClick="debug_expand(\'debug_variable_definition_'.$global_count.'\');" id="debug_variable_definition_'.$global_count.'_lnk">[-]</a></span>
						<span class="debug_var_type">('.$type.')</span> '.$content.'
					</div>';
				}
			}
			
		}


		if( !$return ) { echo $out; } else { return $out; }
		if( $die && $die<=$GLOBALS['debug_global_count'] ) { die('...stopped...'); }
	//}
	//else 
	//return;
}

function debug_declare_styles() {
	if ($GLOBALS['debug_styles_declared']) return;
	$GLOBALS['debug_styles_declared'] = true;
	$GLOBALS['debug_global_count']=0;
?>
<html>
<head>
<style type="text/css">
	.debug_main_frame {
		position:relative;
		border:2px inset #666;
		background:#000;
		font-family:Verdana;
		font-size:11px;
		color:#6F6;
		text-align:left;
		margin:20px;
		padding:16px;
		overflow:hidden;
		z-index:3;
	}
	.debug_main_frame hr { margin:8px 0; border:none; border-top:1px solid #930; height:1px; line-height:1px; }
	.debug_main_frame a:link { color:#900; text-decoration:none; }
	.debug_main_frame a:hover { color:#900; text-decoration:underline; }
	.debug_var_type { color:#f66; }
	.debug_var_title { position:absolute; left:6px; top:2px; color:#999; font-size:10px; }
	.debug_var_title a:link { color:#999; text-decoration:none; }
	.debug_object_definition { display:none; }
	.debug_object_definition ul { padding-left:24px; }
	
</style>

<script type="text/javascript">
	function debug_expand(id) {
		var $element = document.getElementById(id);
		var $expand_link = document.getElementById(id + '_lnk');
		if ($expand_link.innerHTML=='[-]') {
			$element.style.height='2px';
			$element.style.padding='16px 0 0';
			$expand_link.innerHTML='[+]';
		} else {
			$element.style.height='auto';
			$element.style.padding='16px';
			$expand_link.innerHTML='[-]';
		}
	}

	function debug_functions_expand(id) {
		var $element = document.getElementById(id);
		var $expand_link = document.getElementById(id + '_lnk');
		if ($element.style.display=='block') {
			$element.style.display='none';
			$expand_link.innerHTML='+';
		} else {
			$element.style.display='block';
			$expand_link.innerHTML='-';
		}
	}
</script>
</head>
<body>
<?php
}

function frontend_print_arr( $params )
{
	print_arr( $params['var'] );
}

function log_file($str)
{
	$file = fopen("./test/log.txt","a");
	fputs($file, $str);
	fclose($file);
}

if (isset($_GET['debug']) && $_GET['debug']!='off') {
	setcookie('debug', true);
	$_COOKIE['debug']=true;
}

if (isset($_GET['debug']) && $_GET['debug']=='off') {
	setcookie('debug', false);
	$_COOKIE['debug'] = false;
}
function rdate($param, $time=0) {
	if(intval($time)==0)$time=time();
	$MonthNames=array("Января", "Февраля", "Марта", "Апреля", "Мая", "Июня", "Июля", "Августа", "Сентября", "Октября", "Ноября", "Декабря");
	if(strpos($param,'M')===false) return date($param, $time);
		else return date(str_replace('M',$MonthNames[date('n',$time)-1],$param), $time);
}
?>